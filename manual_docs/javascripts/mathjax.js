window.MathJax = {
    loader: {load: ['[tex]/cancel']},
    tex: {
        tex: {packages: {'[+]': ['cancel','color']}},
        inlineMath: [["\\(", "\\)"]],
        displayMath: [["\\[", "\\]"]],
        processEscapes: true,
        processEnvironments: true
    },
    options: {
        ignoreHtmlClass: ".*|",
        processHtmlClass: "arithmatex"
    }
};

document$.subscribe(() => {
    MathJax.typesetPromise()
})

