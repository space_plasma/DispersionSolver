# User Handbook

## Design Choice

The dispersion solver package described here is written in **C++** as a **plug-in** for the **Mathematica** software.

### Why **C++**?

Solving fully kinetic plasma dispersion relations is computation-intensive work.
**C++** does not only stay close to the bare metal and generates codes optimized to the host hardware, but also provides support for multi-programming paradiams and modern programming practices at the language level.

### Why **Plug-in** Model?

While versatile, writing a standalone program with the user-facing part in C++ requires extra work and time with little added benifit.

The typical user interface of many of the earlier dispersion solvers that I saw uses the following pattern.

1. Write an input file which contains all the necessary parameters to do one set of calculation.
1. Run the dispersion solver program which reads the parameters from the input file and spits out the calculation result.
1. Finally, import the results into your choice of data analysis software and visualize them.

The time between crafting your input file and seeing the calculation result is almost eternity if you need to do multiple sets of calculation.

I early on knew that the scripting (or interpreted) languages, such as Mathematica, Matlab, Python, etc., are much better at crafting the input parameters and post-processing the calculation results, because they typically have a rich set of libraries shipped by default.
So, by writing the dispersion solver as a **plug-in** for my favorite data analysis software,
I can delegate all the user-interacting parts to that software and let the dispersion solver backend only do number crunching tasks.
In that way, I can remain in that software environment for the entire cycle from preparing the input parameters to visualizing the results.

### Why **Mathematica**?

Back when I first started this project, I was (semi-) fluent in [IDL](http://www.harrisgeospatial.com/ProductsandTechnology/Software/IDL.aspx), [Matlab](https://www.mathworks.com/products/matlab.html), and [Mathematica](https://www.wolfram.com/mathematica/).
IDL was off the table, because by that time I knew that IDL won't be able to keep up with the evolving programming landscape.

After some research, the plug-in model of Mathematica (one of the plug-in models I should say) called [**WSTP**](https://www.wolfram.com/wstp/) (previously known as **MathLink**) appealed to me for the following reasons:

- Unlike a usual plug-in which is a dynamic library loaded in the host process at runtime, a WSTP plug-in is a standalone executable which runs in a separate, dedicated process.
It communicates with the host process across the process boundary using the Wolfram Symbolic Transfer Protocol or **WSTP**.

    Running a plug-in in a separate process has one big benifit: fault tolerence. That is, if the plug-in crashes, only it dies; the host process can move on after reporting the loss of connection.
    Or if the plug-in hangs, I only need to kill the plug-in and re-launch it.

- I could get a better visual feedback of my calculation state by launching the plug-in in the terminal and monitoring the log messages.

If I knew inner workings of Python enough, I would have considered a plug-in for Python.

## Manuals

- [**`dispersion_solver`**](./dispersion_solver)
: This is the WSTP plug-in described [above](#why-mathematica).
If you came to this page for a manual, this is most likely what you are looking for.

- [**`DispersionLink`**](./DispersionLink)
: This is another plug-in developed much later.
It uses the [**LibraryLink**](https://reference.wolfram.com/language/LibraryLink/tutorial/Overview.html) plug-in model where plug-ins are dynamically loaded in the host process environment at runtime.
This exposes to the Mathematica runtime environment the compute-intensive library functions upon which `dispersion_solver` is built.

## Theory and Algorithm Details

- See `0-DispersionRelation.nb` notebook under the [`blue_print`](https://gitlab.com/space_plasma/DispersionSolver/-/tree/master/blue_print) directory in the project repository for theoretical derivations.
- In addition, the notebooks under the [`blue_print`](https://gitlab.com/space_plasma/DispersionSolver/-/tree/master/blue_print) directory contain prototype algorithms used in the program.
