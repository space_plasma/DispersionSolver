# Manual for `dispersion_solver`

From here, the **host process** or just **host** refers to the Mathematica process (either a notebook or commandline interface) and the **plug-in** or **backend** refers to the `dispersion_solver` process.

If you build and install the target, you can locate the plug-in at `/install_prefix/DispersionSolver/dispersion_solver-wstp`.
For example, if your install prefix is `$HOME/Desktop`, then the plug-in will be placed at `~/Desktop/DispersionSolver/dispersion_solver-wstp`.

## Plug-in Life Cycle

A WSTP plug-in can be launched and connected to the host in several ways ([see this guide for reference](https://reference.wolfram.com/language/tutorial/WSTPAndExternalProgramCommunicationOverview.html)).

I like to do the following way:

- In the terminal, launch the plug-in and create a WSTP communication channel

    ```shell
    $ ~/Desktop/DispersionSolver/dispersion_solver-wstp \
    -linkcreate -linkprotocol tcpip

    Link created on: 56765@192.168.0.102,56766@192.168.0.102

    ```

- In the host, connect to the plug-in

    ```
    In[2]:= link = Install[LinkConnect["56765@192.168.0.102,56766@192.168.0.102", LinkProtocol -> "TCPIP"]]

    Out[2]= LinkObject[56765@192.168.0.102,56766@192.168.0.102, 65, 1]

    In[3]:= (*Do work*)
    ```

    When you are *done*, tear down the plug-in

    ```
    In[4]:= Uninstall[link]

    Out[4]= 56765@192.168.0.102,56766@192.168.0.102
    ```

The plug-in automatically chooses two unused port numbers for the incoming and outgoing channels.
You can specify the port numbers yourself by passing the `-linkname` option.
For example,

```shell
$ ~/Desktop/DispersionSolver/dispersion_solver-wstp \
-linkcreate -linkprotocol tcpip \
-linkname "10000@127.0.0.1,10001@127.0.0.1"
```

Since the `-linkprotocol` is `TCPIP`, you can connect to the plug-in running on a remote (presumably more powerful) machine.

The screenshot below shows how I like to layout host and plug-in windows.

[![Layout Screenshot](assets/layout_screenshot.jpg)](assets/layout_screenshot.jpg)

## Interacting with the Plug-in

### Listing functions and symbols

All exposed symbols are under the `` DispersionKit` `` context which is appended to the `$ContextPath` when connecting to the plug-in.
Available symbols can be listed using ``Names["DispersionKit`wstp*"]`` or ``?DispersionKit`wstp*``, which outputs

![Layout Screenshot](assets/listing_functions.jpg)

The help message is printed by typing `?` followed by each symbol name, for example

```
?wstpDDescription
```

The help message of the exposed symbols and functions provides a good description of how to use them, such as what the required arguments are and what they mean.
This manual focuses more on describing which function does what, what the result of its evaluation means, and the use of some nuanced options.

### Dispersion object management

The dispersion relation is given by the condition that there should be non-trivial solutions of the following homogeneous equations

\[
\mathbf D(\omega, \mathbf k) \cdot \mathbf E(\omega, \mathbf k) = 0
\qquad (1)
\]

Hence, $D(\omega, \mathbf k) \equiv \det \mathbf D = 0$.
The local Cartesian coordinate system is such that $\mathbf k = k_\perp\mathbf e_x + k_\| \mathbf e_z$, where the directional subscripts are with respect to the uniform background magnetic field vector.
The dispersion matrix is given by

\[
\mathbf D(\omega, \mathbf k)
=
(\mathbf{kk} - k^2 \mathbf 1) \frac{c^2}{\omega^2} + \epsilon,
\qquad (2)
\]

where the dielectric tensor is

\[
\epsilon(\omega, \mathbf k)
=
\mathbf 1 + \sum_s \frac{\omega_{ps}^2}{\omega^2} \int \sum_{n = -\infty}^\infty \mathbf S d^3v.
\qquad (3)
\]

See [Theory and Algorithm Details](../#theory-and-algorithm-details) for further details.
The inner summation has to be terminated at $n_{\rm terminal}$ which should be a sufficiently large number such that

\[
\sum_{n = -\infty}^\infty \mathbf S \approx \sum_{n = -n_{\rm terminal}}^{n_{\rm terminal}} \mathbf S.
\]

Internally, the convergence is tested and $n_{\rm terminal}$ is adjusted accordingly.
The test mechanism in place is not perfect and sometimes user guidance is required (see below regarding the `jmin` parameter and `wstpDMinimumCyclotronOrder` and `wstpDSetMinimumCyclotronOrder` functions).

----

The mental model for interacting with the plug-in is as follows.

1. Create a dispersion object (roughly speaking, $\mathbf D(\omega, \mathbf k)$ in Eq. (1)) using the parameters representing your plasma model.

    For this, `wstpDCreateDispersionObject` is used in the host side.
    If the creation is successful, it creates an object representing the plasma model in the plug-in process and returns a handle wrapped in the `wstpDDispersionObject` symbol to the host process.
    Host uses this handle to uniquely identify the object in the plug-in process.
    Unless specified otherwise, all subsequent operations are done with this handle.

    If needed, multiple objects representing different plasma models can be created.
    One can evaluate `wstpDDescription[handle]` to obtain a *textual* representation of the object associated with the `handle`.

    Evaluating `wstpDListAllDispersionObjects[]` returns the handles of all dispersion objects created.

1. Use the returned handle to do calculation.
This will be covered in detail below.

1. If no longer needed, destroy the object(s) to free resources in the plug-in process.

    You can destroy one object by evaluating `wstpDRemoveObject[handle]` and multiple objects by `wstpDRemoveObject[{handle1, ...}]`.

    One thing you may need to watch for is the memory consumption in the plug-in process.
    For distribution functions with which a close form of the dispersion matrix is not available, tables are used to keep the already calculated values.
    For the most cases, the memory exhaustion won't be an issue.
    But if your calculation uses one or more of these distribution functions and needs to cover a broad range of cyclotron resonance orders, this is something you need to be aware.

Let's see these in action.

---

To create an object,

```
In[4]:= handle = With[{c = 5, jmin = 10},
  wstpDCreateDispersionObject[c, {
    wstpDMaxwellianDistribution[-1, c, 0],
    wstpDMaxwellianDistribution[1/1836, c/Sqrt[1836], 0]
    }, jmin]
  ]

Out[4]= wstpDDispersionObject[0x7f8600e0e480]
```

Here for simplicity I am assuming a cold electron-proton plasma.
The function `wstpDCreateDispersionObject` takes three arguments:

> 1. The first argument `c` is the light speed.
> 2. The second argument is a list of distribution functions.
The available distribution functions will be discussed in detail [below](#pre-defined-plasma-distribution-functions), but in this example, the first one is the cold electron population and the second is the cold proton population.
> 3. The third argument takes an integer number.
This instructs that the summation of $\mathbf S$ must be done in the range $-$`jmin` $\le n \le$ `jmin` before an automatic conversion test is enabled.
As you can see from the help message, this is optional; the default is 5.

The following command prints the description associated with `handle` (a similar message is printed in the console where the plug-in is launched)

```
In[7]:= wstpDDescription[handle]

Out[7]= N4Disp10DispersionE[c->5, jmin->10, jmax->2000, vdfs->{
        	N4Disp15BiMaxwellianVDFE[Oc->-1, op->(5,0), th1->0, th2->0, vd->0],
        	N4Disp15BiMaxwellianVDFE[Oc->0.000544662, op->(0.11669,0), th1->0, th2->0, vd->0]
        }]
```

The meaning of this output is

> - The "heads" are mangled C++ class names which model the relevant objects.
> - `jmin` is what we passed when we created this object.
> - `jmax` = 2000 is a hard-coded number; the $n_{\rm terminal}$ cannot pass `jmax`.
> - `vdfs` lists the distribution functions of the plasma populations.
> - Every distribution function contains `Oc`, the cyclotron frequency, and `op`, the real and imaginary parts (the first and second numbers in the parenthesis) of the plasma frequency.
The plasma frequency is either pure real or pure imaginary; the latter case means that the density is negative.
> - For a bi-Maxwellian distribution, `th1`, `th2`, and `vd` denote the parallel, perpendicular thermal speeds and the parallel drift speed, respectively.

You can also see this object by

```
In[11]:= wstpDListAllDispersionObjects[]

Out[11]= {wstpDDispersionObject[0x7f8602804310]}
```

Let's create another object (omitting the `jmin` parameter this time)

```
In[12]:= handle2 = With[{c = 6.},
  wstpDCreateDispersionObject[c, {
    wstpDMaxwellianDistribution[-1., c, 0],
    wstpDMaxwellianDistribution[1/1836., c/Sqrt[1836.], 0]
    }]
  ]

Out[12]= wstpDDispersionObject[0x7f86038e6240]
```

You can confirm by

```
In[13]:= wstpDListAllDispersionObjects[]

Out[13]= {wstpDDispersionObject[0x7f8602804310], wstpDDispersionObject[0x7f86038e6240]}
```

Remove the first object

```
In[14]:= wstpDRemoveObject[handle]

In[15]:= wstpDListAllDispersionObjects[]

Out[15]= {wstpDDispersionObject[0x7f86038e6240]}
```

Remove all remaining objects

```
In[16]:= wstpDListAllDispersionObjects[] // wstpDRemoveObject

Out[16]= {Null}

In[17]:= wstpDListAllDispersionObjects[]

Out[17]= {}
```

### Dispersion matrix and root finding

This section explains the calculation of $\mathbf D(\omega, \mathbf k)$ and solving the nonlinear eqution $D(\omega, \mathbf k) = 0$ for $\omega(\mathbf k)$.

For demonstration, we will consider the whistler instability presented in [this paper](https://doi.org/10.1029/2011GL048375).
The plasma is composed of two bi-Maxwellian electron populations and one cold, charge-neutralizing proton population.
The first electron component constitutes 90% of the whole electron population with $\beta_\| = 0.009$ and $T_\perp/T_\| = 5$, where $\beta_\| = 8\pi n T_\|/B_0^2$.
Here *n* is the partial density of the population considered and $B_0$ is the magnitude of the uniform background magnetic field.
The second electron component constitutes 10% with $\beta_\| = 0.1$ and $T_\perp/T_\| = 2$.
The ratio of the electron plasma to cyclotron frequency is $\omega_{pe}/\Omega_{ce} = 4$.
We normalize the frequency to the electron cyclotron frequency $\Omega_{ce}$ and the length to the electron inertial length (or electron skin depth) $c/\omega_{pe}$.
With this choice of normalization, the speed is in terms of the so-called electron Alfven speed $v_{Ae} = B_0/\sqrt{4\pi n_e m_e}$, where $n_e$ is the total electron density.

Each bi-Maxwellian distribution is represented by `wstpDMaxwellianDistribution`.
This will be described in detail in the following section, but for now the parameters we need are (see the help message of `wstpDMaxwellianDistribution`)

|   | <center>Cyclotron frequency <br/>$\Omega_c$</center> | <center>Plasma frequency <br/> $\omega_p$</center> | <center>Parallel thermal speed <br/> $\theta_\|$</center> | <center>Perpendicular thermal speed <br/> $\theta_\perp$</center> | <center>Parallel drift speed <br/> $v_d$</center> |
| - | ------------------- | ---------------- | ---------------------- | --------------------------- | -------------------- |
| <center>Warm electron</center> | <center> -1</center> | <center>$4\sqrt{0.9}$</center> | <center>$\sqrt{0.009/0.9}$</center> | <center>$\sqrt{0.045/0.9}$</center> | <center>0</center> |
| <center>Hot electron</center> | <center> -1</center> | <center>$4\sqrt{0.1}$</center> | <center>$\sqrt{0.1/0.1}$</center> | <center>$\sqrt{0.2/0.1}$</center> | <center>0</center> |
| <center>Cold proton</center> | <center> 1/1836</center> | <center>$4/\sqrt{1836}$</center> | <center>0</center> | <center>0</center> | <center>0</center> |

To create this plasma system,

```
In[8]:= handle = With[{c = 4},
  wstpDCreateDispersionObject[c, {
    wstpDMaxwellianDistribution[-1, c*Sqrt[0.9], Sqrt[0.009/0.9], Sqrt[0.045/0.9]],
    wstpDMaxwellianDistribution[-1, c*Sqrt[0.1], Sqrt[0.1/0.1], Sqrt[0.2/0.1]],
    wstpDMaxwellianDistribution[1/1836, c/Sqrt[1836], 0]
    }]
  ];

In[9]:= handle // wstpDDescription

Out[9]= N4Disp10DispersionE[c->4, jmin->5, jmax->2000, vdfs->{
            N4Disp15BiMaxwellianVDFE[Oc->-1, op->(3.79473,0), th1->0.1, th2->0.223607, vd->0],
            N4Disp15BiMaxwellianVDFE[Oc->-1, op->(1.26491,0), th1->1, th2->1.41421, vd->0],
            N4Disp15BiMaxwellianVDFE[Oc->0.000544662, op->(0.093352,0), th1->0, th2->0, vd->0]
        }]
```

---

The [paper](https://doi.org/10.1029/2011GL048375) reports that there are two unstable zones.
The weaker unstable zone has the maximum growth rate $\gamma_{\max} = 0.016\Omega_{ce}$ appearing at real frequency $\omega_{\max} = 0.29\Omega_{ce}$, wavenumber $k = 0.63 (c/\omega_{pe})^{-1}$, and wave normal angle $\theta = 0$.
The function `wstpDFindRoot` is used to solve the dispersion relation.
Internally, the [Muller's method](https://en.wikipedia.org/wiki/Muller%27s_method) is used to find the complex frequency roots.
This algorithm requires three initial guesses.
One variant of this function accepts as its argument

- Dispersion object handle
- Parallel wavenumber $k_\|$
- Perpendicular wavenumber $k_\perp$
- Three initial guesses for complex wave frequency


Let's pretend that we only know that the complex frequency root is in the vicinity of $\omega = 0.2$ and $\gamma = 0$.
To find the exact root corresponding to $k_\| = 0.63$ and $k_\perp = 0$, evaluate

```
In[10]:= wstpDFindRoot[handle, 0.63, 0, 0.2 (1 + 0.0001 I {0, 1, 2})]

Out[10]= {flag -> 0, ω -> 0.294003 + 0.0157153 I, k1 -> 0.63, k2 -> 0.}
```

It returns the result as a list of rules, some of which are self-explanatory.
The exact root found is close to the one reported by the [paper](https://doi.org/10.1029/2011GL048375).
The meaning of the flag is described in the help message.

Let's take a look at the plug-in output:

```
k1=0.63, k2=0, |k|=0.63, psi=0
i = 0, o = (0.2,4e-05), jmin = 5
i = 1, o = (0.339595,-0.0290527), jmin = 5
i = 2, o = (0.288123,0.0190378), jmin = 5
i = 3, o = (0.293934,0.0159575), jmin = 5
i = 4, o = (0.294003,0.0157155), jmin = 5
```

- `k1` means $k_\|$.
- `k2` means $k_\perp$.
- `|k|` means $\sqrt{k_\|^2 + k_\perp^2}$.
- `psi` means the wave normal angle, $\tan^{-1}(k_\perp/k_\|)$.
- `i` denotes the iteration step count for iterative root search.
- `o` indicates a pair of the real and imaginary part of the complex frequency found in that step.
- `jmin` is the minimum $n_{\rm terminal}$ needed for convergence.

Let's take a look at the extra options available:

- Passing `"ReturnDispersionMatrix" -> True` returns the components of $\mathbf D$ at the found solution (default `False`).

    ```
    In[3]:= wstpDFindRoot[handle, 0.63, 0, 0.2 (1 + 0.0001 I {0, 1, 2}), "ReturnDispersionMatrix" -> True]

    Out[3]= {flag -> 0, ω -> 0.294003 + 0.0157153 I, k1 -> 0.63, k2 -> 0.,

    >    D -> {{-59.3985 + 6.21292 I, -6.21292 - 59.3985 I, 0. + 0. I},

    >      {6.21292 + 59.3985 I, -59.3985 + 6.21292 I, 0. + 0. I}, {0. + 0. I, 0. + 0. I, -172.042 + 25.7608 I}}}
    ```

- Root finding is an iterative process. The maximum iterations to try is passed by the `MaxIterations` option.
The default value is 20 whereas in the above example, the solution is found at the 5th try.
So, the following example should report failure:

    ```
    In[4]:= wstpDFindRoot[handle, 0.63, 0, 0.2 (1 + 0.0001 I {0, 1, 2}), MaxIterations -> 4]

    DispersionKit::warn: wstpDFindRoot - root finder failed at k1 = 0.63 and k2 = 0.

    wstpDFindRoot::noroot: No root found.

    Out[4]= $Aborted
    ```

    It is instructive to set this number liberally when you try to guess the first root,
    but set it tightly (typically 5) for subsequent root finding starting from the first root.
    This is to avoid a sudden jump between dispersion branches.

- The last option is `"UpdateMinimumCyclotronOrderFromPreviousIteration"`, which will be handy shortly.

Since we have narrowed down the root, let's find a series of roots by scanning the wavenumber domain.
Say, we want to scan the range $k_\| = [0.63, 0.7]$ in steps of 0.01.
We can do

```
In[5]:= With[{k1 = Range[0.63, 0.7, 0.01]},
 wstpDFindRoot[handle, k1, k1 * 0, Complex[0.294003, 0.0157155] (1 + 0.0001 I {0, 1, 2}), MaxIterations -> 5]]
 ]

Out[5]= {flag -> 0, ω ->

>     {0.294003 + 0.0157153 I, 0.299929 + 0.0156969 I, 0.305854 + 0.0156216 I, 0.311779 + 0.0154923 I,

>      0.317705 + 0.0153115 I, 0.323634 + 0.0150824 I, 0.329565 + 0.0148078 I, 0.335501 + 0.014491 I},

>    k1 -> {0.63, 0.64, 0.65, 0.66, 0.67, 0.68, 0.69, 0.7}, k2 -> {0., 0., 0., 0., 0., 0., 0., 0.}}
```

`wstpDFindRoot` automatically threads over the elements of $k_\|$ and $k_\perp$ arrays.
See the help message of `wstpDFindRoot` about how the three initial guesses are setup for the subsequent root finding.
Note that `MaxIterations` is limited to 5.
If this root finding fails, you may want to reduce the scanning step size, instead of increasing `MaxIterations`.

---

Let's take a look at the other unstable zone.
According to the [paper](https://doi.org/10.1029/2011GL048375), the stronger unstable zone has $\gamma_{\max} = 0.03 \Omega_{ce}$, $\omega_{\max} = 0.69 \Omega_{ce}$, $k = 3.8 (c/\omega_{pe})^{-1}$, and wave normal angle $\theta = 48^\circ$.

This time, let's scan the wavenumber space at a fixed wave normal angle.
There is no variant of `wstpDFindRoot` for this job, but we can use Mathematica to fashion the input parameters:

```
In[6]:= sol = With[{k = Range[3.8, 3.7, -0.01], wna = 48.0 Degree},
 wstpDFindRoot[handle, k Cos[wna], k Sin[wna], Complex[0.69, 0.03] (1 + 0.0001 I {0, 1, 2}), MaxIterations -> 5]
 ];
```

with the plug-in console output:

```
k1=2.5427, k2=2.82395, |k|=3.8, psi=48
i = 0, o = (0.689994,0.030138), jmin = 11
i = 1, o = (0.687658,0.0300041), jmin = 11
k1=2.536, k2=2.81652, |k|=3.79, psi=48
i = 0, o = (0.687658,0.0300042), jmin = 11
i = 1, o = (0.687384,0.030009), jmin = 11

<<output shortened...>>

k1=2.48247, k2=2.75707, |k|=3.71, psi=48
i = 0, o = (0.685416,0.0299503), jmin = 11
i = 1, o = (0.685128,0.0299287), jmin = 11
k1=2.47578, k2=2.74964, |k|=3.7, psi=48
i = 0, o = (0.685128,0.0299287), jmin = 11
i = 1, o = (0.684839,0.0299038), jmin = 11
```

Draw your attention to `jmin` which reports 11.
The program determines that $n_{\rm terminal}$ must be at least 11 for convergence.
Recall that this was set to 5 (a default number if not specified).
You can print out `jmin` associated with the dispersion object `handle` by

```
In[8]:= wstpDMinimumCyclotronOrder[handle]

Out[8]= 5
```

What happens in the program is that as the program scans through the wavenumber domain, it starts from `jmin = 5` to find the optimal $n_{\rm terminal}$.
Naturally, you may want to update `jmin` as you go.
This can be achived by passing `"UpdateMinimumCyclotronOrderFromPreviousIteration" -> True` (`False` by default):

```
In[9]:= sol = With[{k = Range[3.8, 3.7, -0.01], wna = 48.0 Degree},
 wstpDFindRoot[handle, k Cos[wna], k Sin[wna], Complex[0.69, 0.03] (1 + 0.0001 I {0, 1, 2}),
  MaxIterations -> 5, "UpdateMinimumCyclotronOrderFromPreviousIteration" -> True]
 ];

In[11]:= wstpDMinimumCyclotronOrder[handle]

Out[11]= 11
```

As you can see, now `jmin` associated with this `handle` is updated.
To double check $n_{\rm terminal} = 11$ is indeed sufficiently large,
you can increase `jmin` from the beginning, do the same calculation, and compare the results:

```
In[12]:= wstpDSetMinimumCyclotronOrder[handle, 20]

Out[12]= {20, 2000}

In[13]:= sol2 = With[{k = Range[3.8, 3.7, -0.01], wna = 48.0 Degree},
 wstpDFindRoot[handle, k Cos[wna], k Sin[wna],
  Complex[0.69, 0.03] (1 + 0.0001 I {0, 1, 2}), MaxIterations -> 5,
  "UpdateMinimumCyclotronOrderFromPreviousIteration" -> True]
 ];
```

The whole point of having these options is to minimize the calculation time with a reasonable accuracy of the roots found.
Of course, for this simple plasma system and with the wave normal angle not too large, you could have set `jmin` to a number $> 100$ from the beginning and don't worry about the fact that the program may erroneously determine $n_{\rm terminal}$.

You can calculate $\mathbf D(\omega, \mathbf k)$ using `wstpDDispersionMatrix` once you know the dispersion relation.
One caveat is that it doesn't remember the `jmin` values used to get the complex frequency.
So, approprivate values for `jmin` need be set beforehand.
A better option is to have `wstpDFindRoot` return $\mathbf D$ as it finds the roots.

---

You may feel that the functionality provided by `wstpDFindRoot` and `wstpDDispersionMatrix` is limited.
For example, you may want it to scan the 2D wavenumber domain bi-directionally.
Or you want it to return the dimensionless ratios among the electric field, magnetic field, current, etc., as it finds the roots.
However, these are not restrictions, because you have at your disposal all the tools and the language support provided by Mathematica to accomplish whatever you want, using what's provided by the plug-in as the building blocks.

### Pre-defined plasma distribution functions

#### 1. Bi-Maxwellian distribution with parallel drift

The bi-Maxwellian velocity distribution function assumed in the program is given by

\[
f(v_\|, v_\perp)
=
\frac{n}{\pi^{3/2} \theta_\| \theta_\perp^2}
\exp\left(- \frac{(v_\| - v_d)^2}{\theta_\|^2}\right)
\exp\left(- \frac{v_\perp^2}{\theta_\perp^2}\right)
\]

It is represented by `wstpDMaxwellianDistribution` which accepts as its arguments

- $\Omega_c$ : The cyclotron frequency of the species (carrying the sign of the charge)
- $\omega_p$ : The plasma frequency of the species (either real if the density $n$ is positive, or imaginary if $n < 0$)
- $\theta_\|$ : The parallel thermal speed (can be 0)
- $\theta_\perp$ : The perpendicular thermal speed (can be 0).
If not specified, $\theta_\perp = \theta_\|$ is assumed.
- $v_d$ : The parallel drift speed.
If not specified, $v_d = 0$ is assumed.

#### 2. Ring-Maxwellian distribution with parallel drift

The ring-Maxwellian velocity distribution function assumed in the program is given by

\[
f(v_\|, v_\perp)
=
\frac{n}{\pi^{3/2} \theta_\| \theta_\perp^2 A_r(v_r/\theta_\perp)}
\exp\left(- \frac{(v_\| - v_d)^2}{\theta_\|^2}\right)
\exp\left(- \frac{(v_\perp - v_r)^2}{\theta_\perp^2}\right)
\]

with $A_r(a) = e^{-a^2} + (\sqrt{\pi} a) \mathrm{erfc}(-a)$.

It is represented by `wstpDRingDistribution` which accepts as its arguments

- $\Omega_c$ : The cyclotron frequency of the species (carrying the sign of the charge)
- $\omega_p$ : The plasma frequency of the species (either real if the density $n$ is positive, or imaginary if $n < 0$)
- $\theta_\|$ : The parallel thermal speed (can be 0)
- $\theta_\perp$ : The perpendicular thermal speed (can be 0).
If not specified, $\theta_\perp = \theta_\|$ is assumed.
- $v_d$ : The parallel drift speed.
If not specified, $v_d = 0$ is assumed.
- $v_r$ : The perpendicular ring speed.
If not specified, $v_r = 0$ is assumed.

#### 3. Bi-Kappa distribution with parallel drift

The bi-Kappa velocity distribution function assumed in the program is given by

\[
f(v_\|, v_\perp)
=
\frac{n}{\pi^{3/2} \theta_\| \theta_\perp^2}
\frac{\Gamma(\kappa)}{\sqrt \kappa \Gamma(\kappa - 1/2)}
\left(
1 + \frac{(v_\| - v_d)^2}{\kappa \theta_\|^2} + \frac{v_\perp^2}{\kappa \theta_\perp^2}
\right)^{-(\kappa + 1)}
\]

with $\kappa > 3/2$.

It is represented by `wstpDKappaDistribution` which accepts as its arguments

- $\Omega_c$ : The cyclotron frequency of the species (carrying the sign of the charge)
- $\omega_p$ : The plasma frequency of the species (either real if the density $n$ is positive, or imaginary if $n < 0$)
- $\kappa$ : The real-valued $\kappa$ parameter greater than 3/2.
- $\theta_\|$ : The parallel thermal speed (can be 0)
- $\theta_\perp$ : The perpendicular thermal speed (can be 0).
If not specified, $\theta_\perp = \theta_\|$ is assumed.
- $v_d$ : The parallel drift speed.
If not specified, $v_d = 0$ is assumed.

#### 4. Product bi-Kappa distribution with parallel drift

The product bi-Kappa velocity distribution function assumed in the program is given by

\[
f(v_\|, v_\perp)
=
\frac{n \sqrt{\kappa_\|} \Gamma(\kappa_\|)}{\Gamma(\kappa_\| + 1/2)}
\frac{\left(1 + (v_\| - v_d)^2/(\kappa_\| \theta_\|^2)\right)^{-(\kappa_\| + 1)}}{\sqrt \pi \theta_\|}
\frac{\left(1 + v_\perp^2/(\kappa_\perp \theta_\|^2)\right)^{-(\kappa_\perp + 1)}}{\pi \theta_\perp^2}
\]

with $\kappa_\| > 1/2$ and $\kappa_\perp > 1$.

It is represented by `wstpDProductKappaDistribution` which accepts as its arguments

- $\Omega_c$ : The cyclotron frequency of the species (carrying the sign of the charge)
- $\omega_p$ : The plasma frequency of the species (either real if the density $n$ is positive, or imaginary if $n < 0$)
- $\kappa_\|$ : The real-valued parallel $\kappa$ parameter greater than 1/2.
- $\kappa_\perp$ : The real-valued perpendicular $\kappa$ parameter greater than 1.
- $\theta_\|$ : The parallel thermal speed (can be 0)
- $\theta_\perp$ : The perpendicular thermal speed (can be 0).
If not specified, $\theta_\perp = \theta_\|$ is assumed.
- $v_d$ : The parallel drift speed.
If not specified, $v_d = 0$ is assumed.

## Note on unit system and normalization

The two fundamental units are the time and length (or the way I like to think is frequency and speed).
All other quantities are represented in terms of these fundamental units.
There is no specific normalization baked into the program.
In the above example, I use the normalization where the quantities of speed are represented in terms of the Alfven speed and the quantities of frequency are in terms of the electron cyclotron frequency.
I could have used m/s for speed and Hz for frequency.
