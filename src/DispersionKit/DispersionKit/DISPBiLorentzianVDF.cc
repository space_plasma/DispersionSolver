/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "DISPBiLorentzianVDF.h"
#include "DISPBesselJ.h"
#include "DISPIntegerKappaPDF.h"
#include "DISPSplineKappaPDF.h"
#include "DISPSquareMatrix.h"
#include <cmath>
#include <pthread.h>
#include <stdexcept>
#include <string>

// MARK: PDFWrapper
//
struct Disp::BiLorentzianVDF::_PDFWrapper {
    virtual ~_PDFWrapper() {}
    virtual bool                                           is_integral() const                 = 0;
    virtual double                                         f(double const x) const             = 0;
    virtual double                                         dfdx(double const x) const          = 0;
    virtual std::tuple<CX::Number, CX::Number, CX::Number> Ztriplet(CX::Number const &z) const = 0;
    virtual std::tuple<CX::Number, CX::Number, CX::Number> Wtriplet(CX::Number const &z) const = 0;

protected:
    _PDFWrapper() {}
    _PDFWrapper(_PDFWrapper const &) = delete;
    _PDFWrapper &operator=(_PDFWrapper const &) = delete;
};
namespace {
struct IntegerKappaPDF : public Disp::BiLorentzianVDF::_PDFWrapper {
    Disp::IntegerKappaPDF pdf;
    explicit IntegerKappaPDF(int const kappa)
    : pdf(kappa) {}
    bool                                           is_integral() const override { return true; }
    double                                         f(double const x) const override { return pdf.f(x); }
    double                                         dfdx(double const x) const override { return pdf.dfdx(x); }
    std::tuple<CX::Number, CX::Number, CX::Number> Ztriplet(CX::Number const &z) const override { return pdf.Ztriplet(z); }
    std::tuple<CX::Number, CX::Number, CX::Number> Wtriplet(CX::Number const &z) const override { return pdf.Wtriplet(z); }
};
struct RealKappaPDF : public Disp::BiLorentzianVDF::_PDFWrapper {
    Disp::SplineKappaPDF pdf;
    explicit RealKappaPDF(double const kappa)
    : pdf(kappa) {}
    bool                                           is_integral() const override { return false; }
    double                                         f(double const x) const override { return pdf.f(x); }
    double                                         dfdx(double const x) const override { return pdf.dfdx(x); }
    std::tuple<CX::Number, CX::Number, CX::Number> Ztriplet(CX::Number const &z) const override { return pdf.Ztriplet(z); }
    std::tuple<CX::Number, CX::Number, CX::Number> Wtriplet(CX::Number const &z) const override { return pdf.Wtriplet(z); }
};
} // namespace

// MARK:- BiLorentzianVDF
//
Disp::BiLorentzianVDF::~BiLorentzianVDF()
{
}
Disp::BiLorentzianVDF::BiLorentzianVDF(real_type const Oc, complex_type const &op, std::tuple<real_type> const kappa, real_type const th1, real_type const th2, real_type const vd)
: VDF(), _op(op), _Oc(Oc), _th1(th1), _th2(th2), _vd(vd), _kappa(std::get<0>(kappa)), _pdf_kp1(), _max_x2()
{
    if (std::abs(op.real() * op.imag() / _Oc) > 1e-10) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - op should be either pure real or pure imaginary");
    }
    if (th1 < 0. || th2 <= 0.) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - either th1 or th2 or both is negative");
    }
    if (_kappa <= 1.5) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - kappa should be greater than 3/2");
    }
    int const round_k = int(std::round(_kappa));
    // note that the pdf should be constructed with kappa + 1!!
    if (std::abs(_kappa - round_k) < 1e-10) { // integral value
        _pdf_kp1.reset(new ::IntegerKappaPDF(round_k + 1));
    } else {
        _pdf_kp1.reset(new ::RealKappaPDF(_kappa + 1));
    }
    _max_x2 = std::sqrt((std::pow(_epsilon, -1. / (1 + _kappa)) - 1) * _kappa);
}

std::string Disp::BiLorentzianVDF::description() const
{
    std::ostringstream os;
    printo(os, typeid(*this).name(), "[Oc->", Oc(), ", op->", op(), ", th1->", _th1, ", th2->", _th2, ", vd->", vd(), ", kappa->", _kappa, "]");
    return os.str();
}

double Disp::BiLorentzianVDF::T1() const
{
    return _kappa / (2 * _kappa - 3) * _th1 * _th1;
}
double Disp::BiLorentzianVDF::T2() const
{
    return _kappa / (2 * _kappa - 3) * _th2 * _th2;
}

double Disp::BiLorentzianVDF::g(real_type const t) const noexcept
{
    return std::pow(1 + t * t / _kappa, -(_kappa + 1));
}
double Disp::BiLorentzianVDF::f(real_type const v1, real_type const v2) const
{
    real_type const x1 = (v1 - _vd) / _th1;
    real_type const x2 = v2 / _th2;
    real_type       C  = .5 * M_2_SQRTPI / M_PI;
    if (_kappa > 100.) { // asymptotic expansion
        C *= 1. - 3 / (8. * _kappa);
    } else {
        C *= std::tgamma(_kappa) / (std::sqrt(_kappa) * std::tgamma(_kappa - .5));
    }
    real_type f = C * g(x2) * g(std::sqrt(_kappa / (_kappa + x2 * x2)) * x1);
    return f /= _th1 * _th2 * _th2;
}
double Disp::BiLorentzianVDF::dfdv1(real_type const v1, real_type const v2) const
{
    real_type const x1 = (v1 - _vd) / _th1;
    real_type const x2 = v2 / _th2;
    real_type       df = -2. * (_kappa + 1) / _kappa * x1 / (1 + (x1 * x1 + x2 * x2) / _kappa) * f(v1, v2);
    return df /= _th1;
}
double Disp::BiLorentzianVDF::dfdv2(real_type const v1, real_type const v2) const
{
    real_type const x1 = (v1 - _vd) / _th1;
    real_type const x2 = v2 / _th2;
    real_type       df = -2. * (_kappa + 1) / _kappa * x2 / (1 + (x1 * x1 + x2 * x2) / _kappa) * f(v1, v2);
    return df /= _th2;
}

namespace {
constexpr double zero_tolerance = 1e-5;

// BesselJ derivatives
inline double J(int const n, double const x)
{
    static Disp::BesselJ const &J = Disp::BesselJ::shared();
    return J(n, x);
}
inline double Jp(int const n, double const x)
{
    return .5 * (J(n - 1, x) - J(n + 1, x));
}

// GaussLegendre integrator
constexpr long N = 10;
using Integrator = Utility::GaussLegendreIntegrator<double, N>;
Integrator const &integrator()
{
    static Integrator const *integrator;
    static pthread_once_t    once = PTHREAD_ONCE_INIT;
    if (pthread_once(&once, []() {
            integrator = new Integrator;
        })) {
        throw std::runtime_error(__PRETTY_FUNCTION__);
    }
    return *integrator;
}

// piecewise integral
template <class F>
auto integrate(double const a /*>=0*/, std::pair<double, double> const x_lim, F const &f) -> CX::Matrix
{
    constexpr double interval = 2. * M_PI;
    double const     abs_a    = std::abs(a);
    double const     dx       = (abs_a < interval ? 1. : interval / abs_a); // unit interval of each piece
    double           x1       = x_lim.first;
    double           x0       = x1;
    CX::Matrix       result{};
    auto const      &xs = integrator().abscissas();
    auto const      &ws = integrator().weights();
    for (x1 += dx; x0 < x_lim.second; x0 = x1, x1 += dx) {
        // GaussLegendre integral
        double const     xr = .5 * dx;
        decltype(result) s{};
        for (unsigned long i = 0, n = xs.size(); i < n; ++i) {
            double const  t = xs[i];
            double const  w = ws[i];
            double const  x = (t + 1) * xr + x0;
            decltype(s) &&y = f(x);
            s += y *= w;
        }
        result += s *= xr;
    }
    return result;
}
} // namespace
CX::Matrix Disp::BiLorentzianVDF::K(std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const
{
    real_type const    k1 = k.first, k2 = k.second;
    real_type const    a     = k2 * _th2 / _Oc;
    real_type const    k1th1 = k1 * _th1, k1th2 = k1 * _th2;
    complex_type const o_jOc      = o - j * _Oc;
    complex_type const o_k1vd     = o - k1 * _vd;
    complex_type const o_k1vd_jOc = o_jOc - k1 * _vd;
    if (std::abs(k1th1) < std::abs(o_k1vd_jOc * ::zero_tolerance)) {
        // asymptotic version
        Utility::Vector<complex_type, 2> A00{ k1 * k1 * (_th2 - _th1) * (_th2 + _th1) / o_k1vd_jOc, o_k1vd };
        A00 /= o_k1vd_jOc;
        Utility::Vector<complex_type, 2> A02{ k1th2 * o_jOc / o_k1vd_jOc, _vd / _th2 * j * _Oc };
        A02 /= o_k1vd_jOc;
        Utility::Vector<complex_type, 2> A20{ k1th2 * o_jOc / o_k1vd_jOc, _vd / _th2 * o_k1vd };
        A20 /= o_k1vd_jOc;
        Utility::Vector<complex_type, 2> A22{ o_jOc * o_jOc / o_k1vd_jOc + _th1 * _th1 / (_th2 * _th2) * j * _Oc, _vd * _vd / (_th2 * _th2) * j * _Oc };
        A22 /= o_k1vd_jOc;
        CX::Matrix &&K = ::integrate(a, { 0., _max_x2 }, [this, j, a, &A00, &A02, &A20, &A22](real_type const x2) -> matrix_type {
            CX::Matrix      K      = this->integrandPrefix(j, a, x2);
            real_type const prefix = (_kappa + x2 * x2) / (2 * _kappa + 1);

            ::get<0, 0>(K) *= prefix * std::get<0>(A00) + std::get<1>(A00); // K11
            ::get<0, 1>(K) *= prefix * std::get<0>(A00) + std::get<1>(A00); // K12
            ::get<1, 1>(K) *= prefix * std::get<0>(A00) + std::get<1>(A00); // K22

            ::get<0, 2>(K) *= prefix * std::get<0>(A02) + std::get<1>(A02); // K13
            ::get<1, 2>(K) *= prefix * std::get<0>(A02) + std::get<1>(A02); // K23

            ::get<2, 0>(K) *= prefix * std::get<0>(A20) + std::get<1>(A20); // K31
            ::get<2, 1>(K) *= prefix * std::get<0>(A20) + std::get<1>(A20); // K32

            ::get<2, 2>(K) *= prefix * std::get<0>(A22) + std::get<1>(A22); // K33
            return K;
        });
        ::get<1, 0>(K) = -::get<0, 1>(K); // K21 = -K12
        return K *= (-1 + 1 / (2 * _kappa)) * (1 + 1 / (2 * _kappa));
    } else {
        // normal version
        complex_type const                     zn = o_k1vd_jOc / k1th1, z0 = o_k1vd / k1th1;
        Utility::Vector<complex_type, 2> const A00{ (_th2 - _th1) * (_th2 + _th1) / (_th1 * _th1), z0 };
        Utility::Vector<complex_type, 2> const A02{ _th2 / _th1 * o_jOc / k1th1 + j * _Oc / k1th2, j * _Oc / k1th1 * _vd / _th2 };
        Utility::Vector<complex_type, 3> const A20{ _th2 / _th1 - _th1 / _th2, (_th2 / _th1 - _th1 / _th2) * _vd / _th1 + _th1 / _th2 * z0, _vd / _th2 * z0 };
        Utility::Vector<complex_type, 3> const A22{ zn + _vd / _th1 + j * _Oc / k1th2 * _th1 / _th2, (zn + _vd / _th1) * _vd / _th1 + 2. * j * _Oc / k1th2 * _vd / _th2, j * _Oc / k1th1 * _vd * _vd / (_th2 * _th2) };

        CX::Matrix &&K = ::integrate(a, { 0., _max_x2 }, [this, j, a, zn, &A00, &A02, &A20, &A22](real_type const x2) -> matrix_type {
            CX::Matrix K = this->integrandPrefix(j, a, x2);

            real_type const     prefix = std::sqrt((_kappa + 1) / (_kappa + x2 * x2));
            triplet_type const &Z      = this->_pdf_kp1->Ztriplet(prefix * zn);

            ::get<0, 0>(K) *= std::get<0>(A00) * std::get<1>(Z) + prefix * std::get<1>(A00) * std::get<0>(Z); // K11
            ::get<0, 1>(K) *= std::get<0>(A00) * std::get<1>(Z) + prefix * std::get<1>(A00) * std::get<0>(Z); // K12
            ::get<1, 1>(K) *= std::get<0>(A00) * std::get<1>(Z) + prefix * std::get<1>(A00) * std::get<0>(Z); // K22

            ::get<0, 2>(K) *= std::get<0>(A02) * std::get<1>(Z) + prefix * std::get<1>(A02) * std::get<0>(Z); // K13
            ::get<1, 2>(K) *= std::get<0>(A02) * std::get<1>(Z) + prefix * std::get<1>(A02) * std::get<0>(Z); // K23

            ::get<2, 0>(K) *= std::get<0>(A20) / prefix * std::get<2>(Z) + std::get<1>(A20) * std::get<1>(Z) + prefix * std::get<2>(A20) * std::get<0>(Z); // K31
            ::get<2, 1>(K) *= std::get<0>(A20) / prefix * std::get<2>(Z) + std::get<1>(A20) * std::get<1>(Z) + prefix * std::get<2>(A20) * std::get<0>(Z); // K32

            ::get<2, 2>(K) *= std::get<0>(A22) / prefix * std::get<2>(Z) + std::get<1>(A22) * std::get<1>(Z) + prefix * std::get<2>(A22) * std::get<0>(Z); // K33
            return K;
        });
        ::get<1, 0>(K) = -::get<0, 1>(K); // K21 = -K12
        return K *= (1 - 1 / (2 * _kappa)) * (1 + 1 / (2 * _kappa));
    }
}

CX::Matrix Disp::BiLorentzianVDF::integrandPrefix(int const j, real_type const a, real_type const x2) const
{
    using Z                 = CX::Number;
    real_type const ax2     = a * x2;
    real_type const _2x2Jpn = 2. * x2 * ::Jp(j, ax2), _2Jn = 2. * ::J(j, ax2);
    real_type const _2nJnOa = x2 * (::J(j + 1, ax2) + ::J(j - 1, ax2));
    CX::Matrix      K{};
    ::get<0, 0>(K) = Z{ _2nJnOa * _2nJnOa, 0. };
    ::get<0, 1>(K) = Z{ 0., _2nJnOa * _2x2Jpn };
    ::get<0, 2>(K) = Z{ _2nJnOa * _2Jn, 0. };
    ::get<1, 1>(K) = Z{ _2x2Jpn * _2x2Jpn, 0. };
    ::get<1, 2>(K) = Z{ 0., -_2Jn * _2x2Jpn };
    ::get<2, 0>(K) = Z{ _2nJnOa * _2Jn, 0. };
    ::get<2, 1>(K) = Z{ 0., _2Jn * _2x2Jpn };
    ::get<2, 2>(K) = Z{ _2Jn * _2Jn, 0. };
    return K *= x2 * std::pow(1 + x2 * x2 / _kappa, -(_kappa + 1.5));
}
