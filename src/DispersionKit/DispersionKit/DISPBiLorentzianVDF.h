/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <DispersionKit/DISPVDF.h>
#include <DispersionKit/DispersionKit-config.h>
#include <memory>
#include <tuple>

DISPERSIONKIT_BEGIN_NAMESPACE
struct BiLorentzianVDF : public VDF {
    ~BiLorentzianVDF();
    explicit BiLorentzianVDF(real_type const Oc, complex_type const &op, std::tuple<real_type> const kappa, real_type const th)
    : BiLorentzianVDF(Oc, op, kappa, th, th) {}
    explicit BiLorentzianVDF(real_type const Oc, complex_type const &op, std::tuple<real_type> const kappa, real_type const th1, real_type const th2)
    : BiLorentzianVDF(Oc, op, kappa, th1, th2, 0.) {}
    explicit BiLorentzianVDF(real_type const Oc, complex_type const &op, std::tuple<real_type> const kappa, real_type const th1, real_type const th2, real_type const vd);

    /**
     @name Overrides
     Overrides of VDF virtual functions.
     */
    ///@{
    std::string description() const override;

    real_type    Oc() const override { return _Oc; }
    complex_type op() const override { return _op; }

    real_type vd() const override { return _vd; }
    real_type T1() const override;
    real_type T2() const override;

    real_type f(real_type const v1, real_type const v2) const override;
    real_type dfdv1(real_type const v1, real_type const v2) const override;
    real_type dfdv2(real_type const v1, real_type const v2) const override;

    matrix_type K(std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const override;
    ///@}

    /**
     @brief A pair of parallel and perpendicular kappa indexes.
     */
    real_type const &kappa() const noexcept { return _kappa; }

    struct _PDFWrapper;

private:
    complex_type                 _op;
    real_type                    _Oc, _th1, _th2, _vd;
    real_type                    _kappa;
    std::unique_ptr<_PDFWrapper> _pdf_kp1; // this should be constructed with kappa + 1!!
    real_type                    _max_x2;
    constexpr static double      _epsilon = 1e-13; //!< Used to determines the maximum v2/th2 for the perp integral.

    using triplet_type = std::tuple<complex_type, complex_type, complex_type>;
    inline real_type   g(real_type const t) const noexcept;
    inline matrix_type integrandPrefix(int const j, real_type const a, real_type const x2) const;
};
DISPERSIONKIT_END_NAMESPACE
