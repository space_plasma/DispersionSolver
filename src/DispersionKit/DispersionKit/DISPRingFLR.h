/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <DispersionKit/DISPBesselJ.h>
#include <DispersionKit/DispersionKit-config.h>
#include <UtilityKit/UtilityKit.h>
#include <array>
#include <memory>
#include <unordered_map>

DISPERSIONKIT_BEGIN_NAMESPACE
/**
 @brief Evaluates integrals involving finite Larmor radius effects for a ring distribution function,
 f(x) = exp(-(x-b)^2)/π*C, where C = exp(-b^2) + √π*b*erfc(-b).
 @discussion The evaluation results for a given parameter set are automatically cached.
 */
struct RingFLR {
    virtual ~RingFLR();
    RingFLR(RingFLR &&) noexcept = default;
    RingFLR &operator=(RingFLR &&) noexcept = default;
    RingFLR(RingFLR const &);
    RingFLR &operator=(RingFLR const &);

    /**
     @brief Construct with a ring speed, b.
     */
    explicit RingFLR(double const b);

    /**
     @brief Default constructor with b = 0, in which case the results should ideally be identical to those of MaxwellianFLR.
     */
    explicit RingFLR()
    : RingFLR(0.) {}

    /**
     @brief Clear the lookup cache.
     */
    void clear_cache();

    /**
     @brief Calculate f(x).
     */
    double f(double const x) const noexcept;

    /**
     @brief Calculate df(x)/dx.
     */
    double dfdx(double const x) const noexcept;

    /**
     @brief Evaluates integral of 2π*J(n, ax)^2*f(x)*(x - c*b) from 0 to inf.
     */
    inline double A(int const n, double const a, bool const c) const;
    /**
     @brief Evaluates integral of 2π*J(n, ax)*J'(n, ax)*f(x)*x*(x - c*b) from 0 to inf.
     */
    inline double B(int const n, double const a, bool const c) const;
    /**
     @brief Evaluates integral of 2π*J'(n, ax)^2*f(x)*x^2*(x - c*b) from 0 to inf.
     */
    inline double C(int const n, double const a, bool const c) const;

    /**
     @brief Evaluates A(n, a, true|false)/a^2 correctly handling the case where |a| -> 0.
     @discussion It is undefined when n = a = 0.
     */
    double AOa2(int const n, double const a, bool const c) const;
    /**
     @brief Evaluates B(n, a, true|false)/a correctly handling the case where |a| -> 0.
     */
    double BOa(int const n, double const a, bool const c) const;

    //@{
    /** Parity to SplineFLR's methods. */
    double A(int const n, double const a) const { return this->A(n, a, false); }
    double P(int const n, double const a) const { return -2. * this->A(n, a, true); }

    double B(int const n, double const a) const { return this->B(n, a, false); }
    double Q(int const n, double const a) const { return -2. * this->B(n, a, true); }

    double C(int const n, double const a) const { return this->C(n, a, false); }
    double R(int const n, double const a) const { return -2. * this->C(n, a, true); }

    double AOa2(int const n, double const a) const { return this->AOa2(n, a, false); }
    double POa2(int const n, double const a) const { return -2. * this->AOa2(n, a, true); }

    double BOa(int const n, double const a) const { return this->BOa(n, a, false); }
    double QOa(int const n, double const a) const { return -2. * this->BOa(n, a, true); }
    //@}

    /**
     @brief Returns the parameter b.
     */
    double b() const noexcept { return _b; }
    /**
     @brief Returns the instance of BesselJ being used to evaluate the integrals.
     @note Currently, it is BesselJ::shared().
     */
    BesselJ const &J() const noexcept { return *_J; }

protected:
    using value_pack_type   = Utility::Vector<double, 6>;
    using optional_type     = Utility::Optional<value_pack_type>;
    using _InnerLookupTable = std::unordered_map<double, optional_type>;
    using _OuterLookupTable = std::array<_InnerLookupTable, std::tuple_size<BesselJ::zero_table_type>::value>;

    std::unique_ptr<_OuterLookupTable> _lookup_table;
    BesselJ const                     *_J;
    double                             _b;
    double                             _constant_factor;
    double                             _zero_tolerance;

    explicit RingFLR(double const b, BesselJ const *J);

    // n >= 0 and a >= 0
    double _A(int const n, double const a, bool const c) const;
    double _B(int const n, double const a, bool const c) const;
    double _C(int const n, double const a, bool const c) const;

    // returns {A_n(a, b, 0), A_n(a, b, b), B_n(a, b, 0), B_n(a, b, b), C_n(a, b, 0), C_n(a, b, b)}
    value_pack_type const   _lookup_ABC(int const n, double const a) const;
    virtual value_pack_type _eval_ABC(int const n, double const a) const;
};

double RingFLR::A(int const n, double const a, bool const c) const
{
    return _A(n < 0 ? -n : n, a < 0. ? -a : a, c);
}
double RingFLR::B(int const n, double const a, bool const c) const
{
    double const a_sign = a < 0. ? -1 : 1;
    return _B(n < 0 ? -n : n, a_sign * a, c) * a_sign;
}
double RingFLR::C(int const n, double const a, bool const c) const
{
    return _C(n < 0 ? -n : n, a < 0. ? -a : a, c);
}
DISPERSIONKIT_END_NAMESPACE
