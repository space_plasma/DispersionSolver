/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <DispersionKit/DispersionKit-config.h>
#include <complex>
#include <tuple>
#include <type_traits>

DISPERSIONKIT_BEGIN_NAMESPACE
/**
 @brief Plasma dispersion function for a maxwellian distribution function,
 f(x) = exp(-x^2)/√π.
 */
struct MaxwellianPDF {
    using real_type    = double;
    using complex_type = std::complex<real_type>;

    //@{
    /**
     @brief Calculate f(x).
     */
    static real_type    f(real_type const x) noexcept;
    static complex_type f(complex_type const &z) noexcept;
    //@}

    //@{
    /**
     @brief Calculate df(x)/dx.
     */
    static real_type    dfdx(real_type const x) noexcept;
    static complex_type dfdx(complex_type const &z) noexcept;
    //@}

    /**
     @brief Evaluates integral of f(x)*x^N/(x - z).
     */
    template <long N>
    static complex_type Z(complex_type const &z) noexcept { return _Z(z, std::integral_constant<long, N>{}); }

    /**
     @brief Evaluates Z<0>, Z<1> and Z<2> using the recurring algorithm.
     */
    std::tuple<complex_type, complex_type, complex_type>
    Ztriplet(complex_type const &z) const;
    /**
     @brief Evaluates W<0> and W<1> using the recurring algorithm.
     */
    std::tuple<complex_type, complex_type, complex_type>
    Wtriplet(complex_type const &z) const;

private:
    static complex_type _Z(complex_type const &z, std::integral_constant<long, 0L> const) noexcept;
    static complex_type _Z(complex_type const &z, std::integral_constant<long, 1L> const) noexcept;
    static complex_type _Z(complex_type const &z, std::integral_constant<long, 2L> const) noexcept;
    static complex_type _Z(complex_type const &z, std::integral_constant<long, 3L> const) noexcept;
};
DISPERSIONKIT_END_NAMESPACE
