/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <DispersionKit/DISPBesselJ.h>
#include <DispersionKit/DispersionKit-config.h>
#include <UtilityKit/UtilityKit.h>
#include <array>
#include <memory>
#include <unordered_map>

DISPERSIONKIT_BEGIN_NAMESPACE
/**
 @brief Evaluates integrals involving finite Larmor radius effects for an arbitrary distribution function approximated by cubic spline interpolants,
 f(x) = S_j(x) for x_j <= x < x_j+1.
 @discussion The evaluation results for a given parameter set are automatically cached.
 */
struct SplineFLR {
    using spline_coefficient_type = Utility::SplineCoefficient<double, 3>;

    SplineFLR(SplineFLR &&) = default;
    SplineFLR &operator=(SplineFLR &&) = default;
    SplineFLR(SplineFLR const &);
    SplineFLR &operator=(SplineFLR const &);

    /**
     @brief Construct by copying the instance of spline_coefficient_type.
     @discussion The min abscissa of the interpolator can be negative, but the max abscissa should be positive.
     */
    explicit SplineFLR(spline_coefficient_type const &);
    /**
     @brief Construct by moving the instance of spline_coefficient_type.
     @discussion The min abscissa of the interpolator can be negative, but the max abscissa should be positive.
     */
    explicit SplineFLR(spline_coefficient_type &&);

    /**
     @brief Clear the lookup cache.
     */
    void clear_cache();

    /**
     @brief Calculate f(x).
     @discussion f(x) is approximated using cubic spline interpolation.
     When x is out-of-bound, f(x) is assumed to be 0.
     */
    double f(double const x) const noexcept;

    /**
     @brief Calculate df(x)/dx.
     @discussion df(x)/dx is approximated using cubic spline interpolation.
     When x is out-of-bound, df(x)/dx is assumed to be 0.
     */
    double dfdx(double const x) const noexcept;

    /**
     @brief Evaluates integral of 2π*J(n, ax)^2*f(x)*x from 0 to inf.
     */
    inline double A(int const n, double const a) const;
    /**
     @brief Evaluates integral of 2π*J(n, ax)^2*f'(x) from 0 to inf.
     */
    inline double P(int const n, double const a) const;

    /**
     @brief Evaluates integral of 2π*J(n, ax)*J'(n, ax)*f(x)*x^2 from 0 to inf.
     */
    inline double B(int const n, double const a) const;
    /**
     @brief Evaluates integral of 2π*J(n, ax)*J'(n, ax)*f'(x)*x from 0 to inf.
     */
    inline double Q(int const n, double const a) const;

    /**
     @brief Evaluates integral of 2π*J'(n, ax)^2*f(x)*x^3 from 0 to inf.
     */
    inline double C(int const n, double const a) const;
    /**
     @brief Evaluates integral of 2π*J'(n, ax)^2*f'(x)*x^2 from 0 to inf.
     */
    inline double R(int const n, double const a) const;

    /**
     @brief Evaluates A(n, a)/a^2 correctly handling the case where |a| -> 0.
     @discussion It is undefined when n = a = 0.
     */
    double AOa2(int const n, double const a) const;
    /**
     @brief Evaluates P(n, a)/a^2 correctly handling the case where |a| -> 0.
     @discussion It is undefined when n = a = 0.
     */
    double POa2(int const n, double const a) const;

    /**
     @brief Evaluates B(n, a)/a correctly handling the case where |a| -> 0.
     */
    double BOa(int const n, double const a) const;
    /**
     @brief Evaluates Q(n, a)/a correctly handling the case where |a| -> 0.
     */
    double QOa(int const n, double const a) const;

    /**
     @brief Returns the instance of spline_coefficient_type.
     */
    spline_coefficient_type const &spline_coefficient() const noexcept { return _f.spline_coefficient(); }
    /**
     @brief Returns the instance of BesselJ being used to evaluate the integrals.
     @note Currently, it is BesselJ::shared().
     */
    BesselJ const &J() const noexcept { return *_J; }

private:
    template <class Spline>
    explicit SplineFLR(Spline &&, BesselJ const *J);

    using value_pack_type   = Utility::Vector<double, 6>;
    using optional_type     = Utility::Optional<value_pack_type>;
    using _InnerLookupTable = std::unordered_map<double, optional_type>;
    using _OuterLookupTable = std::array<_InnerLookupTable, std::tuple_size<BesselJ::zero_table_type>::value>;

    spline_coefficient_type::spline_interpolator_type _f;
    BesselJ const                                    *_J;
    std::unique_ptr<_OuterLookupTable>                _table;
    Utility::Vector<double, 2>                        _moments; // int_0^inf dv2*v2*{v2^2, 1}*f(v2)
    double                                            _thermal_spread;
    double                                            _zero_tolerance;

    // n >= 0 and a >= 0
    double _A(int const n, double const a) const;
    double _P(int const n, double const a) const;

    double _B(int const n, double const a) const;
    double _Q(int const n, double const a) const;

    double _C(int const n, double const a) const;
    double _R(int const n, double const a) const;

    // returns {A_n(a), P_n(a), B_n(a), Q_n(a), C_n(a), R_n(a)}
    value_pack_type const _lookup_APBQCR(int const n, double const a) const;
    value_pack_type       _eval_APBQCR(int const n, double const a) const;
};

double SplineFLR::A(int const n, double const a) const
{
    return _A(n < 0 ? -n : n, a < 0. ? -a : a);
}
double SplineFLR::P(int const n, double const a) const
{
    return _P(n < 0 ? -n : n, a < 0. ? -a : a);
}

double SplineFLR::B(int const n, double const a) const
{
    double const a_sign = a < 0. ? -1 : 1;
    return _B(n < 0 ? -n : n, a_sign * a) * a_sign;
}
double SplineFLR::Q(int const n, double const a) const
{
    double const a_sign = a < 0. ? -1 : 1;
    return _Q(n < 0 ? -n : n, a_sign * a) * a_sign;
}

double SplineFLR::C(int const n, double const a) const
{
    return _C(n < 0 ? -n : n, a < 0. ? -a : a);
}
double SplineFLR::R(int const n, double const a) const
{
    return _R(n < 0 ? -n : n, a < 0. ? -a : a);
}
DISPERSIONKIT_END_NAMESPACE
