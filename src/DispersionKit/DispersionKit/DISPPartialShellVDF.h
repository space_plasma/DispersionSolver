/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <DispersionKit/DISPSplinePDF.h>
#include <DispersionKit/DISPVDF.h>
#include <DispersionKit/DispersionKit-config.h>
//#include <DispersionKit/DISPSplineFLR.h>
#include <map>

DISPERSIONKIT_BEGIN_NAMESPACE
/**
 @brief Velocity distribution represented by a partial shell distribution.
 @discussion f = exp(-(v-vs)^2/θ^2)*(sinα)^n/(π^3/2*θ^3*Cv*Cα), where n >= 0; Cv=0.5*(vs/θ*exp(-vs^2/θ^2) + √π*(vs^2/θ^2 + 0.5)*erfc(-vs/θ)), and Cα=√π*Γ(1+n/2)/Γ(1.5+n/2).
 */
struct PartialShellVDF : public VDF {
    explicit PartialShellVDF(real_type const Oc, complex_type const &op, real_type const th)
    : PartialShellVDF(Oc, op, th, 0) {}
    explicit PartialShellVDF(real_type const Oc, complex_type const &op, real_type const th, real_type const n)
    : PartialShellVDF(Oc, op, th, n, 0.) {}
    explicit PartialShellVDF(real_type const Oc, complex_type const &op, real_type const th, real_type const n, real_type const vd)
    : PartialShellVDF(Oc, op, th, n, vd, 0.) {}
    explicit PartialShellVDF(real_type const Oc, complex_type const &op, real_type const th, real_type const n, real_type const vd, real_type const vs);

    /** @name Overrides
     *  Overrides of VDF virtual functions.
     */
    ///@{
    real_type    Oc() const override { return _Oc; }
    complex_type op() const override { return _op; }

    real_type vd() const override { return _vd; }
    real_type T1() const override;
    real_type T2() const override;

    real_type f(real_type const v1, real_type const v2) const override;
    real_type dfdv1(real_type const v1, real_type const v2) const override;
    real_type dfdv2(real_type const v1, real_type const v2) const override;

    matrix_type K(std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const override;
    ///@}

private:
    complex_type                          _op;
    real_type                             _Oc, _th, _vd, _vs, _n;
    std::map<real_type /*v2*/, SplinePDF> _pdfs;

    complex_type f(complex_type const v1, real_type const v2) const;
    complex_type dfdv1(complex_type const v1, real_type const v2) const;
    complex_type dfdv2(complex_type const v1, real_type const v2) const;

    complex_type _fv(complex_type const v) const noexcept;
    complex_type _dfvdv(complex_type const v) const noexcept;
    complex_type _fa(complex_type const sina) const noexcept;
    complex_type _dfada(complex_type const sina, complex_type const cosa) const noexcept;
};
DISPERSIONKIT_END_NAMESPACE
