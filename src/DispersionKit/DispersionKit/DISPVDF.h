/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <DispersionKit/DispersionKit-config.h>
#include <UtilityKit/UtilityKit.h>
#include <complex>
#include <memory>
#include <string>
#include <type_traits> // integral_constant
#include <utility>     // forward

DISPERSIONKIT_BEGIN_NAMESPACE
/**
 @brief Interface for a gyrotropic velocity distribution function.
 */
struct VDF {
    virtual ~VDF();

    using real_type    = double;
    using complex_type = std::complex<real_type>;
    using matrix_type  = Utility::Vector<Utility::Vector<complex_type, 3>, 3>;

    virtual std::string  description() const;
    virtual real_type    Oc() const = 0;
    virtual complex_type op() const = 0;

    // velocity moments
    virtual real_type vd() const = 0;
    virtual real_type T1() const = 0;
    virtual real_type T2() const = 0;

    virtual real_type f(real_type const v1, real_type const v2) const     = 0;
    virtual real_type dfdv1(real_type const v1, real_type const v2) const = 0;
    virtual real_type dfdv2(real_type const v1, real_type const v2) const = 0;

    virtual void clear_cache();

    virtual matrix_type K(std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const = 0;

protected:
    explicit VDF() noexcept;
    VDF(VDF const &) = delete;
    VDF &operator=(VDF const &) = delete;
};
DISPERSIONKIT_END_NAMESPACE
