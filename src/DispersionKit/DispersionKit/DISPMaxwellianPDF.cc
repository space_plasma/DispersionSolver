/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "DISPMaxwellianPDF.h"
#include "DISPSquareMatrix.h"
#include "Faddeeva.hh"
#include <cmath>

double Disp::MaxwellianPDF::f(real_type const x) noexcept
{
    return .5 * M_2_SQRTPI * std::exp(-x * x);
}
CX::Number Disp::MaxwellianPDF::f(complex_type const &z) noexcept
{
    return .5 * M_2_SQRTPI * std::exp(-z * z);
}

double Disp::MaxwellianPDF::dfdx(real_type const x) noexcept
{
    return -2. * x * f(x);
}
CX::Number Disp::MaxwellianPDF::dfdx(complex_type const &z) noexcept
{
    return -2. * z * f(z);
}

auto Disp::MaxwellianPDF::Ztriplet(complex_type const &z) const -> std::tuple<complex_type, complex_type, complex_type>
{
    complex_type const Z0 = this->Z<0>(z);
    complex_type const Z1 = z * Z0 + 1.;
    complex_type const Z2 = z * Z1;
    return std::make_tuple(Z0, Z1, Z2);
}
auto Disp::MaxwellianPDF::Wtriplet(complex_type const &z) const -> std::tuple<complex_type, complex_type, complex_type>
{
    complex_type const W0 = -2. * this->Z<1>(z);
    complex_type const W1 = z * W0;
    complex_type const W2 = z * W1 - 1.;
    return std::make_tuple(W0, W1, W2);
}

CX::Number Disp::MaxwellianPDF::_Z(const complex_type &z, const std::integral_constant<long, 0L>) noexcept
{
    return 2. * CX::I / M_2_SQRTPI * Faddeeva::w(z);
}
CX::Number Disp::MaxwellianPDF::_Z(const complex_type &z, const std::integral_constant<long, 1L>) noexcept
{
    return 1. + z * Z<0>(z);
}
CX::Number Disp::MaxwellianPDF::_Z(const complex_type &z, const std::integral_constant<long, 2L>) noexcept
{
    return z * Z<1>(z);
}
CX::Number Disp::MaxwellianPDF::_Z(const complex_type &z, const std::integral_constant<long, 3L>) noexcept
{
    return 0.5 + z * Z<2>(z);
}
