/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <DispersionKit/DISPBesselJ.h>
#include <DispersionKit/DispersionKit-config.h>
#include <UtilityKit/UtilityKit.h>
#include <array>
#include <cmath>
#include <memory>
#include <unordered_map>
#include <utility>

DISPERSIONKIT_BEGIN_NAMESPACE
namespace Bessel {
#if defined(DISPERSIONKIT_USE_GSL_BESSELI) && DISPERSIONKIT_USE_GSL_BESSELI
extern double (*_scaledIn)(int const, double const);
#else
double _scaledIn(int const n, double const x) noexcept;
#endif

namespace {
    /**
     @brief Scaled modified Bessel function of the first kind with integer index.
     @disdussion The exponential term (that is, exp(|x|)) is removed.
     The relation with the modified Bessel function, I, is
     I(n, x) = exp(|x|) * scaledI(n, x).

     Implementation from Numerical Recipe.
     */
    inline double scaledI(int const n, double const x) noexcept(noexcept(_scaledIn(n, x)))
    {
        return _scaledIn(n, x);
    }

    template <int N>
    inline double scaledI(double const x) noexcept(noexcept(_scaledIn(N, x)))
    {
        return _scaledIn(N, x);
    }
} // namespace
} // namespace Bessel

/**
 @brief Evaluate Bessel function of the second kind using a lookup table.
 */
struct BesselI {
    using real_type = double;

    /**
     @brief Evaluate the scaled Bessel function, exp(-x)*I_n(x).
     @discussion This call is thread-safe except when the receiver is being copied or moved.
     */
    inline real_type scaled(int const n, real_type const x) const;

    /**
     @brief Returns a shared, global instance.
     */
    static BesselI const &shared();

    explicit BesselI();
    BesselI(BesselI const &);
    BesselI &operator            =(BesselI const &);
    BesselI(BesselI &&) noexcept = default;
    BesselI &operator=(BesselI &&) noexcept = default;

private:
    static constexpr unsigned _n_oscillations = 10; //!< number of oscillations to divide.

    struct _AbscissaRange;
    using _Interpolator     = Utility::SplineInterpolator<real_type, 3>;
    using _InnerLookupTable = std::unordered_map<unsigned, _Interpolator>;
    using _OuterLookupTable = std::array<std::pair<Utility::SpinLock, _InnerLookupTable>, std::tuple_size<BesselJ::zero_table_type>::value>;

    std::unique_ptr<_OuterLookupTable> _table;

    real_type      _scaledI(unsigned const n, real_type const x) const; // defined for positive n and x
    _Interpolator &_interpolator(unsigned const n, _AbscissaRange const &range) const;
};

auto BesselI::scaled(const int n, const real_type x) const -> real_type
{
    unsigned const abs_n = unsigned(std::abs(n));
    int const      sign  = (x < 0.) & (abs_n & 1) ? -1 : 1;
    return _scaledI(abs_n, std::abs(x)) * sign;
}
DISPERSIONKIT_END_NAMESPACE
