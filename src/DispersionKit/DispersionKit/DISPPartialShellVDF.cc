/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "DISPPartialShellVDF.h"
#include "DISPSquareMatrix.h"
#include <cmath>
#include <stdexcept>
#include <string>

Disp::PartialShellVDF::PartialShellVDF(real_type const Oc, complex_type const &op, real_type const th, real_type const n, real_type const vd, real_type const vs)
: _op(op), _Oc(Oc), _th(th), _vd(vd), _vs(vs), _n(n)
{
    if (std::abs(op.real() * op.imag() / _Oc) > 1e-10) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - op should be either pure real or pure imaginary");
    }
    if (th <= 1e-30) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - th is not positive");
    }
    if (vs < 0.) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - vs is negative");
    }
    if (n < 0.) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - n is negative");
    }
    // TODO: on sampling, change of variable v1' = v1 - _vd.
    // TODO: Avoid sampling at v_|| = 0.

    // Sampling
    //
    {
    }
}

double Disp::PartialShellVDF::T1() const
{
    throw std::runtime_error(__PRETTY_FUNCTION__);
}
double Disp::PartialShellVDF::T2() const
{
    throw std::runtime_error(__PRETTY_FUNCTION__);
}

double Disp::PartialShellVDF::f(real_type const v1, real_type const v2) const
{
    return this->f({ v1, 0. }, v2).real();
}
double Disp::PartialShellVDF::dfdv1(real_type const v1, real_type const v2) const
{
    return this->dfdv1({ v1, 0. }, v2).real();
}
double Disp::PartialShellVDF::dfdv2(real_type const v1, real_type const v2) const
{
    return this->dfdv2({ v1, 0. }, v2).real();
}

CX::Number Disp::PartialShellVDF::f(complex_type const v1, real_type const v2) const
{
    complex_type const v    = std::sqrt(v1 * v1 + v2 * v2);
    complex_type const sina = v2 / v;
    return _fv(v) * _fa(sina) / (2. * M_PI); // invalid when v == 0
}
CX::Number Disp::PartialShellVDF::dfdv1(complex_type const v1, real_type const v2) const
{
    complex_type const v    = std::sqrt(v1 * v1 + v2 * v2);
    complex_type const sina = v2 / v, cosa = v1 / v;
    complex_type const result = cosa * _fa(sina) * _dfvdv(v) - sina / v * _fv(v) * _dfada(sina, cosa);
    return result / (2. * M_PI); // invalid when v == 0
}
CX::Number Disp::PartialShellVDF::dfdv2(complex_type const v1, real_type const v2) const
{
    complex_type const v    = std::sqrt(v1 * v1 + v2 * v2);
    complex_type const sina = v2 / v, cosa = v1 / v;
    complex_type const result = sina * _fa(sina) * _dfvdv(v) + cosa / v * _fv(v) * _dfada(sina, cosa);
    return result / (2. * M_PI);
}

namespace {
constexpr double sqrt_pi = 2. / M_2_SQRTPI;
inline double    Cv(double const vsOth) noexcept
{
    return .5 * (std::exp(-vsOth * vsOth) * vsOth + sqrt_pi * (vsOth * vsOth + 0.5) * std::erfc(-vsOth));
}
inline double Ca(double const half_n) noexcept
{
    return sqrt_pi * std::tgamma(1. + half_n) / std::tgamma(1.5 + half_n);
}
} // namespace
CX::Number Disp::PartialShellVDF::_fv(complex_type const v) const noexcept
{
    return std::exp(-std::pow((v - _vs) / _th, 2.)) / (::Cv(_vs / _th) * std::pow(_th, 3.));
}
CX::Number Disp::PartialShellVDF::_dfvdv(complex_type const v) const noexcept
{
    return _fv(v) * (-2. * (v - _vs) / (_th * _th));
}
CX::Number Disp::PartialShellVDF::_fa(complex_type const sina) const noexcept
{
    complex_type result;
    if (std::abs(_n) <= 1e-10) { // n == 0
        result = 1.;
    } else {
        result = std::pow(sina, _n);
    }
    return result /= ::Ca(.5 * _n);
}
CX::Number Disp::PartialShellVDF::_dfada(complex_type const sina, complex_type const cosa) const noexcept
{
    complex_type result;
    if (std::abs(_n) <= 1e-10) { // n == 0
        result = 0.;
    } else if (std::abs(_n - 1.) <= 1e-10) {
        result = cosa;
    } else {
        result = _n * std::pow(sina, _n - 1.) * cosa;
    }
    return result /= ::Ca(.5 * _n);
}

namespace {
// constexpr double zero_tolerance = 1e-5;
}
CX::Matrix Disp::PartialShellVDF::K(std::pair<real_type, real_type> const &, complex_type const &, int const) const
{
    throw std::runtime_error(__PRETTY_FUNCTION__);
}
