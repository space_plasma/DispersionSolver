/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "DISPProductBiLorentzianVDF.h"
#include "DISPIntegerKappaPDF.h"
#include "DISPSplineKappaPDF.h"
#include "DISPSquareMatrix.h"
#include <cmath>
#include <stdexcept>
#include <string>

// MARK: PDFWrapper
//
struct Disp::ProductBiLorentzianVDF::_PDFWrapper {
    virtual ~_PDFWrapper() {}
    virtual bool                                           is_integral() const                 = 0;
    virtual double                                         f(double const x) const             = 0;
    virtual double                                         dfdx(double const x) const          = 0;
    virtual std::tuple<CX::Number, CX::Number, CX::Number> Ztriplet(CX::Number const &z) const = 0;
    virtual std::tuple<CX::Number, CX::Number, CX::Number> Wtriplet(CX::Number const &z) const = 0;

protected:
    _PDFWrapper() {}
    _PDFWrapper(_PDFWrapper const &) = delete;
    _PDFWrapper &operator=(_PDFWrapper const &) = delete;
};
namespace {
struct IntegerKappaPDF : public Disp::ProductBiLorentzianVDF::_PDFWrapper {
    Disp::IntegerKappaPDF pdf;
    explicit IntegerKappaPDF(int const kappa)
    : pdf(kappa) {}
    bool                                           is_integral() const override { return true; }
    double                                         f(double const x) const override { return pdf.f(x); }
    double                                         dfdx(double const x) const override { return pdf.dfdx(x); }
    std::tuple<CX::Number, CX::Number, CX::Number> Ztriplet(CX::Number const &z) const override { return pdf.Ztriplet(z); }
    std::tuple<CX::Number, CX::Number, CX::Number> Wtriplet(CX::Number const &z) const override { return pdf.Wtriplet(z); }
};
struct RealKappaPDF : public Disp::ProductBiLorentzianVDF::_PDFWrapper {
    Disp::SplineKappaPDF pdf;
    explicit RealKappaPDF(double const kappa)
    : pdf(kappa) {}
    bool                                           is_integral() const override { return false; }
    double                                         f(double const x) const override { return pdf.f(x); }
    double                                         dfdx(double const x) const override { return pdf.dfdx(x); }
    std::tuple<CX::Number, CX::Number, CX::Number> Ztriplet(CX::Number const &z) const override { return pdf.Ztriplet(z); }
    std::tuple<CX::Number, CX::Number, CX::Number> Wtriplet(CX::Number const &z) const override { return pdf.Wtriplet(z); }
};
} // namespace

// MARK:- ProductBiLorentzianVDF
//
Disp::ProductBiLorentzianVDF::~ProductBiLorentzianVDF()
{
}
Disp::ProductBiLorentzianVDF::ProductBiLorentzianVDF(real_type const Oc, complex_type const &op, std::pair<real_type, real_type> const kappa, real_type const th1, real_type const th2, real_type const vd)
: VDF(), _op(op), _Oc(Oc), _th1(th1), _th2(th2), _vd(vd), _kappa(kappa), _pdf(), _flr(kappa.second)
{
    if (std::abs(op.real() * op.imag() / _Oc) > 1e-10) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - op should be either pure real or pure imaginary");
    }
    if (th1 < 0. || th2 <= 0.) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - either th1 or th2 or both is negative");
    }
    int const round_k1 = int(std::round(kappa.first));
    if (std::abs(kappa.first - round_k1) < 1e-10) { // integral value
        _pdf.reset(new ::IntegerKappaPDF(round_k1));
    } else {
        _pdf.reset(new ::RealKappaPDF(kappa.first));
    }
}

std::string Disp::ProductBiLorentzianVDF::description() const
{
    std::ostringstream os;
    printo(os, typeid(*this).name(), "[Oc->", Oc(), ", op->", op(), ", th1->", _th1, ", th2->", _th2, ", vd->", vd(), ", kappa->{", _kappa.first, ", ", _kappa.second, "}", "]");
    return os.str();
}

double Disp::ProductBiLorentzianVDF::T1() const
{
    return _kappa.first / (2. * _kappa.first - 1) * _th1 * _th1;
}
double Disp::ProductBiLorentzianVDF::T2() const
{
    return _kappa.second / (2. * _kappa.second - 2) * _th2 * _th2;
}

double Disp::ProductBiLorentzianVDF::f(real_type const v1, real_type const v2) const
{
    real_type const x1 = (v1 - _vd) / _th1;
    real_type const x2 = v2 / _th2;
    real_type       f  = _pdf->f(x1) * _flr.f(x2);
    return f /= _th1 * _th2 * _th2;
}
double Disp::ProductBiLorentzianVDF::dfdv1(real_type const v1, real_type const v2) const
{
    real_type const x1 = (v1 - _vd) / _th1;
    real_type const x2 = v2 / _th2;
    real_type       df = _pdf->dfdx(x1) * _flr.f(x2);
    return df /= _th1 * _th1 * _th2 * _th2;
}
double Disp::ProductBiLorentzianVDF::dfdv2(real_type const v1, real_type const v2) const
{
    real_type const x1 = (v1 - _vd) / _th1;
    real_type const x2 = v2 / _th2;
    real_type       df = _pdf->f(x1) * _flr.dfdx(x2);
    return df /= _th1 * _th2 * _th2 * _th2;
}

void Disp::ProductBiLorentzianVDF::clear_cache()
{
    _flr.clear_cache();
}

namespace {
constexpr double zero_tolerance = 1e-5;
}
CX::Matrix Disp::ProductBiLorentzianVDF::K(std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const
{
    CX::Matrix         K{};
    real_type const    k1th1      = k.first * _th1;
    complex_type const o_jOc_k1vd = o - j * _Oc - k.first * _vd;
    if (std::abs(k1th1) < std::abs(o_jOc_k1vd * ::zero_tolerance)) {
        // asymptotic version
        K11(::get<0, 0>(K), k, o, j);
        K12(::get<0, 1>(K), k, o, j);
        K13(::get<0, 2>(K), k, o, j);

        ::get<1, 0>(K) = -::get<0, 1>(K);
        K22(::get<1, 1>(K), k, o, j);
        K23(::get<1, 2>(K), k, o, j);

        K31(::get<2, 0>(K), k, o, j);
        K32(::get<2, 1>(K), k, o, j);
        K33(::get<2, 2>(K), k, o, j);
    } else {
        // normal version
        triplet_type const &Z = _pdf->Ztriplet(o_jOc_k1vd / k1th1);
        triplet_type const &W = _pdf->Wtriplet(o_jOc_k1vd / k1th1);
        K11(::get<0, 0>(K), k, o, j, Z, W);
        K12(::get<0, 1>(K), k, o, j, Z, W);
        K13(::get<0, 2>(K), k, o, j, Z, W);

        ::get<1, 0>(K) = -::get<0, 1>(K);
        K22(::get<1, 1>(K), k, o, j, Z, W);
        K23(::get<1, 2>(K), k, o, j, Z, W);

        K31(::get<2, 0>(K), k, o, j, Z, W);
        K32(::get<2, 1>(K), k, o, j, Z, W);
        K33(::get<2, 2>(K), k, o, j, Z, W);
    }
    return K;
}

// MARK:- K Elements
void Disp::ProductBiLorentzianVDF::K11(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const
{
    if (0 == j)
        return;
    real_type const    k1 = k.first, k2 = k.second;
    real_type const    a          = k2 / _Oc;
    complex_type const o_k1vd     = o - k1 * _vd;
    complex_type const o_jOc_k1vd = o_k1vd - j * _Oc;
    // asymptotic version
    K += std::pow(k1 / o_jOc_k1vd, 2.) * this->AOa2(j, a);
    K -= o_k1vd / o_jOc_k1vd * this->POa2(j, a);
    K *= real_type(-j) * j;
}
void Disp::ProductBiLorentzianVDF::K11(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j, triplet_type const &Z, triplet_type const &W) const
{
    if (0 == j)
        return;
    real_type const    k1 = k.first, k2 = k.second;
    real_type const    a = k2 / _Oc, k1th1 = k1 * _th1;
    complex_type const o_k1vd = o - k1 * _vd;
    // normal version
    K -= std::get<0>(W) / (_th1 * _th1) * this->AOa2(j, a);
    K += (std::get<1>(Z) - o_k1vd / k1th1 * std::get<0>(Z)) * this->POa2(j, a);
    K *= real_type(j) * j;
}

void Disp::ProductBiLorentzianVDF::K12(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const
{
    if (0 == j)
        return;
    real_type const    k1 = k.first, k2 = k.second;
    real_type const    a          = k2 / _Oc;
    complex_type const o_k1vd     = o - k1 * _vd;
    complex_type const o_jOc_k1vd = o_k1vd - j * _Oc;
    // asymptotic version
    K += std::pow(k1 / o_jOc_k1vd, 2.) * this->BOa(j, a);
    K -= o_k1vd / o_jOc_k1vd * this->QOa(j, a);
    K *= real_type(-j) * CX::I;
}
void Disp::ProductBiLorentzianVDF::K12(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j, triplet_type const &Z, triplet_type const &W) const
{
    if (0 == j)
        return;
    real_type const    k1 = k.first, k2 = k.second;
    real_type const    a = k2 / _Oc, k1th1 = k1 * _th1;
    complex_type const o_k1vd = o - k1 * _vd;
    // normal version
    K -= std::get<0>(W) / (_th1 * _th1) * this->BOa(j, a);
    K += (std::get<1>(Z) - o_k1vd / k1th1 * std::get<0>(Z)) * this->QOa(j, a);
    K *= real_type(j) * CX::I;
}

void Disp::ProductBiLorentzianVDF::K13(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const
{
    if (0 == j)
        return;
    real_type const    k1 = k.first, k2 = k.second;
    real_type const    a          = k2 / _Oc;
    complex_type const o_k1vd     = o - k1 * _vd;
    complex_type const o_jOc_k1vd = o_k1vd - j * _Oc;
    // asymptotic version
    K += a * (o - j * _Oc) * k1 / (o_jOc_k1vd * o_jOc_k1vd) * this->AOa2(j, a);
    K -= j * k2 * _vd / o_jOc_k1vd * this->POa2(j, a);
    K *= real_type(-j);
}
void Disp::ProductBiLorentzianVDF::K13(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j, triplet_type const &Z, triplet_type const &W) const
{
    if (0 == j)
        return;
    real_type const    k1 = k.first, k2 = k.second;
    real_type const    a = k2 / _Oc, k1th1 = k1 * _th1;
    complex_type const zjOth1 = (o - j * _Oc) / k1th1;
    // normal version
    K -= a * zjOth1 / _th1 * std::get<0>(W) * this->AOa2(j, a);
    K -= j * k2 / k1 * (std::get<1>(Z) + _vd / _th1 * std::get<0>(Z)) * this->POa2(j, a);
    K *= real_type(j);
}

void Disp::ProductBiLorentzianVDF::K22(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const
{
    real_type const    k1 = k.first, k2 = k.second;
    real_type const    a          = k2 / _Oc;
    complex_type const o_k1vd     = o - k1 * _vd;
    complex_type const o_jOc_k1vd = o_k1vd - j * _Oc;
    // asymptotic version
    K += std::pow(k1 / o_jOc_k1vd, 2.) * this->C(j, a);
    K -= o_k1vd / o_jOc_k1vd * this->R(j, a);
    K *= -1.;
}
void Disp::ProductBiLorentzianVDF::K22(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j, triplet_type const &Z, triplet_type const &W) const
{
    real_type const    k1 = k.first, k2 = k.second;
    real_type const    a = k2 / _Oc, k1th1 = k1 * _th1;
    complex_type const o_k1vd = o - k1 * _vd;
    // normal version
    K -= std::get<0>(W) / (_th1 * _th1) * this->C(j, a);
    K += (std::get<1>(Z) - o_k1vd / k1th1 * std::get<0>(Z)) * this->R(j, a);
    K *= 1.;
}

void Disp::ProductBiLorentzianVDF::K23(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const
{
    real_type const    k1 = k.first, k2 = k.second;
    real_type const    a          = k2 / _Oc;
    complex_type const o_k1vd     = o - k1 * _vd;
    complex_type const o_jOc_k1vd = o_k1vd - j * _Oc;
    // asymptotic version
    K += (o - j * _Oc) * k1 / (o_jOc_k1vd * o_jOc_k1vd) * this->B(j, a);
    K -= j ? j * k2 * _vd / o_jOc_k1vd * this->QOa(j, a) : 0.;
    K *= CX::I;
}
void Disp::ProductBiLorentzianVDF::K23(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j, triplet_type const &Z, triplet_type const &W) const
{
    real_type const    k1 = k.first, k2 = k.second;
    real_type const    a = k2 / _Oc, k1th1 = k1 * _th1;
    complex_type const zjOth1 = (o - j * _Oc) / k1th1;
    // normal version
    K += zjOth1 / _th1 * std::get<0>(W) * this->B(j, a);
    K += j ? j * k2 / k1 * (std::get<1>(Z) + _vd / _th1 * std::get<0>(Z)) * this->QOa(j, a) : 0.;
    K *= CX::I;
}

void Disp::ProductBiLorentzianVDF::K31(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const
{
    if (0 == j)
        return;
    real_type const    k1 = k.first, k2 = k.second;
    real_type const    a          = k2 / _Oc;
    complex_type const o_k1vd     = o - k1 * _vd;
    complex_type const o_jOc_k1vd = o_k1vd - j * _Oc;
    // asymptotic version
    K += (o - j * _Oc) * k1 / (o_jOc_k1vd * o_jOc_k1vd) * this->AOa2(j, a);
    K -= _vd * o_k1vd / o_jOc_k1vd * this->POa2(j, a);
    K *= -j * a;
}
void Disp::ProductBiLorentzianVDF::K31(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j, triplet_type const &Z, triplet_type const &W) const
{
    if (0 == j)
        return;
    real_type const    k1 = k.first, k2 = k.second;
    real_type const    a = k2 / _Oc, k1th1 = k1 * _th1;
    complex_type const o_k1vd = o - k1 * _vd;
    // normal version
    K -= (std::get<1>(W) + _vd / _th1 * std::get<0>(W)) / _th1 * this->AOa2(j, a);
    K += (_th1 * std::get<2>(Z) + (2. * _vd - o / k1) * std::get<1>(Z) - o_k1vd * _vd / k1th1 * std::get<0>(Z)) * this->POa2(j, a);
    K *= j * a;
}

void Disp::ProductBiLorentzianVDF::K32(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const
{
    real_type const    k1 = k.first, k2 = k.second;
    real_type const    a          = k2 / _Oc;
    complex_type const o_k1vd     = o - k1 * _vd;
    complex_type const o_jOc_k1vd = o_k1vd - j * _Oc;
    // asymptotic version
    K += (o - j * _Oc) * k1 / (o_jOc_k1vd * o_jOc_k1vd) * this->B(j, a);
    K -= a * _vd * o_k1vd / o_jOc_k1vd * this->QOa(j, a);
    K *= -CX::I;
}
void Disp::ProductBiLorentzianVDF::K32(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j, triplet_type const &Z, triplet_type const &W) const
{
    real_type const    k1 = k.first, k2 = k.second;
    real_type const    a = k2 / _Oc, k1th1 = k1 * _th1;
    complex_type const o_k1vd = o - k1 * _vd;
    // normal version
    K -= (std::get<1>(W) + _vd / _th1 * std::get<0>(W)) / _th1 * this->B(j, a);
    K += a * (_th1 * std::get<2>(Z) + (2. * _vd - o / k1) * std::get<1>(Z) - o_k1vd * _vd / k1th1 * std::get<0>(Z)) * this->QOa(j, a);
    K *= CX::I;
}

void Disp::ProductBiLorentzianVDF::K33(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const
{
    real_type const    k1 = k.first, k2 = k.second;
    real_type const    a          = k2 / _Oc;
    complex_type const o_k1vd     = o - k1 * _vd;
    complex_type const o_jOc_k1vd = o_k1vd - j * _Oc;
    // asymptotic version
    K += std::pow((o - j * _Oc) / o_jOc_k1vd, 2.) * this->A(j, a);
    K -= j ? j * _Oc * a * a / o_jOc_k1vd * (_kappa.first / (2. * _kappa.first - 1) * _th1 * _th1 + _vd * _vd) * this->POa2(j, a) : 0.;
    K *= -1.;
}
void Disp::ProductBiLorentzianVDF::K33(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j, triplet_type const &Z, triplet_type const &W) const
{
    real_type const    k1 = k.first, k2 = k.second;
    real_type const    a = k2 / _Oc, k1th1 = k1 * _th1;
    complex_type const zjOth1 = (o - j * _Oc) / k1th1;
    // normal version
    K -= zjOth1 * (std::get<1>(W) + _vd / _th1 * std::get<0>(W)) * this->A(j, a);
    K -= j ? j * _Oc * a * a * (_th1 / k1 * std::get<2>(Z) + 2. * _vd / k1 * std::get<1>(Z) + _vd * _vd / k1th1 * std::get<0>(Z)) * this->POa2(j, a) : 0.;
    K *= 1.;
}
