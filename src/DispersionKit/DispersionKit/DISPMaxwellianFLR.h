/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <DispersionKit/DISPBesselI.h>
#include <DispersionKit/DispersionKit-config.h>

DISPERSIONKIT_BEGIN_NAMESPACE
/**
 @brief Evaluates integrals involving finite Larmor radius effects for a maxwellian distribution function,
 f(x) = exp(-x^2)/π.
 */
struct MaxwellianFLR {
    /**
     @brief Calculate f(x).
     */
    static double f(double const x) noexcept;

    /**
     @brief Calculate df(x)/dx.
     */
    static double dfdx(double const x) noexcept;

    /**
     @brief Evaluates integral of 2π*x*J(n, ax)^2*f(x) from 0 to inf.
     @discussion The closed form is exp(-a^2/2)*I(n, a^2/2).
     */
    static double A(int const n, double const a);
    /**
     @brief Evaluates integral of 2π*x^2*J(n, ax)*Jn'(ax)*f(x) from 0 to inf.
     @discussion The closed form is a/2*exp(-a^2/2)*(I'(n, a^2/2) - I(n, a^2/2)).
     */
    static double B(int const n, double const a);
    /**
     @brief Evaluates integral of 2π*x^3*Jn'(ax)^2*f(x) from 0 to inf.
     @discussion The closed form is n^2/a^2*A(n, a) - a*B(n, a).
     */
    static double C(int const n, double const a);

    /**
     @brief Evaluates A(n, a)/a^2 correctly handling the case where |a| -> 0.
     @discussion It is undefined when n = 0.
     */
    static double AOa2(int const n, double const a);
    /**
     @brief Evaluates B(n, a)/a correctly handling the case where |a| -> 0.
     */
    static double BOa(int const n, double const a);

    //@{
    /** Parity to SplineFLR's methods. */
    static double P(int const n, double const a) { return -2. * A(n, a); }
    static double Q(int const n, double const a) { return -2. * B(n, a); }
    static double R(int const n, double const a) { return -2. * C(n, a); }

    static double POa2(int const n, double const a) { return -2. * AOa2(n, a); }
    static double QOa(int const n, double const a) { return -2. * BOa(n, a); }
    //@}
};

/**
 @brief Evaluates integrals involving finite Larmor radius effects for a maxwellian distribution function,
 f(x) = exp(-x^2)/π.
 */
struct InterpMaxwellianFLR {
    InterpMaxwellianFLR(InterpMaxwellianFLR &&) noexcept = default;
    InterpMaxwellianFLR &operator=(InterpMaxwellianFLR &&) noexcept = default;
    InterpMaxwellianFLR(InterpMaxwellianFLR const &) noexcept       = default;
    InterpMaxwellianFLR &operator=(InterpMaxwellianFLR const &) noexcept = default;

    ~InterpMaxwellianFLR() = default;
    explicit InterpMaxwellianFLR();

    /**
     @brief Calculate f(x).
     */
    static double f(double const x) noexcept { return MaxwellianFLR::f(x); }

    /**
     @brief Calculate df(x)/dx.
     */
    static double dfdx(double const x) noexcept { return MaxwellianFLR::dfdx(x); }

    /**
     @brief Evaluates integral of 2π*x*J(n, ax)^2*f(x) from 0 to inf.
     @discussion The closed form is exp(-a^2/2)*I(n, a^2/2).
     */
    double A(int const n, double const a) const;
    /**
     @brief Evaluates integral of 2π*x^2*J(n, ax)*Jn'(ax)*f(x) from 0 to inf.
     @discussion The closed form is a/2*exp(-a^2/2)*(I'(n, a^2/2) - I(n, a^2/2)).
     */
    double B(int const n, double const a) const;
    /**
     @brief Evaluates integral of 2π*x^3*Jn'(ax)^2*f(x) from 0 to inf.
     @discussion The closed form is n^2/a^2*A(n, a) - a*B(n, a).
     */
    double C(int const n, double const a) const;

    /**
     @brief Evaluates A(n, a)/a^2 correctly handling the case where |a| -> 0.
     @discussion It is undefined when n = 0.
     */
    double AOa2(int const n, double const a) const;
    /**
     @brief Evaluates B(n, a)/a correctly handling the case where |a| -> 0.
     */
    double BOa(int const n, double const a) const;

    //@{
    /** Parity to SplineFLR's methods. */
    double P(int const n, double const a) const { return -2. * A(n, a); }
    double Q(int const n, double const a) const { return -2. * B(n, a); }
    double R(int const n, double const a) const { return -2. * C(n, a); }

    double POa2(int const n, double const a) const { return -2. * AOa2(n, a); }
    double QOa(int const n, double const a) const { return -2. * BOa(n, a); }
    //@}

    /**
     @brief Returns the instance of BesselI being used to evaluate the integrals.
     @note Currently, it is BesselI::shared().
     */
    BesselI const &I() const noexcept { return *_I; }

private:
    InterpMaxwellianFLR(BesselI const *I)
    : _I(I) {}
    BesselI const *_I;

    double _scaledIp(int const n, double const x) const;
};
DISPERSIONKIT_END_NAMESPACE
