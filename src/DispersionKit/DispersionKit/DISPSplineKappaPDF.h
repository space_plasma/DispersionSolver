/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <DispersionKit/DISPSplinePDF.h>
#include <DispersionKit/DispersionKit-config.h>
#include <complex>
#include <memory>
#include <tuple>
#include <type_traits>

DISPERSIONKIT_BEGIN_NAMESPACE
/**
 @brief Plasma dispersion function for a kappa (generalized Lorentzian) distribution function,
 f(x) = √κΓ(κ)/Γ(κ+1/2)*(1+x^2/κ)^-(κ+1)/√π, where κ > 1/2.
 @discussion This version uses SplinePDF.
 */
struct SplineKappaPDF {
    using real_type    = double;
    using complex_type = std::complex<real_type>;

    //@{
    /**
     @brief Calculate f(x).
     */
    real_type    f(real_type const x) const noexcept;
    complex_type f(complex_type const &z) const noexcept;
    //@}

    //@{
    /**
     @brief Calculate df(x)/dx.
     */
    real_type    dfdx(real_type const x) const noexcept;
    complex_type dfdx(complex_type const &z) const noexcept;
    //@}

    /**
     @brief Evaluates integral of f(x)*x^N/(x - z).
     */
    template <long N>
    inline complex_type Z(complex_type const &z) const;
    /**
     @brief Evaluates integral of df(x)*x^N/(x - z).
     */
    template <long N>
    inline complex_type W(complex_type const &z) const;

    /**
     @brief Evaluates Z<0>, Z<1> and Z<2> using the recurring algorithm.
     */
    std::tuple<complex_type, complex_type, complex_type>
    Ztriplet(complex_type const &z) const;
    /**
     @brief Evaluates W<0>, W<1> and W<2> using the recurring algorithm.
     */
    std::tuple<complex_type, complex_type, complex_type>
    Wtriplet(complex_type const &z) const;

    /**
     @brief Returns the kappa index.
     */
    real_type kappa() const noexcept { return _k; }

    /**
     @brief Construct kappa PDF with a real-valued kappa index.
     @exception An std::invalid_argument exception if kappa <= 0.5.
     */
    explicit SplineKappaPDF(real_type const kappa);

    // copy/move:
    SplineKappaPDF(SplineKappaPDF const &);
    SplineKappaPDF &operator          =(SplineKappaPDF const &);
    SplineKappaPDF(SplineKappaPDF &&) = default;
    SplineKappaPDF &operator=(SplineKappaPDF &&) = default;

private:
    real_type                  _k;
    std::unique_ptr<SplinePDF> _pdf;

    constexpr static real_type _delta   = 0.1;   //!< Sampling interval.
    constexpr static real_type _epsilon = 1e-10; //!< Determines the maximum x to sample.
};

template <long N>
auto SplineKappaPDF::Z(complex_type const &z) const -> complex_type
{
    constexpr complex_type _2piI{ 0., 2. * M_PI };
    if (z.imag() < 0.) {
        return _pdf->Z<N>(z, false) + _2piI * this->f(z) * (N ? std::pow(z, N) : complex_type{ 1. });
    } else {
        return _pdf->Z<N>(z, false);
    }
}
template <long N>
auto SplineKappaPDF::W(complex_type const &z) const -> complex_type
{
    constexpr complex_type _2piI{ 0., 2. * M_PI };
    if (z.imag() < 0.) {
        return _pdf->W<N>(z, false) + _2piI * this->dfdx(z) * (N ? std::pow(z, N) : complex_type{ 1. });
    } else {
        return _pdf->W<N>(z, false);
    }
}
DISPERSIONKIT_END_NAMESPACE
