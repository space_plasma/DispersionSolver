/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

// root namespace
//
#ifndef DISPERSIONKIT_NAMESPACE
#define DISPERSIONKIT_NAMESPACE       Disp
#define DISPERSIONKIT_BEGIN_NAMESPACE namespace DISPERSIONKIT_NAMESPACE {
#define DISPERSIONKIT_END_NAMESPACE   }
#endif

// GSL BesselI
//
#ifndef DISPERSIONKIT_USE_GSL_BESSELI
#define DISPERSIONKIT_USE_GSL_BESSELI 1
#endif
