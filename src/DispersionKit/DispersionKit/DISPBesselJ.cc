/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "DISPBesselJ.h"
#include <pthread.h>
#include <stdexcept>
#include <string>

// MARK: Bessel::_Jn
//
Disp::Bessel::real_type Disp::Bessel::_Jn(const int n, const Disp::Bessel::real_type x) noexcept
{
    return jn(n, x);
}

// MARK: BesselJZeros
//
namespace {
using ZeroTable = Disp::BesselJ::zero_table_type;
ZeroTable _zeros;

struct JnOdJn {
    unsigned *n;
    double    operator()(double x) const
    {
        return 2 * Disp::Bessel::J(int(*n), x) / (Disp::Bessel::J(int(*n) - 1, x) - Disp::Bessel::J(int(*n) + 1, x));
    }
};
void buildBesselJZeros()
{
    unsigned                          n;
    Utility::NewtonRootFinder<JnOdJn> root({ &n });
    root.max_iterations = 10;
    root.reltol         = 1e-4;

    _zeros.at(0) = { 2.404825557695773, 5.5200781102863115 };
    for (n = 1; n < std::tuple_size<ZeroTable>::value; ++n) {
        ZeroTable::value_type const &x0 = _zeros[n - 1];
        ZeroTable::value_type       &x1 = _zeros[n];

        // first zero
        //
        decltype(root)::optional_type x = root(x0.first + M_PI_2);
        if (!x) {
            throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - failed to find the first zero");
        } else {
            x1.first = *x;
        }

        // second zero
        //
        x = root(x0.second + M_PI_2);
        if (!x) {
            throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - failed to find the second zero");
        } else {
            x1.second = *x;
        }
    }
}
ZeroTable const &BesselJZeros()
{
    static pthread_once_t once = PTHREAD_ONCE_INIT;
    if (pthread_once(&once, &buildBesselJZeros)) {
        throw std::runtime_error(__PRETTY_FUNCTION__);
    }
    return _zeros;
}
} // namespace

// MARK: BesselJ::_AbscissaRange
//
struct Disp::BesselJ::_AbscissaRange {
    static real_type offset(unsigned const n) noexcept
    {
        static zero_table_type const &zeros = BesselJ::zeros();
        return zeros[n].second; // start from the second zero
    }

    real_type a, b;
    unsigned  key;

    _AbscissaRange(unsigned const n, real_type const x) noexcept
    {
        real_type const x0 = offset(n);
        if (x < x0) {
            key = 0;
            a   = 0.;
            b   = x0;
        } else {
            constexpr real_type interval = 2. * M_PI * _n_oscillations;

            key = static_cast<unsigned>((x - x0) / interval) + 1;
            a   = x0 + (key - 1) * interval;
            b   = x0 + (key)*interval;
        }
    }
};

// MARK: BesselJ
//
auto Disp::BesselJ::zeros() -> zero_table_type const &
{
    return ::BesselJZeros();
}

auto Disp::BesselJ::shared() -> BesselJ const &
{
    static Disp::BesselJ const *shared;
    static pthread_once_t       once = PTHREAD_ONCE_INIT;
    if (pthread_once(&once, []() {
            shared = new BesselJ;
        })) {
        throw std::runtime_error(__PRETTY_FUNCTION__);
    }
    return *shared;
}

Disp::BesselJ::BesselJ()
: _table(new _OuterLookupTable)
{
}
Disp::BesselJ::BesselJ(BesselJ const &o)
: BesselJ()
{
    for (unsigned long i = 0; this->_table->size(); ++i) {
        // note that mutex lock cannot be copied
        (*this->_table)[i].second = (*o._table)[i].second;
    }
}
auto Disp::BesselJ::operator=(BesselJ const &o) -> BesselJ &
{
    if (this != &o) {
        BesselJ tmp(o);
        *this = std::move(tmp);
    }
    return *this;
}

namespace {
double large_argument_scale_factor(unsigned const n) noexcept
{
    constexpr double   scale_min = 5, scale_max = 100;
    constexpr unsigned n_max = 500;
    if (n == 0) {
        return 200.;
    } else if (n > n_max) {
        return scale_max;
    } else {
        return std::log(n) / std::log(n_max) * (scale_min - scale_max) + scale_max;
    }
}
inline double J_large_argument_expansion(unsigned const n, double const x) noexcept
{
    return M_2_SQRTPI * M_SQRT1_2 * std::sqrt(1. / x) * std::cos(M_PI_4 + n * M_PI_2 - x)
         + (((4. * n) * n - 1.) * std::pow(1. / x, 1.5) * std::sin(M_PI_4 + n * M_PI_2 - x)) / (4. * std::sqrt(2. * M_PI));
}
} // namespace
auto Disp::BesselJ::_J(const unsigned n, const real_type x) const -> real_type
{
#if DEBUG
    if (x < 0.) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - x < 0");
    }
#endif
    if (x >= std::abs(real_type(n) * n - .25) * ::large_argument_scale_factor(n)) { // large argument expansion
        return ::J_large_argument_expansion(n, x);
    } else {
        if (n < _min_n) { // for small n, normal evaluation
            return Bessel::J(int(n), x);
        } else if (n >= std::tuple_size<zero_table_type>::value) { // for too large n
            return Bessel::J(int(n), x);
        } else {
            return *_interpolator(n, { n, x })(x);
        }
    }
}

namespace {
using Sampler = Utility::AdaptiveSampling1D<double>;
Sampler _sampler;
void    initSampler()
{
    _sampler.maxRecursion  = 50;
    _sampler.accuracyGoal  = 7;
    _sampler.initialPoints = 40;
}
Sampler const &sampler()
{
    static pthread_once_t once = PTHREAD_ONCE_INIT;
    if (pthread_once(&once, &initSampler)) {
        throw std::runtime_error(__PRETTY_FUNCTION__);
    }
    return _sampler;
}
} // namespace
auto Disp::BesselJ::_interpolator(unsigned const n, _AbscissaRange const &range) const -> _Interpolator &
{
    // FIXME: This locking is a bottle neck. A better solution is needed.
    Utility::LockG<Utility::SpinLock> l((*_table)[n].first); // the lock is unique in index n, but not in abscissa range

    _Interpolator &interp = (*_table)[n].second[range.key]; // after lock guard, populate interpolator if not exists
    if (!interp) {
        zero_pair_type const &zero    = this->zeros()[n];
        ::Sampler             sampler = ::sampler(); // copy sampler to dynamically determine initialPoints for the first interval of J
        if (range.key == 0)
            sampler.initialPoints = static_cast<unsigned>(zero.second / (zero.second - zero.first)) + 1;

        ::Sampler::point_list_type const &points = sampler(
            [n](real_type const x) -> real_type {
                return Bessel::J(int(n), x);
            },
            range.a, range.b, .1);

        interp = Utility::CubicSplineCoefficient<real_type>(points.begin(), points.end()).interpolator();
    }
    return interp;
}
