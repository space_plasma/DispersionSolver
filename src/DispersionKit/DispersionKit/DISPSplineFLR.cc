/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "DISPSplineFLR.h"
#include <cmath>
#include <limits>
#include <pthread.h>
#include <stdexcept>
#include <string>
#include <utility>

Disp::SplineFLR::SplineFLR(SplineFLR const &o)
: SplineFLR(o.spline_coefficient(), o._J)
{
    *_table = *o._table;
}
auto Disp::SplineFLR::operator=(SplineFLR const &o) -> SplineFLR &
{
    if (this != &o) {
        SplineFLR tmp(o); // copy through a temporary copied object to reduce clutter of copying all member variables
        *this = std::move(tmp);
    }
    return *this;
}

Disp::SplineFLR::SplineFLR(spline_coefficient_type const &s)
: SplineFLR(s, &BesselJ::shared())
{
}
Disp::SplineFLR::SplineFLR(spline_coefficient_type &&s)
: SplineFLR(std::move(s), &BesselJ::shared())
{
}

namespace {
using _Vec2 = Utility::Vector<double, 2>; // int_0^inf dv2*v2*{v2^2, 1}*f(v2)
inline std::pair<_Vec2, double> moments_helper(Utility::SplineInterpolator<double, 3> const &f)
{
    using _Vec4 = Utility::Vector<double, 4>; // {<v2^3>, <v2^2>, <v2>, <1>}
    Utility::TrapzIntegrator<double, _Vec4> integrator;
    integrator.accuracyGoal = 7;
    integrator.maxRecursion = 20;
    integrator.minRecursion = 5;
    _Vec4 &&vec
        = integrator
              .qsimp(
                  [&f](double const v2) -> _Vec4 {
                      _Vec4 vec{ *f(v2) };
                      std::get<0>(vec) *= v2 * v2 * v2;
                      std::get<1>(vec) *= v2 * v2;
                      std::get<2>(vec) *= v2;
                      return vec;
                  },
                  f.min_abscissa() < 0. ? 0. : f.min_abscissa(),
                  f.max_abscissa(),
                  [](_Vec4 const &y) -> double {
                      return std::abs(std::get<0>(y));
                  })
              .first;
    // moments
    _Vec2 const mom{ std::get<0>(vec), std::get<2>(vec) };
    // thermal spread
    vec /= std::get<3>(vec);                                 // normalize
    std::get<1>(vec) -= std::get<2>(vec) * std::get<2>(vec); // thermal^2/2
    return { mom, std::sqrt(std::get<1>(vec) * 2) };
}
} // namespace
template <class Spline>
Disp::SplineFLR::SplineFLR(Spline &&s, BesselJ const *J)
: _f(), _J(J), _table(new _OuterLookupTable), _moments(), _thermal_spread(std::numeric_limits<double>::quiet_NaN()), _zero_tolerance(1e-5)
{
    if (!s) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - invalid spline coefficient instance");
    } else if (s.max_abscissa() <= 0.) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - max abscissa of spline less than or equal to 0");
    }
    _f                                  = std::forward<Spline>(s).interpolator();
    std::tie(_moments, _thermal_spread) = ::moments_helper(_f);
    _zero_tolerance /= _thermal_spread; // scale it by thermal speed
}

double Disp::SplineFLR::f(double const x) const noexcept
{
    return (x < _f.min_abscissa() || x > _f.max_abscissa()) ? 0. : *_f.interpolate(x);
}
double Disp::SplineFLR::dfdx(double const x) const noexcept
{
    return (x < _f.min_abscissa() || x > _f.max_abscissa()) ? 0. : *_f.derivative(x);
}

void Disp::SplineFLR::clear_cache()
{
    for (auto &map : *_table) {
        map.clear();
    }
}

double Disp::SplineFLR::AOa2(int const n, double const a) const
{
    if (std::abs(a) < _zero_tolerance) { // small argument expansion
        if (n == 0) {
            return std::numeric_limits<double>::quiet_NaN();
        } else if ((n == 1) | (n == -1)) {
            return M_PI_2 * std::get<0>(_moments);
        } else {
            return 0.;
        }
    } else { // normal division
        return this->A(n, a) / (a * a);
    }
}
double Disp::SplineFLR::POa2(int const n, double const a) const
{
    if (std::abs(a) < _zero_tolerance) { // small argument expansion
        if (n == 0) {
            return std::numeric_limits<double>::quiet_NaN();
        } else if ((n == 1) | (n == -1)) {
            return -M_PI * std::get<1>(_moments);
        } else {
            return 0.;
        }
    } else { // normal division
        return this->P(n, a) / (a * a);
    }
}

double Disp::SplineFLR::BOa(int const n, double const a) const
{
    if (std::abs(a) < _zero_tolerance) { // small argument expansion
        if (n == 0) {
            return -M_PI * std::get<0>(_moments);
        } else if ((n == 1) | (n == -1)) {
            return M_PI_2 * std::get<0>(_moments);
        } else {
            return 0.;
        }
    } else { // normal division
        return this->B(n, a) / a;
    }
}
double Disp::SplineFLR::QOa(int const n, double const a) const
{
    if (std::abs(a) < _zero_tolerance) { // small argument expansion
        if (n == 0) {
            return 2. * M_PI * std::get<1>(_moments);
        } else if ((n == 1) | (n == -1)) {
            return -M_PI * std::get<1>(_moments);
        } else {
            return 0.;
        }
    } else { // normal division
        return this->Q(n, a) / a;
    }
}

double Disp::SplineFLR::_A(int const n, double const a) const
{
#ifdef DEBUG
    if (n < 0 || a < 0.) {
        throw std::invalid_argument(__PRETTY_FUNCTION__);
    }
#endif
    return std::get<0>(_lookup_APBQCR(n, a));
}
double Disp::SplineFLR::_P(int const n, double const a) const
{
#ifdef DEBUG
    if (n < 0 || a < 0.) {
        throw std::invalid_argument(__PRETTY_FUNCTION__);
    }
#endif
    return std::get<1>(_lookup_APBQCR(n, a));
}

double Disp::SplineFLR::_B(int const n, double const a) const
{
#ifdef DEBUG
    if (n < 0 || a < 0.) {
        throw std::invalid_argument(__PRETTY_FUNCTION__);
    }
#endif
    return std::get<2>(_lookup_APBQCR(n, a));
}
double Disp::SplineFLR::_Q(int const n, double const a) const
{
#ifdef DEBUG
    if (n < 0 || a < 0.) {
        throw std::invalid_argument(__PRETTY_FUNCTION__);
    }
#endif
    return std::get<3>(_lookup_APBQCR(n, a));
}

double Disp::SplineFLR::_C(int const n, double const a) const
{
#ifdef DEBUG
    if (n < 0 || a < 0.) {
        throw std::invalid_argument(__PRETTY_FUNCTION__);
    }
#endif
    return std::get<4>(_lookup_APBQCR(n, a));
}
double Disp::SplineFLR::_R(int const n, double const a) const
{
#ifdef DEBUG
    if (n < 0 || a < 0.) {
        throw std::invalid_argument(__PRETTY_FUNCTION__);
    }
#endif
    return std::get<5>(_lookup_APBQCR(n, a));
}

auto Disp::SplineFLR::_lookup_APBQCR(int const n, double const a) const -> value_pack_type const
{
    optional_type _, *p{ &_ };
    if (n < 0 || unsigned(n) >= std::tuple_size<_OuterLookupTable>::value || !*(p = &(*_table)[unsigned(n)][a])) {
        *p = _eval_APBQCR(n, a);
    }
    return **p;
}

namespace {
// BesselJ derivatives
inline double Jp(Disp::BesselJ const &J, int const n, double const x)
{
    return .5 * (J(n - 1, x) - J(n + 1, x));
}
inline double Jpp(Disp::BesselJ const &J, int const n, double const x)
{
    return .5 * (Jp(J, n - 1, x) - Jp(J, n + 1, x));
}

// GaussLegendre integrator
constexpr long N = 10;
using Integrator = Utility::GaussLegendreIntegrator<double, N>;
Integrator const *_integrator;
Integrator const &integrator()
{
    static pthread_once_t once = PTHREAD_ONCE_INIT;
    if (pthread_once(&once, []() {
            _integrator = new Integrator;
        })) {
        throw std::runtime_error(__PRETTY_FUNCTION__);
    }
    return *_integrator;
}

// piecewise integral
using Vector = Utility::Vector<double, 6>;
template <class F>
auto integrate(double const a /*>=0*/, double const th /*>0*/, std::pair<double, double> const v_lim, F const &f) -> Vector
{
    constexpr double interval = 2. * M_PI;
    double const     abs_ath  = std::abs(a * th);
    double const     dv       = th * (abs_ath < interval ? 1. : interval / abs_ath); // unit interval of each piece
    double           v1       = v_lim.first;
    double           v0       = v1;
    Vector           result{};
    for (v1 += dv; v0 < v_lim.second; v0 = v1, v1 += dv) {
        result = integrator().operator()<F const &>(f, v0, (v1 < v_lim.second ? v1 : v_lim.second) /*guard against out-of-bound of interpolator*/, result);
    }
    return result;
}
} // namespace
auto Disp::SplineFLR::_eval_APBQCR(int const n, double const a) const -> value_pack_type
{
    double const     vmin         = _f.min_abscissa() < 0. ? 0. : _f.min_abscissa();
    constexpr double scale_factor = 5;
#if 1
    value_pack_type result = ::integrate(a, _thermal_spread * scale_factor, { vmin, _f.max_abscissa() }, [n, a, this](double const v) -> value_pack_type {
        double const    f = *this->_f(v), vf = v * f; // guaranteed to be within bound
        value_pack_type vec{ vf, f, vf, f, vf, f };
        double const    J  = (*this->_J)(n, a * v);
        double const    Jp = ::Jp(*this->_J, n, a * v), vJp = v * Jp;
        double const    avJpp = a * v * ::Jpp(*this->_J, n, a * v);
        return vec *= { J * J, -2. * (a * J * Jp), J * vJp, -(a * vJp * Jp + J * avJpp + J * Jp), vJp * vJp, -2. * (vJp * avJpp + vJp * Jp) };
    });
    if (!n && _f.min_abscissa() <= 0.) {
        std::get<1>(result) -= _f(0.)();
    }
    result *= 2. * M_PI;
#else
    value_pack_type result = ::integrate(a, _thermal_spread * scale_factor, { vmin, _f.max_abscissa() }, [n, a, this](double const v) -> value_pack_type {
        double const    vf = v * *this->_f.interpolate(v), df = *this->_f.derivative(v); // guaranteed to be within bound
        value_pack_type vec{ vf, df, vf, df, vf, df };
        double const    J   = (*this->_J)(n, a * v);
        double const    vJp = v * ::Jp(*this->_J, n, a * v);
        return vec *= { J * J, J * J, J * vJp, J * vJp, vJp * vJp, vJp * vJp };
    });
    result *= 2. * M_PI;
#endif
    return result;
}
