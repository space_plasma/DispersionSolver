/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "DISPMaxwellianRingVDF.h"
#include "DISPColdRingFLR.h"
#include "DISPInterpRingFLR.h"
#include "DISPSquareMatrix.h"
#include <cmath>
#include <stdexcept>
#include <string>

// MARK: FLR Wrappers
//
struct Disp::MaxwellianRingVDF::_FLRWrapper {
    virtual ~_FLRWrapper(){};
    virtual bool is_cold() const = 0;

    virtual void clear_cache() {}

    virtual double f(double const x) const noexcept    = 0;
    virtual double dfdx(double const x) const noexcept = 0;

    virtual double A(int const n, double const a) const    = 0;
    virtual double B(int const n, double const a) const    = 0;
    virtual double C(int const n, double const a) const    = 0;
    virtual double AOa2(int const n, double const a) const = 0;
    virtual double BOa(int const n, double const a) const  = 0;

    virtual double P(int const n, double const a) const    = 0;
    virtual double Q(int const n, double const a) const    = 0;
    virtual double R(int const n, double const a) const    = 0;
    virtual double POa2(int const n, double const a) const = 0;
    virtual double QOa(int const n, double const a) const  = 0;

protected:
    _FLRWrapper() {}
    _FLRWrapper(_FLRWrapper const &) = delete;
    _FLRWrapper &operator=(_FLRWrapper const &) = delete;
};
namespace {
struct WarmRingFLR : public Disp::MaxwellianRingVDF::_FLRWrapper {
    Disp::InterpRingFLR flr;
    double              th;
    explicit WarmRingFLR(double const vr, double const th)
    : flr(vr / th) { this->th = th; }
    bool   is_cold() const override { return false; }
    void   clear_cache() override { flr.clear_cache(); }
    double f(double const x) const noexcept override { return flr.f(x); }
    double dfdx(double const x) const noexcept override { return flr.dfdx(x); }
    double A(int const n, double const a) const override { return flr.A(n, a * th); }
    double B(int const n, double const a) const override { return flr.B(n, a * th) * th; }
    double C(int const n, double const a) const override { return flr.C(n, a * th) * th * th; }
    double AOa2(int const n, double const a) const override { return flr.AOa2(n, a * th) * th * th; }
    double BOa(int const n, double const a) const override { return flr.BOa(n, a * th) * th * th; }
    double P(int const n, double const a) const override { return flr.P(n, a * th) / (th * th); }
    double Q(int const n, double const a) const override { return flr.Q(n, a * th) / th; }
    double R(int const n, double const a) const override { return flr.R(n, a * th); }
    double POa2(int const n, double const a) const override { return flr.POa2(n, a * th); }
    double QOa(int const n, double const a) const override { return flr.QOa(n, a * th); }
};
struct ColdRingFLR : public Disp::MaxwellianRingVDF::_FLRWrapper {
    Disp::ColdRingFLR flr;
    explicit ColdRingFLR(double const vr)
    : flr(vr) {}
    bool   is_cold() const override { return true; }
    double f(double const x) const noexcept override { return flr.f(x); }
    double dfdx(double const x) const noexcept override { return flr.dfdx(x); }
    double A(int const n, double const a) const override { return flr.A(n, a); }
    double B(int const n, double const a) const override { return flr.B(n, a); }
    double C(int const n, double const a) const override { return flr.C(n, a); }
    double AOa2(int const n, double const a) const override { return flr.AOa2(n, a); }
    double BOa(int const n, double const a) const override { return flr.BOa(n, a); }
    double P(int const n, double const a) const override { return flr.P(n, a); }
    double Q(int const n, double const a) const override { return flr.Q(n, a); }
    double R(int const n, double const a) const override { return flr.R(n, a); }
    double POa2(int const n, double const a) const override { return flr.POa2(n, a); }
    double QOa(int const n, double const a) const override { return flr.QOa(n, a); }
};
} // namespace

// MARK: MaxwellianRingVDF Implementation
//
namespace {
constexpr double zero_tolerance = 1e-5;
}

Disp::MaxwellianRingVDF::~MaxwellianRingVDF()
{
}
Disp::MaxwellianRingVDF::MaxwellianRingVDF(real_type const Oc, complex_type const &op, real_type const th1, real_type const th2, real_type const vd, real_type const vr)
: VDF(), _op(op), _Oc(Oc), _th1(th1), _th2(th2), _vd(vd), _vr(vr), _pdf(), _flr()
{
    if (std::abs(op.real() * op.imag() / _Oc) > 1e-10) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - op should be either pure real or pure imaginary");
    }
    if (th1 < 0. || th2 < 0.) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - either th1 or th2 or both is negative");
    }
    if (_th2 <= std::abs(_vr * ::zero_tolerance)) { // also take into account th2 == vr == 0
        // cold perp
        _flr.reset(new ::ColdRingFLR(vr));
    } else {
        // warm perp
        _flr.reset(new ::WarmRingFLR(vr, th2));
    }
}

std::string Disp::MaxwellianRingVDF::description() const
{
    std::ostringstream os;
    printo(os, typeid(*this).name(), "[Oc->", Oc(), ", op->", op(), ", th1->", _th1, ", th2->", _th2, ", vd->", vd(), ", vr->", _vr, "]");
    return os.str();
}

namespace {
inline double Ar(double const b) noexcept
{
    constexpr double sqrt_pi = 2. / M_2_SQRTPI;
    return std::exp(-b * b) + sqrt_pi * b * std::erfc(-b);
}
} // namespace
double Disp::MaxwellianRingVDF::T2() const
{
    if (_flr->is_cold()) {
        return .5 * _vr * _vr;
    } else {
        constexpr double sqrt_pi = 2. / M_2_SQRTPI;
        double const     xr = _vr / _th2, xr2 = xr * xr;
        return .25 * _th2 * _th2 * (3. + 2. * xr2 - std::exp(-xr2) / (::Ar(-xr) + 2. * sqrt_pi * xr));
    }
}
double Disp::MaxwellianRingVDF::f(real_type const v1, real_type const v2) const
{
    real_type const x1 = (v1 - _vd) / _th1;
    real_type const x2 = v2 / _th2;
    real_type       f  = _pdf.f(x1) * _flr->f(x2);
    return f /= _th1 * _th2 * _th2;
}
double Disp::MaxwellianRingVDF::dfdv1(real_type const v1, real_type const v2) const
{
    real_type const x1 = (v1 - _vd) / _th1;
    real_type const x2 = v2 / _th2;
    real_type       df = _pdf.dfdx(x1) * _flr->f(x2);
    return df /= _th1 * _th1 * _th2 * _th2;
}
double Disp::MaxwellianRingVDF::dfdv2(real_type const v1, real_type const v2) const
{
    real_type const x1 = (v1 - _vd) / _th1;
    real_type const x2 = v2 / _th2;
    real_type       df = _pdf.f(x1) * _flr->dfdx(x2);
    return df /= _th1 * _th2 * _th2 * _th2;
}

void Disp::MaxwellianRingVDF::clear_cache()
{
    _flr->clear_cache();
}

CX::Matrix Disp::MaxwellianRingVDF::K(std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const
{
    CX::Matrix         K{};
    real_type const    k1th1      = k.first * _th1;
    complex_type const o_jOc_k1vd = o - j * _Oc - k.first * _vd;
    if (std::abs(k1th1) < std::abs(o_jOc_k1vd * ::zero_tolerance)) {
        // asymptotic version
        K11(::get<0, 0>(K), k, o, j);
        K12(::get<0, 1>(K), k, o, j);
        K13(::get<0, 2>(K), k, o, j);

        ::get<1, 0>(K) = -::get<0, 1>(K);
        K22(::get<1, 1>(K), k, o, j);
        K23(::get<1, 2>(K), k, o, j);

        K31(::get<2, 0>(K), k, o, j);
        K32(::get<2, 1>(K), k, o, j);
        K33(::get<2, 2>(K), k, o, j);
    } else {
        // normal version
        triplet_type const &Z = _pdf.Ztriplet(o_jOc_k1vd / k1th1);
        K11(::get<0, 0>(K), k, o, j, Z);
        K12(::get<0, 1>(K), k, o, j, Z);
        K13(::get<0, 2>(K), k, o, j, Z);

        ::get<1, 0>(K) = -::get<0, 1>(K);
        K22(::get<1, 1>(K), k, o, j, Z);
        K23(::get<1, 2>(K), k, o, j, Z);

        K31(::get<2, 0>(K), k, o, j, Z);
        K32(::get<2, 1>(K), k, o, j, Z);
        K33(::get<2, 2>(K), k, o, j, Z);
    }
    return K;
}

// MARK:- K Elements
void Disp::MaxwellianRingVDF::K11(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const
{
    if (0 == j)
        return;
    real_type const    k1 = k.first, k2 = k.second;
    real_type const    a          = k2 / _Oc;
    complex_type const o_k1vd     = o - k1 * _vd;
    complex_type const o_jOc_k1vd = o_k1vd - j * _Oc;
    // asymptotic version
    K += std::pow(k1 / o_jOc_k1vd, 2.) * _flr->AOa2(j, a);
    K -= o_k1vd / o_jOc_k1vd * _flr->POa2(j, a);
    K *= real_type(-j) * j;
}
void Disp::MaxwellianRingVDF::K11(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j, triplet_type const &Z) const
{
    if (0 == j)
        return;
    real_type const    k1 = k.first, k2 = k.second;
    real_type const    a = k2 / _Oc, k1th1 = k1 * _th1;
    complex_type const o_k1vd = o - k1 * _vd;
    // normal version
    K += std::get<1>(Z) / (_th1 * _th1) * _flr->AOa2(j, a);
    K += .5 * (std::get<1>(Z) - o_k1vd / k1th1 * std::get<0>(Z)) * _flr->POa2(j, a);
    K *= 2. * j * j;
}

void Disp::MaxwellianRingVDF::K12(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const
{
    if (0 == j)
        return;
    real_type const    k1 = k.first, k2 = k.second;
    real_type const    a          = k2 / _Oc;
    complex_type const o_k1vd     = o - k1 * _vd;
    complex_type const o_jOc_k1vd = o_k1vd - j * _Oc;
    // asymptotic version
    K += std::pow(k1 / o_jOc_k1vd, 2.) * _flr->BOa(j, a);
    K -= o_k1vd / o_jOc_k1vd * _flr->QOa(j, a);
    K *= real_type(-j) * CX::I;
}
void Disp::MaxwellianRingVDF::K12(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j, triplet_type const &Z) const
{
    if (0 == j)
        return;
    real_type const    k1 = k.first, k2 = k.second;
    real_type const    a = k2 / _Oc, k1th1 = k1 * _th1;
    complex_type const o_k1vd = o - k1 * _vd;
    // normal version
    K += std::get<1>(Z) / (_th1 * _th1) * _flr->BOa(j, a);
    K += .5 * (std::get<1>(Z) - o_k1vd / k1th1 * std::get<0>(Z)) * _flr->QOa(j, a);
    K *= 2. * j * CX::I;
}

void Disp::MaxwellianRingVDF::K13(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const
{
    if (0 == j)
        return;
    real_type const    k1 = k.first, k2 = k.second;
    real_type const    a          = k2 / _Oc;
    complex_type const o_k1vd     = o - k1 * _vd;
    complex_type const o_jOc_k1vd = o_k1vd - j * _Oc;
    // asymptotic version
    K += a * k1 * (o - j * _Oc) / (o_jOc_k1vd * o_jOc_k1vd) * _flr->AOa2(j, a);
    K -= j * k2 * _vd / o_jOc_k1vd * _flr->POa2(j, a);
    K *= real_type(-j);
}
void Disp::MaxwellianRingVDF::K13(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j, triplet_type const &Z) const
{
    if (0 == j)
        return;
    real_type const    k1 = k.first, k2 = k.second;
    real_type const    a = k2 / _Oc, k1th1 = k1 * _th1;
    complex_type const zjOth1 = (o - j * _Oc) / k1th1;
    // normal version
    K += zjOth1 / _th1 * std::get<1>(Z) * _flr->AOa2(j, a);
    K -= .5 * j * _Oc / k1 * (std::get<1>(Z) + _vd / _th1 * std::get<0>(Z)) * _flr->POa2(j, a);
    K *= 2. * j * a;
}

void Disp::MaxwellianRingVDF::K22(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const
{
    real_type const    k1 = k.first, k2 = k.second;
    real_type const    a          = k2 / _Oc;
    complex_type const o_k1vd     = o - k1 * _vd;
    complex_type const o_jOc_k1vd = o_k1vd - j * _Oc;
    // asymptotic version
    K += std::pow(k1 / o_jOc_k1vd, 2.) * _flr->C(j, a);
    K -= o_k1vd / o_jOc_k1vd * _flr->R(j, a);
    K *= -1.;
}
void Disp::MaxwellianRingVDF::K22(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j, triplet_type const &Z) const
{
    real_type const    k1 = k.first, k2 = k.second;
    real_type const    a = k2 / _Oc, k1th1 = k1 * _th1;
    complex_type const o_k1vd = o - k1 * _vd;
    // normal version
    K += std::get<1>(Z) / (_th1 * _th1) * _flr->C(j, a);
    K += .5 * (std::get<1>(Z) - o_k1vd / k1th1 * std::get<0>(Z)) * _flr->R(j, a);
    K *= 2.;
}

void Disp::MaxwellianRingVDF::K23(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const
{
    real_type const    k1 = k.first, k2 = k.second;
    real_type const    a          = k2 / _Oc;
    complex_type const o_k1vd     = o - k1 * _vd;
    complex_type const o_jOc_k1vd = o_k1vd - j * _Oc;
    // asymptotic version
    K += k1 * (o - j * _Oc) / (o_jOc_k1vd * o_jOc_k1vd) * _flr->B(j, a);
    K -= j ? j * k2 * _vd / o_jOc_k1vd * _flr->QOa(j, a) : 0.;
    K *= CX::I;
}
void Disp::MaxwellianRingVDF::K23(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j, triplet_type const &Z) const
{
    real_type const    k1 = k.first, k2 = k.second;
    real_type const    a = k2 / _Oc, k1th1 = k1 * _th1;
    complex_type const zjOth1 = (o - j * _Oc) / k1th1;
    // normal version
    K += zjOth1 / _th1 * std::get<1>(Z) * _flr->B(j, a);
    K -= j ? .5 * j * k2 / k1 * (std::get<1>(Z) + _vd / _th1 * std::get<0>(Z)) * _flr->QOa(j, a) : 0.;
    K *= -2. * CX::I;
}

void Disp::MaxwellianRingVDF::K31(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const
{
    if (0 == j)
        return;
    real_type const    k1 = k.first, k2 = k.second;
    real_type const    a          = k2 / _Oc;
    complex_type const o_k1vd     = o - k1 * _vd;
    complex_type const o_jOc_k1vd = o_k1vd - j * _Oc;
    // asymptotic version
    K += (o - j * _Oc) * k1 / (o_jOc_k1vd * o_jOc_k1vd) * _flr->AOa2(j, a);
    K -= _vd * o_k1vd / o_jOc_k1vd * _flr->POa2(j, a);
    K *= -j * a;
}
void Disp::MaxwellianRingVDF::K31(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j, triplet_type const &Z) const
{
    if (0 == j)
        return;
    real_type const    k1 = k.first, k2 = k.second;
    real_type const    a = k2 / _Oc, k1th1 = k1 * _th1;
    complex_type const o_k1vd = o - k1 * _vd;
    // normal version
    K += (std::get<2>(Z) + _vd / _th1 * std::get<1>(Z)) / _th1 * _flr->AOa2(j, a);
    K += .5 * (_th1 * std::get<2>(Z) + (2. * _vd - o / k1) * std::get<1>(Z) - o_k1vd * _vd / k1th1 * std::get<0>(Z)) * _flr->POa2(j, a);
    K *= 2. * j * a;
}

void Disp::MaxwellianRingVDF::K32(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const
{
    real_type const    k1 = k.first, k2 = k.second;
    real_type const    a          = k2 / _Oc;
    complex_type const o_k1vd     = o - k1 * _vd;
    complex_type const o_jOc_k1vd = o_k1vd - j * _Oc;
    // asymptotic version
    K += (o - j * _Oc) * k1 / (o_jOc_k1vd * o_jOc_k1vd) * _flr->B(j, a);
    K -= a * _vd * o_k1vd / o_jOc_k1vd * _flr->QOa(j, a);
    K *= -CX::I;
}
void Disp::MaxwellianRingVDF::K32(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j, triplet_type const &Z) const
{
    real_type const    k1 = k.first, k2 = k.second;
    real_type const    a = k2 / _Oc, k1th1 = k1 * _th1;
    complex_type const o_k1vd = o - k1 * _vd;
    // normal version
    K += (std::get<2>(Z) + _vd / _th1 * std::get<1>(Z)) / _th1 * _flr->B(j, a);
    K += .5 * a * (_th1 * std::get<2>(Z) + (2. * _vd - o / k1) * std::get<1>(Z) - o_k1vd * _vd / k1th1 * std::get<0>(Z)) * _flr->QOa(j, a);
    K *= 2. * CX::I;
}

void Disp::MaxwellianRingVDF::K33(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const
{
    real_type const    k1 = k.first, k2 = k.second;
    real_type const    a          = k2 / _Oc;
    complex_type const o_k1vd     = o - k1 * _vd;
    complex_type const o_jOc_k1vd = o_k1vd - j * _Oc;
    // asymptotic version
    K += std::pow((o - j * _Oc) / o_jOc_k1vd, 2.) * _flr->A(j, a);
    K -= j ? .5 * j * _Oc * a * a * (_th1 * _th1 + 2. * _vd * _vd) / o_jOc_k1vd * _flr->POa2(j, a) : 0.;
    K *= -1.;
}
void Disp::MaxwellianRingVDF::K33(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j, triplet_type const &Z) const
{
    real_type const    k1 = k.first, k2 = k.second;
    real_type const    a = k2 / _Oc, k1th1 = k1 * _th1;
    complex_type const zjOth1 = (o - j * _Oc) / k1th1;
    // normal version
    K += zjOth1 * (std::get<2>(Z) + _vd / _th1 * std::get<1>(Z)) * _flr->A(j, a);
    K -= j ? .5 * j * _Oc * a * a * (_th1 / k1 * std::get<2>(Z) + 2. * _vd / k1 * std::get<1>(Z) + _vd * _vd / k1th1 * std::get<0>(Z)) * _flr->POa2(j, a) : 0.;
    K *= 2.;
}
