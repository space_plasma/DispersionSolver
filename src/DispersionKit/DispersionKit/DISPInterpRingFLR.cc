/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "DISPInterpRingFLR.h"
#include <cmath>
#include <stdexcept>
#include <utility>

Disp::InterpRingFLR::InterpRingFLR(InterpRingFLR const &o)
: InterpRingFLR(o.b())
{
    *_interpolator_table = *o._interpolator_table;
}
auto Disp::InterpRingFLR::operator=(InterpRingFLR const &o) -> InterpRingFLR &
{
    if (this != &o) {
        InterpRingFLR tmp(o); // copy through a temporary copied object to reduce clutter of copying all member variables
        *this = std::move(tmp);
    }
    return *this;
}
Disp::InterpRingFLR::InterpRingFLR(double const b)
: RingFLR(b), _interpolator_table(new _OuterInterpolatorTable)
{
}

struct Disp::InterpRingFLR::_AbscissaRange {
    static double offset(unsigned const n) noexcept
    {
        static BesselJ::zero_table_type const &zeros = BesselJ::zeros();
        return zeros[n].first; // start from the first zero
    }

    Utility::Vector<double, 2> alim;
    unsigned                   key;

    _AbscissaRange(unsigned const n, double const a, double const b) noexcept
    {
        double const x0 = offset(n);
        double const x  = a * b;
        if (x < x0) {
            key    = 0;
            alim.x = 0.;
            alim.y = x0;
        } else {
            constexpr double interval = 2. * M_PI * _n_oscillations;

            key    = static_cast<unsigned>((x - x0) / interval) + 1;
            alim.x = x0 + (key - 1) * interval;
            alim.y = x0 + (key - 0) * interval;
        }
        alim /= b;
    }
};

auto Disp::InterpRingFLR::_eval_ABC(int const n, double const a) const -> value_pack_type
{
#if defined(DEBUG)
    if (n < 0 || a < 0.)
        throw std::invalid_argument(__PRETTY_FUNCTION__);
#endif

    if (unsigned(n) >= std::tuple_size<_OuterInterpolatorTable>::value) {
        return RingFLR::_eval_ABC(n, a);
    }
    if (a < 1.0)
        return RingFLR::_eval_ABC(n, a); // avoid too small a

    constexpr double     bmin = 3;
    _AbscissaRange const range{ unsigned(n), a, (_b < bmin ? bmin : _b) };
    _InterpolatorPack   &interpolators = (*_interpolator_table)[unsigned(n)][range.key];
    if (!interpolators.front()) {
        _sample_ABC(n, a, range, interpolators);
    }
    return {
        *std::get<0>(interpolators)(a),
        *std::get<1>(interpolators)(a),
        *std::get<2>(interpolators)(a),
        *std::get<3>(interpolators)(a),
        *std::get<4>(interpolators)(a),
        *std::get<5>(interpolators)(a),
    };
}

void Disp::InterpRingFLR::_sample_ABC(int const n, double const, _AbscissaRange const &range, _InterpolatorPack &interpolators) const
{
    // sampling
    //
    std::array<double, _n_samples + 1> as, A0s, A1s, B0s, B1s, C0s, C1s;
    double const                       da = (range.alim.y - range.alim.x) / _n_samples;
    value_pack_type
        pack
        = RingFLR::_eval_ABC(n, as[0] = range.alim.x);
    A0s.front() = std::get<0>(pack);
    A1s.front() = std::get<1>(pack);
    B0s.front() = std::get<2>(pack);
    B1s.front() = std::get<3>(pack);
    C0s.front() = std::get<4>(pack);
    C1s.front() = std::get<5>(pack);
    for (unsigned i = 1; i < std::tuple_size<decltype(as)>::value - 1; ++i) {
        double const a = double(i) * da + range.alim.x;
        pack           = RingFLR::_eval_ABC(n, as[i] = a);
        A0s[i]         = std::get<0>(pack);
        A1s[i]         = std::get<1>(pack);
        B0s[i]         = std::get<2>(pack);
        B1s[i]         = std::get<3>(pack);
        C0s[i]         = std::get<4>(pack);
        C1s[i]         = std::get<5>(pack);
    }
    pack       = RingFLR::_eval_ABC(n, as.back() = range.alim.y);
    A0s.back() = std::get<0>(pack);
    A1s.back() = std::get<1>(pack);
    B0s.back() = std::get<2>(pack);
    B1s.back() = std::get<3>(pack);
    C0s.back() = std::get<4>(pack);
    C1s.back() = std::get<5>(pack);

    // interpolators
    //
    using Cubic                = Utility::CubicSplineCoefficient<double>;
    std::get<0>(interpolators) = Cubic(as.begin(), as.end(), A0s.begin()).interpolator();
    std::get<1>(interpolators) = Cubic(as.begin(), as.end(), A1s.begin()).interpolator();
    std::get<2>(interpolators) = Cubic(as.begin(), as.end(), B0s.begin()).interpolator();
    std::get<3>(interpolators) = Cubic(as.begin(), as.end(), B1s.begin()).interpolator();
    std::get<4>(interpolators) = Cubic(as.begin(), as.end(), C0s.begin()).interpolator();
    std::get<5>(interpolators) = Cubic(as.begin(), as.end(), C1s.begin()).interpolator();
}
