/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "DISPRingFLR.h"
#include <cmath>
#include <limits>
#include <pthread.h>
#include <stdexcept>
#include <utility>

Disp::RingFLR::~RingFLR()
{
}

Disp::RingFLR::RingFLR(RingFLR const &o)
: RingFLR(o._b, o._J)
{
    *_lookup_table = *o._lookup_table;
    // the rest is derived from b
}
auto Disp::RingFLR::operator=(RingFLR const &o) -> RingFLR &
{
    if (this != &o) {
        RingFLR tmp(o); // copy through a temporary copied object to reduce clutter of copying all member variables
        *this = std::move(tmp);
    }
    return *this;
}

namespace {
inline double Ar(double const b) noexcept
{
    constexpr double sqrt_pi = 2. / M_2_SQRTPI;
    return std::exp(-b * b) + sqrt_pi * b * std::erfc(-b);
}
} // namespace
Disp::RingFLR::RingFLR(double const b)
: RingFLR(b, &BesselJ::shared())
{
}
Disp::RingFLR::RingFLR(double const b, BesselJ const *J)
: _lookup_table(new _OuterLookupTable), _J(J), _b(b)
{
    _constant_factor = 2. / ::Ar(b);
    _zero_tolerance  = 1e-5 / (b < 0. ? 5 : b + 5);
}

double Disp::RingFLR::f(double const x) const noexcept
{
    return M_1_PI / ::Ar(_b) * std::exp(-(x - _b) * (x - _b));
}
double Disp::RingFLR::dfdx(double const x) const noexcept
{
    return -2. * (x - _b) * f(x);
}

void Disp::RingFLR::clear_cache()
{
    for (auto &map : *_lookup_table) {
        map.clear();
    }
}

namespace {
inline double small_argument_expansion_for_divide_by_zero(double const b) noexcept
{
    constexpr double sqrt_pi = 2. / M_2_SQRTPI;
    return 1.5 + b * b - .5 * std::exp(-b * b) / (Ar(-b) + 2. * sqrt_pi * b);
}
} // namespace
double Disp::RingFLR::AOa2(int const n, double const a, bool const c) const
{
    if (std::abs(a) < _zero_tolerance) { // small argument expansion
        if (n == 0) {
            return std::numeric_limits<double>::quiet_NaN();
        } else if ((n == 1) | (n == -1)) {
            return .25 * (c ? 1. : ::small_argument_expansion_for_divide_by_zero(_b));
        } else {
            return 0.;
        }
    } else { // normal division
        return this->A(n, a, c) / (a * a);
    }
}
double Disp::RingFLR::BOa(int const n, double const a, bool const c) const
{
    if (std::abs(a) < _zero_tolerance) { // small argument expansion
        if (n == 0) {
            return -.5 * (c ? 1. : ::small_argument_expansion_for_divide_by_zero(_b));
        } else if ((n == 1) | (n == -1)) {
            return .25 * (c ? 1. : ::small_argument_expansion_for_divide_by_zero(_b));
        } else {
            return 0.;
        }
    } else { // normal division
        return this->B(n, a, c) / a;
    }
}

double Disp::RingFLR::_A(int const n, double const a, bool const c) const
{
#ifdef DEBUG
    if (n < 0 || a < 0.) {
        throw std::invalid_argument(__PRETTY_FUNCTION__);
    }
#endif

    constexpr long offset = 0;
    return c ? std::get<offset + 1>(_lookup_ABC(n, a)) : std::get<offset + 0>(_lookup_ABC(n, a));
}
double Disp::RingFLR::_B(int const n, double const a, bool const c) const
{
#ifdef DEBUG
    if (n < 0 || a < 0.) {
        throw std::invalid_argument(__PRETTY_FUNCTION__);
    }
#endif

    constexpr long offset = 2;
    return c ? std::get<offset + 1>(_lookup_ABC(n, a)) : std::get<offset + 0>(_lookup_ABC(n, a));
}
double Disp::RingFLR::_C(int const n, double const a, bool const c) const
{
#ifdef DEBUG
    if (n < 0 || a < 0.) {
        throw std::invalid_argument(__PRETTY_FUNCTION__);
    }
#endif

    constexpr long offset = 4;
    return c ? std::get<offset + 1>(_lookup_ABC(n, a)) : std::get<offset + 0>(_lookup_ABC(n, a));
}

auto Disp::RingFLR::_lookup_ABC(int const n, double const a) const -> value_pack_type const
{
    optional_type _, *p{ &_ };
    if (n < 0 || unsigned(n) >= std::tuple_size<_OuterLookupTable>::value || !*(p = &(*_lookup_table)[unsigned(n)][a])) {
        *p = _eval_ABC(n, a);
    }
    return **p;
}

namespace {
// BesselJ derivatives
inline double Jp(Disp::BesselJ const &J, int const n, double const x)
{
    return .5 * (J(n - 1, x) - J(n + 1, x));
}

// GaussLegendre integrator
constexpr long N = 10;
using Integrator = Utility::GaussLegendreIntegrator<double, N>;
Integrator const *_integrator;
Integrator const &integrator()
{
    static pthread_once_t once = PTHREAD_ONCE_INIT;
    if (pthread_once(&once, []() {
            _integrator = new Integrator;
        })) {
        throw std::runtime_error(__PRETTY_FUNCTION__);
    }
    return *_integrator;
}

// piecewise integral
using Vector = Utility::Vector<double, 6>;
template <class F> // change of integral variable should be applied in F
auto integrate(double const a /*>=0*/, double const b, F const &f) -> Vector
{
    constexpr double interval = 2. * M_PI, critical = 5.5, accuracy = 11.;
    double const     xmax  = b < critical ? critical : std::sqrt(3. * std::log(b) + accuracy * std::log(10.));
    double const     abs_a = std::abs(a);
    double const     dx    = (abs_a < interval ? 1. : interval / abs_a); // unit interval of each piece
    double           x1    = -(b < xmax ? b : xmax);                     // minimum integral bound
    double           x0    = x1;
    Vector           result{};
    for (x1 += dx; x0 < xmax; x0 = x1, x1 += dx) {
        result = integrator().operator()<F const &>(f, x0, /*(x1 < xmax ? x1 : xmax)*/ x1, result);
    }
    return result;
}
} // namespace
auto Disp::RingFLR::_eval_ABC(int const n, double const a) const -> value_pack_type
{
    value_pack_type result = ::integrate(a, _b, [n, a, this](double const x) -> value_pack_type {
        double const    xpb = x + this->_b;
        value_pack_type v{ xpb, x, xpb, x, xpb, x };

        double const J      = (*this->_J)(n, a * xpb);
        double const Jp_xpb = ::Jp(*this->_J, n, a * xpb) * xpb;
        v *= { J * J, J * J, J * Jp_xpb, J * Jp_xpb, Jp_xpb * Jp_xpb, Jp_xpb * Jp_xpb };
        return v *= std::exp(-x * x);
    });
    return result *= _constant_factor;
}
