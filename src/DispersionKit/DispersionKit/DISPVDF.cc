/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "DISPVDF.h"
#include <UtilityKit/UtilityKit.h>
#include <sstream>

Disp::VDF::~VDF()
{
}
Disp::VDF::VDF() noexcept
{
}

std::string Disp::VDF::description() const
{
    std::ostringstream os;
    printo(os, typeid(*this).name(), "[Oc->", Oc(), ", op->", op(), ", T1->", T1(), ", T2->", T2(), ", vd->", vd(), "]");
    return os.str();
}

void Disp::VDF::clear_cache()
{
}
