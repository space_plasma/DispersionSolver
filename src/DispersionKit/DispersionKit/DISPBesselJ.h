/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <DispersionKit/DispersionKit-config.h>
#include <UtilityKit/UtilityKit.h>
#include <array>
#include <cmath>
#include <memory>
#include <unordered_map>
#include <utility>

DISPERSIONKIT_BEGIN_NAMESPACE
namespace Bessel {
using real_type = double;

real_type _Jn(int const n, real_type const x) noexcept;

namespace {
    /**
     @brief Bessel function of the first kind with integer index which is given by a function argument.
     @disdussion It internally calls standard math library's jn.
     */
    inline real_type J(int const n, real_type const x) noexcept
    {
        return _Jn(n, x);
    }
    /**
     @brief Bessel function of the first kind with integer index that is given by a template parameter.
     */
    template <int N>
    inline real_type J(real_type const x) noexcept
    {
        return _Jn(N, x);
    }
} // namespace
} // namespace Bessel

/**
 @brief Evaluate Bessel function of the first kind using a lookup table.
 */
struct BesselJ {
    static constexpr unsigned max_n = 2000; //!< maximum n to generate table

    using real_type       = double;
    using zero_pair_type  = std::pair<real_type, real_type>;
    using zero_table_type = std::array<zero_pair_type, max_n + 1>;

    /**
     @brief Table for first and second zeros of the Bessel function.
     @discussion Entry zeros[n] for n >= 0 contains the first two zeros for J(n, x) = 0.
     */
    static zero_table_type const &zeros();

    /**
     @brief Returns a shared, global instance.
     */
    static BesselJ const &shared();

    /**
     @brief Evaluate the Bessel function.
     @discussion This call is thread-safe except when the receiver is being copied or moved.
     */
    inline real_type operator()(int const n, real_type const x) const;

    explicit BesselJ();
    BesselJ(BesselJ const &);
    BesselJ &operator            =(BesselJ const &);
    BesselJ(BesselJ &&) noexcept = default;
    BesselJ &operator=(BesselJ &&) noexcept = default;

private:
    static constexpr unsigned _min_n          = 10; //!< minimum n below which analytic J is used
    static constexpr unsigned _n_oscillations = 10; //!< number of oscillations to divide

    struct _AbscissaRange;
    using _Interpolator     = Utility::SplineInterpolator<real_type, 3>;
    using _InnerLookupTable = std::unordered_map<unsigned, _Interpolator>;
    using _OuterLookupTable = std::array<std::pair<Utility::SpinLock, _InnerLookupTable>, std::tuple_size<zero_table_type>::value>;

    std::unique_ptr<_OuterLookupTable> _table;

    real_type      _J(unsigned const n, real_type const x) const; // defined for positive n and x
    _Interpolator &_interpolator(unsigned const n, _AbscissaRange const &key) const;
};

auto BesselJ::operator()(const int n, const real_type x) const -> real_type
{
    unsigned const abs_n = unsigned(std::abs(n));
    int const      sign  = (n * x < 0.) & (abs_n & 1) ? -1 : 1;
    return _J(abs_n, std::abs(x)) * sign;
}
DISPERSIONKIT_END_NAMESPACE
