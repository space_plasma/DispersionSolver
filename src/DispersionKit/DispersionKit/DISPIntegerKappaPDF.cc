/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "DISPIntegerKappaPDF.h"
#include <cmath>
#include <stdexcept>

namespace {
inline double C(double const k) noexcept
{
    double A;
    if (k > 100.) { // asymptotic expansion
        A = 1 + 1 / (8. * k);
    } else {
        A = std::sqrt(k) * std::tgamma(k) / std::tgamma(k + .5);
    }
    return A *= .5 * M_2_SQRTPI;
}
template <class T>
inline T f(double const k, T const &x) noexcept
{
    return C(k) * std::pow(1. + x * x / k, -(k + 1));
}
template <class T>
inline T df(double const k, T const &x) noexcept
{
    return -2. * x * (k + 1) / (k + x * x) * f(k, x);
}
} // namespace

auto Disp::IntegerKappaPDF::f(real_type const x) const noexcept -> real_type
{
    return ::f(_k, x);
}
auto Disp::IntegerKappaPDF::f(complex_type const &z) const noexcept -> complex_type
{
    return ::f(_k, z);
}

auto Disp::IntegerKappaPDF::dfdx(real_type const x) const noexcept -> real_type
{
    return ::df(_k, x);
}
auto Disp::IntegerKappaPDF::dfdx(complex_type const &z) const noexcept -> complex_type
{
    return ::df(_k, z);
}

Disp::IntegerKappaPDF::IntegerKappaPDF(int const kappa)
: _k()
{
    if (kappa < 1) {
        throw std::invalid_argument(__PRETTY_FUNCTION__);
    }
    _k = decltype(_k)(kappa);
}

auto Disp::IntegerKappaPDF::Ztriplet(complex_type const &z) const -> std::tuple<complex_type, complex_type, complex_type>
{
    complex_type const Z0 = this->Z<0>(z);
    complex_type const Z1 = z * Z0 + 1.;
    complex_type const Z2 = z * Z1;
    return std::make_tuple(Z0, Z1, Z2);
}
auto Disp::IntegerKappaPDF::Wtriplet(complex_type const &z) const -> std::tuple<complex_type, complex_type, complex_type>
{
    complex_type const s  = std::sqrt(real_type(_k + 1) / _k) * z;
    complex_type const Z1 = s * _Z(_k + 1, s) + 1.;
    complex_type const W0 = -(2. + 1. / _k) * Z1;
    complex_type const W1 = z * W0;
    complex_type const W2 = z * W1 - 1.;
    return std::make_tuple(W0, W1, W2);
}

auto Disp::IntegerKappaPDF::_Z(unsigned long k, complex_type const &z) noexcept -> complex_type
{
    constexpr complex_type I{ 0., 1. };
    real_type const        inverse_sqrt_k = 1. / std::sqrt(k);
    complex_type const     kernel         = 2. / (z * inverse_sqrt_k + I);
    complex_type           series{ kernel }, Z{ 0. };
    unsigned long          l = k;
    do { // summation backward
        Z += series * std::pow(I, (k - l) % 4);
        series *= l / (real_type(k) + l) * kernel; // (k!/l!)/((k+l)!/(2k)!) * (2/(z/√k+I))^(k+1-l)
        if (std::abs(series) < _reltol * std::abs(Z))
            break;
    } while (l--);
    return Z *= -.5 * inverse_sqrt_k;
}
