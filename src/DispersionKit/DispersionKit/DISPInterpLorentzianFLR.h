/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <DispersionKit/DISPLorentzianFLR.h>
#include <DispersionKit/DispersionKit-config.h>
#include <UtilityKit/UtilityKit.h>
#include <array>
#include <memory>
#include <unordered_map>

DISPERSIONKIT_BEGIN_NAMESPACE
/**
 @brief LorentzianFLR with dynamic tabulation of interpolators.
 */
struct InterpLorentzianFLR : protected LorentzianFLR {
    InterpLorentzianFLR(InterpLorentzianFLR &&) noexcept = default;
    InterpLorentzianFLR &operator=(InterpLorentzianFLR &&) noexcept = default;
    InterpLorentzianFLR(InterpLorentzianFLR const &);
    InterpLorentzianFLR &operator=(InterpLorentzianFLR const &);

    explicit InterpLorentzianFLR(double const kappa);

    //@{
    /** Inherited interfaces. */
    using LorentzianFLR::clear_cache;

    using LorentzianFLR::dfdx;
    using LorentzianFLR::f;

    using LorentzianFLR::A;
    using LorentzianFLR::AOa2;
    using LorentzianFLR::B;
    using LorentzianFLR::BOa;
    using LorentzianFLR::C;

    using LorentzianFLR::P;
    using LorentzianFLR::POa2;
    using LorentzianFLR::Q;
    using LorentzianFLR::QOa;
    using LorentzianFLR::R;

    using LorentzianFLR::J;
    using LorentzianFLR::kappa;
    //@}

protected:
    static constexpr unsigned _n_oscillations = 10;  //!< number of oscillations to divide
    static constexpr unsigned _n_samples      = 150; //!< number of samples used to construct interpolators

    struct _AbscissaRange;
    using _InterpolatorPack       = std::array<Utility::SplineInterpolator<double, 3>, value_pack_type::size()>;
    using _InnerInterpolatorTable = std::unordered_map<unsigned, _InterpolatorPack>;
    using _OuterInterpolatorTable = std::array<_InnerInterpolatorTable, std::tuple_size<BesselJ::zero_table_type>::value>;

    std::unique_ptr<_OuterInterpolatorTable> _interpolator_table;

    value_pack_type _eval_APBQCR(int const n, double const a) const override;
    void            _sample_APBQCR(int const n, double const a, _AbscissaRange const &range, _InterpolatorPack &interpolators) const;
};
DISPERSIONKIT_END_NAMESPACE
