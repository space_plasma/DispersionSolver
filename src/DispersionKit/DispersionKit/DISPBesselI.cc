/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "DISPBesselI.h"
#include <pthread.h>
#include <stdexcept>
#include <type_traits>
#include <utility>

// MARK: Bessel::_scaledIn
//
#if defined(DISPERSIONKIT_USE_GSL_BESSELI) && DISPERSIONKIT_USE_GSL_BESSELI
#include <gsl/gsl_sf_bessel.h>
double (*Disp::Bessel::_scaledIn)(int const, double const) = &gsl_sf_bessel_In_scaled;
// double Disp::Bessel::_scaledIn(int const n, double const x) noexcept {
//     return gsl_sf_bessel_In_scaled(n, x);
// }
#else
namespace {
using real_type = double;

// MARK: Coefficients
constexpr std::array<real_type, 14> i0p{ { 9.999999999999997e-1, 2.466405579426905e-1, 1.478980363444585e-2, 3.826993559940360e-4, 5.395676869878828e-6, 4.700912200921704e-8, 2.733894920915608e-10, 1.115830108455192e-12, 3.301093025084127e-15, 7.209167098020555e-18, 1.166898488777214e-20, 1.378948246502109e-23, 1.124884061857506e-26, 5.498556929587117e-30 } };
constexpr std::array<real_type, 5>  i0q{ { 4.463598170691436e-1, 1.702205745042606e-3, 2.792125684538934e-6, 2.369902034785866e-9, 8.965900179621208e-13 } };
constexpr std::array<real_type, 5>  i0pp{ { 1.192273748120670e-1, 1.947452015979746e-1, 7.629241821600588e-2, 8.474903580801549e-3, 2.023821945835647e-4 } };
constexpr std::array<real_type, 6>  i0qq{ { 2.962898424533095e-1, 4.866115913196384e-1, 1.938352806477617e-1, 2.261671093400046e-2, 6.450448095075585e-4, 1.529835782400450e-6 } };
constexpr std::array<real_type, 14> i1p{ { 5.000000000000000e-1, 6.090824836578078e-2, 2.407288574545340e-3, 4.622311145544158e-5, 5.161743818147913e-7, 3.712362374847555e-9, 1.833983433811517e-11, 6.493125133990706e-14, 1.693074927497696e-16, 3.299609473102338e-19, 4.813071975603122e-22, 5.164275442089090e-25, 3.846870021788629e-28, 1.712948291408736e-31 } };
constexpr std::array<real_type, 5>  i1q{ { 4.665973211630446e-1, 1.677754477613006e-3, 2.583049634689725e-6, 2.045930934253556e-9, 7.166133240195285e-13 } };
constexpr std::array<real_type, 5>  i1pp{ { 1.286515211317124e-1, 1.930915272916783e-1, 6.965689298161343e-2, 7.345978783504595e-3, 1.963602129240502e-4 } };
constexpr std::array<real_type, 6>  i1qq{ { 3.309385098860755e-1, 4.878218424097628e-1, 1.663088501568696e-1, 1.473541892809522e-2, 1.964131438571051e-4, -1.034524660214173e-6 } };

// MARK: Polynomial Evaluation
template <unsigned long N>
inline real_type poly(std::array<real_type, N> const &c, real_type const x, std::integral_constant<unsigned long, N>) noexcept
{
    return 0.;
}
template <unsigned long N, unsigned long K>
inline real_type poly(std::array<real_type, N> const &c, real_type const x, std::integral_constant<unsigned long, K>) noexcept
{
    return std::get<K>(c) + ::poly(c, x, std::integral_constant<unsigned long, K + 1>{}) * x;
}
template <unsigned long N>
inline real_type poly(std::array<real_type, N> const &c, real_type const x) noexcept
{
    return ::poly(c, x, std::integral_constant<unsigned long, 0UL>{});
}

// MARK: Scaled Modified Bessel Function 0
inline real_type scaledI0(real_type const x) noexcept
{
    constexpr real_type lim = 15., lim2 = lim * lim;
    auto const          ax = std::abs(x);
    if (ax < lim) {
        auto const y = x * x;
        return ::poly(::i0p, y) * std::exp(-ax) / (::poly(::i0q, lim2 - y));
    } else {
        auto const z = 1. - lim / ax;
        return ::poly(::i0pp, z) / (::poly(::i0qq, z) * std::sqrt(ax));
    }
}

// MARK: Scaled Modified Bessel Function 1
inline real_type scaledI1(real_type const x) noexcept
{
    constexpr real_type lim = 15., lim2 = lim * lim;
    if (auto const ax = std::abs(x); ax < lim) {
        auto const y = x * x;
        return x * ::poly(::i1p, y) * std::exp(-ax) / (::poly(::i1q, lim2 - y));
    } else {
        auto const z   = 1. - lim / ax;
        auto const ans = ::poly(::i1pp, z) / (::poly(::i1qq, z) * std::sqrt(ax));
        return x > 0. ? ans : -ans;
    }
}
} // namespace

// MARK: Implementation
double Disp::Bessel::_scaledIn(int const n, double const x) noexcept
{
    if (0 == n) {
        return ::scaledI0(x);
    } else if (1 == n) {
        return ::scaledI1(x);
    } else if (x * x <= 8. * std::numeric_limits<double>::min()) {
        return 0.;
    }

    constexpr double ACC  = 200.; // determines accuracy
    constexpr int    IEXP = std::numeric_limits<double>::max_exponent / 2;
    auto const       tox  = 2. / std::abs(x);
    double           bip, bi, ans;
    bip = ans = 0.;
    bi        = 1.;
    for (int j = 2 * (n + int(std::sqrt(ACC * n))); j > 0; --j) {
        auto const bim = bip + j * tox * bi;
        bip            = bi;
        bi             = bim;
        int k;
        std::frexp(bi, &k);
        if (k > IEXP) {
            ans = std::ldexp(ans, -IEXP);
            bi  = std::ldexp(bi, -IEXP);
            bip = std::ldexp(bip, -IEXP);
        }
        if (n == j) {
            ans = bip;
        }
    }
    ans *= ::scaledI0(x) / bi;
    return (x < 0. && (n & 1) ? -ans : ans);
}
#endif

// MARK: BesselI::_AbscissaRange
//
struct Disp::BesselI::_AbscissaRange {
    static double offset(unsigned const n) noexcept
    {
        static BesselJ::zero_table_type const &zeros = BesselJ::zeros();
        return zeros[n].first; // start from the first zero
    }

    Utility::Vector<double, 2> alim;
    unsigned                   key;

    _AbscissaRange(unsigned const n, double const a, double const b) noexcept
    {
        double const x0 = offset(n);
        double const x  = a * b;
        if (x < x0) {
            key    = 0;
            alim.x = 0.;
            alim.y = x0;
        } else {
            constexpr double interval = 2. * M_PI * _n_oscillations;

            key    = static_cast<unsigned>((x - x0) / interval) + 1;
            alim.x = x0 + (key - 1) * interval;
            alim.y = x0 + (key - 0) * interval;
        }
        alim /= b;
    }
};

// MARK: BesselI
//
auto Disp::BesselI::shared() -> BesselI const &
{
    static Disp::BesselI const *shared;
    static pthread_once_t       once = PTHREAD_ONCE_INIT;
    if (pthread_once(&once, []() {
            shared = new BesselI;
        })) {
        throw std::runtime_error(__PRETTY_FUNCTION__);
    }
    return *shared;
}

Disp::BesselI::BesselI()
: _table(new _OuterLookupTable)
{
}

Disp::BesselI::BesselI(BesselI const &o)
: BesselI()
{
    for (unsigned long i = 0; this->_table->size(); ++i) {
        // note that mutex lock cannot be copied
        (*this->_table)[i].second = (*o._table)[i].second;
    }
}
auto Disp::BesselI::operator=(BesselI const &o) -> BesselI &
{
    if (this != &o) {
        BesselI tmp(o);
        *this = std::move(tmp);
    }
    return *this;
}

auto Disp::BesselI::_scaledI(const unsigned n, const real_type x) const -> real_type
{
#if DEBUG
    if (x < 0.) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - x < 0");
    }
#endif
    if (n >= std::tuple_size<BesselJ::zero_table_type>::value) {
        return Bessel::scaledI(int(n), x);
    } else {
        real_type const a = std::sqrt(2. * x);
        // if (!range.key) return Bessel::scaledI(int(n), .5*a*a);
        if (a < 1.0)
            return Bessel::scaledI(int(n), .5 * a * a); // avoid too small a

        constexpr double bmin = 1;
        return *_interpolator(n, { n, a, bmin })(a);
    }
}

namespace {
using Sampler = Utility::AdaptiveSampling1D<double>;
Sampler const &sampler()
{
    static Sampler       *sampler;
    static pthread_once_t once = PTHREAD_ONCE_INIT;
    if (pthread_once(&once, []() -> void {
            sampler                          = new Sampler;
            sampler->maxRecursion            = 100;
            sampler->accuracyGoal            = 10;
            sampler->initialPoints           = 50;
            sampler->yScaleAbsoluteTolerance = 1e-15;
        })) {
        throw std::runtime_error(__PRETTY_FUNCTION__);
    }
    return *sampler;
}
} // namespace
auto Disp::BesselI::_interpolator(unsigned const n, _AbscissaRange const &range) const
    -> _Interpolator &
{
    // FIXME: Locking seems necessary even though it is not supposed to be called from multiple thread with the same n...
    Utility::LockG<Utility::SpinLock> l((*_table)[n].first); // the lock is unique in index n, but not in abscissa range

    _Interpolator &interp = (*_table)[n].second[range.key];
    if (!interp) {
        ::Sampler const                 &sampler = ::sampler();
        ::Sampler::point_list_type const points  = sampler(
             [n](real_type const a) -> real_type {
                return Bessel::scaledI(int(n), .5 * a * a);
             },
             range.alim[0], range.alim[1]);

        interp = Utility::CubicSplineCoefficient<real_type>(points.begin(), points.end()).interpolator();
    }
    return interp;
}
