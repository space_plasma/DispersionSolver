/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "DISPSquareMatrix.h"
#include <algorithm>

double Disp::_norm(CX::Matrix const &m, std::integral_constant<long, 1L>)
{
    Utility::Vector<double, 3> const abs{
        std::abs(::get<0, 0>(m)) + std::abs(::get<1, 0>(m)) + std::abs(::get<2, 0>(m)),
        std::abs(::get<0, 1>(m)) + std::abs(::get<1, 1>(m)) + std::abs(::get<2, 1>(m)),
        std::abs(::get<0, 2>(m)) + std::abs(::get<1, 2>(m)) + std::abs(::get<2, 2>(m))
    };
    return *std::max_element(abs.begin(), abs.end());
}

CX::Number Disp::_determinant(CX::Matrix const &m)
{
    return (::get<0, 1>(m) * ::get<1, 2>(m) * ::get<2, 0>(m) + ::get<0, 2>(m) * ::get<1, 0>(m) * ::get<2, 1>(m) + ::get<0, 0>(m) * ::get<1, 1>(m) * ::get<2, 2>(m)) - (::get<0, 2>(m) * ::get<1, 1>(m) * ::get<2, 0>(m) + ::get<0, 0>(m) * ::get<1, 2>(m) * ::get<2, 1>(m) + ::get<0, 1>(m) * ::get<1, 0>(m) * ::get<2, 2>(m));
}

CX::Number Disp::_trace(CX::Matrix const &m)
{
    return ::get<0, 0>(m) + ::get<1, 1>(m) + ::get<2, 2>(m);
}

CX::Matrix Disp::_transpose(CX::Matrix const &m)
{
    return {
        { ::get<0, 0>(m), ::get<1, 0>(m), ::get<2, 0>(m) },
        { ::get<0, 1>(m), ::get<1, 1>(m), ::get<2, 1>(m) },
        { ::get<0, 2>(m), ::get<1, 2>(m), ::get<2, 2>(m) }
    };
}

CX::Matrix Disp::_inner_product(CX::Matrix const &a, CX::Matrix const &b)
{
    return {
        { ::get<0, 0>(a) * ::get<0, 0>(b) + ::get<0, 1>(a) * ::get<1, 0>(b) + ::get<0, 2>(a) * ::get<2, 0>(b),
          ::get<0, 0>(a) * ::get<0, 1>(b) + ::get<0, 1>(a) * ::get<1, 1>(b) + ::get<0, 2>(a) * ::get<2, 1>(b),
          ::get<0, 0>(a) * ::get<0, 2>(b) + ::get<0, 1>(a) * ::get<1, 2>(b) + ::get<0, 2>(a) * ::get<2, 2>(b) },
        { ::get<0, 0>(b) * ::get<1, 0>(a) + ::get<1, 0>(b) * ::get<1, 1>(a) + ::get<1, 2>(a) * ::get<2, 0>(b),
          ::get<0, 1>(b) * ::get<1, 0>(a) + ::get<1, 1>(a) * ::get<1, 1>(b) + ::get<1, 2>(a) * ::get<2, 1>(b),
          ::get<0, 2>(b) * ::get<1, 0>(a) + ::get<1, 1>(a) * ::get<1, 2>(b) + ::get<1, 2>(a) * ::get<2, 2>(b) },
        { ::get<0, 0>(b) * ::get<2, 0>(a) + ::get<1, 0>(b) * ::get<2, 1>(a) + ::get<2, 0>(b) * ::get<2, 2>(a),
          ::get<0, 1>(b) * ::get<2, 0>(a) + ::get<1, 1>(b) * ::get<2, 1>(a) + ::get<2, 1>(b) * ::get<2, 2>(a),
          ::get<0, 2>(b) * ::get<2, 0>(a) + ::get<1, 2>(b) * ::get<2, 1>(a) + ::get<2, 2>(a) * ::get<2, 2>(b) }
    };
}
CX::Vector Disp::_inner_product(CX::Matrix const &m, CX::Vector const &v)
{
    return {
        ::get<0, 0>(m) * ::get<0>(v) + ::get<0, 1>(m) * ::get<1>(v) + ::get<0, 2>(m) * ::get<2>(v),
        ::get<1, 0>(m) * ::get<0>(v) + ::get<1, 1>(m) * ::get<1>(v) + ::get<1, 2>(m) * ::get<2>(v),
        ::get<2, 0>(m) * ::get<0>(v) + ::get<2, 1>(m) * ::get<1>(v) + ::get<2, 2>(m) * ::get<2>(v)
    };
}

CX::Matrix Disp::_adjugate(CX::Matrix const &m)
{
    CX::Number const tr   = _trace(m);
    CX::Matrix const mm   = _inner_product(m, m);
    CX::Matrix const diag = ::eye() * .5 * (tr * tr - _trace(mm));
    return diag - m * tr + mm;
}
