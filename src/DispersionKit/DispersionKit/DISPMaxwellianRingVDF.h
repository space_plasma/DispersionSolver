/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <DispersionKit/DISPMaxwellianPDF.h>
#include <DispersionKit/DISPVDF.h>
#include <DispersionKit/DispersionKit-config.h>
#include <memory>
#include <tuple>

DISPERSIONKIT_BEGIN_NAMESPACE
struct MaxwellianRingVDF : public VDF {
    ~MaxwellianRingVDF();
    explicit MaxwellianRingVDF(real_type const Oc, complex_type const &op, real_type const th)
    : MaxwellianRingVDF(Oc, op, th, th) {}
    explicit MaxwellianRingVDF(real_type const Oc, complex_type const &op, real_type const th1, real_type const th2)
    : MaxwellianRingVDF(Oc, op, th1, th2, 0.) {}
    explicit MaxwellianRingVDF(real_type const Oc, complex_type const &op, real_type const th1, real_type const th2, real_type const vd)
    : MaxwellianRingVDF(Oc, op, th1, th2, vd, 0.) {}
    explicit MaxwellianRingVDF(real_type const Oc, complex_type const &op, real_type const th1, real_type const th2, real_type const vd, real_type const vr);

    /** @name Overrides
     *  Overrides of VDF virtual functions.
     */
    ///@{
    std::string description() const override;

    real_type    Oc() const override { return _Oc; }
    complex_type op() const override { return _op; }

    real_type vd() const override { return _vd; }
    real_type T1() const override { return .5 * _th1 * _th1; }
    real_type T2() const override;

    real_type f(real_type const v1, real_type const v2) const override;
    real_type dfdv1(real_type const v1, real_type const v2) const override;
    real_type dfdv2(real_type const v1, real_type const v2) const override;

    void clear_cache() override;

    matrix_type K(std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const override;
    ///@}

    struct _FLRWrapper; //!< Private class for RingFLR wrapper.
private:
    complex_type                 _op;
    real_type                    _Oc, _th1, _th2, _vd, _vr;
    MaxwellianPDF                _pdf;
    std::unique_ptr<_FLRWrapper> _flr;

    // FLR::ABCPQR are already cached so no need for argument pass
    using triplet_type = std::tuple<complex_type, complex_type, complex_type>;

    inline void K11(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const;
    inline void K11(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j, triplet_type const &Z) const;

    inline void K12(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const;
    inline void K12(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j, triplet_type const &Z) const;

    inline void K13(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const;
    inline void K13(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j, triplet_type const &Z) const;

    inline void K22(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const;
    inline void K22(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j, triplet_type const &Z) const;

    inline void K23(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const;
    inline void K23(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j, triplet_type const &Z) const;

    inline void K31(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const;
    inline void K31(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j, triplet_type const &Z) const;

    inline void K32(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const;
    inline void K32(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j, triplet_type const &Z) const;

    inline void K33(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const;
    inline void K33(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j, triplet_type const &Z) const;
};
DISPERSIONKIT_END_NAMESPACE
