/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <DispersionKit/DISPBesselJ.h>
#include <DispersionKit/DispersionKit-config.h>
#include <UtilityKit/UtilityKit.h>
#include <array>
#include <memory>
#include <unordered_map>

DISPERSIONKIT_BEGIN_NAMESPACE
/**
 @brief Evaluates integrals involving finite Larmor radius effects for a Lorentzian (kappa) distribution function,
 f(x) = (1+x^2/κ)^-(κ+1)/π, where κ > 1.
 @discussion The evaluation results for a given parameter set are automatically cached.
 */
struct LorentzianFLR {
    virtual ~LorentzianFLR();
    LorentzianFLR(LorentzianFLR &&) noexcept = default;
    LorentzianFLR &operator=(LorentzianFLR &&) noexcept = default;
    LorentzianFLR(LorentzianFLR const &);
    LorentzianFLR &operator=(LorentzianFLR const &);

    /**
     @brief Construct with kappa index.
     */
    explicit LorentzianFLR(double const kappa);

    /**
     @brief Clear the lookup cache.
     */
    void clear_cache();

    /**
     @brief Calculate f(x).
     */
    double f(double const x) const noexcept;

    /**
     @brief Calculate df(x)/dx.
     */
    double dfdx(double const x) const noexcept;

    /**
     @brief Evaluates integral of 2π*J(n, ax)^2*f(x)*x from 0 to inf.
     */
    inline double A(int const n, double const a) const;
    /**
     @brief Evaluates integral of 2π*J(n, ax)^2*f'(x) from 0 to inf.
     */
    inline double P(int const n, double const a) const;

    /**
     @brief Evaluates integral of 2π*J(n, ax)*J'(n, ax)*f(x)*x^2 from 0 to inf.
     */
    inline double B(int const n, double const a) const;
    /**
     @brief Evaluates integral of 2π*J(n, ax)*J'(n, ax)*f'(x)*x from 0 to inf.
     */
    inline double Q(int const n, double const a) const;

    /**
     @brief Evaluates integral of 2π*J'(n, ax)^2*f(x)*x^3 from 0 to inf.
     */
    inline double C(int const n, double const a) const;
    /**
     @brief Evaluates integral of 2π*J'(n, ax)^2*f'(x)*x^2 from 0 to inf.
     */
    inline double R(int const n, double const a) const;

    /**
     @brief Evaluates A(n, a)/a^2 correctly handling the case where |a| -> 0.
     @discussion It is undefined when n = a = 0.
     */
    double AOa2(int const n, double const a) const;
    /**
     @brief Evaluates P(n, a)/a^2 correctly handling the case where |a| -> 0.
     @discussion It is undefined when n = a = 0.
     */
    double POa2(int const n, double const a) const;

    /**
     @brief Evaluates B(n, a)/a correctly handling the case where |a| -> 0.
     */
    double BOa(int const n, double const a) const;
    /**
     @brief Evaluates Q(n, a)/a correctly handling the case where |a| -> 0.
     */
    double QOa(int const n, double const a) const;

    /**
     @brief Returns the kappa index.
     */
    double kappa() const noexcept { return _k; }
    /**
     @brief Returns the instance of BesselJ being used to evaluate the integrals.
     @note Currently, it is BesselJ::shared().
     */
    BesselJ const &J() const noexcept { return *_J; }

protected:
    using value_pack_type   = Utility::Vector<double, 6>; // {A_n(a), P_n(a), B_n(a), Q_n(a), C_n(a), R_n(a)}
    using optional_type     = Utility::Optional<value_pack_type>;
    using _InnerLookupTable = std::unordered_map<double, optional_type>;
    using _OuterLookupTable = std::array<_InnerLookupTable, std::tuple_size<BesselJ::zero_table_type>::value>;

    double                             _k;
    BesselJ const                     *_J;
    std::unique_ptr<_OuterLookupTable> _lookup_table;
    double                             _max_x;
    double                             _zero_tolerance;
    constexpr static double            _epsilon = 1e-13; //!< Used to determines the maximum x.

    explicit LorentzianFLR(double const kappa, BesselJ const *J);

    // n >= 0 and a >= 0
    double _A(int const n, double const a) const;
    double _P(int const n, double const a) const;

    double _B(int const n, double const a) const;
    double _Q(int const n, double const a) const;

    double _C(int const n, double const a) const;
    double _R(int const n, double const a) const;

    // returns {A_n(a), P_n(a), B_n(a), Q_n(a), C_n(a), R_n(a)}
    value_pack_type const   _lookup_APBQCR(int const n, double const a) const;
    virtual value_pack_type _eval_APBQCR(int const n, double const a) const;
};

double LorentzianFLR::A(int const n, double const a) const
{
    return _A(n < 0 ? -n : n, a < 0. ? -a : a);
}
double LorentzianFLR::P(int const n, double const a) const
{
    return _P(n < 0 ? -n : n, a < 0. ? -a : a);
}

double LorentzianFLR::B(int const n, double const a) const
{
    double const a_sign = a < 0. ? -1 : 1;
    return _B(n < 0 ? -n : n, a_sign * a) * a_sign;
}
double LorentzianFLR::Q(int const n, double const a) const
{
    double const a_sign = a < 0. ? -1 : 1;
    return _Q(n < 0 ? -n : n, a_sign * a) * a_sign;
}

double LorentzianFLR::C(int const n, double const a) const
{
    return _C(n < 0 ? -n : n, a < 0. ? -a : a);
}
double LorentzianFLR::R(int const n, double const a) const
{
    return _R(n < 0 ? -n : n, a < 0. ? -a : a);
}
DISPERSIONKIT_END_NAMESPACE
