/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "DISPRealKappaPDF.h"
#include <cmath>
#include <limits>
#include <stdexcept>

namespace {
inline double C(double const k) noexcept
{
    double A;
    if (k > 100.) { // asymptotic expansion
        A = 1 + 1 / (8. * k);
    } else {
        A = std::sqrt(k) * std::tgamma(k) / std::tgamma(k + .5);
    }
    return A *= .5 * M_2_SQRTPI;
}
template <class T>
inline T f(double const k, T const &x) noexcept
{
    return C(k) * std::pow(1. + x * x / k, -(k + 1));
}
template <class T>
inline T df(double const k, T const &x) noexcept
{
    return -2. * x * (k + 1) / (k + x * x) * f(k, x);
}
} // namespace

auto Disp::RealKappaPDF::f(real_type const x) const noexcept -> real_type
{
    return ::f(_k, x);
}
auto Disp::RealKappaPDF::f(complex_type const &z) const noexcept -> complex_type
{
    return ::f(_k, z);
}

auto Disp::RealKappaPDF::dfdx(real_type const x) const noexcept -> real_type
{
    return ::df(_k, x);
}
auto Disp::RealKappaPDF::dfdx(complex_type const &z) const noexcept -> complex_type
{
    return ::df(_k, z);
}

Disp::RealKappaPDF::RealKappaPDF(real_type const kappa)
: _k(kappa)
{
    if (kappa <= .5) {
        throw std::invalid_argument(__PRETTY_FUNCTION__);
    }
}

auto Disp::RealKappaPDF::Ztriplet(complex_type const &z) const -> std::tuple<complex_type, complex_type, complex_type>
{
    complex_type const Z0 = this->Z<0>(z);
    complex_type const Z1 = z * Z0 + 1.;
    complex_type const Z2 = z * Z1;
    return std::make_tuple(Z0, Z1, Z2);
}
auto Disp::RealKappaPDF::Wtriplet(complex_type const &z) const -> std::tuple<complex_type, complex_type, complex_type>
{
    complex_type const s  = std::sqrt(real_type(_k + 1) / _k) * z;
    complex_type const Z1 = s * _Z(_k + 1, s) + 1.;
    complex_type const W0 = -(2. + 1. / _k) * Z1;
    complex_type const W1 = z * W0;
    complex_type const W2 = z * W1 - 1.;
    return std::make_tuple(W0, W1, W2);
}

namespace {
inline std::complex<double> I_sqrt_kxGamma_kp2_Gamma_kp3_2(double const k)
{
    if (k > 100.) { // asymptotic expansion of I/√κ*Γ(κ + 2)/Γ(κ + 3/2)
        return { 0., 1 + 5 / (8. * k) };
    } else {
        return { 0., std::tgamma(k + 2) / (std::sqrt(k) * std::tgamma(k + 1.5)) };
    }
}
} // namespace
auto Disp::RealKappaPDF::_Z(real_type const k, complex_type const &z) noexcept -> complex_type
{
    constexpr complex_type I{ 0., 1. };
    complex_type const     I_sqrt_k = I / std::sqrt(k);
    complex_type const     arg      = .5 * (1. + I_sqrt_k * z);
    if (std::abs(arg) < 1) {
        return (k + .5) / (k + 1) * I_sqrt_k * _2F1(2 * (k + 1), k + 2, arg);
    } else {
        complex_type F = -I_sqrt_k / (1. + I_sqrt_k * z) * _2F1(-k, -2 * k, 1. / arg);
        F -= 2. / M_2_SQRTPI / std::cos(M_PI * k) * (k + .5) / (k + 1) * ::I_sqrt_kxGamma_kp2_Gamma_kp3_2(k)
           * std::pow(-(1. + I_sqrt_k * z) * (1. - I_sqrt_k * z), -(k + 1));
        return F;
    }
}

auto Disp::RealKappaPDF::_2F1(real_type const b, real_type const c, complex_type const &z) noexcept -> complex_type
{
    complex_type _2F1{ 0. }, series{ 1. };
    unsigned     i = 0;
    do {
        _2F1 += series;
        series *= z * (b + i) / (c + i);
        if (std::abs(series) < _reltol * std::abs(_2F1)) {
            return _2F1;
        }
    } while (++i < _max_n);
    return std::numeric_limits<real_type>::quiet_NaN();
}
