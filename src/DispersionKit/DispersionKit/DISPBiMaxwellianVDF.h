/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <DispersionKit/DISPMaxwellianFLR.h>
#include <DispersionKit/DISPMaxwellianPDF.h>
#include <DispersionKit/DISPVDF.h>
#include <DispersionKit/DispersionKit-config.h>
#include <tuple>

DISPERSIONKIT_BEGIN_NAMESPACE
struct BiMaxwellianVDF : public VDF {
    explicit BiMaxwellianVDF(real_type const Oc, complex_type const &op, real_type const th)
    : BiMaxwellianVDF(Oc, op, th, th) {}
    explicit BiMaxwellianVDF(real_type const Oc, complex_type const &op, real_type const th1, real_type const th2)
    : BiMaxwellianVDF(Oc, op, th1, th2, 0.) {}
    explicit BiMaxwellianVDF(real_type const Oc, complex_type const &op, real_type const th1, real_type const th2, real_type const vd);

    /** @name Overrides
     *  Overrides of VDF virtual functions.
     */
    ///@{
    std::string description() const override;

    real_type    Oc() const override { return _Oc; }
    complex_type op() const override { return _op; }

    real_type vd() const override { return _vd; }
    real_type T1() const override { return .5 * _th1 * _th1; }
    real_type T2() const override { return .5 * _th2 * _th2; }

    real_type f(real_type const v1, real_type const v2) const override;
    real_type dfdv1(real_type const v1, real_type const v2) const override;
    real_type dfdv2(real_type const v1, real_type const v2) const override;

    matrix_type K(std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const override;
    ///@}

private:
    using MaxwellianFLR = Disp::InterpMaxwellianFLR;

    complex_type  _op;
    real_type     _Oc, _th1, _th2, _vd;
    MaxwellianPDF _pdf;
    MaxwellianFLR _flr;

    // FLR::ABCPQR are already cached so no need for argument pass
    using triplet_type = std::tuple<complex_type, complex_type, complex_type>;

    inline void K11(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const;
    inline void K11(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j, triplet_type const &Z) const;

    inline void K12(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const;
    inline void K12(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j, triplet_type const &Z) const;

    inline void K13(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const;
    inline void K13(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j, triplet_type const &Z) const;

    inline void K22(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const;
    inline void K22(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j, triplet_type const &Z) const;

    inline void K23(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const;
    inline void K23(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j, triplet_type const &Z) const;

    inline void K31(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const;
    inline void K31(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j, triplet_type const &Z) const;

    inline void K32(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const;
    inline void K32(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j, triplet_type const &Z) const;

    inline void K33(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const;
    inline void K33(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j, triplet_type const &Z) const;
};
DISPERSIONKIT_END_NAMESPACE
