/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "DISPMaxwellianFLR.h"

// MARK: MaxwellianFLR
//
double Disp::MaxwellianFLR::f(double const x) noexcept
{
    return M_1_PI * std::exp(-x * x);
}
double Disp::MaxwellianFLR::dfdx(double const x) noexcept
{
    return -2. * x * f(x);
}

namespace {
inline double scaledIp(int const n, double const x)
{
    return .5 * (Disp::Bessel::scaledI(n - 1, x) + Disp::Bessel::scaledI(n + 1, x));
}
} // namespace

double Disp::MaxwellianFLR::A(int const n, double const a)
{
    double const x = .5 * a * a;
    return Bessel::scaledI(n, x);
}
double Disp::MaxwellianFLR::B(int const n, double const a)
{
    double const x = .5 * a * a;
    return .5 * a * (::scaledIp(n, x) - Bessel::scaledI(n, x));
}
double Disp::MaxwellianFLR::C(int const n, double const a)
{
    return (0 == n ? 0. : double(n) * n * AOa2(n, a)) - a * B(n, a);
}

double Disp::MaxwellianFLR::AOa2(int const n, double const a)
{
    double const x = .5 * a * a;
    if (0 == n) {
        return .5 * Bessel::scaledI(0, x) / x;
    } else {
        return .25 / n * (Bessel::scaledI(n - 1, x) - Bessel::scaledI(n + 1, x));
    }
}
double Disp::MaxwellianFLR::BOa(int const n, double const a)
{
    double const x = .5 * a * a;
    return .5 * (::scaledIp(n, x) - Bessel::scaledI(n, x));
}

// MARK: InterpMaxwellianFLR
//
Disp::InterpMaxwellianFLR::InterpMaxwellianFLR()
: InterpMaxwellianFLR(&BesselI::shared())
{
}

double Disp::InterpMaxwellianFLR::A(int const n, double const a) const
{
    double const x = .5 * a * a;
    return _I->scaled(n, x);
}
double Disp::InterpMaxwellianFLR::B(int const n, double const a) const
{
    double const x = .5 * a * a;
    return .5 * a * (_scaledIp(n, x) - _I->scaled(n, x));
}
double Disp::InterpMaxwellianFLR::C(int const n, double const a) const
{
    return (0 == n ? 0. : double(n) * n * AOa2(n, a)) - a * B(n, a);
}

double Disp::InterpMaxwellianFLR::AOa2(int const n, double const a) const
{
    double const x = .5 * a * a;
    if (0 == n) {
        return .5 * _I->scaled(0, x) / x;
    } else {
        return .25 / n * (_I->scaled(n - 1, x) - _I->scaled(n + 1, x));
    }
}
double Disp::InterpMaxwellianFLR::BOa(int const n, double const a) const
{
    double const x = .5 * a * a;
    return .5 * (_scaledIp(n, x) - _I->scaled(n, x));
}

double Disp::InterpMaxwellianFLR::_scaledIp(int const n, double const x) const
{
    return .5 * (_I->scaled(n - 1, x) + _I->scaled(n + 1, x));
}
