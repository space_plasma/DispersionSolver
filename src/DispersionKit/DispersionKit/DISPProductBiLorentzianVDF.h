/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <DispersionKit/DISPInterpLorentzianFLR.h>
#include <DispersionKit/DISPVDF.h>
#include <DispersionKit/DispersionKit-config.h>
#include <memory>
#include <tuple>

DISPERSIONKIT_BEGIN_NAMESPACE
struct ProductBiLorentzianVDF : public VDF {
    ~ProductBiLorentzianVDF();
    explicit ProductBiLorentzianVDF(real_type const Oc, complex_type const &op, std::pair<real_type, real_type> const kappa, real_type const th)
    : ProductBiLorentzianVDF(Oc, op, kappa, th, th) {}
    explicit ProductBiLorentzianVDF(real_type const Oc, complex_type const &op, std::pair<real_type, real_type> const kappa, real_type const th1, real_type const th2)
    : ProductBiLorentzianVDF(Oc, op, kappa, th1, th2, 0.) {}
    explicit ProductBiLorentzianVDF(real_type const Oc, complex_type const &op, std::pair<real_type, real_type> const kappa, real_type const th1, real_type const th2, real_type const vd);

    /**
     @name Overrides
     Overrides of VDF virtual functions.
     */
    ///@{
    std::string description() const override;

    real_type    Oc() const override { return _Oc; }
    complex_type op() const override { return _op; }

    real_type vd() const override { return _vd; }
    real_type T1() const override;
    real_type T2() const override;

    real_type f(real_type const v1, real_type const v2) const override;
    real_type dfdv1(real_type const v1, real_type const v2) const override;
    real_type dfdv2(real_type const v1, real_type const v2) const override;

    void clear_cache() override;

    matrix_type K(std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const override;
    ///@}

    /**
     @brief A pair of parallel and perpendicular kappa indexes.
     */
    std::pair<real_type, real_type> const &kappa() const noexcept { return _kappa; }

    struct _PDFWrapper;

private:
    using LorentzianFLR = Disp::InterpLorentzianFLR;

    complex_type                    _op;
    real_type                       _Oc, _th1, _th2, _vd;
    std::pair<real_type, real_type> _kappa;
    std::unique_ptr<_PDFWrapper>    _pdf;
    LorentzianFLR                   _flr;

    // rescaling
    double A(int const n, double const a) const { return _flr.A(n, a * _th2); }
    double B(int const n, double const a) const { return _flr.B(n, a * _th2) * _th2; }
    double C(int const n, double const a) const { return _flr.C(n, a * _th2) * _th2 * _th2; }
    double AOa2(int const n, double const a) const { return _flr.AOa2(n, a * _th2) * _th2 * _th2; }
    double BOa(int const n, double const a) const { return _flr.BOa(n, a * _th2) * _th2 * _th2; }
    double P(int const n, double const a) const { return _flr.P(n, a * _th2) / (_th2 * _th2); }
    double Q(int const n, double const a) const { return _flr.Q(n, a * _th2) / _th2; }
    double R(int const n, double const a) const { return _flr.R(n, a * _th2); }
    double POa2(int const n, double const a) const { return _flr.POa2(n, a * _th2); }
    double QOa(int const n, double const a) const { return _flr.QOa(n, a * _th2); }

    // FLR::ABCPQR are already cached so no need for argument pass
    using triplet_type = std::tuple<complex_type, complex_type, complex_type>;

    inline void K11(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const;
    inline void K11(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j, triplet_type const &Z, triplet_type const &W) const;

    inline void K12(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const;
    inline void K12(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j, triplet_type const &Z, triplet_type const &W) const;

    inline void K13(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const;
    inline void K13(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j, triplet_type const &Z, triplet_type const &W) const;

    inline void K22(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const;
    inline void K22(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j, triplet_type const &Z, triplet_type const &W) const;

    inline void K23(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const;
    inline void K23(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j, triplet_type const &Z, triplet_type const &W) const;

    inline void K31(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const;
    inline void K31(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j, triplet_type const &Z, triplet_type const &W) const;

    inline void K32(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const;
    inline void K32(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j, triplet_type const &Z, triplet_type const &W) const;

    inline void K33(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const;
    inline void K33(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j, triplet_type const &Z, triplet_type const &W) const;
};
DISPERSIONKIT_END_NAMESPACE
