/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <DispersionKit/DispersionKit-config.h>
#include <complex>
#include <tuple>
#include <type_traits>

DISPERSIONKIT_BEGIN_NAMESPACE
/**
 @brief Plasma dispersion function for a kappa (generalized Lorentzian) distribution function,
 f(x) = √κΓ(κ)/Γ(κ+1/2)*(1+x^2/κ)^-(κ+1)/√π, where κ is an integer greater than or equal to 1.
 @discussion This version uses closed form derived by Summers and Thorne (1991).
 */
struct IntegerKappaPDF {
    using real_type    = double;
    using complex_type = std::complex<real_type>;

    //@{
    /**
     @brief Calculate f(x).
     */
    real_type    f(real_type const x) const noexcept;
    complex_type f(complex_type const &z) const noexcept;
    //@}

    //@{
    /**
     @brief Calculate df(x)/dx.
     */
    real_type    dfdx(real_type const x) const noexcept;
    complex_type dfdx(complex_type const &z) const noexcept;
    //@}

    /**
     @brief Evaluates integral of f(x)*x^N/(x - z).
     */
    template <long N>
    complex_type Z(complex_type const &z) const noexcept { return _Z(z, std::integral_constant<long, N>{}); }

    /**
     @brief Evaluates Z<0>, Z<1> and Z<2> using the recurring algorithm.
     */
    std::tuple<complex_type, complex_type, complex_type>
    Ztriplet(complex_type const &z) const;
    /**
     @brief Evaluates W<0>, W<1> and W<2> using the recurring algorithm.
     */
    std::tuple<complex_type, complex_type, complex_type>
    Wtriplet(complex_type const &z) const;

    /**
     @brief Returns the kappa index.
     */
    real_type kappa() const noexcept { return _k; }

    /**
     @brief Construct kappa PDF with a positive kappa index.
     @exception An std::invalid_argument exception if kappa < 1.
     */
    explicit IntegerKappaPDF(int const kappa); // int type is used, unstead of unsigned, to thrown an exception when type-cased negative real kappa is passed by the caller

private:
    unsigned long              _k;
    constexpr static real_type _reltol = 1e-12;

    complex_type _Z(complex_type const &z, std::integral_constant<long, 0L> const) const noexcept { return _Z(_k, z); }
    complex_type _Z(complex_type const &z, std::integral_constant<long, 1L> const) const noexcept { return 1. + z * Z<0>(z); }
    complex_type _Z(complex_type const &z, std::integral_constant<long, 2L> const) const noexcept { return z * Z<1>(z); }
    complex_type _Z(complex_type const &z, std::integral_constant<long, 3L> const) const noexcept { return _k / (2. * _k - 1) + z * Z<2>(z); }

    static complex_type _Z(unsigned long k, complex_type const &z) noexcept;
};
DISPERSIONKIT_END_NAMESPACE
