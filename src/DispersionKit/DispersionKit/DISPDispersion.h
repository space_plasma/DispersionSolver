/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <DispersionKit/DISPVDF.h>
#include <DispersionKit/DispersionKit-config.h>
#include <complex>
#include <functional>
#include <memory>
#include <string>
#include <utility>
#include <vector>

DISPERSIONKIT_BEGIN_NAMESPACE
struct Dispersion {
    using real_type         = double;
    using complex_type      = std::complex<real_type>;
    using matrix_type       = Utility::Vector<Utility::Vector<complex_type, 3>, 3>;
    using step_monitor_type = std::function<bool(unsigned /*j*/, matrix_type /*K*/)>;

    static constexpr real_type abstol = 1e-10; //!< Absolute tolerance.
    static constexpr real_type reltol = 1e-5;  //!< Relative tolerance.

    virtual ~Dispersion();
    explicit Dispersion(real_type const c);

    virtual std::string description() const;
    virtual void        clear_cache();

    real_type                c() const { return _c; }
    unsigned                 jmin() const noexcept { return _jminmax.first; }
    unsigned                 jmax() const noexcept { return _jminmax.second; }
    std::vector<VDF const *> vdfs() const;

    void set_jmin(unsigned const jmin) noexcept { _jminmax.first = !jmin ? 1 : jmin; } // jmin >= 1
    // void set_jmax(unsigned const jmax) noexcept { _jminmax.second = jmax; }

    matrix_type D(std::pair<real_type, real_type> const &k /*{k1, k2}*/, complex_type const &o, step_monitor_type const &step_monitor) const;
    matrix_type o2_x_D(std::pair<real_type, real_type> const &k /*{k1, k2}*/, complex_type const &o, step_monitor_type const &step_monitor) const;

    template <class ConcreteVDF, class... Args>
    void addVDF(Args &&...args) { _vdfs.push_back(_createVDF<ConcreteVDF>(std::forward<Args>(args)...)); }
    // void deleteVDF(VDF const* p);

private:
    std::vector<std::unique_ptr<VDF>> _vdfs;
    real_type                         _c;
    std::pair<unsigned, unsigned>     _jminmax;

    template <class ConcreteVDF, class... Args>
    std::unique_ptr<VDF> _createVDF(Args &&...args) { return std::unique_ptr<VDF>{ new ConcreteVDF(std::forward<Args>(args)...) }; }

    enum _Concurrency {
        serial     = 0,
        concurrent = 1
    };
    constexpr static _Concurrency concurrency = concurrent;

    matrix_type _o2_x_epsilon(std::pair<real_type, real_type> const &k, complex_type const &o, step_monitor_type const &step_monitor) const;
    matrix_type _o2_x_epsilon(std::pair<real_type, real_type> const &k, complex_type const &o, step_monitor_type const &step_monitor, _Concurrency const concurrency) const;
    matrix_type _o2_x_epsilon(std::pair<real_type, real_type> const &k, complex_type const &o, int const j, _Concurrency const concurrency) const;
};
DISPERSIONKIT_END_NAMESPACE
