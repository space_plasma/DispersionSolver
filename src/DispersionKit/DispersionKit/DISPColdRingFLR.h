/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <DispersionKit/DISPBesselJ.h>
#include <DispersionKit/DispersionKit-config.h>
#include <UtilityKit/UtilityKit.h>

DISPERSIONKIT_BEGIN_NAMESPACE
/**
 @brief Evaluates integrals involving finite Larmor radius effects for a cold ring distribution function,
 f(x) = δ(x-vr)/2π*vr.
 */
struct ColdRingFLR {
    ColdRingFLR(ColdRingFLR &&) noexcept = default;
    ColdRingFLR &operator=(ColdRingFLR &&) noexcept = default;
    ColdRingFLR(ColdRingFLR const &) noexcept       = default;
    ColdRingFLR &operator=(ColdRingFLR const &) noexcept = default;

    /**
     @brief Construct with a ring speed, vr.
     */
    explicit ColdRingFLR(double const vr);

    /**
     @brief Default constructor with vr = 0.
     */
    explicit ColdRingFLR()
    : ColdRingFLR(0.) {}

    /**
     @brief Calculate f(x).
     */
    double f(double const x) const noexcept;

    /**
     @brief Calculate df(x)/dx.
     @return NaN.
     */
    double dfdx(double const x) const noexcept;

    /**
     @brief Evaluates integral of 2π*J(n, ax)^2*f(x)*x from 0 to inf.
     */
    double A(int const n, double const a) const;
    /**
     @brief Evaluates integral of 2π*J(n, ax)^2*f'(x) from 0 to inf.
     */
    double P(int const n, double const a) const;

    /**
     @brief Evaluates integral of 2π*J(n, ax)*J'(n, ax)*f(x)*x^2 from 0 to inf.
     */
    double B(int const n, double const a) const;
    /**
     @brief Evaluates integral of 2π*J(n, ax)*J'(n, ax)*f'(x)*x from 0 to inf.
     */
    double Q(int const n, double const a) const;

    /**
     @brief Evaluates integral of 2π*J'(n, ax)^2*f(x)*x^3 from 0 to inf.
     */
    double C(int const n, double const a) const;
    /**
     @brief Evaluates integral of 2π*J'(n, ax)^2*f'(x)*x^2 from 0 to inf.
     */
    double R(int const n, double const a) const;

    /**
     @brief Evaluates A(n, a)/a^2 correctly handling the case where |a| -> 0.
     @discussion It is undefined when n = a = 0.
     */
    double AOa2(int const n, double const a) const;
    /**
     @brief Evaluates P(n, a)/a^2 correctly handling the case where |a| -> 0.
     */
    double POa2(int const n, double const a) const;

    /**
     @brief Evaluates B(n, a)/a correctly handling the case where |a| -> 0.
     */
    double BOa(int const n, double const a) const;
    /**
     @brief Evaluates Q(n, a)/a correctly handling the case where |a| -> 0.
     */
    double QOa(int const n, double const a) const;

    /**
     @brief Returns the parameter vr.
     */
    double vr() const noexcept { return _vr; }
    /**
     @brief Returns the instance of BesselJ being used to evaluate the integrals.
     @note Currently, it is BesselJ::shared().
     */
    BesselJ const &J() const noexcept { return *_J; }

private:
    explicit ColdRingFLR(double const vr, BesselJ const *J)
    : _vr(vr), _J(J) {}
    double         _vr;
    BesselJ const *_J;

    double _Jp(int const n, double const x) const;
    double _Jpp(int const n, double const x) const;
};
DISPERSIONKIT_END_NAMESPACE
