/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "DISPLorentzianFLR.h"
#include <cmath>
#include <limits>
#include <pthread.h>
#include <stdexcept>
#include <utility>

namespace {
inline double f(double const k, double const x) noexcept
{
    return std::pow(1. + x * x / k, -(k + 1)) / M_PI;
}
} // namespace
double Disp::LorentzianFLR::f(double const x) const noexcept
{
    return ::f(_k, x);
}
double Disp::LorentzianFLR::dfdx(double const x) const noexcept
{
    return -2. * x * (_k + 1) / (_k + x * x) * this->f(x);
}

Disp::LorentzianFLR::~LorentzianFLR()
{
}

Disp::LorentzianFLR::LorentzianFLR(LorentzianFLR const &o)
: LorentzianFLR(o._k, o._J)
{
    *_lookup_table = *o._lookup_table;
    // the rest is done in the constructor
}
auto Disp::LorentzianFLR::operator=(LorentzianFLR const &o) -> LorentzianFLR &
{
    if (this != &o) {
        LorentzianFLR tmp(o);
        *this = std::move(tmp);
    }
    return *this;
}

Disp::LorentzianFLR::LorentzianFLR(double const kappa)
: LorentzianFLR(kappa, &BesselJ::shared())
{
}
Disp::LorentzianFLR::LorentzianFLR(double const kappa, BesselJ const *J)
: _k(std::numeric_limits<double>::quiet_NaN())
{
    if (kappa <= 1.) {
        throw std::invalid_argument(__PRETTY_FUNCTION__);
    }
    _k = kappa;
    _J = J;
    _lookup_table.reset(new _OuterLookupTable);
    _max_x          = std::sqrt((std::pow(_epsilon, -1. / (1 + _k)) - 1) * _k);
    _zero_tolerance = 1e-5 / _max_x;
}

void Disp::LorentzianFLR::clear_cache()
{
    for (auto &map : *_lookup_table) {
        map.clear();
    }
}

double Disp::LorentzianFLR::AOa2(int const n, double const a) const
{
    if (std::abs(a) < _zero_tolerance) { // small argument expansion
        if (n == 0) {
            return std::numeric_limits<double>::quiet_NaN();
        } else if ((n == 1) | (n == -1)) {
            return .25 * _k / (_k - 1);
        } else {
            return 0.;
        }
    } else { // normal division
        return this->A(n, a) / (a * a);
    }
}
double Disp::LorentzianFLR::POa2(int const n, double const a) const
{
    if (std::abs(a) < _zero_tolerance) { // small argument expansion
        if (n == 0) {
            return std::numeric_limits<double>::quiet_NaN();
        } else if ((n == 1) | (n == -1)) {
            return -.5;
        } else {
            return 0.;
        }
    } else { // normal division
        return this->P(n, a) / (a * a);
    }
}

double Disp::LorentzianFLR::BOa(int const n, double const a) const
{
    if (std::abs(a) < _zero_tolerance) { // small argument expansion
        if (n == 0) {
            return -.5 * _k / (_k - 1);
        } else if ((n == 1) | (n == -1)) {
            return .25 * _k / (_k - 1);
        } else {
            return 0.;
        }
    } else { // normal division
        return this->B(n, a) / a;
    }
}
double Disp::LorentzianFLR::QOa(int const n, double const a) const
{
    if (std::abs(a) < _zero_tolerance) { // small argument expansion
        if (n == 0) {
            return 1.;
        } else if ((n == 1) | (n == -1)) {
            return -.5;
        } else {
            return 0.;
        }
    } else { // normal division
        return this->Q(n, a) / a;
    }
}

double Disp::LorentzianFLR::_A(int const n, double const a) const
{
#ifdef DEBUG
    if (n < 0 || a < 0.) {
        throw std::invalid_argument(__PRETTY_FUNCTION__);
    }
#endif
    return std::get<0>(_lookup_APBQCR(n, a));
}
double Disp::LorentzianFLR::_P(int const n, double const a) const
{
#ifdef DEBUG
    if (n < 0 || a < 0.) {
        throw std::invalid_argument(__PRETTY_FUNCTION__);
    }
#endif
    return std::get<1>(_lookup_APBQCR(n, a));
}

double Disp::LorentzianFLR::_B(int const n, double const a) const
{
#ifdef DEBUG
    if (n < 0 || a < 0.) {
        throw std::invalid_argument(__PRETTY_FUNCTION__);
    }
#endif
    return std::get<2>(_lookup_APBQCR(n, a));
}
double Disp::LorentzianFLR::_Q(int const n, double const a) const
{
#ifdef DEBUG
    if (n < 0 || a < 0.) {
        throw std::invalid_argument(__PRETTY_FUNCTION__);
    }
#endif
    return std::get<3>(_lookup_APBQCR(n, a));
}

double Disp::LorentzianFLR::_C(int const n, double const a) const
{
#ifdef DEBUG
    if (n < 0 || a < 0.) {
        throw std::invalid_argument(__PRETTY_FUNCTION__);
    }
#endif
    return std::get<4>(_lookup_APBQCR(n, a));
}
double Disp::LorentzianFLR::_R(int const n, double const a) const
{
#ifdef DEBUG
    if (n < 0 || a < 0.) {
        throw std::invalid_argument(__PRETTY_FUNCTION__);
    }
#endif
    return std::get<5>(_lookup_APBQCR(n, a));
}

auto Disp::LorentzianFLR::_lookup_APBQCR(int const n, double const a) const -> value_pack_type const
{
    optional_type _, *p{ &_ };
    if (n < 0 || unsigned(n) >= std::tuple_size<_OuterLookupTable>::value || !*(p = &(*_lookup_table)[unsigned(n)][a])) {
        *p = _eval_APBQCR(n, a);
    }
    return **p;
}

namespace {
// BesselJ derivatives
inline double Jp(Disp::BesselJ const &J, int const n, double const x)
{
    return .5 * (J(n - 1, x) - J(n + 1, x));
}

// GaussLegendre integrator
constexpr long N = 10;
using Integrator = Utility::GaussLegendreIntegrator<double, N>;
Integrator const &integrator()
{
    static Integrator const *integrator;
    static pthread_once_t    once = PTHREAD_ONCE_INIT;
    if (pthread_once(&once, []() {
            integrator = new Integrator;
        })) {
        throw std::runtime_error(__PRETTY_FUNCTION__);
    }
    return *integrator;
}

// piecewise integral
using Vector = Utility::Vector<double, 6>;
template <class F>
auto integrate(double const a /*>=0*/, std::pair<double, double> const x_lim, F const &f) -> Vector
{
    constexpr double interval = 2. * M_PI;
    double const     abs_a    = std::abs(a);
    double const     dx       = (abs_a < interval ? 1. : interval / abs_a); // unit interval of each piece
    double           x1       = x_lim.first;
    double           x0       = x1;
    Vector           result{};
    for (x1 += dx; x0 < x_lim.second; x0 = x1, x1 += dx) {
        result = integrator().operator()<F const &>(f, x0, x1, result);
    }
    return result;
}
} // namespace
auto Disp::LorentzianFLR::_eval_APBQCR(int const n, double const a) const
    -> value_pack_type
{
    value_pack_type result = ::integrate(a, { 0., _max_x }, [n, a, this](double const x) -> value_pack_type {
        double const    J     = (*this->_J)(n, a * x);
        double const    xJp   = x * ::Jp(*this->_J, n, a * x);
        double const    ratio = -2. * (_k + 1) / (_k + x * x);
        value_pack_type v{ J * J, J * J * ratio, J * xJp, J * xJp * ratio, xJp * xJp, xJp * xJp * ratio };
        return v *= x * std::pow(1. + x * x / _k, -(_k + 1));
    });
    return result *= 2.;
}
