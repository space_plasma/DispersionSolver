/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "DISPSplineKappaPDF.h"
#include <UtilityKit/UtilityKit.h>
#include <cmath>
#include <deque>
#include <limits>
#include <stdexcept>
#include <utility>

namespace {
inline double C(double const k) noexcept
{
    double A;
    if (k > 100.) { // asymptotic expansion
        A = 1 + 1 / (8. * k);
    } else {
        A = std::sqrt(k) * std::tgamma(k) / std::tgamma(k + .5);
    }
    return A *= .5 * M_2_SQRTPI;
}
template <class T>
inline T f(double const k, T const &x) noexcept
{
    return C(k) * std::pow(1. + x * x / k, -(k + 1));
}
template <class T>
inline T df(double const k, T const &x) noexcept
{
    return -2. * x * (k + 1) / (k + x * x) * f(k, x);
}
} // namespace

auto Disp::SplineKappaPDF::f(real_type const x) const noexcept -> real_type
{
    return ::f(_k, x);
}
auto Disp::SplineKappaPDF::f(complex_type const &z) const noexcept -> complex_type
{
    return ::f(_k, z);
}

auto Disp::SplineKappaPDF::dfdx(real_type const x) const noexcept -> real_type
{
    return ::df(_k, x);
}
auto Disp::SplineKappaPDF::dfdx(complex_type const &z) const noexcept -> complex_type
{
    return ::df(_k, z);
}

Disp::SplineKappaPDF::SplineKappaPDF(real_type const kappa)
: _k(std::numeric_limits<real_type>::quiet_NaN()), _pdf()
{
    if (kappa <= 0.5) {
        throw std::invalid_argument(__PRETTY_FUNCTION__);
    }
    _k = kappa;

    // determine the number of samples
    //
    real_type const x_max = std::sqrt((std::pow(_epsilon, -1. / (1 + _k)) - 1) * _k);
    long            n     = static_cast<long>(std::round(x_max / _delta));
    if (n < 5) {
        n = 5;
    }

    // sample points
    //
    std::deque<std::pair<real_type, real_type>> pts;
    pts.push_back({ 0., this->f(0.) }); // zero first
    for (long i = 1; i <= n; ++i) {
        real_type const x = i * _delta;
        real_type const y = this->f(x);
        pts.push_front({ -x, y });
        pts.push_back({ +x, y });
    }

    // construct pdf
    //
    Utility::CubicSplineCoefficient<real_type> coef(pts.begin(), pts.end());
    _pdf.reset(new SplinePDF(std::move(coef)));
}

Disp::SplineKappaPDF::SplineKappaPDF(SplineKappaPDF const &o)
: _k(o._k), _pdf(new SplinePDF(*o._pdf))
{
}
auto Disp::SplineKappaPDF::operator=(SplineKappaPDF const &o) -> SplineKappaPDF &
{
    if (this != &o) {
        SplineKappaPDF tmp(o);
        *this = std::move(o);
    }
    return *this;
}

auto Disp::SplineKappaPDF::Ztriplet(complex_type const &z) const -> std::tuple<complex_type, complex_type, complex_type>
{
    complex_type const Z0 = this->Z<0>(z);
    complex_type const Z1 = z * Z0 + 1.;
    complex_type const Z2 = z * Z1;
    return std::make_tuple(Z0, Z1, Z2);
}
auto Disp::SplineKappaPDF::Wtriplet(complex_type const &z) const -> std::tuple<complex_type, complex_type, complex_type>
{
    complex_type const W0 = this->W<0>(z);
    complex_type const W1 = z * W0;
    complex_type const W2 = z * W1 - 1.;
    return std::make_tuple(W0, W1, W2);
}
