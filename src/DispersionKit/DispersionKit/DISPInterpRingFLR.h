/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <DispersionKit/DISPRingFLR.h>
#include <DispersionKit/DispersionKit-config.h>
#include <UtilityKit/UtilityKit.h>
#include <array>
#include <memory>
#include <unordered_map>

DISPERSIONKIT_BEGIN_NAMESPACE
/**
 @brief RingFLR with dynamic tabulation of interpolators.
 */
struct InterpRingFLR : protected RingFLR {
    InterpRingFLR(InterpRingFLR &&) noexcept = default;
    InterpRingFLR &operator=(InterpRingFLR &&) noexcept = default;
    InterpRingFLR(InterpRingFLR const &);
    InterpRingFLR &operator=(InterpRingFLR const &);

    explicit InterpRingFLR(double const b);
    explicit InterpRingFLR()
    : InterpRingFLR(0.) {}

    //@{
    /** Inherited interfaces. */
    using RingFLR::clear_cache;

    using RingFLR::dfdx;
    using RingFLR::f;

    using RingFLR::A;
    using RingFLR::AOa2;
    using RingFLR::B;
    using RingFLR::BOa;
    using RingFLR::C;

    using RingFLR::P;
    using RingFLR::POa2;
    using RingFLR::Q;
    using RingFLR::QOa;
    using RingFLR::R;

    using RingFLR::b;
    using RingFLR::J;
    //@}

protected:
    static constexpr unsigned _n_oscillations = 10;  //!< number of oscillations to divide
    static constexpr unsigned _n_samples      = 150; //!< number of samples used to construct interpolators

    struct _AbscissaRange;
    using _InterpolatorPack       = std::array<Utility::SplineInterpolator<double, 3>, value_pack_type::size()>;
    using _InnerInterpolatorTable = std::unordered_map<unsigned, _InterpolatorPack>;
    using _OuterInterpolatorTable = std::array<_InnerInterpolatorTable, std::tuple_size<BesselJ::zero_table_type>::value>;

    std::unique_ptr<_OuterInterpolatorTable> _interpolator_table;

    value_pack_type _eval_ABC(int const n, double const a) const override;
    void            _sample_ABC(int const n, double const a, _AbscissaRange const &range, _InterpolatorPack &interpolators) const;
};
DISPERSIONKIT_END_NAMESPACE
