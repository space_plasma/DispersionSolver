/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "DISPInterpLorentzianFLR.h"
#include <cmath>
#include <stdexcept>
#include <utility>

Disp::InterpLorentzianFLR::InterpLorentzianFLR(InterpLorentzianFLR const &o)
: InterpLorentzianFLR(o.kappa())
{
    *_interpolator_table = *o._interpolator_table;
}
auto Disp::InterpLorentzianFLR::operator=(InterpLorentzianFLR const &o) -> InterpLorentzianFLR &
{
    if (this != &o) {
        InterpLorentzianFLR tmp(o);
        *this = std::move(tmp);
    }
    return *this;
}
Disp::InterpLorentzianFLR::InterpLorentzianFLR(double const kappa)
: LorentzianFLR(kappa), _interpolator_table(new _OuterInterpolatorTable)
{
}

struct Disp::InterpLorentzianFLR::_AbscissaRange {
    static double offset(unsigned const n) noexcept
    {
        static BesselJ::zero_table_type const &zeros = BesselJ::zeros();
        return zeros[n].first; // start from the first zero
    }

    Utility::Vector<double, 2> alim;
    unsigned                   key;

    _AbscissaRange(unsigned const n, double const a, double const b) noexcept
    {
        double const x0 = offset(n);
        double const x  = a * b;
        if (x < x0) {
            key    = 0;
            alim.x = 0.;
            alim.y = x0;
        } else {
            constexpr double interval = 2. * M_PI * _n_oscillations;

            key    = static_cast<unsigned>((x - x0) / interval) + 1;
            alim.x = x0 + (key - 1) * interval;
            alim.y = x0 + (key - 0) * interval;
        }
        alim /= b;
    }
};

auto Disp::InterpLorentzianFLR::_eval_APBQCR(int const n, double const a) const -> value_pack_type
{
#if defined(DEBUG)
    if (n < 0 || a < 0.)
        throw std::invalid_argument(__PRETTY_FUNCTION__);
#endif

    if (unsigned(n) >= std::tuple_size<_OuterInterpolatorTable>::value) {
        return LorentzianFLR::_eval_APBQCR(n, a);
    }
    if (a < 1.0)
        return LorentzianFLR::_eval_APBQCR(n, a); // avoid too small a

    constexpr double     bmin = 3;
    _AbscissaRange const range{ unsigned(n), a, bmin };
    _InterpolatorPack   &interpolators = (*_interpolator_table)[unsigned(n)][range.key];
    if (!interpolators.front()) {
        _sample_APBQCR(n, a, range, interpolators);
    }
    return {
        *std::get<0>(interpolators)(a),
        *std::get<1>(interpolators)(a),
        *std::get<2>(interpolators)(a),
        *std::get<3>(interpolators)(a),
        *std::get<4>(interpolators)(a),
        *std::get<5>(interpolators)(a),
    };
}

void Disp::InterpLorentzianFLR::_sample_APBQCR(int const n, double const, _AbscissaRange const &range, _InterpolatorPack &interpolators) const
{
    // sampling
    //
    std::array<double, _n_samples + 1> aList, As, Ps, Bs, Qs, Cs, Rs;
    double const                       da = (range.alim.y - range.alim.x) / _n_samples;
    value_pack_type
        pack
        = LorentzianFLR::_eval_APBQCR(n, aList[0] = range.alim.x);
    As.front() = std::get<0>(pack);
    Ps.front() = std::get<1>(pack);
    Bs.front() = std::get<2>(pack);
    Qs.front() = std::get<3>(pack);
    Cs.front() = std::get<4>(pack);
    Rs.front() = std::get<5>(pack);
    for (unsigned i = 1; i < std::tuple_size<decltype(aList)>::value - 1; ++i) {
        double const a = double(i) * da + range.alim.x;
        pack           = LorentzianFLR::_eval_APBQCR(n, aList[i] = a);
        As[i]          = std::get<0>(pack);
        Ps[i]          = std::get<1>(pack);
        Bs[i]          = std::get<2>(pack);
        Qs[i]          = std::get<3>(pack);
        Cs[i]          = std::get<4>(pack);
        Rs[i]          = std::get<5>(pack);
    }
    pack      = LorentzianFLR::_eval_APBQCR(n, aList.back() = range.alim.y);
    As.back() = std::get<0>(pack);
    Ps.back() = std::get<1>(pack);
    Bs.back() = std::get<2>(pack);
    Qs.back() = std::get<3>(pack);
    Cs.back() = std::get<4>(pack);
    Rs.back() = std::get<5>(pack);

    // interpolators
    //
    using Cubic                = Utility::CubicSplineCoefficient<double>;
    std::get<0>(interpolators) = Cubic(aList.begin(), aList.end(), As.begin()).interpolator();
    std::get<1>(interpolators) = Cubic(aList.begin(), aList.end(), Ps.begin()).interpolator();
    std::get<2>(interpolators) = Cubic(aList.begin(), aList.end(), Bs.begin()).interpolator();
    std::get<3>(interpolators) = Cubic(aList.begin(), aList.end(), Qs.begin()).interpolator();
    std::get<4>(interpolators) = Cubic(aList.begin(), aList.end(), Cs.begin()).interpolator();
    std::get<5>(interpolators) = Cubic(aList.begin(), aList.end(), Rs.begin()).interpolator();
}
