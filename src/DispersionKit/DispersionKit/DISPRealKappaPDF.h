/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <DispersionKit/DispersionKit-config.h>
#include <complex>
#include <tuple>
#include <type_traits>

DISPERSIONKIT_BEGIN_NAMESPACE
/**
 @brief Plasma dispersion function for a kappa (generalized Lorentzian) distribution function,
 f(x) = √κΓ(κ)/Γ(κ+1/2)*(1+x^2/κ)^-(κ+1)/√π, where κ is a real number greater than 1/2.
 @discussion This version uses the gauss hypergeometric function.
 @note This is an experimental implementation. Test shows that convergence of the hypergeometric function is not reliable.
 */
struct RealKappaPDF {
    using real_type    = double;
    using complex_type = std::complex<real_type>;

    //@{
    /**
     @brief Calculate f(x).
     */
    real_type    f(real_type const x) const noexcept;
    complex_type f(complex_type const &z) const noexcept;
    //@}

    //@{
    /**
     @brief Calculate df(x)/dx.
     */
    real_type    dfdx(real_type const x) const noexcept;
    complex_type dfdx(complex_type const &z) const noexcept;
    //@}

    /**
     @brief Evaluates integral of f(x)*x^N/(x - z).
     @discussion Returns NaN when convergence failed.
     */
    template <long N>
    complex_type Z(complex_type const &z) const noexcept { return _Z(z, std::integral_constant<long, N>{}); }

    /**
     @brief Evaluates Z<0>, Z<1> and Z<2> using the recurring algorithm.
     */
    std::tuple<complex_type, complex_type, complex_type>
    Ztriplet(complex_type const &z) const;
    /**
     @brief Evaluates W<0>, W<1> and W<2> using the recurring algorithm.
     */
    std::tuple<complex_type, complex_type, complex_type>
    Wtriplet(complex_type const &z) const;

    /**
     @brief Returns the kappa index.
     */
    real_type kappa() const noexcept { return _k; }

    /**
     @brief Construct kappa PDF with a positive kappa index.
     @exception An std::invalid_argument exception if kappa < 1.
     */
    explicit RealKappaPDF(real_type const kappa);

private:
    real_type                  _k;
    constexpr static real_type _reltol = 1e-12; //!< Relative torelance used to test 2F1 convergence.
    constexpr static unsigned  _max_n  = 50000; //!< Maximum number of summations beyond which 2F1 is considered not converging.

    complex_type _Z(complex_type const &z, std::integral_constant<long, 0L> const) const noexcept { return _Z(_k, z); }
    complex_type _Z(complex_type const &z, std::integral_constant<long, 1L> const) const noexcept { return 1. + z * Z<0>(z); }
    complex_type _Z(complex_type const &z, std::integral_constant<long, 2L> const) const noexcept { return z * Z<1>(z); }
    complex_type _Z(complex_type const &z, std::integral_constant<long, 3L> const) const noexcept { return _k / (2. * _k - 1) + z * Z<2>(z); }

    static complex_type _Z(real_type const k, complex_type const &z) noexcept;
    static complex_type _2F1(real_type const b, real_type const c, complex_type const &z) noexcept; // return nan when not converged
};
DISPERSIONKIT_END_NAMESPACE
