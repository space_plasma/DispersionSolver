/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "DISPDispersion.h"
#include "DISPBesselJ.h"
#include "DISPSquareMatrix.h"
#include <algorithm>
#include <atomic>
#include <limits>
#include <typeinfo>
#if defined(__APPLE__) && defined(__OBJC__)
#import <Foundation/Foundation.h>
#else
#include <taskflow/taskflow.hpp>
#endif

Disp::Dispersion::~Dispersion()
{
}
Disp::Dispersion::Dispersion(real_type const c)
: _vdfs(), _c(c), _jminmax()
{
    set_jmin(0);
    _jminmax.second = static_cast<unsigned>(BesselJ::zeros().size() - 1);
}

std::string Disp::Dispersion::description() const
{
    std::ostringstream os;
    printo(os, typeid(*this).name(), "[c->", c(), ", jmin->", jmin(), ", jmax->", jmax(), ", vdfs->{");
    if (!_vdfs.empty()) {
        auto it = _vdfs.begin();
        printo(os, "\n\t", (*it++)->description());
        while (it != _vdfs.end()) {
            printo(os, ",\n\t", (*it++)->description());
        }
    }
    printo(os, "\n}]");
    return os.str();
}

void Disp::Dispersion::clear_cache()
{
    for (auto &p : _vdfs) {
        p->clear_cache();
    }
}

auto Disp::Dispersion::vdfs() const -> std::vector<VDF const *>
{
    std::vector<VDF const *> vdfs;
    for (auto &p : _vdfs) {
        vdfs.push_back(p.get());
    }
    return vdfs;
}

CX::Matrix Disp::Dispersion::D(std::pair<real_type, real_type> const &k, complex_type const &o, step_monitor_type const &step_monitor) const
{
    return o2_x_D(k, o, step_monitor) / (o * o);
}
CX::Matrix Disp::Dispersion::o2_x_D(std::pair<real_type, real_type> const &k, complex_type const &o, step_monitor_type const &step_monitor) const
{
    real_type k1, k2;
    std::tie(k1, k2) = k;
    matrix_type D{
        { k2 * k2, 0., k1 * k2 },
        { 0., 0., 0. },
        { k1 * k2, 0., k1 * k1 }
    };
    D -= ::eye() * (k2 * k2 + k1 * k1);
    D *= c() * c();
    if (!_vdfs.empty())
        D += _o2_x_epsilon(k, o, step_monitor);
    return D;
}

CX::Matrix Disp::Dispersion::_o2_x_epsilon(std::pair<real_type, real_type> const &k, complex_type const &o, step_monitor_type const &step_monitor) const
{
    matrix_type result = ::eye() * (o * o); // identity matrix

    // Landau term; j = 0
    //
    result += _o2_x_epsilon(k, o, 0, serial);
    if (!isfinite(result))
        return result;

    // cyclotron terms; j != 0
    //
    result += _o2_x_epsilon(k, o, step_monitor, concurrency);
    return result;
}
CX::Matrix Disp::Dispersion::_o2_x_epsilon(std::pair<real_type, real_type> const &k, complex_type const &o, step_monitor_type const &step_monitor, _Concurrency const concurrency) const
{
    matrix_type result{};

    // iterate jmin-1 times
    //
    if (concurrency == concurrent && jmin() > 2) {          // concurrent && for sufficiently large jmin
        std::vector<matrix_type> v(jmin());                 // v[0] is not used
        std::atomic<bool>        should_terminate{ false }; // error signal
                                                            // be careful for jmin < 1

        auto const evaluate =
            [this, &v, &should_terminate, k, o, &step_monitor](unsigned long const index) -> void {
            if (should_terminate.load())
                return; // flag is set; bail out

            unsigned const    j  = static_cast<unsigned>(index) + 1; // j==0 case has been handled
            matrix_type const _1 = this->_o2_x_epsilon(k, o, +static_cast<int>(j), serial);
            if (should_terminate.load() || !isfinite(_1)) {
                should_terminate.store(true);
                return;
            }
            matrix_type const _2 = this->_o2_x_epsilon(k, o, -static_cast<int>(j), serial);
            if (should_terminate.load() || !isfinite(_2)) {
                should_terminate.store(true);
                return;
            }
            v.at(j) += _1 + _2;
            if (should_terminate.load() || !step_monitor(j, v[j])) {
                should_terminate.store(true);
                return;
            }
        };

#if defined(__APPLE__) && defined(__OBJC__)
        dispatch_apply(jmin() - 1U, dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), evaluate);
#else
        tf::Taskflow taskflow;
        taskflow.for_each_index(0U, jmin() - 1U, 1U, evaluate);
        tf::Executor{}.run(std::move(taskflow)).wait();
#endif

        if (should_terminate.load()) { // Check if error occured
            constexpr matrix_type::value_type nan{ std::numeric_limits<real_type>::quiet_NaN() };
            return matrix_type{ nan };
        } else { // Otherwise, add the results together (excluding v[0] component)
            result = std::accumulate(v.begin() + 1, v.end(), result);
        }
    } else { // serial
        for (unsigned j = 1; j < jmin(); ++j) {
            matrix_type const _1 = _o2_x_epsilon(k, o, +static_cast<int>(j), concurrency);
            if (!isfinite(_1)) {
                return _1;
            }
            matrix_type const _2 = _o2_x_epsilon(k, o, -static_cast<int>(j), concurrency);
            if (!isfinite(_2)) {
                return _2;
            }
            result += _1 + _2;
            if (!step_monitor(j, result)) {
                return result;
            }
        }
    }

    // iterate maximum jmax-1 times
    //
    for (unsigned j = jmin(); j < jmax(); ++j) {
        matrix_type const _1 = _o2_x_epsilon(k, o, +static_cast<int>(j), concurrency);
        if (!isfinite(_1)) {
            return _1;
        }
        matrix_type const _2 = _o2_x_epsilon(k, o, -static_cast<int>(j), concurrency);
        if (!isfinite(_2)) {
            return _2;
        }
        real_type const prev = ::norm<1>(result); // FIXME: Use determinant for convergence test.
        result += _1 + _2;
        if (!step_monitor(j, result)) {
            return result;
        } else if (prev <= abstol || std::abs(::norm<1>(result) - prev) <= prev * reltol) {
            return result;
        }
    }

    // final check for convergence at jmax
    // FIXME: Probably no convergence test and just print message to stderr?
    //
    {
        matrix_type const _1 = _o2_x_epsilon(k, o, +static_cast<int>(jmax()), concurrency);
        if (!isfinite(_1)) {
            return _1;
        }
        matrix_type const _2 = _o2_x_epsilon(k, o, -static_cast<int>(jmax()), concurrency);
        if (!isfinite(_2)) {
            return _2;
        }
        real_type const prev = ::norm<1>(result);
        result += _1 + _2;
        if (!step_monitor(jmax(), result)) {
            return result;
        } else if (prev <= abstol || std::abs(::norm<1>(result) - prev) <= prev * reltol) {
            return result;
        }
    }

    constexpr matrix_type::value_type nan{ std::numeric_limits<real_type>::quiet_NaN() };
    return matrix_type{ nan };
}
namespace {
inline CX::Matrix o2_x_epsilon(Disp::VDF const &vdf, std::pair<double, double> const &k, CX::Number const &o, int const j)
{
    CX::Matrix &&result = vdf.K(k, o, j);
    return result *= vdf.op() * vdf.op();
}
} // namespace
CX::Matrix Disp::Dispersion::_o2_x_epsilon(std::pair<real_type, real_type> const &k, complex_type const &o, int const j, _Concurrency const concurrency) const
{
    if (concurrency == concurrent) { // concurrent
        std::vector<matrix_type> v(_vdfs.size());

        auto const evaluate =
            [this, &v, k, o, j](unsigned long const i) -> void {
            v[i] = ::o2_x_epsilon(*this->_vdfs[i], k, o, j);
        };

#if defined(__APPLE__) && defined(__OBJC__)
        dispatch_apply(v.size(), dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), evaluate);
#else
        tf::Taskflow taskflow;
        taskflow.for_each_index(0UL, v.size(), 1UL, evaluate);
        tf::Executor{}.run(std::move(taskflow)).wait();
#endif

        return std::accumulate(v.begin() + 1, v.end(), v.front());
    } else { // serial
        matrix_type result{};
        for (auto const &vdf : _vdfs) {
            result += ::o2_x_epsilon(*vdf, k, o, j);
        }
        return result;
    }
}
