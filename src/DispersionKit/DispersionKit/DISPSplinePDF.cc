/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "DISPSplinePDF.h"
#include "DISPSquareMatrix.h"
#include <limits>
#include <stdexcept>

namespace {
// Imaginary part of z should not be negative!!
inline CX::Number unitZ(CX::Number z, std::integral_constant<long, 0L>)
{
#if defined(DEBUG)
    if (z.imag() < 0.) {
        std::invalid_argument(__PRETTY_FUNCTION__);
    }
#endif

    // chop off smallness.
    constexpr double eps = 1e-10;
    if (std::abs(z.real()) < eps) {
        z.real(0.);
    }
    if (std::abs(z.imag()) < eps) {
        z.imag(eps);
    }

    return std::log((z - 1.) / z);
}
inline CX::Number unitZ(CX::Number z, std::integral_constant<long, 1L>)
{
    return 1. + z * unitZ(z, std::integral_constant<long, 0L>{});
}
template <long N>
inline CX::Number unitZ(CX::Number z, std::integral_constant<long, N>)
{
    static_assert(N >= 2, "N should be greater than 2");

    CX::Number result = 1. / N + std::pow(z, N) * unitZ(z, std::integral_constant<long, 0L>{});
    for (long i = 1; i < N; ++i) {
        result += std::pow(z, i) / double(N - i);
    }
    return result;
}
template <long N>
inline CX::Number unitZ(CX::Number z)
{
    return unitZ(z, std::integral_constant<long, N>{});
}
} // namespace

// MARK: SplinePDF
//
Disp::SplinePDF::SplinePDF(spline_coefficient_type const &spline)
: SplinePDF(nullptr, spline)
{
}
Disp::SplinePDF::SplinePDF(spline_coefficient_type &&spline)
: SplinePDF(nullptr, std::move(spline))
{
}

template <class Spline>
Disp::SplinePDF::SplinePDF(std::nullptr_t, Spline &&s)
: _cubic_spline_coef(), _0th_mom(std::numeric_limits<real_type>::quiet_NaN()), _1st_mom(std::numeric_limits<real_type>::quiet_NaN())
{
    if (!s.is_regular_grid()) {
        throw std::invalid_argument(__PRETTY_FUNCTION__);
    }
    _cubic_spline_coef = std::forward<Spline>(s);
    _0th_mom           = _cubic_spline_coef.integrate();
    _1st_mom           = 0;
    {
        auto const     &coefs = _cubic_spline_coef.table(); // std::vector<std::pair<real_type, std::array<real_type, 4>>>
        real_type const delta = *_cubic_spline_coef.delta();
        for (unsigned long i = 0, n = coefs.size() - 1 /*drop the last*/; i < n; ++i) {
            std::array<real_type, 4> const &cj = coefs[i].second;
            _1st_mom += delta * (cj[0] / 2. + cj[1] / 3. + cj[2] / 4. + cj[3] / 5.);
            _1st_mom += coefs[i].first * (cj[0] / 1. + cj[1] / 2. + cj[2] / 3. + cj[3] / 4.);
        }
        _1st_mom *= delta;
    }
}

// MARK: Triplets
std::tuple<CX::Number, CX::Number, CX::Number> Disp::SplinePDF::Ztriplet(complex_type const &z, bool const should_make_analytically_continuous) const
{
    complex_type const Z0 = this->Z<0>(z, should_make_analytically_continuous);
    complex_type const Z1 = z * Z0 + _0th_mom;
    complex_type const Z2 = z * Z1 + _1st_mom;
    return std::make_tuple(Z0, Z1, Z2);
}
std::tuple<CX::Number, CX::Number, CX::Number> Disp::SplinePDF::Wtriplet(complex_type const &z, bool const should_make_analytically_continuous) const
{
    complex_type const W0 = this->W<0>(z, should_make_analytically_continuous);
    complex_type const W1 = z * W0;
    complex_type const W2 = z * W1 - _0th_mom;
    return std::make_tuple(W0, W1, W2);
}

// MARK: Z
CX::Number Disp::SplinePDF::_Z(const complex_type &z, const std::integral_constant<long, 0L>) const
{
    auto const              &coefs = _cubic_spline_coef.table(); // std::vector<std::pair<real_type, std::array<real_type, 4>>>
    real_type const          Dj = *_cubic_spline_coef.delta(), Dj2 = Dj * Dj;
    complex_type const       z2 = z * z;
    real_type                xj;
    std::array<real_type, 4> cj;
    complex_type             Z = 0.;

    if (std::abs(z) > large) { // large argument expansion
        for (unsigned long i = 0, n = coefs.size() - 1; i < n; ++i) {
            std::tie(xj, cj)    = coefs[i];
            real_type const xj2 = xj * xj;
            for (unsigned long k = 0; k < std::tuple_size<decltype(cj)>::value; ++k) {
                Z += ((xj + z) * (xj2 + z2) / (k + 1.) + (3. * xj2 + 2. * xj * z + z2) * Dj / (k + 2.) + (3. * xj + z) * Dj2 / (k + 3.) + Dj2 * Dj / (k + 4.)) * Dj * cj[k];
            }
        }
        Z /= -z2 * z2;
    } else { // piecewise splice of unitZ
        for (unsigned long i = 0, n = coefs.size() - 1; i < n; ++i) {
            std::tie(xj, cj)      = coefs[i];
            complex_type const zj = (z - xj) / Dj;

            Z += std::get<0>(cj) * (::unitZ<0>(zj));
            Z += std::get<1>(cj) * (::unitZ<1>(zj));
            Z += std::get<2>(cj) * (::unitZ<2>(zj));
            Z += std::get<3>(cj) * (::unitZ<3>(zj));
        }
    }

    return Z;
}
CX::Number Disp::SplinePDF::_Z(const complex_type &z, const std::integral_constant<long, 1L>) const
{
    auto const              &coefs = _cubic_spline_coef.table(); // std::vector<std::pair<real_type, std::array<real_type, 4>>>
    real_type const          Dj = *_cubic_spline_coef.delta(), Dj2 = Dj * Dj;
    complex_type const       z2 = z * z;
    real_type                xj;
    std::array<real_type, 4> cj;
    complex_type             Z = 0.;

    if (std::abs(z) > large) { // large argument expansion
        for (unsigned long i = 0, n = coefs.size() - 1; i < n; ++i) {
            std::tie(xj, cj)    = coefs[i];
            real_type const xj2 = xj * xj;
            for (unsigned long k = 0; k < std::tuple_size<decltype(cj)>::value; ++k) {
                Z += (xj * (xj + z) * (xj2 + z2) / (k + 1.) + (4. * xj2 * xj + 3. * xj2 * z + 2. * xj * z2 + z2 * z) * Dj / (k + 2.) + (6. * xj2 + 3. * xj * z + z2) * Dj2 / (k + 3.) + (4. * xj + z) * Dj2 * Dj / (k + 4.) + Dj2 * Dj2 / (k + 5.)) * Dj * cj[k];
            }
        }
        Z /= -z2 * z2;
    } else { // piecewise splice of unitZ
        for (unsigned long i = 0, n = coefs.size() - 1; i < n; ++i) {
            std::tie(xj, cj)      = coefs[i];
            complex_type const zj = (z - xj) / Dj;

            std::array<complex_type, 5> const unitZs{ ::unitZ<0>(zj), ::unitZ<1>(zj), ::unitZ<2>(zj), ::unitZ<3>(zj), ::unitZ<4>(zj) };
            Z += std::get<0>(cj) * (xj * std::get<0>(unitZs) + Dj * std::get<1>(unitZs));
            Z += std::get<1>(cj) * (xj * std::get<1>(unitZs) + Dj * std::get<2>(unitZs));
            Z += std::get<2>(cj) * (xj * std::get<2>(unitZs) + Dj * std::get<3>(unitZs));
            Z += std::get<3>(cj) * (xj * std::get<3>(unitZs) + Dj * std::get<4>(unitZs));
        }
    }

    return Z;
}
CX::Number Disp::SplinePDF::_Z(const complex_type &z, const std::integral_constant<long, 2L>) const
{
    auto const              &coefs = _cubic_spline_coef.table(); // std::vector<std::pair<real_type, std::array<real_type, 4>>>
    real_type const          Dj = *_cubic_spline_coef.delta(), Dj2 = Dj * Dj;
    complex_type const       z2 = z * z;
    real_type                xj;
    std::array<real_type, 4> cj;
    complex_type             Z = 0.;

    if (std::abs(z) > large) { // large argument expansion
        for (unsigned long i = 0, n = coefs.size() - 1; i < n; ++i) {
            std::tie(xj, cj)    = coefs[i];
            real_type const xj2 = xj * xj;
            for (unsigned long k = 0; k < std::tuple_size<decltype(cj)>::value; ++k) {
                Z += (xj2 * (xj + z) * (xj2 + z2) / (k + 1.) + xj * (5. * xj2 * xj + 4. * xj2 * z + 3. * xj * z2 + 2. * z2 * z) * Dj / (k + 2.) + (10. * xj2 * xj + 6. * xj2 * z + 3. * xj * z2 + z2 * z) * Dj2 / (k + 3.) + (10. * xj2 + 4. * xj * z + z2) * Dj2 * Dj / (k + 4.) + (5. * xj + z) * Dj2 * Dj2 / (k + 5.) + Dj2 * Dj2 * Dj / (k + 6.)) * Dj * cj[k];
            }
        }
        Z /= -z2 * z2;
    } else { // piecewise splice of unitZ
        for (unsigned long i = 0, n = coefs.size() - 1; i < n; ++i) {
            std::tie(xj, cj)       = coefs[i];
            real_type const    xj2 = xj * xj;
            complex_type const zj  = (z - xj) / Dj;

            std::array<complex_type, 6> const unitZs{ ::unitZ<0>(zj), ::unitZ<1>(zj), ::unitZ<2>(zj), ::unitZ<3>(zj), ::unitZ<4>(zj), ::unitZ<5>(zj) };
            Z += std::get<0>(cj) * (xj2 * std::get<0>(unitZs) + 2. * xj * Dj * std::get<1>(unitZs) + Dj2 * std::get<2>(unitZs));
            Z += std::get<1>(cj) * (xj2 * std::get<1>(unitZs) + 2. * xj * Dj * std::get<2>(unitZs) + Dj2 * std::get<3>(unitZs));
            Z += std::get<2>(cj) * (xj2 * std::get<2>(unitZs) + 2. * xj * Dj * std::get<3>(unitZs) + Dj2 * std::get<4>(unitZs));
            Z += std::get<3>(cj) * (xj2 * std::get<3>(unitZs) + 2. * xj * Dj * std::get<4>(unitZs) + Dj2 * std::get<5>(unitZs));
        }
    }

    return Z;
}

// MARK: W
CX::Number Disp::SplinePDF::_W(const complex_type &z, const std::integral_constant<long, 0L>) const
{
    auto const              &coefs = _cubic_spline_coef.table(); // std::vector<std::pair<real_type, std::array<real_type, 4>>>
    real_type const          Dj = *_cubic_spline_coef.delta(), Dj2 = Dj * Dj;
    complex_type const       z2 = z * z;
    real_type                xj;
    std::array<real_type, 4> cj;
    complex_type             Z = 0.;

    if (std::abs(z) > large) { // large argument expansion
        for (unsigned long i = 0, n = coefs.size() - 1; i < n; ++i) {
            std::tie(xj, cj)    = coefs[i];
            real_type const xj2 = xj * xj;
            for (unsigned long k = 0; k < std::tuple_size<decltype(cj)>::value; ++k) {
                Z += (1. * (3. * xj2 + 2. * xj * z + z2) / (k + 1.) + 2. * (3. * xj + z) * Dj / (k + 2.) + 3. * Dj2 / (k + 3.)) * Dj * cj[k];
            }
        }
        Z /= z2 * z2;
    } else { // piecewise splice of unitZ
        for (unsigned long i = 0, n = coefs.size() - 1; i < n; ++i) {
            std::tie(xj, cj)      = coefs[i];
            complex_type const zj = (z - xj) / Dj;

            Z += std::get<1>(cj) * 1. * (1. / Dj * ::unitZ<0>(zj));
            Z += std::get<2>(cj) * 2. * (1. / Dj * ::unitZ<1>(zj));
            Z += std::get<3>(cj) * 3. * (1. / Dj * ::unitZ<2>(zj));
        }
    }

    return Z;
}
CX::Number Disp::SplinePDF::_W(const complex_type &z, const std::integral_constant<long, 1L>) const
{
    auto const              &coefs = _cubic_spline_coef.table(); // std::vector<std::pair<real_type, std::array<real_type, 4>>>
    real_type const          Dj = *_cubic_spline_coef.delta(), Dj2 = Dj * Dj;
    complex_type const       z2 = z * z;
    real_type                xj;
    std::array<real_type, 4> cj;
    complex_type             Z = 0.;

    if (std::abs(z) > large) { // large argument expansion
        for (unsigned long i = 0, n = coefs.size() - 1; i < n; ++i) {
            std::tie(xj, cj)    = coefs[i];
            real_type const xj2 = xj * xj;
            for (unsigned long k = 0; k < std::tuple_size<decltype(cj)>::value; ++k) {
                Z += (1. * (4. * xj2 * xj + 3. * xj2 * z + 2. * xj * z2 + z2 * z) / (k + 1.) + 2. * (6. * xj2 + 3. * xj * z + z2) * Dj / (k + 2.) + 3. * (4. * xj + z) * Dj2 / (k + 3.) + 4. * Dj2 * Dj / (k + 4.)) * Dj * cj[k];
            }
        }
        Z /= z2 * z2;
    } else { // piecewise splice of unitZ
        for (unsigned long i = 0, n = coefs.size() - 1; i < n; ++i) {
            std::tie(xj, cj)      = coefs[i];
            complex_type const zj = (z - xj) / Dj;

            std::array<complex_type, 4> const unitZs{ ::unitZ<0>(zj), ::unitZ<1>(zj), ::unitZ<2>(zj), ::unitZ<3>(zj) };
            Z += std::get<1>(cj) * 1. * (xj / Dj * std::get<0>(unitZs) + std::get<1>(unitZs));
            Z += std::get<2>(cj) * 2. * (xj / Dj * std::get<1>(unitZs) + std::get<2>(unitZs));
            Z += std::get<3>(cj) * 3. * (xj / Dj * std::get<2>(unitZs) + std::get<3>(unitZs));
        }
    }

    return Z;
}
CX::Number Disp::SplinePDF::_W(const complex_type &z, const std::integral_constant<long, 2L>) const
{
    auto const              &coefs = _cubic_spline_coef.table(); // std::vector<std::pair<real_type, std::array<real_type, 4>>>
    real_type const          Dj = *_cubic_spline_coef.delta(), Dj2 = Dj * Dj;
    complex_type const       z2 = z * z;
    real_type                xj;
    std::array<real_type, 4> cj;
    complex_type             Z = 0.;

    if (std::abs(z) > large) { // large argument expansion
        for (unsigned long i = 0, n = coefs.size() - 1; i < n; ++i) {
            std::tie(xj, cj)    = coefs[i];
            real_type const xj2 = xj * xj;
            for (unsigned long k = 0; k < std::tuple_size<decltype(cj)>::value; ++k) {
                Z += (1. * xj * (5. * xj2 * xj + 4. * xj2 * z + 3. * xj * z2 + 2. * z2 * z) / (k + 1.) + 2. * (10. * xj2 * xj + 6. * xj2 * z + 3. * xj * z2 + z2 * z) * Dj / (k + 2.) + 3. * (10. * xj2 + 4. * xj * z + z2) * Dj2 / (k + 3.) + 4. * (5. * xj + z) * Dj2 * Dj / (k + 4.) + 5. * Dj2 * Dj2 / (k + 5.)) * Dj * cj[k];
            }
        }
        Z /= z2 * z2;
    } else { // piecewise splice of unitZ
        for (unsigned long i = 0, n = coefs.size() - 1; i < n; ++i) {
            std::tie(xj, cj)       = coefs[i];
            real_type const    xj2 = xj * xj;
            complex_type const zj  = (z - xj) / Dj;

            std::array<complex_type, 5> const unitZs{ ::unitZ<0>(zj), ::unitZ<1>(zj), ::unitZ<2>(zj), ::unitZ<3>(zj), ::unitZ<4>(zj) };
            Z += std::get<1>(cj) * 1. * (xj2 / Dj * std::get<0>(unitZs) + 2. * xj * std::get<1>(unitZs) + Dj * std::get<2>(unitZs));
            Z += std::get<2>(cj) * 2. * (xj2 / Dj * std::get<1>(unitZs) + 2. * xj * std::get<2>(unitZs) + Dj * std::get<3>(unitZs));
            Z += std::get<3>(cj) * 3. * (xj2 / Dj * std::get<2>(unitZs) + 2. * xj * std::get<3>(unitZs) + Dj * std::get<4>(unitZs));
        }
    }

    return Z;
}

// MARK: Distribution Function Evaluation
auto Disp::SplinePDF::f(real_type const x) const noexcept -> real_type
{
    return this->f(complex_type{ x }).real();
}

auto Disp::SplinePDF::dfdx(real_type const x) const noexcept -> real_type
{
    return this->dfdx(complex_type{ x }).real();
}

CX::Number Disp::SplinePDF::f(complex_type const &z) const noexcept
{
    auto const     &S  = _cubic_spline_coef.table(); // std::vector<std::pair<real_type, std::array<real_type, 4>>>
    real_type const dx = _cubic_spline_coef.delta()();
    real_type const x0 = S.front().first, xn = S.back().first;
    real_type const u = z.imag() / dx, u2 = u * u;
    real_type       t = z.real(); // t is actually x, where z = x + iy
    if (t < x0 || t > xn) {
        return 0.;
    }

    // locate spline
    //
    unsigned long const n = S.size() - 1;  // n should be >= 2
    t                     = (t - x0) / dx; // now t is normalized by dx
    unsigned long j       = static_cast<unsigned long>(std::floor(t));
    j -= (j == n);                                         // if j==n (i.e., x==xn); j is always less than n
    t                                  = t - real_type(j); // renormalization of t for the jth spline
    real_type const                 t2 = t * t;
    std::array<real_type, 4> const &cj = S[j].second; // jth spline coefficients

    complex_type f(0.);
    // Interpolate real part
    //
    f += std::get<0>(cj) + std::get<1>(cj) * t + std::get<2>(cj) * t2 + std::get<3>(cj) * t2 * t;
    f -= .5 * (2. * std::get<2>(cj) + 6. * std::get<3>(cj) * t) * u2;
    // Interpolate imaginary part
    //
    f += CX::I * (std::get<1>(cj) + 2. * std::get<2>(cj) * t + 3. * std::get<3>(cj) * t2) * u;
    f -= CX::I * (j + 1 < n ? // linear interpolation
                      std::get<3>(cj) * (1. - t) + std::get<3>(S[j + 1].second) * t
                            : // linear extrapolation
                      std::get<3>(S[j - 1].second) * t + std::get<3>(cj) * (t + 1.))
       * u2 * u;

    return f;
}
CX::Number Disp::SplinePDF::dfdx(complex_type const &z) const noexcept
{
    auto const     &S  = _cubic_spline_coef.table(); // std::vector<std::pair<real_type, std::array<real_type, 4>>>
    real_type const dx = _cubic_spline_coef.delta()();
    real_type const x0 = S.front().first, xn = S.back().first;
    real_type const u = z.imag() / dx, u2 = u * u;
    real_type       t = z.real(); // t is actually x, where z = x + iy
    if (t < x0 || t > xn) {
        return 0.;
    }

    // locate spline
    //
    unsigned long const n = S.size() - 1;  // n should be >= 2
    t                     = (t - x0) / dx; // now t is normalized by dx
    unsigned long j       = static_cast<unsigned long>(std::floor(t));
    j -= (j == n);                                         // if j==n (i.e., x==xn); j is always less than n
    t                                  = t - real_type(j); // renormalization of t for the jth spline
    real_type const                 t2 = t * t;
    std::array<real_type, 4> const &cj = S[j].second; // jth spline coefficients

    complex_type df(0.);
    // Interpolate real part
    //
    df += (std::get<1>(cj) + 2. * std::get<2>(cj) * t + 3. * std::get<3>(cj) * t2);
    df -= (j + 1 < n ? // linear interpolation
               std::get<3>(cj) * (1. - t) + std::get<3>(S[j + 1].second) * t
                     : // linear extrapolation
               std::get<3>(S[j - 1].second) * t + std::get<3>(cj) * (t + 1.))
        * 3. * u2;
    // Interpolate imaginary part
    //
    df += CX::I * (2. * std::get<2>(cj) + 6. * std::get<3>(cj) * t) * u;

    return df / dx;
}
