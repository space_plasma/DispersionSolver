/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <DispersionKit/DispersionKit-config.h>
#include <UtilityKit/UtilityKit.h>
#include <cmath>
#include <complex>
#include <tuple>
#include <type_traits>
#include <vector>

DISPERSIONKIT_BEGIN_NAMESPACE
// MARK: SplinePDF
//
/**
 @brief Plasma dispersion function for an arbitrary distribution function approximated by cubic spline interpolants,
 f(x) = S_j(x) for x_j <= x < x_j+1.
 */
struct SplinePDF {
    using real_type               = double;
    using complex_type            = std::complex<real_type>;
    using spline_coefficient_type = Utility::SplineCoefficient<real_type, 3>;

    //@{
    /**
     @brief Calculate f(x).
     @discussion f(x) is approximated using cubic spline interpolation.
     When Re(x) is out-of-bound, f(x) is assumed to be 0.
     */
    real_type    f(real_type const x) const noexcept;
    complex_type f(complex_type const &z) const noexcept;
    //@}

    //@{
    /**
     @brief Calculate df(x)/dx.
     @discussion df(x)/dx is approximated using cubic spline interpolation.
     When Re(x) is out-of-bound, df(x)/dx is assumed to be 0.
     */
    real_type    dfdx(real_type const x) const noexcept;
    complex_type dfdx(complex_type const &z) const noexcept;
    //@}

    /**
     @brief Get the cubic spline coefficients of f(x).
     */
    spline_coefficient_type const &spline_coefficient() const noexcept { return _cubic_spline_coef; }

    /**
     @brief Evaluates integral of f(x)*x^N/(x - z) with an option for analytic continuation for negative imaginary z.
     @discussion If should_make_analytically_continuous is set, 2π*i*f(z)*z^N is added to the return value of Z<N>(z).
     */
    template <long N>
    complex_type Z(complex_type const &z, bool const should_make_analytically_continuous) const;
    /**
     @brief Evaluates integral of df(x)*x^N/(x - z) with an option for analytic continuation for negative imaginary z.
     @discussion If should_make_analytically_continuous is set, 2π*i*df(z)*z^N is added to the return value of W<N>(z).
     */
    template <long N>
    complex_type W(complex_type const &z, bool const should_make_analytically_continuous) const;

    /**
     @brief Evaluates Z<0>, Z<1> and Z<2> using the recurring algorithm.
     */
    std::tuple<complex_type, complex_type, complex_type>
    Ztriplet(complex_type const &z, bool const should_make_analytically_continuous) const;
    /**
     @brief Evaluates W<0>, W<1> and W<2> using the recurring algorithm.
     */
    std::tuple<complex_type, complex_type, complex_type>
    Wtriplet(complex_type const &z, bool const should_make_analytically_continuous) const;

    /**
     @brief Construct by copying an instance of spline_coefficient_type.
     @discussion An std::invalid_argument exception is thrown should the call is_regular_grid() to the instance return false.
     */
    explicit SplinePDF(spline_coefficient_type const &);
    /**
     @brief Construct by moving an instance of spline_coefficient_type.
     @discussion An std::invalid_argument exception is thrown should the call is_regular_grid() to the instance return false.
     */
    explicit SplinePDF(spline_coefficient_type &&);

    // copy/move:
    explicit SplinePDF()         = delete; // Default constructor is deleted so that no check for validity of the cubic spline coefficients is necessary.
    SplinePDF(SplinePDF const &) = default;
    SplinePDF(SplinePDF &&)      = default;
    SplinePDF &operator=(SplinePDF const &) = default;
    SplinePDF &operator=(SplinePDF &&) = default;

private:
    spline_coefficient_type _cubic_spline_coef{};
    real_type               _0th_mom;
    real_type               _1st_mom;

    template <class Spline>
    explicit SplinePDF(std::nullptr_t, Spline &&);

    /**
     @brief Evaluates integral of f(x)*x^N/(x - z) WITHOUT analytic continuation for negative imaginary z.
     */
    template <long N>
    inline complex_type _Z(complex_type const &z) const;
    /**
     @brief Evaluates integral of df(x)*x^N/(x - z) WITHOUT analytic continuation for negative imaginary z.
     */
    template <long N>
    inline complex_type _W(complex_type const &z) const;

    // no analytic continuation
    // imaginary part of z should not be negative!!
    complex_type _Z(complex_type const &z, std::integral_constant<long, 0L> const) const;
    complex_type _Z(complex_type const &z, std::integral_constant<long, 1L> const) const;
    complex_type _Z(complex_type const &z, std::integral_constant<long, 2L> const) const;

    complex_type _W(complex_type const &z, std::integral_constant<long, 0L> const) const;
    complex_type _W(complex_type const &z, std::integral_constant<long, 1L> const) const;
    complex_type _W(complex_type const &z, std::integral_constant<long, 2L> const) const;

    static constexpr real_type large = 50;
};

// MARK: Out-of-line Implementation
//
template <long N>
auto SplinePDF::_Z(complex_type const &z) const -> complex_type
{
    static_assert(N >= 0 && N <= 2, "0 <= N <= 2");

    if (z.imag() < 0.) {
        complex_type const Z = _Z(std::conj(z), std::integral_constant<long, N>{});
        return std::conj(Z);
    } else {
        return _Z(z, std::integral_constant<long, N>{});
    }
}
template <long N>
auto SplinePDF::_W(complex_type const &z) const -> complex_type
{
    static_assert(N >= 0 && N <= 2, "0 <= N <= 2");

    if (z.imag() < 0.) {
        complex_type const W = _W(std::conj(z), std::integral_constant<long, N>{});
        return std::conj(W);
    } else {
        return _W(z, std::integral_constant<long, N>{});
    }
}

template <long N>
auto SplinePDF::Z(complex_type const &z, bool const should_make_analytically_continuous) const -> complex_type
{
    static_assert(N >= 0 && N <= 2, "0 <= N <= 2");
    constexpr complex_type _2piI{ 0., 2. * M_PI };

    complex_type Z = this->template _Z<N>(z);
    if (should_make_analytically_continuous && z.imag() < 0.) {
        Z += _2piI * this->f(z) * (N > 0 ? std::pow(z, N) : complex_type(1.));
    }
    return Z;
}
template <long N>
auto SplinePDF::W(complex_type const &z, bool const should_make_analytically_continuous) const -> complex_type
{
    static_assert(N >= 0 && N <= 2, "0 <= N <= 2");
    constexpr complex_type _2piI{ 0., 2. * M_PI };

    complex_type W = this->template _W<N>(z);
    if (should_make_analytically_continuous && z.imag() < 0.) {
        W += _2piI * this->dfdx(z) * (N > 0 ? std::pow(z, N) : complex_type(1.));
    }
    return W;
}
DISPERSIONKIT_END_NAMESPACE
