/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "DISPColdRingFLR.h"
#include <cmath>
#include <limits>

Disp::ColdRingFLR::ColdRingFLR(double const vr)
: ColdRingFLR(vr, &BesselJ::shared())
{
}

double Disp::ColdRingFLR::f(double const x) const noexcept
{
    return x == _vr ? std::numeric_limits<double>::infinity() : 0.;
}
double Disp::ColdRingFLR::dfdx(double const) const noexcept
{
    return std::numeric_limits<double>::quiet_NaN();
}

double Disp::ColdRingFLR::A(int const n, double const a) const
{
    double const avr = a * _vr;
    return std::pow(J()(n, avr), 2.);
}
double Disp::ColdRingFLR::B(int const n, double const a) const
{
    double const avr = a * _vr;
    return _vr * J()(n, avr) * _Jp(n, avr);
}
double Disp::ColdRingFLR::C(int const n, double const a) const
{
    double const avr = a * _vr;
    return std::pow(_vr * _Jp(n, avr), 2.);
}

double Disp::ColdRingFLR::P(int const n, double const a) const
{
    double const avr = a * _vr;
    if (std::abs(avr) < 1e-5) {
        return a * a * POa2(n, a);
    } else {
        return -2. * a / _vr * J()(n, avr) * _Jp(n, avr);
    }
}
double Disp::ColdRingFLR::R(int const n, double const a) const
{
    double const avr = a * _vr;
    return -2. * (std::pow(_Jp(n, avr), 2.) + avr * _Jp(n, avr) * _Jpp(n, avr));
}
double Disp::ColdRingFLR::Q(int const n, double const a) const
{
    double const avr = a * _vr;
    if (std::abs(avr) < 1e-5) {
        return a * QOa(n, a);
    } else {
        return -1. / _vr * (J()(n, avr) * _Jp(n, avr) + avr * std::pow(_Jp(n, avr), 2.) + avr * J()(n, avr) * _Jpp(n, avr));
    }
}

double Disp::ColdRingFLR::AOa2(int const n, double const a) const
{
    double const avr = a * _vr;
    if (n) {
        return std::pow(.5 * _vr / n * (J()(n - 1, avr) + J()(n + 1, avr)), 2.);
    } else {
        return std::pow(J()(0, avr) / a, 2.);
    }
}
double Disp::ColdRingFLR::POa2(int const n, double const a) const
{
    double const avr = a * _vr;
    if (n) {
        return -1. / n * _Jp(n, avr) * (J()(n - 1, avr) + J()(n + 1, avr));
    } else {
        return J()(0, avr) * (J()(2, avr) + J()(0, avr));
    }
}
double Disp::ColdRingFLR::BOa(int const n, double const a) const
{
    double const avr = a * _vr;
    if (n) {
        return .5 * _vr * _vr / n * _Jp(n, avr) * (J()(n - 1, avr) + J()(n + 1, avr));
    } else {
        return -.5 * _vr * _vr * J()(0, avr) * (J()(2, avr) + J()(0, avr));
    }
}
double Disp::ColdRingFLR::QOa(int const n, double const a) const
{
    double const avr = a * _vr;
    if (n) {
        return -(.5 / n * _Jp(n, avr) * (J()(n - 1, avr) + J()(n + 1, avr)) + std::pow(_Jp(n, avr), 2.) + J()(n, avr) * _Jpp(n, avr));
    } else {
        return -(-.5 * J()(0, avr) * (J()(2, avr) + J()(0, avr)) + std::pow(_Jp(0, avr), 2.) + J()(0, avr) * _Jpp(0, avr));
    }
}

double Disp::ColdRingFLR::_Jp(int const n, double const x) const
{
    return .5 * (J()(n - 1, x) - J()(n + 1, x));
}
double Disp::ColdRingFLR::_Jpp(int const n, double const x) const
{
    return .5 * (_Jp(n - 1, x) - _Jp(n + 1, x));
}
