/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <DispersionKit/DispersionKit-config.h>
#include <UtilityKit/UtilityKit.h>
#include <cmath>
#include <complex>
#include <type_traits>

// MARK: Typedefs
//
namespace CX {
using Number = std::complex<double>;
using Vector = Utility::Vector<CX::Number, 3>;
using Matrix = Utility::Vector<CX::Vector, 3>;

constexpr Number I{ 0., 1. };
} // namespace CX

// MARK: Helpers
//
DISPERSIONKIT_BEGIN_NAMESPACE
double     _norm(CX::Matrix const &m, std::integral_constant<long, 1L>);
CX::Number _determinant(CX::Matrix const &m);
CX::Number _trace(CX::Matrix const &m);
CX::Matrix _transpose(CX::Matrix const &m);
CX::Matrix _inner_product(CX::Matrix const &a, CX::Matrix const &b);
CX::Vector _inner_product(CX::Matrix const &m, CX::Vector const &v);
CX::Matrix _adjugate(CX::Matrix const &m);
DISPERSIONKIT_END_NAMESPACE

// MARK: Matrix Operations
//
namespace {
// MARK: Pre-defined Constants
//
template <class U>
constexpr U zero()
{
    using T = typename U::value_type;
    return U{ T{ 0. } };
}
template <class U>
constexpr U one()
{
    using T = typename U::value_type;
    return U{ T{ 1. } };
}
constexpr CX::Matrix eye()
{
    return { CX::Vector{ 1., 0., 0. }, CX::Vector{ 0., 1., 0. }, CX::Vector{ 0., 0., 1. } };
}

// MARK: get
//
template <long i>
constexpr CX::Number &get(CX::Vector &v)
{
    static_assert(i >= 0 && i < CX::Vector::size(), "Index out-of-bound");
    return std::get<i>(v);
}
template <long i>
constexpr CX::Number const &get(CX::Vector const &v)
{
    static_assert(i >= 0 && i < CX::Vector::size(), "Index out-of-bound");
    return std::get<i>(v);
}

template <long i, long j>
constexpr CX::Number &get(CX::Matrix &m)
{
    static_assert(i >= 0 && i < CX::Vector::size(), "Index out-of-bound");
    return get<j>(std::get<i>(m));
}
template <long i, long j>
constexpr CX::Number const &get(CX::Matrix const &m)
{
    static_assert(i >= 0 && i < CX::Vector::size(), "Index out-of-bound");
    return get<j>(std::get<i>(m));
}

// MARK: CX::Vector {+,-,*,/}= double
//
inline CX::Vector &operator+=(CX::Vector &a, double const &s)
{
    return a += CX::Number{ s };
}
inline CX::Vector &operator-=(CX::Vector &a, double const &s)
{
    return a -= CX::Number{ s };
}
inline CX::Vector &operator*=(CX::Vector &a, double const &s)
{
    return a *= CX::Number{ s };
}
inline CX::Vector &operator/=(CX::Vector &a, double const &s)
{
    return a /= CX::Number{ s };
}

// MARK: CX::Matrix {+,-,*,/}= CX::Number
//
inline CX::Matrix &operator+=(CX::Matrix &a, CX::Number const &s)
{
    return a += CX::Vector{ s };
}
inline CX::Matrix &operator-=(CX::Matrix &a, CX::Number const &s)
{
    return a -= CX::Vector{ s };
}
inline CX::Matrix &operator*=(CX::Matrix &a, CX::Number const &s)
{
    return a *= CX::Vector{ s };
}
inline CX::Matrix &operator/=(CX::Matrix &a, CX::Number const &s)
{
    return a /= CX::Vector{ s };
}

// MARK: CX::Matrix {+,-,*,/}= double
//
inline CX::Matrix &operator+=(CX::Matrix &a, double const &s)
{
    return a += CX::Number{ s };
}
inline CX::Matrix &operator-=(CX::Matrix &a, double const &s)
{
    return a -= CX::Number{ s };
}
inline CX::Matrix &operator*=(CX::Matrix &a, double const &s)
{
    return a *= CX::Number{ s };
}
inline CX::Matrix &operator/=(CX::Matrix &a, double const &s)
{
    return a /= CX::Number{ s };
}

// MARK: CX::Vector = CX::Vector {+,-,*,/} double
//
inline CX::Vector operator+(CX::Vector const &a, double const &s)
{
    return a + CX::Number{ s };
}
inline CX::Vector operator-(CX::Vector const &a, double const &s)
{
    return a - CX::Number{ s };
}
inline CX::Vector operator*(CX::Vector const &a, double const &s)
{
    return a * CX::Number{ s };
}
inline CX::Vector operator/(CX::Vector const &a, double const &s)
{
    return a / CX::Number{ s };
}

// MARK: CX::Vector = double {+,-,*,/} CX::Vector
//
inline CX::Vector operator+(double const &s, CX::Vector const &a)
{
    return CX::Number{ s } + a;
}
inline CX::Vector operator-(double const &s, CX::Vector const &a)
{
    return CX::Number{ s } - a;
}
inline CX::Vector operator*(double const &s, CX::Vector const &a)
{
    return CX::Number{ s } * a;
}
inline CX::Vector operator/(double const &s, CX::Vector const &a)
{
    return CX::Number{ s } / a;
}

// MARK: CX::Matrix = CX::Matrix {+,-,*,/} CX::Number
//
inline CX::Matrix operator+(CX::Matrix const &a, CX::Number const &s)
{
    return a + CX::Vector{ s };
}
inline CX::Matrix operator-(CX::Matrix const &a, CX::Number const &s)
{
    return a - CX::Vector{ s };
}
inline CX::Matrix operator*(CX::Matrix const &a, CX::Number const &s)
{
    return a * CX::Vector{ s };
}
inline CX::Matrix operator/(CX::Matrix const &a, CX::Number const &s)
{
    return a / CX::Vector{ s };
}

// MARK: CX::Matrix = CX::Number {+,-,*,/} CX::Matrix
//
inline CX::Matrix operator+(CX::Number const &s, CX::Matrix const &a)
{
    return CX::Vector{ s } + a;
}
inline CX::Matrix operator-(CX::Number const &s, CX::Matrix const &a)
{
    return CX::Vector{ s } - a;
}
inline CX::Matrix operator*(CX::Number const &s, CX::Matrix const &a)
{
    return CX::Vector{ s } * a;
}
inline CX::Matrix operator/(CX::Number const &s, CX::Matrix const &a)
{
    return CX::Vector{ s } / a;
}

// MARK: CX::Matrix = CX::Matrix {+,-,*,/} double
//
inline CX::Matrix operator+(CX::Matrix const &a, double const &s)
{
    return a + CX::Number{ s };
}
inline CX::Matrix operator-(CX::Matrix const &a, double const &s)
{
    return a - CX::Number{ s };
}
inline CX::Matrix operator*(CX::Matrix const &a, double const &s)
{
    return a * CX::Number{ s };
}
inline CX::Matrix operator/(CX::Matrix const &a, double const &s)
{
    return a / CX::Number{ s };
}

// MARK: CX::Matrix = double {+,-,*,/} CX::Matrix
//
inline CX::Matrix operator+(double const &s, CX::Matrix const &a)
{
    return CX::Number{ s } + a;
}
inline CX::Matrix operator-(double const &s, CX::Matrix const &a)
{
    return CX::Number{ s } - a;
}
inline CX::Matrix operator*(double const &s, CX::Matrix const &a)
{
    return CX::Number{ s } * a;
}
inline CX::Matrix operator/(double const &s, CX::Matrix const &a)
{
    return CX::Number{ s } / a;
}

// MARK: isfinite
//
inline bool isfinite(CX::Number const &a) noexcept
{
    return std::isfinite(a.real()) && std::isfinite(a.imag());
}
inline bool isfinite(CX::Vector const &a) noexcept
{
    return isfinite(a.x) && isfinite(a.y) && isfinite(a.z);
}
inline bool isfinite(CX::Matrix const &a) noexcept
{
    return isfinite(a.x) && isfinite(a.y) && isfinite(a.z);
}

/**
 @brief p norm of the matrix.
 @discussion Currently, only p=1 is supported.
 */
template <long p>
inline double norm(CX::Matrix const &m)
{
    return Disp::_norm(m, std::integral_constant<long, p>{});
}
/**
 @brief Determinant.
 */
inline CX::Number det(CX::Matrix const &m)
{
    return Disp::_determinant(m);
}
/**
 @brief Trace.
 */
inline CX::Number tr(CX::Matrix const &m)
{
    return Disp::_trace(m);
}
/**
 @brief Transposition.
 */
inline CX::Matrix transpose(CX::Matrix const &m)
{
    return Disp::_transpose(m);
}
/**
 @brief Dot product of two matrix.
 */
inline CX::Matrix dot(CX::Matrix const &a, CX::Matrix const &b)
{
    return Disp::_inner_product(a, b);
}
/**
 @brief Dot product of a matrix to a vector.
 */
inline CX::Vector dot(CX::Matrix const &m, CX::Vector const &v)
{
    return Disp::_inner_product(m, v);
}
/**
 @brief Adjugate.
 @discussion Transpose of cofactor matrix.
 */
inline CX::Matrix adj(CX::Matrix const &m)
{
    return Disp::_adjugate(m);
}
} // namespace
