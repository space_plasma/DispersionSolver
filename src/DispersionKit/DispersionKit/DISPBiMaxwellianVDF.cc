/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "DISPBiMaxwellianVDF.h"
#include "DISPSquareMatrix.h"
#include <cmath>
#include <stdexcept>
#include <string>

Disp::BiMaxwellianVDF::BiMaxwellianVDF(real_type const Oc, complex_type const &op, real_type const th1, real_type const th2, real_type const vd)
: VDF(), _op(op), _Oc(Oc), _th1(th1), _th2(th2), _vd(vd), _pdf(), _flr()
{
    if (std::abs(op.real() * op.imag() / _Oc) > 1e-10) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - op should be either pure real or pure imaginary");
    }
    if (th1 < 0. || th2 < 0.) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - either th1 or th2 or both is negative");
    }
}

std::string Disp::BiMaxwellianVDF::description() const
{
    std::ostringstream os;
    printo(os, typeid(*this).name(), "[Oc->", Oc(), ", op->", op(), ", th1->", _th1, ", th2->", _th2, ", vd->", vd(), "]");
    return os.str();
}

double Disp::BiMaxwellianVDF::f(real_type const v1, real_type const v2) const
{
    real_type const x1 = (v1 - _vd) / _th1;
    real_type const x2 = v2 / _th2;
    real_type       f  = _pdf.f(x1) * _flr.f(x2);
    return f /= _th1 * _th2 * _th2;
}
double Disp::BiMaxwellianVDF::dfdv1(real_type const v1, real_type const v2) const
{
    real_type const x1 = (v1 - _vd) / _th1;
    real_type const x2 = v2 / _th2;
    real_type       df = _pdf.dfdx(x1) * _flr.f(x2);
    return df /= _th1 * _th1 * _th2 * _th2;
}
double Disp::BiMaxwellianVDF::dfdv2(real_type const v1, real_type const v2) const
{
    real_type const x1 = (v1 - _vd) / _th1;
    real_type const x2 = v2 / _th2;
    real_type       df = _pdf.f(x1) * _flr.dfdx(x2);
    return df /= _th1 * _th2 * _th2 * _th2;
}

namespace {
constexpr double zero_tolerance = 1e-5;
}
CX::Matrix Disp::BiMaxwellianVDF::K(std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const
{
    CX::Matrix         K{};
    real_type const    k1th1      = k.first * _th1;
    complex_type const o_jOc_k1vd = o - j * _Oc - k.first * _vd;
    if (std::abs(k1th1) < std::abs(o_jOc_k1vd * ::zero_tolerance)) {
        // asymptotic version
        K11(::get<0, 0>(K), k, o, j);
        K12(::get<0, 1>(K), k, o, j);
        K13(::get<0, 2>(K), k, o, j);

        ::get<1, 0>(K) = -::get<0, 1>(K);
        K22(::get<1, 1>(K), k, o, j);
        K23(::get<1, 2>(K), k, o, j);

        K31(::get<2, 0>(K), k, o, j);
        K32(::get<2, 1>(K), k, o, j);
        K33(::get<2, 2>(K), k, o, j);
    } else {
        // normal version
        triplet_type const &Z = _pdf.Ztriplet(o_jOc_k1vd / k1th1);
        K11(::get<0, 0>(K), k, o, j, Z);
        K12(::get<0, 1>(K), k, o, j, Z);
        K13(::get<0, 2>(K), k, o, j, Z);

        ::get<1, 0>(K) = -::get<0, 1>(K);
        K22(::get<1, 1>(K), k, o, j, Z);
        K23(::get<1, 2>(K), k, o, j, Z);

        K31(::get<2, 0>(K), k, o, j, Z);
        K32(::get<2, 1>(K), k, o, j, Z);
        K33(::get<2, 2>(K), k, o, j, Z);
    }
    return K;
}

// MARK:- K Elements
void Disp::BiMaxwellianVDF::K11(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const
{
    if (0 == j)
        return;
    real_type const    k1 = k.first, k2 = k.second;
    real_type const    ath2       = k2 * _th2 / _Oc;
    complex_type const o_k1vd     = o - k1 * _vd;
    complex_type const o_jOc_k1vd = o_k1vd - j * _Oc;
    // asymptotic version
    K += std::pow(k1 * _th2 / o_jOc_k1vd, 2.);
    K += 2. * o_k1vd / o_jOc_k1vd;
    K *= real_type(-j) * j * _flr.AOa2(j, ath2);
}
void Disp::BiMaxwellianVDF::K11(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j, triplet_type const &Z) const
{
    if (0 == j)
        return;
    real_type const    k1 = k.first, k2 = k.second;
    real_type const    ath2 = k2 * _th2 / _Oc, k1th1 = k1 * _th1;
    complex_type const o_k1vd = o - k1 * _vd;
    // normal version
    K += (_th2 / _th1 - 1.) * (_th2 / _th1 + 1.) * std::get<1>(Z);
    K += o_k1vd / k1th1 * std::get<0>(Z);
    K *= 2. * j * j * _flr.AOa2(j, ath2);
}

void Disp::BiMaxwellianVDF::K12(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const
{
    if (0 == j)
        return;
    real_type const    k1 = k.first, k2 = k.second;
    real_type const    ath2       = k2 * _th2 / _Oc;
    complex_type const o_k1vd     = o - k1 * _vd;
    complex_type const o_jOc_k1vd = o_k1vd - j * _Oc;
    // asymptotic version
    K += std::pow(k1 * _th2 / o_jOc_k1vd, 2.);
    K += 2. * o_k1vd / o_jOc_k1vd;
    K *= real_type(-j) * CX::I * _flr.BOa(j, ath2);
}
void Disp::BiMaxwellianVDF::K12(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j, triplet_type const &Z) const
{
    if (0 == j)
        return;
    real_type const    k1 = k.first, k2 = k.second;
    real_type const    ath2 = k2 * _th2 / _Oc, k1th1 = k1 * _th1;
    complex_type const o_k1vd = o - k1 * _vd;
    // normal version
    K += (_th2 / _th1 - 1.) * (_th2 / _th1 + 1.) * std::get<1>(Z);
    K += o_k1vd / k1th1 * std::get<0>(Z);
    K *= 2. * j * CX::I * _flr.BOa(j, ath2);
}

void Disp::BiMaxwellianVDF::K13(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const
{
    if (0 == j)
        return;
    real_type const    k1 = k.first, k2 = k.second;
    real_type const    ath2       = k2 * _th2 / _Oc;
    complex_type const o_k1vd     = o - k1 * _vd;
    complex_type const o_jOc_k1vd = o_k1vd - j * _Oc;
    // asymptotic version
    K += ath2 * k1 * _th2 * (o - j * _Oc) / (o_jOc_k1vd * o_jOc_k1vd);
    K += 2. * j * k2 * _vd / o_jOc_k1vd;
    K *= -j * _flr.AOa2(j, ath2);
}
void Disp::BiMaxwellianVDF::K13(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j, triplet_type const &Z) const
{
    if (0 == j)
        return;
    real_type const    k1 = k.first, k2 = k.second;
    real_type const    ath2 = k2 * _th2 / _Oc, k1th1 = k1 * _th1;
    complex_type const zjOth1 = (o - j * _Oc) / k1th1;
    // normal version
    K += (zjOth1 * ath2 * _th2 / _th1 + j * k2 / k1) * std::get<1>(Z);
    K += j * k2 * _vd / k1th1 * std::get<0>(Z);
    K *= 2. * j * _flr.AOa2(j, ath2);
}

void Disp::BiMaxwellianVDF::K22(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const
{
    real_type const    k1 = k.first, k2 = k.second;
    real_type const    ath2       = k2 * _th2 / _Oc;
    complex_type const o_k1vd     = o - k1 * _vd;
    complex_type const o_jOc_k1vd = o_k1vd - j * _Oc;
    // asymptotic version
    K += std::pow(k1 * _th2 / o_jOc_k1vd, 2.);
    K += 2. * o_k1vd / o_jOc_k1vd;
    K *= -_flr.C(j, ath2);
}
void Disp::BiMaxwellianVDF::K22(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j, triplet_type const &Z) const
{
    real_type const    k1 = k.first, k2 = k.second;
    real_type const    ath2 = k2 * _th2 / _Oc, k1th1 = k1 * _th1;
    complex_type const o_k1vd = o - k1 * _vd;
    // normal version
    K += (_th2 / _th1 - 1.) * (_th2 / _th1 + 1.) * std::get<1>(Z);
    K += o_k1vd / k1th1 * std::get<0>(Z);
    K *= 2. * _flr.C(j, ath2);
}

void Disp::BiMaxwellianVDF::K23(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const
{
    real_type const    k1 = k.first, k2 = k.second;
    real_type const    ath2       = k2 * _th2 / _Oc;
    complex_type const o_k1vd     = o - k1 * _vd;
    complex_type const o_jOc_k1vd = o_k1vd - j * _Oc;
    // asymptotic version
    K += k1 * _th2 * (o - j * _Oc) / (o_jOc_k1vd * o_jOc_k1vd) * _flr.B(j, ath2);
    K += j ? 2. * j * k2 * _vd / o_jOc_k1vd * _flr.BOa(j, ath2) : 0.;
    K *= CX::I;
}
void Disp::BiMaxwellianVDF::K23(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j, triplet_type const &Z) const
{
    real_type const    k1 = k.first, k2 = k.second;
    real_type const    ath2 = k2 * _th2 / _Oc, k1th1 = k1 * _th1;
    complex_type const zjOth1 = (o - j * _Oc) / k1th1;
    // normal version
    K += zjOth1 * _th2 / _th1 * std::get<1>(Z) * _flr.B(j, ath2);
    K += j ? j * k2 / k1 * (std::get<1>(Z) + _vd / _th1 * std::get<0>(Z)) * _flr.BOa(j, ath2) : 0.;
    K *= -2. * CX::I;
}

void Disp::BiMaxwellianVDF::K31(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const
{
    if (0 == j)
        return;
    real_type const    k1 = k.first, k2 = k.second;
    real_type const    a = k2 / _Oc, ath2 = a * _th2;
    complex_type const o_k1vd     = o - k1 * _vd;
    complex_type const o_jOc_k1vd = o_k1vd - j * _Oc;
    // asymptotic version
    K += (o - j * _Oc) * k1 * _th2 * _th2 / (o_jOc_k1vd * o_jOc_k1vd);
    K += 2. * _vd * o_k1vd / o_jOc_k1vd;
    K *= -j * a * _flr.AOa2(j, ath2);
}
void Disp::BiMaxwellianVDF::K31(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j, triplet_type const &Z) const
{
    if (0 == j)
        return;
    real_type const    k1 = k.first, k2 = k.second;
    real_type const    a = k2 / _Oc, ath2 = a * _th2, k1th1 = k1 * _th1;
    complex_type const o_k1vd = o - k1 * _vd;
    // normal version
    K += _th2 * _th2 / _th1 * (std::get<2>(Z) + _vd / _th1 * std::get<1>(Z));
    K -= _th1 * std::get<2>(Z) + (2. * _vd - o / k1) * std::get<1>(Z) - o_k1vd * _vd / k1th1 * std::get<0>(Z);
    K *= 2. * j * a * _flr.AOa2(j, ath2);
}

void Disp::BiMaxwellianVDF::K32(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const
{
    real_type const    k1 = k.first, k2 = k.second;
    real_type const    a = k2 / _Oc, ath2 = a * _th2;
    complex_type const o_k1vd     = o - k1 * _vd;
    complex_type const o_jOc_k1vd = o_k1vd - j * _Oc;
    // asymptotic version
    K += (o - j * _Oc) * k1 * _th2 / (o_jOc_k1vd * o_jOc_k1vd) * _flr.B(j, ath2);
    K += 2. * a * _vd * o_k1vd / o_jOc_k1vd * _flr.BOa(j, ath2);
    K *= -CX::I;
}
void Disp::BiMaxwellianVDF::K32(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j, triplet_type const &Z) const
{
    real_type const    k1 = k.first, k2 = k.second;
    real_type const    a = k2 / _Oc, ath2 = a * _th2, k1th1 = k1 * _th1;
    complex_type const o_k1vd = o - k1 * _vd;
    // normal version
    K += _th2 / _th1 * (std::get<2>(Z) + _vd / _th1 * std::get<1>(Z)) * _flr.B(j, ath2);
    K -= a * (_th1 * std::get<2>(Z) + (2. * _vd - o / k1) * std::get<1>(Z) - o_k1vd * _vd / k1th1 * std::get<0>(Z)) * _flr.BOa(j, ath2);
    K *= 2. * CX::I;
}

void Disp::BiMaxwellianVDF::K33(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j) const
{
    real_type const    k1 = k.first, k2 = k.second;
    real_type const    a = k2 / _Oc, ath2 = a * _th2;
    complex_type const o_k1vd     = o - k1 * _vd;
    complex_type const o_jOc_k1vd = o_k1vd - j * _Oc;
    // asymptotic version
    K += std::pow((o - j * _Oc) / o_jOc_k1vd, 2.) * _flr.A(j, ath2);
    K += j ? j * _Oc * a * a * (_th1 * _th1 + 2. * _vd * _vd) / o_jOc_k1vd * _flr.AOa2(j, ath2) : 0.;
    K *= -1.;
}
void Disp::BiMaxwellianVDF::K33(complex_type &K, std::pair<real_type, real_type> const &k, complex_type const &o, int const j, triplet_type const &Z) const
{
    real_type const    k1 = k.first, k2 = k.second;
    real_type const    a = k2 / _Oc, ath2 = a * _th2, k1th1 = k1 * _th1;
    complex_type const zjOth1 = (o - j * _Oc) / k1th1;
    // normal version
    K += zjOth1 * (std::get<2>(Z) + _vd / _th1 * std::get<1>(Z)) * _flr.A(j, ath2);
    K += j ? j * _Oc * a * a * (_th1 / k1 * std::get<2>(Z) + 2. * _vd / k1 * std::get<1>(Z) + _vd * _vd / k1th1 * std::get<0>(Z)) * _flr.AOa2(j, ath2) : 0.;
    K *= 2.;
}
