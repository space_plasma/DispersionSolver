//
//  DispersionKitTests-Bessel.mm
//  DispersionKit
//
//  Created by KYUNGGUK MIN on 2/18/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#import <XCTest/XCTest.h>
#include <DispersionKit/DISPBesselJ.h>
#include <DispersionKit/DISPBesselI.h>
#include <UtilityKit/UtilityKit.h>
#include <iostream>
#include <sstream>
#include <exception>

@interface DispersionKitTests_Bessel : XCTestCase

@end

@implementation DispersionKitTests_Bessel

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testBesselJ {
    namespace Bessel = Disp::Bessel;

    std::ostringstream os;
    os.setf(os.fixed);
    constexpr int n = 2;
    printo(os, "{{0, ", Bessel::J<n>(0), "}");
    for (double x = -10; x < 10; x += .1) {
        printo(os, ", {", x, ", ", Bessel::J<n>(x), "}");
    }
    printo(os, "}\n");

    //printo(std::cout, os.str());
}

- (void)testBesselI {
    namespace Bessel = Disp::Bessel;

    std::ostringstream os;
    os.setf(os.fixed);
    constexpr int n = -16;
    printo(os, "{{0, ", Bessel::scaledI<n>(0), "}");
    for (double x = -10000; x < 10000; x += 500) {
        printo(os, ", {", x, ", ", Bessel::scaledI<n>(x), "}");
    }
    printo(os, "}\n");

    //printo(std::cout, os.str());
}

- (void)testBesselJZeros {
    using Disp::BesselJ;

    std::ostringstream os;
    os.setf(os.fixed);
    auto const& zeros = BesselJ::zeros();
    printo(os, "{{", zeros.at(0).first, ", ", zeros.at(0).second, "}");
    for (unsigned i = 1; i < zeros.size(); ++i) {
        printo(os, ", {", zeros[i].first, ", ", zeros[i].second, "}");
    }
    printo(os, "}\n");

    //printo(std::cout, os.str());
}

- (void)testBesselJLookup {
    using Disp::BesselJ;
    namespace Bessel = Disp::Bessel;

    std::ostringstream os;
    os.setf(os.fixed);
    os.precision(15);

    //printo(std::cout, BesselJ::shared()(482, 3068383.750716), "\n");
    //return;

    try {
        os.str("");
        auto const& zeros = BesselJ::zeros();
        BesselJ bslJ;
        Utility::NRRandomReal rng(100);
        constexpr long npts = 1000;
        constexpr int dn = 1, n_min = 199;
        printo(os, "{");
        for (long i = 0; i < npts; ++i) {
            int n = int(rng() * dn) + n_min;
            n *= rng() < .5 ? -1 : 1;

            double x1, x2;
            std::tie(x1, x2) = zeros[unsigned(std::abs(n))];
#if 0
            double x = rng()*(x2-x1) + x1; // focused only on the first interval
#else
            double x = rng() * x2*100;
#endif
            x *= rng() < .5 ? -1 : 1;

            double const J_real = Bessel::J(n, x);
            double const J_lookup = bslJ(n, x);
            //printo(os, i ? ", " : "", "{", n, ", ", x, ", ", J_real, ", ", J_lookup, "}");

            bool const is_same = std::abs(J_real - J_lookup) < 1e-5;
            XCTAssert(is_same, @"i = %ld, J_real(%d, %f) = %f, J_lookup(%d, %f) = %f", i, n, x, J_real, n, x, J_lookup);
            if (!is_same) break;
        }
        printo(os, "}\n");
        //printo(std::cout, os.str());

        if (0) {
            NSString *s = [NSString stringWithUTF8String:os.str().c_str()];
            //s = [s stringByReplacingOccurrencesOfString:@"|" withString:@","];

            NSString *filename = [NSString stringWithFormat:@"testBesselJLookup.m"];
            NSURL *url = [NSURL fileURLWithPath:[NSHomeDirectory() stringByAppendingPathComponent:@"Downloads"] isDirectory:YES];
            url = [url URLByAppendingPathComponent:filename];
            NSError *error;
            XCTAssert([s writeToURL:url atomically:YES encoding:NSUTF8StringEncoding error:&error], @"%@", error);
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // large argument expansion
    try {
        os.str("");
        auto const& zeros = BesselJ::zeros();
        BesselJ bslJ;
        Utility::NRRandomReal rng(100);
        constexpr long npts = 1000;
        constexpr int dn = 2000, n_min = 0;
        printo(os, "{");
        for (long i = 0; i < npts; ++i) {
            int n = int(rng() * dn) + n_min;
            n *= rng() < .5 ? -1 : 1;

            double x1, x2;
            std::tie(x1, x2) = zeros[unsigned(std::abs(n))];
            double x = rng() * x2*10000;
            x *= rng() < .5 ? -1 : 1;

            double const J_real = Bessel::J(n, x);
            double const J_lookup = bslJ(n, x);
            //printo(os, i ? ", " : "", "{", n, ", ", x, ", ", J_real, ", ", J_lookup, "}");

            bool const is_same = std::abs(J_real - J_lookup) < 1e-5;
            XCTAssert(is_same, @"i = %ld, J_real(%d, %f) = %f, J_lookup(%d, %f) = %f", i, n, x, J_real, n, x, J_lookup);
            if (!is_same) break;
        }
        printo(os, "}\n");
        //printo(std::cout, os.str());
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testBesselILookup {
    using Disp::BesselJ;
    using Disp::BesselI;
    namespace Bessel = Disp::Bessel;

    try {
        std::ostringstream os;

        auto const& zeros = BesselJ::zeros();
        BesselI bslI;
        Utility::NRRandomReal rng(101);
        constexpr long npts = 20000;
        constexpr int dn = 2100, n_min = 0;
        printo(os, "{");
        for (long i = 0; i < npts; ++i) {
            int n = int(rng() * dn) + n_min;
            n *= rng() < .5 ? -1 : 1;

            double x1, x2;
            std::tie(x1, x2) = zeros[unsigned(std::abs(n))];
            double x = rng() * x2*100;
            x *= rng() < .5 ? -1 : 1;

            double const I_real = Bessel::scaledI(n, x);
            double const I_lookup = bslI.scaled(n, x);
            printo(os, i ? ", " : "", "{", n, ", ", x, ", ", I_real, ", ", I_lookup, "}");

            bool const is_same = std::abs(I_real - I_lookup) < 1e-8;
            XCTAssert(is_same, @"i = %ld, J_real(%d, %f) = %f, J_lookup(%d, %f) = %f", i, n, x, I_real, n, x, I_lookup);
            if (!is_same) break;
        }
        printo(os, "}\n");

        if (0) {
            NSString *s = [NSString stringWithUTF8String:os.str().c_str()];
            s = [s stringByReplacingOccurrencesOfString:@"e" withString:@"*^"];

            NSString *filename = [NSString stringWithFormat:@"testBesselILookup.m"];
            NSURL *url = [NSURL fileURLWithPath:[NSHomeDirectory() stringByAppendingPathComponent:@"Downloads"] isDirectory:YES];
            url = [url URLByAppendingPathComponent:filename];
            NSError *error;
            XCTAssert([s writeToURL:url atomically:YES encoding:NSUTF8StringEncoding error:&error], @"%@", error);
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

@end
