//
//  DispersionKitTests-CXMatrix.mm
//  DispersionKit
//
//  Created by KYUNGGUK MIN on 3/30/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#import <XCTest/XCTest.h>
#include "DISPSquareMatrix.h"
#include <UtilityKit/UtilityKit.h>
#include <iostream>
#include <sstream>

@interface DispersionKitTests_CXMatrix : XCTestCase

@end

@implementation DispersionKitTests_CXMatrix

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testPredefinedConstants {
    using CX::I;
    constexpr double eps = 1e-10;
    XCTAssert(std::abs(I.real()) < eps && std::abs(I.imag() - 1.) < eps, @"");

    union {CX::Matrix const* m; double const* p;} _;
    unsigned long const sz = sizeof(CX::Matrix)/sizeof(double);

    CX::Matrix const zeros = ::zero<CX::Matrix>();
    _.m = &zeros;
    for (unsigned i = 0; i < sz; ++i) {
        XCTAssert(std::abs(_.p[i]) < eps, @"");
    }
    CX::Matrix const ones = ::one<CX::Matrix>();
    _.m = &ones;
    for (unsigned i = 0; i < sz; i += 2) {
        XCTAssert(std::abs(_.p[i] - 1.) < eps && std::abs(_.p[i+1]) < eps, @"");
    }

    constexpr CX::Matrix eye = ::eye();
    constexpr CX::Number one = ::one<CX::Number>();
    constexpr CX::Number zero = ::zero<CX::Number>();
    XCTAssert((::get<0,0>(eye)) == one && (::get<0,1>(eye)) == zero && (::get<0,2>(eye)) == zero, @"");
    XCTAssert((::get<1,0>(eye)) == zero && (::get<1,1>(eye)) == one && (::get<1,2>(eye)) == zero, @"");
    XCTAssert((::get<2,0>(eye)) == zero && (::get<2,1>(eye)) == zero && (::get<2,2>(eye)) == one, @"");

    XCTAssert(isfinite(eye) && !isfinite(CX::Number{NAN}), @"");
}

- (void)testArithematicCompound {
    union {CX::Matrix const* m; double const* p;} _1, _2;
    unsigned long const sz = sizeof(CX::Matrix)/sizeof(double);

    CX::Matrix const a{{1., 2., 3.}, {4., 5., 6.}, {7., 8., 9.}};
    _1.m = &a;
    CX::Matrix b;
    _2.m = &b;

    b = a;
    b += 1.;
    for (unsigned i = 0; i < sz; i += 2) {
        XCTAssert(_1.p[i] + 1. == _2.p[i] && _1.p[i+1] + 0. == _2.p[i+1], @"");
    }

    b = a;
    b -= 1.;
    for (unsigned i = 0; i < sz; i += 2) {
        XCTAssert(_1.p[i] - 1. == _2.p[i] && _1.p[i+1] - 0. == _2.p[i+1], @"");
    }

    b = a;
    b *= 1.;
    for (unsigned i = 0; i < sz; i += 2) {
        XCTAssert(_1.p[i] * 1. == _2.p[i] && _1.p[i+1] * 1. == _2.p[i+1], @"");
    }

    b = a;
    b /= 2.;
    for (unsigned i = 0; i < sz; i += 2) {
        XCTAssert(_1.p[i] / 2. == _2.p[i] && _1.p[i+1] / 2. == _2.p[i+1], @"");
    }
}

- (void)testArithematicBinary {
    union {CX::Matrix const* m; double const* p;} _1, _2;
    unsigned long const sz = sizeof(CX::Matrix)/sizeof(double);

    CX::Matrix const a{{1., 2., 3.}, {4., 5., 6.}, {7., 8., 9.}};
    _1.m = &a;
    CX::Matrix b;
    _2.m = &b;

    b = a + 1.;
    for (unsigned i = 0; i < sz; i += 2) {
        XCTAssert(_1.p[i] + 1. == _2.p[i] && _1.p[i+1] + 0. == _2.p[i+1], @"");
    }

    b = a - 1.;
    for (unsigned i = 0; i < sz; i += 2) {
        XCTAssert(_1.p[i] - 1. == _2.p[i] && _1.p[i+1] - 0. == _2.p[i+1], @"");
    }

    b = a * 1.;
    for (unsigned i = 0; i < sz; i += 2) {
        XCTAssert(_1.p[i] * 1. == _2.p[i] && _1.p[i+1] * 1. == _2.p[i+1], @"");
    }

    b = a / 2.;
    for (unsigned i = 0; i < sz; i += 2) {
        XCTAssert(_1.p[i] / 2. == _2.p[i] && _1.p[i+1] / 2. == _2.p[i+1], @"");
    }

    b = 1. + a;
    for (unsigned i = 0; i < sz; i += 2) {
        XCTAssert(1. + _1.p[i] == _2.p[i] && 0. + _1.p[i+1] == _2.p[i+1], @"");
    }

    b = 1. - a;
    for (unsigned i = 0; i < sz; i += 2) {
        XCTAssert(1. - _1.p[i] == _2.p[i] && 0. - _1.p[i+1] == _2.p[i+1], @"");
    }

    b = 1. * a;
    for (unsigned i = 0; i < sz; i += 2) {
        XCTAssert(1. * _1.p[i] == _2.p[i] && 1. * _1.p[i+1] == _2.p[i+1], @"");
    }

    b = 2. / a;
    for (unsigned i = 0; i < sz; i += 2) {
        XCTAssert(2. / _1.p[i] == _2.p[i] && 0. / _1.p[i] == _2.p[i+1], @"");
    }
}

- (void)testMatrixOperations {
    using CX::I;
    constexpr double eps = 1e-5;

    const CX::Matrix a{
        {0.0612641 + 0.993952*I, 0.589196 + 0.973922*I, 0.364383 + 0.150839*I},
        {0.729586 + 0.746415*I, 0.933801 + 0.692737*I, 0.38953 + 0.0243735*I},
        {0.460205 + 0.692291*I, 0.859902 + 0.536029*I, 0.250939 + 0.621587*I}
    };
    const CX::Matrix b{
        {0.891563 + 0.863381*I, 0.685841 + 0.462205*I, 0.706949 + 0.873515*I},
        {0.144794 + 0.796736*I, 0.97187 + 0.612398*I, 0.0539649 + 0.325265*I},
        {0.656398 + 0.777872*I, 0.853154 + 0.313419*I, 0.643419 + 0.554075*I}
    };
    const CX::Vector v = b.x;

    double const norm = ::norm<1>(a);
    XCTAssert(std::abs(norm - 3.31427) < eps, @"norm = %f", norm);

    CX::Number const det = ::det(a);
    XCTAssert(std::abs(det - (0.149388 - 0.196794*I)) < eps, @"det = %f %+f", det.real(), det.imag());

    CX::Number const tr = ::tr(a);
    XCTAssert(std::abs(tr - (1.246 + 2.30828*I)) < eps, @"det = %f %+f", tr.real(), tr.imag());

    CX::Matrix const tr1 = ::transpose(a), tr2{
        {0.0612641 + 0.993952*I, 0.729586 + 0.746415*I, 0.460205 + 0.692291*I},
        {0.589196 + 0.973922*I, 0.933801 + 0.692737*I, 0.859902 + 0.536029*I},
        {0.364383 + 0.150839*I, 0.38953 + 0.0243735*I, 0.250939 + 0.621587*I}
    };
    XCTAssert(std::abs(::get<0,0>(tr1) - ::get<0,0>(tr2)) < eps &&
              std::abs(::get<0,1>(tr1) - ::get<0,1>(tr2)) < eps &&
              std::abs(::get<0,2>(tr1) - ::get<0,2>(tr2)) < eps,
              @"");
    XCTAssert(std::abs(::get<1,0>(tr1) - ::get<1,0>(tr2)) < eps &&
              std::abs(::get<1,1>(tr1) - ::get<1,1>(tr2)) < eps &&
              std::abs(::get<1,2>(tr1) - ::get<1,2>(tr2)) < eps,
              @"");
    XCTAssert(std::abs(::get<2,0>(tr1) - ::get<2,0>(tr2)) < eps &&
              std::abs(::get<2,1>(tr1) - ::get<2,1>(tr2)) < eps &&
              std::abs(::get<2,2>(tr1) - ::get<2,2>(tr2)) < eps,
              @"");

    CX::Matrix const dot1 = ::dot(a, b), dot2{
        {-1.37234 + 1.93197*I, -0.177599 + 2.26025*I, -0.959033 +1.29934*I},
        {-0.173961 + 2.45869*I, 0.963376 + 2.23713*I, -0.0740283 + 1.73761*I},
        {-0.808774 + 2.38049*I, 0.52237 + 2.34402*I, -0.590278 + 1.73901*I}
    };
    XCTAssert(std::abs(::get<0,0>(dot1) - ::get<0,0>(dot2)) < eps &&
              std::abs(::get<0,1>(dot1) - ::get<0,1>(dot2)) < eps &&
              std::abs(::get<0,2>(dot1) - ::get<0,2>(dot2)) < eps,
              @"");
    XCTAssert(std::abs(::get<1,0>(dot1) - ::get<1,0>(dot2)) < eps &&
              std::abs(::get<1,1>(dot1) - ::get<1,1>(dot2)) < eps &&
              std::abs(::get<1,2>(dot1) - ::get<1,2>(dot2)) < eps,
              @"");
    XCTAssert(std::abs(::get<2,0>(dot1) - ::get<2,0>(dot2)) < eps &&
              std::abs(::get<2,1>(dot1) - ::get<2,1>(dot2)) < eps &&
              std::abs(::get<2,2>(dot1) - ::get<2,2>(dot2)) < eps,
              @"");

    CX::Vector const dot3 = ::dot(a, v), dot4{-0.723756 + 2.30428*I, 0.580371 + 2.55959*I, -0.210972 + 2.43826*I};
    XCTAssert(std::abs(::get<0>(dot3) - ::get<0>(dot4)) < eps &&
              std::abs(::get<1>(dot3) - ::get<1>(dot4)) < eps &&
              std::abs(::get<2>(dot3) - ::get<2>(dot4)) < eps,
              @"");
}

@end
