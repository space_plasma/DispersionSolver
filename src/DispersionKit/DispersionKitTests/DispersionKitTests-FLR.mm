//
//  DispersionKitTests-FLR.mm
//  DispersionKit
//
//  Created by KYUNGGUK MIN on 3/29/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#import <XCTest/XCTest.h>
#include <DispersionKit/DISPRingFLR.h>
#include <DispersionKit/DISPInterpRingFLR.h>
#include <DispersionKit/DISPMaxwellianFLR.h>
#include <DispersionKit/DISPSplineFLR.h>
#include <DispersionKit/DISPColdRingFLR.h>
#include <DispersionKit/DISPLorentzianFLR.h>
#include <DispersionKit/DISPInterpLorentzianFLR.h>
#include <UtilityKit/UtilityKit.h>
#include <iostream>
#include <sstream>
#include <exception>
#include <memory>
#include <cmath>

@interface DispersionKitTests_FLR : XCTestCase

@end

@implementation DispersionKitTests_FLR

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testMaxwellianFLR {
    using Disp::BesselJ;
    using Disp::RingFLR;
    //using MaxwellianFLR = Disp::MaxwellianFLR;
    using MaxwellianFLR = Disp::InterpMaxwellianFLR;

    Utility::NRRandomReal rng(300);

    try {
        MaxwellianFLR flr{};

        constexpr double a_small = 1e-5;

        double const exact_AOa2 = flr.AOa2(1, 0);
        double const appro_AOa2 = flr.A(1, a_small)/(a_small*a_small);
        XCTAssert(std::abs(exact_AOa2 - appro_AOa2) < 1e-4*std::abs(exact_AOa2), @"%f, %f", exact_AOa2, appro_AOa2);

        double exact_BOa = flr.BOa(0, 0);
        double appro_BOa = flr.B(0, a_small)/a_small;
        XCTAssert(std::abs(exact_BOa - appro_BOa) < 1e-4*std::abs(exact_BOa), @"%f, %f", exact_BOa, appro_BOa);

        exact_BOa = flr.BOa(1, 0);
        appro_BOa = flr.B(1, a_small)/a_small;
        XCTAssert(std::abs(exact_BOa - appro_BOa) < 1e-4*std::abs(exact_BOa), @"%f, %f", exact_BOa, appro_BOa);
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    try {
        constexpr long npts = 100;
        constexpr int dn = 20, n_min = 1;
        RingFLR ring;
        MaxwellianFLR max;
        constexpr double tolerance = 1e-6;

        auto const& zeros = BesselJ::zeros();
        for (long i = 0; i < npts; ++i) {
            int n = int(rng() * dn) + n_min;
            n *= rng() < .5 ? -1 : 1;

            double x1, x2;
            std::tie(x1, x2) = zeros.at(unsigned(std::abs(n)));
            double a = rng()*x2*5;
            a *= rng() < .5 ? -1 : 1;

            bool is_same = true;

            double const A_ring = ring.A(n, a, 0);
            double const A_max = max.A(n, a);
            is_same &= std::abs(A_ring - A_max) < tolerance;
            XCTAssert(std::abs(A_ring - A_max) < tolerance, @"i = %ld, n = %d, a = %f, A_ring = %f, A_max = %f", i, n, a, A_ring, A_max);

            double const B_ring = ring.B(n, a, 0);
            double const B_max = max.B(n, a);
            is_same &= std::abs(B_ring - B_max) < tolerance;
            XCTAssert(std::abs(B_ring - B_max) < tolerance, @"i = %ld, n = %d, a = %f, B_ring = %f, B_max = %f", i, n, a, B_ring, B_max);

            double const C_ring = ring.C(n, a, 0);
            double const C_max = max.C(n, a);
            is_same &= std::abs(C_ring - C_max) < tolerance;
            XCTAssert(std::abs(C_ring - C_max) < tolerance, @"i = %ld, n = %d, a = %f, C_ring = %f, C_max = %f", i, n, a, C_ring, C_max);

            double const AOa2_ring = ring.AOa2(n, a, 0);
            double const AOa2_max = max.AOa2(n, a);
            is_same &= std::abs(AOa2_ring - AOa2_max) < tolerance;
            XCTAssert(std::abs(AOa2_ring - AOa2_max) < tolerance, @"i = %ld, n = %d, a = %f, AOa2_ring = %f, AOa2_max = %f", i, n, a, AOa2_ring, AOa2_max);

            double const BOa_ring = ring.BOa(n, a, 0);
            double const BOa_max = max.BOa(n, a);
            is_same &= std::abs(BOa_ring - BOa_max) < tolerance;
            XCTAssert(std::abs(BOa_ring - BOa_max) < tolerance, @"i = %ld, n = %d, a = %f, BOa_ring = %f, BOa_max = %f", i, n, a, BOa_ring, BOa_max);

            if (!is_same) break;
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testRingFLR {
    using Disp::BesselJ;
    using RingFLR = Disp::RingFLR;

    Utility::NRRandomReal rng(200);
    std::ostringstream os;

    try {
        os.str("");
        constexpr double b = 1;
        RingFLR ring(b);
        constexpr double a_small = 2e-5;

        double const exact_AOa20 = ring.AOa2(1, a_small, 0);
        double const appro_AOa20 = ring.AOa2(1, 0, 0);
        XCTAssert(std::abs(exact_AOa20 - appro_AOa20) < 1e-4*std::abs(exact_AOa20), @"%f, %f", exact_AOa20, appro_AOa20);

        double const exact_AOa21 = ring.AOa2(1, a_small, 1);
        double const appro_AOa21 = ring.AOa2(1, 0, 1);
        XCTAssert(std::abs(exact_AOa21 - appro_AOa21) < 1e-4*std::abs(exact_AOa21), @"%f, %f", exact_AOa21, appro_AOa21);

        double exact_BOa0 = ring.BOa(0, a_small, 0);
        double appro_BOa0 = ring.BOa(0, 0, 0);
        XCTAssert(std::abs(exact_BOa0 - appro_BOa0) < 1e-4*std::abs(exact_BOa0), @"%f, %f", exact_BOa0, appro_BOa0);

        double exact_BOa1 = ring.BOa(0, a_small, 1);
        double appro_BOa1 = ring.BOa(0, 0, 1);
        XCTAssert(std::abs(exact_BOa1 - appro_BOa1) < 1e-4*std::abs(exact_BOa1), @"%f, %f", exact_BOa1, appro_BOa1);

        exact_BOa0 = ring.BOa(1, a_small, 0);
        appro_BOa0 = ring.BOa(1, 0, 0);
        XCTAssert(std::abs(exact_BOa0 - appro_BOa0) < 1e-4*std::abs(exact_BOa0), @"%f, %f", exact_BOa0, appro_BOa0);

        exact_BOa1 = ring.BOa(1, a_small, 1);
        appro_BOa1 = ring.BOa(1, 0, 1);
        XCTAssert(std::abs(exact_BOa1 - appro_BOa1) < 1e-4*std::abs(exact_BOa1), @"%f, %f", exact_BOa1, appro_BOa1);
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    try {
        os.str("");
        constexpr double b = 100;
        constexpr long npts = 100;
        constexpr int dn = 20, n_min = 1;
        RingFLR ring(b);

        auto const& zeros = BesselJ::zeros();
        printo(os, "{");
        for (long i = 0; i < npts; ++i) {
            int n = int(rng() * dn) + n_min;
            n *= rng() < .5 ? -1 : 1;

            double x1, x2;
            std::tie(x1, x2) = zeros.at(unsigned(std::abs(n)));
            double a = rng()*x2*100 + x1 - 3*(x2-x1);
            a *= rng() < .5 ? -1 : 1;

            double const A0 = ring.A(n, a, 0);
            double const A1 = ring.A(n, a, 1);

            double const B0 = ring.B(n, a, 0);
            double const B1 = ring.B(n, a, 1);

            double const C0 = ring.C(n, a, 0);
            double const C1 = ring.C(n, a, 1);

            double const AOa20 = ring.AOa2(n, a, 0);
            double const AOa21 = ring.AOa2(n, a, 1);

            double const BOa0 = ring.BOa(n, a, 0);
            double const BOa1 = ring.BOa(n, a, 1);

            printo(os, i ? ", " : "", "{", n, ", ", a, ", ", b, ", ", A0, ", ", A1, ", ", B0, ", ", B1, ", ", C0, ", ", C1, ", ", AOa20, ", ", AOa21, ", ", BOa0, ", ", BOa1, "}");
        }
        printo(os, "}\n");
        //printo(std::cout, os.str());

        if (0) {
            NSString *s = [NSString stringWithUTF8String:os.str().c_str()];
            s = [s stringByReplacingOccurrencesOfString:@"e" withString:@"*^"];

            NSString *filename = [NSString stringWithFormat:@"testRingFLR.m"];
            NSURL *url = [NSURL fileURLWithPath:[NSHomeDirectory() stringByAppendingPathComponent:@"Downloads"] isDirectory:YES];
            url = [url URLByAppendingPathComponent:filename];
            NSError *error;
            XCTAssert([s writeToURL:url atomically:YES encoding:NSUTF8StringEncoding error:&error], @"%@", error);
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testInterpRingFLR {
    using Disp::BesselJ;
    using RingFLR = Disp::InterpRingFLR;

    Utility::NRRandomReal rng(201);
    std::ostringstream os;

    try {
        os.str("");
        constexpr double b = 1;
        RingFLR ring(b);
        constexpr double a_small = 2e-5;

        double const exact_AOa20 = ring.AOa2(1, a_small, 0);
        double const appro_AOa20 = ring.AOa2(1, 0, 0);
        XCTAssert(std::abs(exact_AOa20 - appro_AOa20) < 1e-4*std::abs(exact_AOa20), @"%f, %f", exact_AOa20, appro_AOa20);

        double const exact_AOa21 = ring.AOa2(1, a_small, 1);
        double const appro_AOa21 = ring.AOa2(1, 0, 1);
        XCTAssert(std::abs(exact_AOa21 - appro_AOa21) < 1e-4*std::abs(exact_AOa21), @"%f, %f", exact_AOa21, appro_AOa21);

        double exact_BOa0 = ring.BOa(0, a_small, 0);
        double appro_BOa0 = ring.BOa(0, 0, 0);
        XCTAssert(std::abs(exact_BOa0 - appro_BOa0) < 1e-4*std::abs(exact_BOa0), @"%f, %f", exact_BOa0, appro_BOa0);

        double exact_BOa1 = ring.BOa(0, a_small, 1);
        double appro_BOa1 = ring.BOa(0, 0, 1);
        XCTAssert(std::abs(exact_BOa1 - appro_BOa1) < 1e-4*std::abs(exact_BOa1), @"%f, %f", exact_BOa1, appro_BOa1);

        exact_BOa0 = ring.BOa(1, a_small, 0);
        appro_BOa0 = ring.BOa(1, 0, 0);
        XCTAssert(std::abs(exact_BOa0 - appro_BOa0) < 1e-4*std::abs(exact_BOa0), @"%f, %f", exact_BOa0, appro_BOa0);

        exact_BOa1 = ring.BOa(1, a_small, 1);
        appro_BOa1 = ring.BOa(1, 0, 1);
        XCTAssert(std::abs(exact_BOa1 - appro_BOa1) < 1e-4*std::abs(exact_BOa1), @"%f, %f", exact_BOa1, appro_BOa1);
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    try {
        os.str("");
        constexpr double b = 100;
        constexpr long npts = 20;
        constexpr int dn = 20, n_min = 1;
        RingFLR ring(b);

        auto const& zeros = BesselJ::zeros();
        printo(os, "{");
        for (long i = 0; i < npts; ++i) {
            int n = int(rng() * dn) + n_min;
            n *= rng() < .5 ? -1 : 1;

            double x1, x2;
            std::tie(x1, x2) = zeros.at(unsigned(std::abs(n)));
            double a = rng()*x2*10 + x1 - 3*(x2-x1);
            a *= rng() < .5 ? -1 : 1;

            double const A0 = ring.A(n, a, 0);
            double const A1 = ring.A(n, a, 1);

            double const B0 = ring.B(n, a, 0);
            double const B1 = ring.B(n, a, 1);

            double const C0 = ring.C(n, a, 0);
            double const C1 = ring.C(n, a, 1);

            double const AOa20 = ring.AOa2(n, a, 0);
            double const AOa21 = ring.AOa2(n, a, 1);

            double const BOa0 = ring.BOa(n, a, 0);
            double const BOa1 = ring.BOa(n, a, 1);

            printo(os, i ? ", " : "", "{", n, ", ", a, ", ", b, ", ", A0, ", ", A1, ", ", B0, ", ", B1, ", ", C0, ", ", C1, ", ", AOa20, ", ", AOa21, ", ", BOa0, ", ", BOa1, "}");
        }
        printo(os, "}\n");
        //printo(std::cout, os.str());

        if (0) {
            NSString *s = [NSString stringWithUTF8String:os.str().c_str()];
            s = [s stringByReplacingOccurrencesOfString:@"e" withString:@"*^"];

            NSString *filename = [NSString stringWithFormat:@"testRingFLR.m"];
            NSURL *url = [NSURL fileURLWithPath:[NSHomeDirectory() stringByAppendingPathComponent:@"Downloads"] isDirectory:YES];
            url = [url URLByAppendingPathComponent:filename];
            NSError *error;
            XCTAssert([s writeToURL:url atomically:YES encoding:NSUTF8StringEncoding error:&error], @"%@", error);
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testSplineFLR {
    using Disp::BesselJ;
    using Disp::RingFLR;
    using Disp::SplineFLR;

    Utility::NRRandomReal rng(400);
    std::ostringstream os;
    os.setf(os.fixed);
    os.precision(15);

    constexpr double b = -1;
    SplineFLR::spline_coefficient_type ring_coefs;
    try {
        Utility::AdaptiveSampling1D<double> sampler;
        sampler.initialPoints = 100;
        sampler.maxRecursion = 50;
        sampler.accuracyGoal = 7;
        auto points = sampler([](double const v2)->double {
            return std::exp(-(v2 - b)*(v2 - b)); // * 2*M_PI*v2;
        }, b - 7, b + 7, 1);

        double const normalization = M_PI*(std::exp(-b*b) + 2./M_2_SQRTPI*b*std::erfc(-b));
        for (auto& pair : points) {
            pair.second /= normalization;
        }

        ring_coefs = Utility::CubicSplineCoefficient<double>(points.begin(), points.end());

        double const exact = std::erfc(-b)/M_2_SQRTPI/normalization;
        double approx = ring_coefs.integrate();
        if (1) {
            Utility::TrapzIntegrator<double, double> integrator;
            integrator.accuracyGoal = 7;
            integrator.maxRecursion = 20;
            integrator.minRecursion = 5;
            auto const f = ring_coefs.interpolator();
            approx = integrator.qsimp([&f](double const v2)->double {
                return *f(v2);
            }, f.min_abscissa() < 0. ? 0. : f.min_abscissa(), f.max_abscissa()).first;
        }
        bool const is_valid_distribution = std::abs(approx - exact) < 1e-5;
        XCTAssert(is_valid_distribution, @"%f", approx);
        if (!is_valid_distribution) {
            return;
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    try {
        os.str("");
        constexpr long npts = 100;
        constexpr int dn = 20, n_min = 1;
        RingFLR ring(b);
        SplineFLR grid(ring_coefs);

        constexpr double tolerance = 1e-5;

        auto const& zeros = BesselJ::zeros();
        for (long i = 0; i < npts; ++i) {
            int n = int(rng() * dn) + n_min;
            n *= rng() < .5 ? -1 : 1;

            double x1, x2;
            std::tie(x1, x2) = zeros.at(unsigned(std::abs(n)));
            double a = rng()*x2*10;
            a *= rng() < .5 ? -1 : 1;

            bool is_same = true;

            double const A_ring = ring.A(n, a);
            double const A_grid = grid.A(n, a);
            is_same &= std::abs(A_ring - A_grid) < tolerance;
            XCTAssert(std::abs(A_ring - A_grid) < tolerance, @"i = %ld, n = %d, a = %f, A_ring = %f, A_grid = %f", i, n, a, A_ring, A_grid);

            double const P_ring = ring.P(n, a);
            double const P_grid = grid.P(n, a);
            is_same &= std::abs(P_ring - P_grid) < tolerance;
            XCTAssert(std::abs(P_ring - P_grid) < tolerance, @"i = %ld, n = %d, a = %f, P_ring = %f, A_grid = %f", i, n, a, P_ring, P_grid);

            double const B_ring = ring.B(n, a);
            double const B_grid = grid.B(n, a);
            is_same &= std::abs(B_ring - B_grid) < tolerance;
            XCTAssert(std::abs(B_ring - B_grid) < tolerance, @"i = %ld, n = %d, a = %f, B_ring = %f, B_grid = %f", i, n, a, B_ring, B_grid);

            double const Q_ring = ring.Q(n, a);
            double const Q_grid = grid.Q(n, a);
            is_same &= std::abs(Q_ring - Q_grid) < tolerance;
            XCTAssert(std::abs(Q_ring - Q_grid) < tolerance, @"i = %ld, n = %d, a = %f, Q_ring = %f, Q_grid = %f", i, n, a, Q_ring, Q_grid);

            double const C_ring = ring.C(n, a);
            double const C_grid = grid.C(n, a);
            is_same &= std::abs(C_ring - C_grid) < tolerance;
            XCTAssert(std::abs(C_ring - C_grid) < tolerance, @"i = %ld, n = %d, a = %f, C_ring = %f, C_grid = %f", i, n, a, C_ring, C_grid);

            double const R_ring = ring.R(n, a);
            double const R_grid = grid.R(n, a);
            is_same &= std::abs(R_ring - R_grid) < tolerance;
            XCTAssert(std::abs(R_ring - R_grid) < tolerance, @"i = %ld, n = %d, a = %f, R_ring = %f, R_grid = %f", i, n, a, R_ring, R_grid);

            double const AOa2_ring = ring.AOa2(n, a);
            double const AOa2_grid = grid.AOa2(n, a);
            is_same &= std::abs(AOa2_ring - AOa2_grid) < tolerance;
            XCTAssert(std::abs(AOa2_ring - AOa2_grid) < tolerance, @"i = %ld, n = %d, a = %f, AOa2_ring = %f, AOa2_grid = %f", i, n, a, AOa2_ring, AOa2_grid);

            double const POa2_ring = ring.POa2(n, a);
            double const POa2_grid = grid.POa2(n, a);
            is_same &= std::abs(POa2_ring - POa2_grid) < tolerance;
            XCTAssert(std::abs(POa2_ring - POa2_grid) < tolerance, @"i = %ld, n = %d, a = %f, POa2_ring = %f, POa2_grid = %f", i, n, a, POa2_ring, POa2_grid);

            double const BOa_ring = ring.BOa(n, a);
            double const BOa_grid = grid.BOa(n, a);
            is_same &= std::abs(BOa_ring - BOa_grid) < tolerance;
            XCTAssert(std::abs(BOa_ring - BOa_grid) < tolerance, @"i = %ld, n = %d, a = %f, BOa_ring = %f, BOa_grid = %f", i, n, a, BOa_ring, BOa_grid);

            double const QOa_ring = ring.QOa(n, a);
            double const QOa_grid = grid.QOa(n, a);
            is_same &= std::abs(QOa_ring - QOa_grid) < tolerance;
            XCTAssert(std::abs(QOa_ring - QOa_grid) < tolerance, @"i = %ld, n = %d, a = %f, QOa_ring = %f, QOa_grid = %f", i, n, a, QOa_ring, QOa_grid);

            if (!is_same) break;
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    try {
        os.str("");
        RingFLR ring(b);
        SplineFLR grid(std::move(ring_coefs));

        double const exact_AOa2 = ring.AOa2(1, 0);
        double const appro_AOa2 = grid.AOa2(1, 0);
        XCTAssert(std::abs(exact_AOa2 - appro_AOa2) < 1e-4*std::abs(exact_AOa2), @"%f, %f", exact_AOa2, appro_AOa2);
        
        double exact_BOa = ring.BOa(0, 0);
        double appro_BOa = grid.BOa(0, 0);
        XCTAssert(std::abs(exact_BOa - appro_BOa) < 1e-4*std::abs(exact_BOa), @"%f, %f", exact_BOa, appro_BOa);

        exact_BOa = ring.BOa(1, 0);
        appro_BOa = grid.BOa(1, 0);
        XCTAssert(std::abs(exact_BOa - appro_BOa) < 1e-4*std::abs(exact_BOa), @"%f, %f", exact_BOa, appro_BOa);
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}

- (void)testSplineFLR_SmallTemperature_Maxwellian {
    using Disp::MaxwellianFLR;
    using Disp::SplineFLR;

    try {
        constexpr double th = .0001, kappa = 2000;
        SplineFLR::spline_coefficient_type coefs; {
            Utility::AdaptiveSampling1D<double> sampler;
            sampler.initialPoints = 100;
            sampler.maxRecursion = 50;
            sampler.accuracyGoal = 7;
            auto points = sampler([](double const v2)->double {
                return std::pow(1 + v2*v2/(th*th*kappa), -(kappa + 1));
            }, -50*th, 50*th, 1);

            double const normalization = M_PI*th*th;
            for (auto& pair : points) {
                pair.second /= normalization;
            }
            coefs = Utility::CubicSplineCoefficient<double>(points.begin(), points.end());
        }

        double exact = 1;
        double approx; {
            Utility::TrapzIntegrator<double, double> integrator;
            integrator.accuracyGoal = 7;
            integrator.maxRecursion = 20;
            integrator.minRecursion = 5;
            auto const f = coefs.interpolator();
            approx = integrator.qsimp([&f](double const v2)->double {
                return 2*M_PI*v2**f(v2);
            }, f.min_abscissa() < 0. ? 0. : f.min_abscissa(), f.max_abscissa()).first;
        }
        bool const is_valid_distribution = std::abs(approx - exact) < 1e-5;
        XCTAssert(is_valid_distribution, @"%f", approx);
        if (!is_valid_distribution) {
            return;
        }

        MaxwellianFLR maxFLR;
        SplineFLR splineFLR(coefs);

        constexpr double tolerance = 1e-5;

        constexpr int n = 1;
        constexpr double a = 1;

        exact = maxFLR.A(n, a*th); approx = splineFLR.A(n, a);
        XCTAssert(std::abs(exact - approx) < tolerance, @"maxFLR.A = %f, splineFLR.A = %f", exact, approx);

        exact = maxFLR.AOa2(n, a*th)*th*th; approx = splineFLR.AOa2(n, a);
        XCTAssert(std::abs(exact - approx) < tolerance, @"maxFLR.AOa2 = %f, splineFLR.AOa2 = %f", exact, approx);

        exact = maxFLR.B(n, a*th)*th; approx = splineFLR.B(n, a);
        XCTAssert(std::abs(exact - approx) < tolerance, @"maxFLR.B = %f, splineFLR.B = %f", exact, approx);

        exact = maxFLR.BOa(n, a*th)*th*th; approx = splineFLR.BOa(n, a);
        XCTAssert(std::abs(exact - approx) < tolerance, @"maxFLR.BOa = %f, splineFLR.BOa = %f", exact, approx);

        exact = maxFLR.C(n, a*th)*th*th; approx = splineFLR.C(n, a);
        XCTAssert(std::abs(exact - approx) < tolerance, @"maxFLR.C = %f, splineFLR.C = %f", exact, approx);

        exact = maxFLR.P(n, a*th)/(th*th); approx = splineFLR.P(n, a);
        XCTAssert(std::abs(exact - approx) < tolerance, @"maxFLR.P = %f, splineFLR.P = %f", exact, approx);

        exact = maxFLR.POa2(n, a*th); approx = splineFLR.POa2(n, a);
        XCTAssert(std::abs(exact - approx) < tolerance, @"maxFLR.POa2 = %f, splineFLR.POa2 = %f", exact, approx);

        exact = maxFLR.Q(n, a*th)/th; approx = splineFLR.Q(n, a);
        XCTAssert(std::abs(exact - approx) < tolerance, @"maxFLR.Q = %f, splineFLR.Q = %f", exact, approx);

        exact = maxFLR.QOa(n, a*th); approx = splineFLR.QOa(n, a);
        XCTAssert(std::abs(exact - approx) < tolerance, @"maxFLR.QOa = %f, splineFLR.QOa = %f", exact, approx);

        exact = maxFLR.R(n, a*th); approx = splineFLR.R(n, a);
        XCTAssert(std::abs(exact - approx) < tolerance, @"maxFLR.R = %f, splineFLR.R = %f", exact, approx);
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}

- (void)testSplineFLR_SmallTemperature_Ring {
    using Disp::RingFLR;
    using Disp::SplineFLR;

    try {
        constexpr double th = .0001, b = 10;
        SplineFLR::spline_coefficient_type coefs; {
            Utility::AdaptiveSampling1D<double> sampler;
            sampler.initialPoints = 100;
            sampler.maxRecursion = 50;
            sampler.accuracyGoal = 7;
            auto points = sampler([](double const x)->double {
                return std::exp(-(x - b)*(x - b));
            }, b - 7, b + 7, 1);

            double const normalization = M_PI*th*th*(std::exp(-b*b) + 2./M_2_SQRTPI*b*std::erfc(-b));
            for (auto& pair : points) {
                pair.first *= th;
                pair.second /= normalization;
            }
            coefs = Utility::CubicSplineCoefficient<double>(points.begin(), points.end());
        }

        double exact = 1;
        double approx; {
            Utility::TrapzIntegrator<double, double> integrator;
            integrator.accuracyGoal = 7;
            integrator.maxRecursion = 20;
            integrator.minRecursion = 5;
            auto const f = coefs.interpolator();
            approx = integrator.qsimp([&f](double const v2)->double {
                return 2*M_PI*v2**f(v2);
            }, f.min_abscissa() < 0. ? 0. : f.min_abscissa(), f.max_abscissa()).first;
        }
        bool const is_valid_distribution = std::abs(approx - exact) < 1e-5;
        XCTAssert(is_valid_distribution, @"%f", approx);
        if (!is_valid_distribution) {
            return;
        }

        RingFLR ringFLR(b);
        SplineFLR splineFLR(coefs);

        constexpr double tolerance = 1e-5;

        constexpr int n = 1;
        constexpr double a = 1;

        exact = ringFLR.A(n, a*th); approx = splineFLR.A(n, a);
        XCTAssert(std::abs(exact - approx) < tolerance, @"ringFLR.A = %f, splineFLR.A = %f", exact, approx);

        exact = ringFLR.AOa2(n, a*th)*th*th; approx = splineFLR.AOa2(n, a);
        XCTAssert(std::abs(exact - approx) < tolerance, @"ringFLR.AOa2 = %f, splineFLR.AOa2 = %f", exact, approx);

        exact = ringFLR.B(n, a*th)*th; approx = splineFLR.B(n, a);
        XCTAssert(std::abs(exact - approx) < tolerance, @"ringFLR.B = %f, splineFLR.B = %f", exact, approx);

        exact = ringFLR.BOa(n, a*th)*th*th; approx = splineFLR.BOa(n, a);
        XCTAssert(std::abs(exact - approx) < tolerance, @"ringFLR.BOa = %f, splineFLR.BOa = %f", exact, approx);

        exact = ringFLR.C(n, a*th)*th*th; approx = splineFLR.C(n, a);
        XCTAssert(std::abs(exact - approx) < tolerance, @"ringFLR.C = %f, splineFLR.C = %f", exact, approx);

        exact = ringFLR.P(n, a*th)/(th*th); approx = splineFLR.P(n, a);
        XCTAssert(std::abs(exact - approx) < tolerance, @"ringFLR.P = %f, splineFLR.P = %f", exact, approx);

        exact = ringFLR.POa2(n, a*th); approx = splineFLR.POa2(n, a);
        XCTAssert(std::abs(exact - approx) < tolerance, @"ringFLR.POa2 = %f, splineFLR.POa2 = %f", exact, approx);

        exact = ringFLR.Q(n, a*th)/th; approx = splineFLR.Q(n, a);
        XCTAssert(std::abs(exact - approx) < tolerance, @"ringFLR.Q = %f, splineFLR.Q = %f", exact, approx);

        exact = ringFLR.QOa(n, a*th); approx = splineFLR.QOa(n, a);
        XCTAssert(std::abs(exact - approx) < tolerance, @"ringFLR.QOa = %f, splineFLR.QOa = %f", exact, approx);

        exact = ringFLR.R(n, a*th); approx = splineFLR.R(n, a);
        XCTAssert(std::abs(exact - approx) < tolerance, @"ringFLR.R = %f, splineFLR.R = %f", exact, approx);
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}

- (void)testColdRingFLR {
    using Disp::BesselJ;
    using Disp::RingFLR;
    using Disp::ColdRingFLR;

    Utility::NRRandomReal rng(500);
    std::ostringstream os;
    os.setf(os.fixed);
    os.precision(15);

    constexpr double vr = 10, th = 1e-5, th2 = th*th, b = vr/th;

    try {
        os.str("");
        constexpr long npts = 100;
        constexpr int dn = 20, n_min = 1;
        RingFLR warm(b);
        ColdRingFLR cold(vr);

        constexpr double tolerance = 1e-4;

//        {
//            int const n = -17;
//            double const a = -235.137393/10, ath = a*th;
//            printo(std::cout, warm.Q(n, ath)/th, ", ", cold.Q(n, a), "\n");
//            return;
//        }

        auto const& zeros = BesselJ::zeros();
        for (long i = 0; i < npts; ++i) {
            int n = int(rng() * dn) + n_min;
            n *= rng() < .5 ? -1 : 1;

            double x1, x2;
            std::tie(x1, x2) = zeros.at(unsigned(std::abs(n)));
            double a = rng()*x2*10;
            a *= rng() < .5 ? -1 : 1;
            double const ath = a*th;

            bool is_same = true;

            double const A_warm = warm.A(n, ath);
            double const A_cold = cold.A(n, a);
            is_same &= std::abs(A_warm - A_cold) < tolerance;
            XCTAssert(std::abs(A_warm - A_cold) < tolerance, @"i = %ld, n = %d, a = %f, A_warm = %f, A_cold = %f", i, n, a, A_warm, A_cold);

            double const P_warm = warm.P(n, ath)/th2;
            double const P_cold = cold.P(n, a);
            is_same &= std::abs(P_warm - P_cold) < tolerance;
            XCTAssert(std::abs(P_warm - P_cold) < tolerance, @"i = %ld, n = %d, a = %f, P_warm = %f, P_cold = %f", i, n, a, P_warm, P_cold);

            double const B_warm = warm.B(n, ath)*th;
            double const B_cold = cold.B(n, a);
            is_same &= std::abs(B_warm - B_cold) < tolerance;
            XCTAssert(std::abs(B_warm - B_cold) < tolerance, @"i = %ld, n = %d, a = %f, B_warm = %f, B_cold = %f", i, n, a, B_warm, B_cold);

            double const Q_warm = warm.Q(n, ath)/th;
            double const Q_cold = cold.Q(n, a);
            is_same &= std::abs(Q_warm - Q_cold) < tolerance;
            XCTAssert(std::abs(Q_warm - Q_cold) < tolerance, @"i = %ld, n = %d, a = %f, Q_warm = %f, Q_cold = %f", i, n, a, Q_warm, Q_cold);

            double const C_warm = warm.C(n, ath)*th2;
            double const C_cold = cold.C(n, a);
            is_same &= std::abs(C_warm - C_cold) < tolerance;
            XCTAssert(std::abs(C_warm - C_cold) < tolerance, @"i = %ld, n = %d, a = %f, C_warm = %f, C_cold = %f", i, n, a, C_warm, C_cold);

            double const R_warm = warm.R(n, ath);
            double const R_cold = cold.R(n, a);
            is_same &= std::abs(R_warm - R_cold) < tolerance;
            XCTAssert(std::abs(R_warm - R_cold) < tolerance, @"i = %ld, n = %d, a = %f, R_warm = %f, R_cold = %f", i, n, a, R_warm, R_cold);

            double const AOa2_warm = warm.AOa2(n, ath)*th2;
            double const AOa2_cold = cold.AOa2(n, a);
            is_same &= std::abs(AOa2_warm - AOa2_cold) < tolerance;
            XCTAssert(std::abs(AOa2_warm - AOa2_cold) < tolerance, @"i = %ld, n = %d, a = %f, AOa2_warm = %f, AOa2_cold = %f", i, n, a, AOa2_warm, AOa2_cold);

            double const POa2_warm = warm.POa2(n, ath);
            double const POa2_cold = cold.POa2(n, a);
            is_same &= std::abs(POa2_warm - POa2_cold) < tolerance;
            XCTAssert(std::abs(POa2_warm - POa2_cold) < tolerance, @"i = %ld, n = %d, a = %f, POa2_warm = %f, POa2_cold = %f", i, n, a, POa2_warm, POa2_cold);

            double const BOa_warm = warm.BOa(n, ath)*th2;
            double const BOa_cold = cold.BOa(n, a);
            is_same &= std::abs(BOa_warm - BOa_cold) < tolerance;
            XCTAssert(std::abs(BOa_warm - BOa_cold) < tolerance, @"i = %ld, n = %d, a = %f, BOa_warm = %f, BOa_cold = %f", i, n, a, BOa_warm, BOa_cold);

            double const QOa_warm = warm.QOa(n, ath);
            double const QOa_cold = cold.QOa(n, a);
            is_same &= std::abs(QOa_warm - QOa_cold) < tolerance;
            XCTAssert(std::abs(QOa_warm - QOa_cold) < tolerance, @"i = %ld, n = %d, a = %f, QOa_warm = %f, QOa_cold = %f", i, n, a, QOa_warm, QOa_cold);

            if (!is_same) break;
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    try {
        os.str("");
        RingFLR warm(b);
        ColdRingFLR cold(vr);

        constexpr double tolerance = 1e-4;
        double const a = 0., ath = a*th;

        double warm_AOa2 = warm.AOa2(1, ath)*th2;
        double cold_AOa2 = cold.AOa2(1, a);
        XCTAssert(std::abs(warm_AOa2 - cold_AOa2) < tolerance*std::abs(warm_AOa2), @"%f, %f", warm_AOa2, cold_AOa2);

        cold_AOa2 = cold.AOa2(2, a);
        XCTAssert(std::abs(cold_AOa2) < tolerance, @"%f", cold_AOa2);

        double warm_BOa = warm.BOa(0, ath)*th2;
        double cold_BOa = cold.BOa(0, a);
        XCTAssert(std::abs(warm_BOa - cold_BOa) < tolerance*std::abs(warm_BOa), @"%f, %f", warm_BOa, cold_BOa);

        warm_BOa = warm.BOa(1, ath)*th2;
        cold_BOa = cold.BOa(1, a);
        XCTAssert(std::abs(warm_BOa - cold_BOa) < tolerance*std::abs(warm_BOa), @"%f, %f", warm_BOa, cold_BOa);

        cold_BOa = cold.BOa(2, a);
        XCTAssert(std::abs(cold_BOa) < tolerance, @"%f", cold_BOa);

        double warm_POa2 = cold.POa2(0, 1e-4);
        double cold_POa2 = cold.POa2(0, a);
        XCTAssert(std::abs(warm_POa2 - cold_POa2) < tolerance*std::abs(warm_POa2), @"%f, %f", warm_POa2, cold_POa2);

        warm_POa2 = warm.POa2(1, ath);
        cold_POa2 = cold.POa2(1, a);
        XCTAssert(std::abs(warm_POa2 - cold_POa2) < tolerance*std::abs(warm_POa2), @"%f, %f", warm_POa2, cold_POa2);

        cold_POa2 = cold.POa2(2, a);
        XCTAssert(std::abs(cold_POa2) < tolerance, @"%f", cold_POa2);

        double warm_QOa = warm.QOa(0, ath);
        double cold_QOa = cold.QOa(0, a);
        XCTAssert(std::abs(warm_QOa - cold_QOa) < tolerance*std::abs(warm_QOa), @"%f, %f", warm_QOa, cold_QOa);

        warm_QOa = warm.QOa(1, ath);
        cold_QOa = cold.QOa(1, a);
        XCTAssert(std::abs(warm_QOa - cold_QOa) < tolerance*std::abs(warm_QOa), @"%f, %f", warm_QOa, cold_QOa);

        cold_QOa = cold.QOa(2, a);
        XCTAssert(std::abs(cold_QOa) < tolerance, @"%f", cold_QOa);
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testLorentzianFLR_DivisionBySmallA {
    //using LorentzianFLR = Disp::LorentzianFLR;
    using LorentzianFLR = Disp::InterpLorentzianFLR;

    try {
        constexpr double kappa = 2;
        LorentzianFLR flr{kappa};

        constexpr double a_small = 1e-6;
        constexpr double torelance = 1e-4;

        double const exact_AOa2 = flr.AOa2(1, 0);
        double const appro_AOa2 = flr.A(1, a_small)/(a_small*a_small);
        XCTAssert(std::abs(exact_AOa2 - appro_AOa2) < torelance*std::abs(exact_AOa2), @"%f, %f", exact_AOa2, appro_AOa2);

        double exact_BOa = flr.BOa(0, 0);
        double appro_BOa = flr.B(0, a_small)/a_small;
        XCTAssert(std::abs(exact_BOa - appro_BOa) < torelance*std::abs(exact_BOa), @"%f, %f", exact_BOa, appro_BOa);

        exact_BOa = flr.BOa(1, 0);
        appro_BOa = flr.B(1, a_small)/a_small;
        XCTAssert(std::abs(exact_BOa - appro_BOa) < torelance*std::abs(exact_BOa), @"%f, %f", exact_BOa, appro_BOa);

        double const exact_POa2 = flr.POa2(1, 0);
        double const appro_POa2 = flr.P(1, a_small)/(a_small*a_small);
        XCTAssert(std::abs(exact_POa2 - appro_POa2) < torelance*std::abs(exact_POa2), @"%f, %f", exact_POa2, appro_POa2);

        double exact_QOa = flr.QOa(0, 0);
        double appro_QOa = flr.Q(0, a_small)/a_small;
        XCTAssert(std::abs(exact_QOa - appro_QOa) < torelance*std::abs(exact_QOa), @"%f, %f", exact_QOa, appro_QOa);

        exact_QOa = flr.QOa(1, 0);
        appro_QOa = flr.Q(1, a_small)/a_small;
        XCTAssert(std::abs(exact_QOa - appro_QOa) < torelance*std::abs(exact_QOa), @"%f, %f", exact_QOa, appro_QOa);
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testLorentzianFLR_InfiniteKappa {
    using Disp::BesselJ;
    //using MaxwellianFLR = Disp::MaxwellianFLR;
    using MaxwellianFLR = Disp::InterpMaxwellianFLR;
    //using LorentzianFLR = Disp::LorentzianFLR;
    using LorentzianFLR = Disp::InterpLorentzianFLR;

    Utility::NRRandomReal rng(600);

    try {
        LorentzianFLR kappa{10000};
        MaxwellianFLR maxwe{};

        constexpr long npts = 100;
        constexpr int dn = 50, n_min = 0;
        constexpr double tolerance = 1e-5;

        auto const& zeros = BesselJ::zeros();
        for (long i = 0; i < npts; ++i) {
            int n = int(rng() * dn) + n_min;
            n *= rng() < .5 ? -1 : 1;

            double x1, x2;
            std::tie(x1, x2) = zeros.at(unsigned(std::abs(n)));
            double a = rng()*x2*10;
            a *= rng() < .5 ? -1 : 1;

            bool is_same = true;

            double const maxweA = maxwe.A(n, a);
            double const kappaA = kappa.A(n, a);
            is_same &= std::abs(maxweA - kappaA) < tolerance;
            XCTAssert(std::abs(maxweA - kappaA) < tolerance, @"i = %ld, n = %d, a = %f, maxweA = %f, kappaA = %f", i, n, a, maxweA, kappaA);

            double const maxweP = maxwe.P(n, a);
            double const kappaP = kappa.P(n, a);
            is_same &= std::abs(maxweP - kappaP) < tolerance;
            XCTAssert(std::abs(maxweP - kappaP) < tolerance, @"i = %ld, n = %d, a = %f, maxweP = %f, kappaP = %f", i, n, a, maxweP, kappaP);

            double const maxweB = maxwe.B(n, a);
            double const kappaB = kappa.B(n, a);
            is_same &= std::abs(maxweB - kappaB) < tolerance;
            XCTAssert(std::abs(maxweB - kappaB) < tolerance, @"i = %ld, n = %d, a = %f, maxweB = %f, kappaB = %f", i, n, a, maxweB, kappaB);

            double const maxweQ = maxwe.Q(n, a);
            double const kappaQ = kappa.Q(n, a);
            is_same &= std::abs(maxweQ - kappaQ) < tolerance;
            XCTAssert(std::abs(maxweQ - kappaQ) < tolerance, @"i = %ld, n = %d, a = %f, maxweQ = %f, kappaQ = %f", i, n, a, maxweQ, kappaQ);

            double const maxweC = maxwe.C(n, a);
            double const kappaC = kappa.C(n, a);
            is_same &= std::abs(maxweC - kappaC) < tolerance;
            XCTAssert(std::abs(maxweC - kappaC) < tolerance, @"i = %ld, n = %d, a = %f, maxweC = %f, kappaC = %f", i, n, a, maxweC, kappaC);

            double const maxweR = maxwe.R(n, a);
            double const kappaR = kappa.R(n, a);
            is_same &= std::abs(maxweR - kappaR) < tolerance;
            XCTAssert(std::abs(maxweR - kappaR) < tolerance, @"i = %ld, n = %d, a = %f, maxweR = %f, kappaR = %f", i, n, a, maxweR, kappaR);

            double const maxweAOa2 = maxwe.AOa2(n, a);
            double const kappaAOa2 = kappa.AOa2(n, a);
            is_same &= std::abs(maxweAOa2 - kappaAOa2) < tolerance;
            XCTAssert(std::abs(maxweAOa2 - kappaAOa2) < tolerance, @"i = %ld, n = %d, a = %f, maxweAOa2 = %f, kappaAOa2 = %f", i, n, a, maxweAOa2, kappaAOa2);

            double const maxwePOa2 = maxwe.POa2(n, a);
            double const kappaPOa2 = kappa.POa2(n, a);
            is_same &= std::abs(maxwePOa2 - kappaPOa2) < tolerance;
            XCTAssert(std::abs(maxwePOa2 - kappaPOa2) < tolerance, @"i = %ld, n = %d, a = %f, maxwePOa2 = %f, kappaPOa2 = %f", i, n, a, maxwePOa2, kappaPOa2);

            double const maxweBOa = maxwe.BOa(n, a);
            double const kappaBOa = kappa.BOa(n, a);
            is_same &= std::abs(maxweBOa - kappaBOa) < tolerance;
            XCTAssert(std::abs(maxweBOa - kappaBOa) < tolerance, @"i = %ld, n = %d, a = %f, maxweBOa = %f, kappaBOa = %f", i, n, a, maxweBOa, kappaBOa);

            double const maxweQOa = maxwe.QOa(n, a);
            double const kappaQOa = kappa.QOa(n, a);
            is_same &= std::abs(maxweQOa - kappaQOa) < tolerance;
            XCTAssert(std::abs(maxweQOa - kappaQOa) < tolerance, @"i = %ld, n = %d, a = %f, maxweQOa = %f, kappaQOa = %f", i, n, a, maxweQOa, kappaQOa);

            if (!is_same) break;
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testLorentzianFLR_FiniteKappa {
    using Disp::BesselJ;
    using Disp::SplineFLR;
    //using LorentzianFLR = Disp::LorentzianFLR;
    using LorentzianFLR = Disp::InterpLorentzianFLR;

    Utility::NRRandomReal rng(600);
    constexpr double kappa = 2;

    std::unique_ptr<SplineFLR> _spline_flr;
    try {
        Utility::AdaptiveSampling1D<double> sampler;
        sampler.initialPoints = 100;
        sampler.maxRecursion = 100;
        sampler.accuracyGoal = 10;
        constexpr double epsilon = 1e-13;
        auto points = sampler([](double const x)->double {
            return std::pow(1 + x*x/kappa, -(kappa + 1));
        }, 0., std::sqrt((std::pow(epsilon, -1./(1 + kappa)) - 1)*kappa), 1);

        double const normalization = M_PI;
        for (auto& pair : points) {
            pair.second /= normalization;
        }

        SplineFLR::spline_coefficient_type coefs = Utility::CubicSplineCoefficient<double>(points.begin(), points.end());
        double const exact = .25*M_2_SQRTPI*std::tgamma(kappa + .5)/(std::sqrt(kappa)*std::tgamma(kappa));
        bool const is_valid_distribution = std::abs(coefs.integrate() - exact) < 1e-5;
        XCTAssert(is_valid_distribution, @"%f", coefs.integrate());
        if (!is_valid_distribution) {
            return;
        }

        _spline_flr.reset(new SplineFLR(std::move(coefs)));
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
    SplineFLR const &splin_flr = *_spline_flr;

    try {
        LorentzianFLR kappa_flr{kappa};

        constexpr long npts = 100;
        constexpr int dn = 20, n_min = 0;
        constexpr double tolerance = 1e-5;

        auto const& zeros = BesselJ::zeros();
        for (long i = 0; i < npts; ++i) {
            int n = int(rng() * dn) + n_min;
            n *= rng() < .5 ? -1 : 1;

            double x1, x2;
            std::tie(x1, x2) = zeros.at(unsigned(std::abs(n)));
            double a = rng()*x2*10;
            a *= rng() < .5 ? -1 : 1;

            bool is_same = true;

            double const splinA = splin_flr.A(n, a);
            double const kappaA = kappa_flr.A(n, a);
            is_same &= std::abs(splinA - kappaA) < tolerance;
            XCTAssert(std::abs(splinA - kappaA) < tolerance, @"i = %ld, n = %d, a = %f, splinA = %f, kappaA = %f", i, n, a, splinA, kappaA);

            double const splinP = splin_flr.P(n, a);
            double const kappaP = kappa_flr.P(n, a);
            is_same &= std::abs(splinP - kappaP) < tolerance;
            XCTAssert(std::abs(splinP - kappaP) < tolerance, @"i = %ld, n = %d, a = %f, splinP = %f, kappaP = %f", i, n, a, splinP, kappaP);

            double const splinB = splin_flr.B(n, a);
            double const kappaB = kappa_flr.B(n, a);
            is_same &= std::abs(splinB - kappaB) < tolerance;
            XCTAssert(std::abs(splinB - kappaB) < tolerance, @"i = %ld, n = %d, a = %f, splinB = %f, kappaB = %f", i, n, a, splinB, kappaB);

            double const splinQ = splin_flr.Q(n, a);
            double const kappaQ = kappa_flr.Q(n, a);
            is_same &= std::abs(splinQ - kappaQ) < tolerance;
            XCTAssert(std::abs(splinQ - kappaQ) < tolerance, @"i = %ld, n = %d, a = %f, splinQ = %f, kappaQ = %f", i, n, a, splinQ, kappaQ);

            double const splinC = splin_flr.C(n, a);
            double const kappaC = kappa_flr.C(n, a);
            is_same &= std::abs(splinC - kappaC) < tolerance;
            XCTAssert(std::abs(splinC - kappaC) < tolerance, @"i = %ld, n = %d, a = %f, splinC = %f, kappaC = %f", i, n, a, splinC, kappaC);

            double const splinR = splin_flr.R(n, a);
            double const kappaR = kappa_flr.R(n, a);
            is_same &= std::abs(splinR - kappaR) < tolerance;
            XCTAssert(std::abs(splinR - kappaR) < tolerance, @"i = %ld, n = %d, a = %f, splinR = %f, kappaR = %f", i, n, a, splinR, kappaR);

            double const splinAOa2 = splin_flr.AOa2(n, a);
            double const kappaAOa2 = kappa_flr.AOa2(n, a);
            is_same &= std::abs(splinAOa2 - kappaAOa2) < tolerance;
            XCTAssert(std::abs(splinAOa2 - kappaAOa2) < tolerance, @"i = %ld, n = %d, a = %f, splinAOa2 = %f, kappaAOa2 = %f", i, n, a, splinAOa2, kappaAOa2);

            double const splinPOa2 = splin_flr.POa2(n, a);
            double const kappaPOa2 = kappa_flr.POa2(n, a);
            is_same &= std::abs(splinPOa2 - kappaPOa2) < tolerance;
            XCTAssert(std::abs(splinPOa2 - kappaPOa2) < tolerance, @"i = %ld, n = %d, a = %f, splinPOa2 = %f, kappaPOa2 = %f", i, n, a, splinPOa2, kappaPOa2);

            double const splinBOa = splin_flr.BOa(n, a);
            double const kappaBOa = kappa_flr.BOa(n, a);
            is_same &= std::abs(splinBOa - kappaBOa) < tolerance;
            XCTAssert(std::abs(splinBOa - kappaBOa) < tolerance, @"i = %ld, n = %d, a = %f, splinBOa = %f, kappaBOa = %f", i, n, a, splinBOa, kappaBOa);

            double const splinQOa = splin_flr.QOa(n, a);
            double const kappaQOa = kappa_flr.QOa(n, a);
            is_same &= std::abs(splinQOa - kappaQOa) < tolerance;
            XCTAssert(std::abs(splinQOa - kappaQOa) < tolerance, @"i = %ld, n = %d, a = %f, splinQOa = %f, kappaQOa = %f", i, n, a, splinQOa, kappaQOa);
            
            if (!is_same) break;
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

@end
