//
//  DispersionKitTests-PDF.mm
//  DispersionKit
//
//  Created by KYUNGGUK MIN on 2/18/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#import <XCTest/XCTest.h>
#include "DispersionKitTests-PDF.h"
#include <DispersionKit/DISPMaxwellianPDF.h>
#include <DispersionKit/DISPSplinePDF.h>
#include <DispersionKit/DISPIntegerKappaPDF.h>
#include "DISPRealKappaPDF.h"
#include <DispersionKit/DISPSplineKappaPDF.h>
#include <UtilityKit/UtilityKit.h>
#include <vector>
#include <utility>
#include <cmath>
#include <complex>
#include <sstream>
#include <iostream>
#include <exception>


@interface DispersionKitTests_PDF : XCTestCase

@end

@implementation DispersionKitTests_PDF

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.

}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testMaxwellianPDF {
    std::vector<std::complex<double>> zList, ZList;
    std::tie(zList, ZList) = ::exactMaxwellianPDF();

    using mPDF = Disp::MaxwellianPDF;
    for (unsigned long i = 0; i < zList.size(); ++i) {
        auto const z = zList.at(i);
        auto const Z0 = ZList.at(i);
        auto const Z1 = mPDF::Z<0>(z);
        XCTAssert(std::abs(Z0.real()-Z1.real()) < 1e-10 && std::abs(Z0.imag()-Z1.imag()) < 1e-10,
                  @"Z0 = %f %+fi, Z1 = %f %+fi", Z0.real(), Z0.imag(), Z1.real(), Z1.imag());
    }
}

- (void)testSplinePDF {
    using Disp::SplinePDF;

    // construct spline
    SplinePDF::spline_coefficient_type spline;
    try {
        auto f0 = [](double x) -> double {
            return std::exp(-x*x)*M_2_SQRTPI/2.;
        };
        std::vector<std::pair<double, double>> points;
        for (double xj = -6; xj <=5; xj += 0.05) {
            points.push_back({xj, f0(xj)});
        }
        spline = Utility::CubicSplineCoefficient<double>(points.begin(), points.end());
        XCTAssert(spline.is_regular_grid(), @"");
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    constexpr double y = -.5;
    Utility::Vector<double, 2> xlim{-5, 5};
    xlim -= 1.;
    constexpr unsigned nx = 200;
    constexpr long m = 2;
    try {
        SplinePDF PDF(std::move(spline));
        std::vector<std::pair<std::complex<double>, std::complex<double>>> Z, W;
        for (long i = 0; i <= nx; ++i) {
            double const x = double(i)/nx*(xlim.back() - xlim.front()) + xlim.front();
            Z.push_back({{x, y}, PDF.Z<m>({x, y}, true)});
            W.push_back({{x, y}, PDF.W<m>({x, y}, true)});
        }

        std::ostringstream os;
        os.setf(os.fixed);
        os.setf(os.showpos);
        os.precision(15);

        printo(os, "Z = {{", Z.front().first, ", ", Z.front().second, "}");
        for (unsigned i = 1; i < Z.size(); ++i) {
            printo(os, ", {", Z[i].first, ", ", Z[i].second, "}");
        }
        printo(os, "}\n\n");

        printo(os, "W = {{", W.front().first, ", ", W.front().second, "}");
        for (unsigned i = 1; i < W.size(); ++i) {
            printo(os, ", {", W[i].first, ", ", W[i].second, "}");
        }
        printo(os, "}\n\n");

        if (0) {
            NSString *output = [NSString stringWithUTF8String:os.str().c_str()];
            output = [output stringByReplacingOccurrencesOfString:@"(" withString:@"{"];
            output = [output stringByReplacingOccurrencesOfString:@")" withString:@"}"];
            NSURL *url = [NSURL fileURLWithPath:[NSHomeDirectory() stringByAppendingPathComponent:@"Downloads/testSplinePDF.m"]];
            NSError *error;
            XCTAssert([output writeToURL:url atomically:YES encoding:NSUTF8StringEncoding error:&error], @"%@", error);
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testSplinePDFTriplet {
    using Disp::SplinePDF;

    // construct spline
    SplinePDF::spline_coefficient_type spline;
    try {
        auto f0 = [](double x) -> double {
            return std::exp(-x*x)*M_2_SQRTPI/2.;
        };
        std::vector<std::pair<double, double>> points;
        for (double xj = -6; xj <=5; xj += 0.05) {
            points.push_back({xj, f0(xj)});
        }
        spline = Utility::CubicSplineCoefficient<double>(points.begin(), points.end());
        XCTAssert(spline.is_regular_grid(), @"");
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    constexpr double y = -.5;
    Utility::Vector<double, 2> xlim{-5, 5};
    xlim -= 1.;
    constexpr unsigned nx = 200;
    constexpr unsigned m = 2;
    try {
        SplinePDF PDF(std::move(spline));
        std::vector<std::pair<std::complex<double>, std::complex<double>>> Z, W;
        for (long i = 0; i <= nx; ++i) {
            double const x = double(i)/nx*(xlim.back() - xlim.front()) + xlim.front();
            Z.push_back({{x, y}, std::get<m>(PDF.Ztriplet({x, y}, true))});
            W.push_back({{x, y}, std::get<m>(PDF.Wtriplet({x, y}, true))});
        }

        std::ostringstream os;
        os.setf(os.fixed);
        os.setf(os.showpos);
        os.precision(15);

        printo(os, "Z = {{", Z.front().first, ", ", Z.front().second, "}");
        for (unsigned i = 1; i < Z.size(); ++i) {
            printo(os, ", {", Z[i].first, ", ", Z[i].second, "}");
        }
        printo(os, "}\n\n");

        printo(os, "W = {{", W.front().first, ", ", W.front().second, "}");
        for (unsigned i = 1; i < W.size(); ++i) {
            printo(os, ", {", W[i].first, ", ", W[i].second, "}");
        }
        printo(os, "}\n\n");

        if (0) {
            NSString *output = [NSString stringWithUTF8String:os.str().c_str()];
            output = [output stringByReplacingOccurrencesOfString:@"(" withString:@"{"];
            output = [output stringByReplacingOccurrencesOfString:@")" withString:@"}"];
            NSURL *url = [NSURL fileURLWithPath:[NSHomeDirectory() stringByAppendingPathComponent:@"Downloads/testSplinePDFTriplet.m"]];
            NSError *error;
            XCTAssert([output writeToURL:url atomically:YES encoding:NSUTF8StringEncoding error:&error], @"%@", error);
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testIntegerKappaPDF {
    using Disp::IntegerKappaPDF;
    using Disp::MaxwellianPDF;

    try {
        auto const list = ::exactIntegerKappaPDF();
        for (unsigned i = 0; i < list.size(); ++i) {
            unsigned k;
            std::complex<double> z, exactZ;
            std::tie(k, z, exactZ) = list[i];
            IntegerKappaPDF pdf(static_cast<int>(k));
            double const factor = std::tgamma(k + .5)/(std::sqrt(k)*std::tgamma(k)) * std::tgamma(k + 1)/(std::pow(k, 1.5)*std::tgamma(k - .5)); // constant factor correction
            std::complex<double> const approxZ = pdf.Z<0>(z) * factor;
            bool const is_same = std::abs(exactZ - approxZ) < 1e-10;
            XCTAssert(is_same, @"i = %d, kappa = %d, z = %f %+f I, exactZ = %f %+f I, approxZ = %f %+f I", i, k, z.real(), z.imag(), exactZ.real(), exactZ.imag(), approxZ.real(), approxZ.imag());
            if (!is_same) break;
        }

        //printo(std::cout, IntegerKappaPDF(1000000).Z<0>({.2, 1.}), "\n");
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // large kappa limit
    try {
        constexpr MaxwellianPDF max_pdf{};
        IntegerKappaPDF const kap_pdf(1000000);
        auto const list = ::exactIntegerKappaPDF();
        for (unsigned i = 0; i < list.size(); ++i) {
            std::complex<double> const z = std::get<2>(list[i]);
            std::complex<double> const maxZ = std::get<2>(max_pdf.Wtriplet(z));
            std::complex<double> const kapZ = std::get<2>(kap_pdf.Wtriplet(z));
            bool const is_same = std::abs(maxZ - kapZ) < 1e-7;
            XCTAssert(is_same, @"i = %d, z = %f %+f I, exactZ = %f %+f I, approxZ = %f %+f I", i, z.real(), z.imag(), maxZ.real(), maxZ.imag(), kapZ.real(), kapZ.imag());
            if (!is_same) break;
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

//- (void)testRealKappaPDF {
//    using Disp::RealKappaPDF;
//    using Disp::MaxwellianPDF;
//
//    try {
//        auto const list = ::exactIntegerKappaPDF();
//        for (unsigned i = 0; i < list.size(); ++i) {
//            unsigned k;
//            std::complex<double> z, exactZ;
//            std::tie(k, z, exactZ) = list[i];
//            RealKappaPDF pdf(k);
//            double const factor = std::tgamma(k + .5)/(std::sqrt(k)*std::tgamma(k)) * std::tgamma(k + 1)/(std::pow(k, 1.5)*std::tgamma(k - .5)); // constant factor correction
//            std::complex<double> const approxZ = pdf.Z<0>(z) * factor;
//            if (!std::isfinite(approxZ.real())) {
//                XCTAssert(false, @"i = %d, z = %f %+f I", i, z.real(), z.imag());
//                continue;
//            }
//            bool const is_same = std::abs(exactZ - approxZ) < 1e-6;
//            XCTAssert(is_same, @"i = %d, kappa = %d, z = %f %+f I, exactZ = %f %+f I, approxZ = %f %+f I", i, k, z.real(), z.imag(), exactZ.real(), exactZ.imag(), approxZ.real(), approxZ.imag());
//            if (!is_same) break;
//        }
//    } catch (std::exception &e) {
//        XCTAssert(false, @"%s", e.what());
//    }
//
//    // large kappa limit
//    try {
//        constexpr MaxwellianPDF max_pdf{};
//        RealKappaPDF const kap_pdf(100000);
//        auto const list = ::exactIntegerKappaPDF();
//        for (unsigned i = 0; i < list.size(); ++i) {
//            std::complex<double> const z = std::get<2>(list[i]);
//            std::complex<double> const maxZ = std::get<2>(max_pdf.Wtriplet(z));
//            std::complex<double> const kapZ = std::get<2>(kap_pdf.Wtriplet(z));
//            if (!std::isfinite(kapZ.real())) {
//                XCTAssert(false, @"i = %d, z = %f %+f I", i, z.real(), z.imag());
//                continue;
//            }
//            bool const is_same = std::abs(maxZ - kapZ) < 1e-5;
//            XCTAssert(is_same, @"i = %d, z = %f %+f I, exactZ = %f %+f I, approxZ = %f %+f I", i, z.real(), z.imag(), maxZ.real(), maxZ.imag(), kapZ.real(), kapZ.imag());
//            if (!is_same) break;
//        }
//    } catch (std::exception &e) {
//        XCTAssert(false, @"%s", e.what());
//    }
//}

- (void)testSplineKappaPDF {
    using Disp::SplineKappaPDF;
    using Disp::MaxwellianPDF;

    try {
        auto const list = ::exactIntegerKappaPDF();
        for (unsigned i = 0; i < list.size(); ++i) {
            unsigned k;
            std::complex<double> z, exactZ;
            std::tie(k, z, exactZ) = list[i];
            SplineKappaPDF pdf(k);
            double const factor = std::tgamma(k + .5)/(std::sqrt(k)*std::tgamma(k)) * std::tgamma(k + 1)/(std::pow(k, 1.5)*std::tgamma(k - .5)); // constant factor correction
            std::complex<double> const approxZ = pdf.Z<0>(z) * factor;
            bool const is_same = std::abs(exactZ - approxZ) < 1e-6;
            XCTAssert(is_same, @"i = %d, kappa = %d, z = %f %+f I, exactZ = %f %+f I, approxZ = %f %+f I", i, k, z.real(), z.imag(), exactZ.real(), exactZ.imag(), approxZ.real(), approxZ.imag());
            if (!is_same) break;
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // large kappa limit
    try {
        constexpr MaxwellianPDF max_pdf{};
        SplineKappaPDF const kap_pdf(100000);
        auto const list = ::exactIntegerKappaPDF();
        for (unsigned i = 0; i < list.size(); ++i) {
            std::complex<double> const z = std::get<2>(list[i]);
            std::complex<double> const maxZ = std::get<2>(max_pdf.Wtriplet(z));
            std::complex<double> const kapZ = std::get<2>(kap_pdf.Wtriplet(z));
            bool const is_same = std::abs(maxZ - kapZ) < 1e-5;
            XCTAssert(is_same, @"i = %d, z = %f %+f I, exactZ = %f %+f I, approxZ = %f %+f I", i, z.real(), z.imag(), maxZ.real(), maxZ.imag(), kapZ.real(), kapZ.imag());
            if (!is_same) break;
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testKappaPDFComparison {
    using Disp::SplineKappaPDF;
    using Disp::IntegerKappaPDF;
    using Disp::RealKappaPDF;

    try {
        auto const list = ::exactIntegerKappaPDF();
        for (int kappa = 2; kappa < 1000; kappa += 5) {
            SplineKappaPDF const real_pdf(kappa);
            IntegerKappaPDF const int_pdf{kappa};
            for (unsigned i = 0; i < list.size(); ++i) {
                unsigned _k;
                std::complex<double> z, _Z;
                std::tie(_k, z, _Z) = list[i];
                std::complex<double> const realZ = std::get<2>(real_pdf.Wtriplet(z));
                std::complex<double> const intZ = std::get<2>(int_pdf.Wtriplet(z));
                bool const is_same = std::abs(realZ - intZ) < 1e-5;
                XCTAssert(is_same, @"i = %d, z = %f %+f I, realZ = %f %+f I, intZ = %f %+f I", i, z.real(), z.imag(), realZ.real(), realZ.imag(), intZ.real(), intZ.imag());
                if (!is_same) break;
            }
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

//    try {
//        auto const list = ::exactIntegerKappaPDF();
//        for (int kappa = 2; kappa < 1000; kappa += 5) {
//            RealKappaPDF const real_pdf(kappa);
//            IntegerKappaPDF const int_pdf{kappa};
//            for (unsigned i = 0; i < list.size(); ++i) {
//                unsigned _k;
//                std::complex<double> z, _Z;
//                std::tie(_k, z, _Z) = list[i];
//                std::complex<double> const realZ = std::get<2>(real_pdf.Wtriplet(z));
//                std::complex<double> const intZ = std::get<2>(int_pdf.Wtriplet(z));
//                if (!std::isfinite(realZ.real())) {
//                    XCTAssert(false, @"i = %d, k = %d, z = %f %+f I", i, kappa, z.real(), z.imag());
//                    continue;
//                }
//                bool const is_same = std::abs(realZ - intZ) < 1e-5;
//                XCTAssert(is_same, @"i = %d, z = %f %+f I, realZ = %f %+f I, intZ = %f %+f I", i, z.real(), z.imag(), realZ.real(), realZ.imag(), intZ.real(), intZ.imag());
//                if (!is_same) break;
//            }
//        }
//    } catch (std::exception &e) {
//        XCTAssert(false, @"%s", e.what());
//    }
}

@end
