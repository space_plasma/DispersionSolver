//
//  DispersionKitTests-VDF.mm
//  DispersionKit
//
//  Created by KYUNGGUK MIN on 3/31/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#import <XCTest/XCTest.h>
#include <DispersionKit/DISPBiMaxwellianVDF.h>
#include <DispersionKit/DISPMaxwellianRingVDF.h>
#include <DispersionKit/DISPProductBiLorentzianVDF.h>
#include <DispersionKit/DISPBiLorentzianVDF.h>
#include <DispersionKit/DISPSquareMatrix.h>
#include <UtilityKit/UtilityKit.h>
#include <sstream>
#include <iostream>
#include <exception>

@interface DispersionKitTests_VDF : XCTestCase

@end

@implementation DispersionKitTests_VDF

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testMaxwellianVDF {
    using Disp::VDF;
    using Disp::BiMaxwellianVDF;
    using Disp::MaxwellianRingVDF;

    std::ostringstream os;
    os.setf(os.fixed);
    os.precision(15);
    unsigned long const seed = arc4random();
    Utility::NRRandomReal rng(seed);

    try {
        os.str("");
        constexpr double Oc = -1;
        constexpr std::complex<double> op = 1;
        constexpr double th1 = 2, th2 = 3, vd = -2;

        VDF const& bimax = BiMaxwellianVDF(Oc, op, th1, th2, vd);
        VDF const& ring = MaxwellianRingVDF(Oc, op, th1, th2, vd);
        printo(std::cout, bimax.description(), "\n", ring.description(), "\n");

        constexpr double tolerance = 1e-5;
        XCTAssert(std::abs(bimax.Oc() - ring.Oc()) < tolerance && std::abs(bimax.op() - ring.op()) < tolerance, @"");
        XCTAssert(std::abs(bimax.vd() - ring.vd()) < tolerance && std::abs(bimax.T1() - ring.T1()) < tolerance && std::abs(bimax.T2() - ring.T2()) < tolerance, @"");
        XCTAssert(std::abs(bimax.f(1, 1) - ring.f(1, 1)) < tolerance, @"");
        XCTAssert(std::abs(bimax.dfdv1(1, 1) - ring.dfdv1(1, 1)) < tolerance && std::abs(bimax.dfdv2(1., 1) - ring.dfdv2(1., 1)) < tolerance, @"");

        if (0) {
            constexpr double k1 = -48.299488, k2 = 94.421125;
            std::complex<double> const o{96.827939, 5.170337};
            int const j = -99;
            auto const biK = bimax.K({k1, k2}, o, j);
            auto const riK = ring.K({k1, k2}, o, j);
            printo(os, "bimax = ", ::get<0, 0>(biK), ", ring = ", ::get<0, 0>(riK), "\n");
            printo(std::cout, os.str());
            return;
        }

        bool is_same = true;
        for (int j = -100; is_same && j <= 100; ++j) {
            double const k1 = (2.*rng() - 1.)*100, k2 = (2.*rng() - 1.)*100;
            std::complex<double> const o{(2.*rng() - 1.)*100, (2.*rng() - 1.)*10};
            std::complex<double> a, b;

            auto const biK = bimax.K({k1, k2}, o, j);
            auto const riK = ring.K({k1, k2}, o, j);

            a = ::get<0, 0>(biK);
            b = ::get<0, 0>(riK);
            is_same &= std::abs(a - b) < tolerance;
            XCTAssert(std::abs(a - b) < tolerance, @"seed = %ld, k1 = %f, k2 = %f, o = %f %+fi, j = %d, bimax<1,1> = %f %+fi, ring<1,1> = %f %+fi", seed, k1, k2, o.real(), o.imag(), j, a.real(), a.imag(), b.real(), b.imag());

            a = ::get<0, 1>(biK);
            b = ::get<0, 1>(riK);
            is_same &= std::abs(a - b) < tolerance;
            XCTAssert(std::abs(a - b) < tolerance, @"seed = %ld, k1 = %f, k2 = %f, o = %f %+fi, j = %d, bimax<1,2> = %f %+fi, ring<1,2> = %f %+fi", seed, k1, k2, o.real(), o.imag(), j, a.real(), a.imag(), b.real(), b.imag());

            a = ::get<0, 2>(biK);
            b = ::get<0, 2>(riK);
            is_same &= std::abs(a - b) < tolerance;
            XCTAssert(std::abs(a - b) < tolerance, @"seed = %ld, k1 = %f, k2 = %f, o = %f %+fi, j = %d, bimax<1,3> = %f %+fi, ring<1,3> = %f %+fi", seed, k1, k2, o.real(), o.imag(), j, a.real(), a.imag(), b.real(), b.imag());

            a = ::get<1, 0>(biK);
            b = ::get<1, 0>(riK);
            is_same &= std::abs(a - b) < tolerance;
            XCTAssert(std::abs(a - b) < tolerance, @"seed = %ld, k1 = %f, k2 = %f, o = %f %+fi, j = %d, bimax<2,1> = %f %+fi, ring<2,1> = %f %+fi", seed, k1, k2, o.real(), o.imag(), j, a.real(), a.imag(), b.real(), b.imag());

            a = ::get<1, 1>(biK);
            b = ::get<1, 1>(riK);
            is_same &= std::abs(a - b) < tolerance;
            XCTAssert(std::abs(a - b) < tolerance, @"seed = %ld, k1 = %f, k2 = %f, o = %f %+fi, j = %d, bimax<2,2> = %f %+fi, ring<2,2> = %f %+fi", seed, k1, k2, o.real(), o.imag(), j, a.real(), a.imag(), b.real(), b.imag());

            a = ::get<1, 2>(biK);
            b = ::get<1, 2>(riK);
            is_same &= std::abs(a - b) < tolerance;
            XCTAssert(std::abs(a - b) < tolerance, @"seed = %ld, k1 = %f, k2 = %f, o = %f %+fi, j = %d, bimax<2,3> = %f %+fi, ring<2,3> = %f %+fi", seed, k1, k2, o.real(), o.imag(), j, a.real(), a.imag(), b.real(), b.imag());

            a = ::get<2, 0>(biK);
            b = ::get<2, 0>(riK);
            is_same &= std::abs(a - b) < tolerance;
            XCTAssert(std::abs(a - b) < tolerance, @"seed = %ld, k1 = %f, k2 = %f, o = %f %+fi, j = %d, bimax<3,1> = %f %+fi, ring<3,1> = %f %+fi", seed, k1, k2, o.real(), o.imag(), j, a.real(), a.imag(), b.real(), b.imag());

            a = ::get<2, 1>(biK);
            b = ::get<2, 1>(riK);
            is_same &= std::abs(a - b) < tolerance;
            XCTAssert(std::abs(a - b) < tolerance, @"seed = %ld, k1 = %f, k2 = %f, o = %f %+fi, j = %d, bimax<3,2> = %f %+fi, ring<3,2> = %f %+fi", seed, k1, k2, o.real(), o.imag(), j, a.real(), a.imag(), b.real(), b.imag());

            a = ::get<2, 2>(biK);
            b = ::get<2, 2>(riK);
            is_same &= std::abs(a - b) < tolerance;
            XCTAssert(std::abs(a - b) < tolerance, @"seed = %ld, k1 = %f, k2 = %f, o = %f %+fi, j = %d, bimax<3,3> = %f %+fi, ring<3,3> = %f %+fi", seed, k1, k2, o.real(), o.imag(), j, a.real(), a.imag(), b.real(), b.imag());
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testProductLorentzianVDF {
    using Disp::VDF;
    using Disp::BiMaxwellianVDF;
    using Disp::ProductBiLorentzianVDF;

    std::ostringstream os;
    unsigned long const seed = arc4random();
    Utility::NRRandomReal rng(seed);

    try {
        os.str("");
        constexpr double Oc = -1, k = 1000000.1;
        constexpr std::complex<double> op = 1;
        constexpr double th1 = 2, th2 = 3, vd = 1;

        VDF const& bimax = BiMaxwellianVDF(Oc, op, th1, th2, vd);
        VDF const& kappa = ProductBiLorentzianVDF(Oc, op, {k, k*2}, th1, th2, vd);
        printo(std::cout, bimax.description(), "\n", kappa.description(), "\n");

        constexpr double tolerance = 1e-5;
        XCTAssert(std::abs(bimax.Oc() - kappa.Oc()) < tolerance && std::abs(bimax.op() - kappa.op()) < tolerance, @"");
        XCTAssert(std::abs(bimax.vd() - kappa.vd()) < tolerance && std::abs(bimax.T1() - kappa.T1()) < tolerance && std::abs(bimax.T2() - kappa.T2()) < tolerance, @"bimaxT1 = %f, kappaT1 = %f, bimaxT2 = %f, kappaT2 = %f", bimax.T1(), kappa.T1(), bimax.T2(), kappa.T2());
        XCTAssert(std::abs(bimax.f(1, 1) - kappa.f(1, 1)) < tolerance, @"");
        XCTAssert(std::abs(bimax.dfdv1(1, 1) - kappa.dfdv1(1, 1)) < tolerance && std::abs(bimax.dfdv2(1., 1) - kappa.dfdv2(1., 1)) < tolerance, @"");

        if (0) {
            constexpr double k1 = -48.299488, k2 = 94.421125;
            std::complex<double> const o{96.827939, 5.170337};
            int const j = -99;
            auto const biK = bimax.K({k1, k2}, o, j);
            auto const kaK = kappa.K({k1, k2}, o, j);
            printo(os, "bimax = ", ::get<0, 0>(biK), ", kappa = ", ::get<0, 0>(kaK), "\n");
            printo(std::cout, os.str());
            return;
        }

        bool is_same = true;
        for (int j = -100; is_same && j <= 100; ++j) {
            double const k1 = (2.*rng() - 1.)*100, k2 = (2.*rng() - 1.)*100;
            std::complex<double> const o{(2.*rng() - 1.)*100, (2.*rng() - 1.)*10};
            std::complex<double> a, b;

            auto const biK = bimax.K({k1, k2}, o, j);
            auto const kaK = kappa.K({k1, k2}, o, j);

            a = ::get<0, 0>(biK);
            b = ::get<0, 0>(kaK);
            is_same &= std::abs(a - b) < tolerance;
            XCTAssert(std::abs(a - b) < tolerance, @"seed = %ld, k1 = %f, k2 = %f, o = %f %+fi, j = %d, bimax<1,1> = %f %+fi, kappa<1,1> = %f %+fi", seed, k1, k2, o.real(), o.imag(), j, a.real(), a.imag(), b.real(), b.imag());

            a = ::get<0, 1>(biK);
            b = ::get<0, 1>(kaK);
            is_same &= std::abs(a - b) < tolerance;
            XCTAssert(std::abs(a - b) < tolerance, @"seed = %ld, k1 = %f, k2 = %f, o = %f %+fi, j = %d, bimax<1,2> = %f %+fi, kappa<1,2> = %f %+fi", seed, k1, k2, o.real(), o.imag(), j, a.real(), a.imag(), b.real(), b.imag());

            a = ::get<0, 2>(biK);
            b = ::get<0, 2>(kaK);
            is_same &= std::abs(a - b) < tolerance;
            XCTAssert(std::abs(a - b) < tolerance, @"seed = %ld, k1 = %f, k2 = %f, o = %f %+fi, j = %d, bimax<1,3> = %f %+fi, kappa<1,3> = %f %+fi", seed, k1, k2, o.real(), o.imag(), j, a.real(), a.imag(), b.real(), b.imag());

            a = ::get<1, 0>(biK);
            b = ::get<1, 0>(kaK);
            is_same &= std::abs(a - b) < tolerance;
            XCTAssert(std::abs(a - b) < tolerance, @"seed = %ld, k1 = %f, k2 = %f, o = %f %+fi, j = %d, bimax<2,1> = %f %+fi, kappa<2,1> = %f %+fi", seed, k1, k2, o.real(), o.imag(), j, a.real(), a.imag(), b.real(), b.imag());

            a = ::get<1, 1>(biK);
            b = ::get<1, 1>(kaK);
            is_same &= std::abs(a - b) < tolerance*10;
            XCTAssert(std::abs(a - b) < tolerance*10, @"seed = %ld, k1 = %f, k2 = %f, o = %f %+fi, j = %d, bimax<2,2> = %f %+fi, kappa<2,2> = %f %+fi", seed, k1, k2, o.real(), o.imag(), j, a.real(), a.imag(), b.real(), b.imag());

            a = ::get<1, 2>(biK);
            b = ::get<1, 2>(kaK);
            is_same &= std::abs(a - b) < tolerance;
            XCTAssert(std::abs(a - b) < tolerance, @"seed = %ld, k1 = %f, k2 = %f, o = %f %+fi, j = %d, bimax<2,3> = %f %+fi, kappa<2,3> = %f %+fi", seed, k1, k2, o.real(), o.imag(), j, a.real(), a.imag(), b.real(), b.imag());

            a = ::get<2, 0>(biK);
            b = ::get<2, 0>(kaK);
            is_same &= std::abs(a - b) < tolerance;
            XCTAssert(std::abs(a - b) < tolerance, @"seed = %ld, k1 = %f, k2 = %f, o = %f %+fi, j = %d, bimax<3,1> = %f %+fi, kappa<3,1> = %f %+fi", seed, k1, k2, o.real(), o.imag(), j, a.real(), a.imag(), b.real(), b.imag());

            a = ::get<2, 1>(biK);
            b = ::get<2, 1>(kaK);
            is_same &= std::abs(a - b) < tolerance;
            XCTAssert(std::abs(a - b) < tolerance, @"seed = %ld, k1 = %f, k2 = %f, o = %f %+fi, j = %d, bimax<3,2> = %f %+fi, kappa<3,2> = %f %+fi", seed, k1, k2, o.real(), o.imag(), j, a.real(), a.imag(), b.real(), b.imag());

            a = ::get<2, 2>(biK);
            b = ::get<2, 2>(kaK);
            is_same &= std::abs(a - b) < tolerance;
            XCTAssert(std::abs(a - b) < tolerance, @"seed = %ld, k1 = %f, k2 = %f, o = %f %+fi, j = %d, bimax<3,3> = %f %+fi, kappa<3,3> = %f %+fi", seed, k1, k2, o.real(), o.imag(), j, a.real(), a.imag(), b.real(), b.imag());
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testLorentzianVDF {
    using Disp::VDF;
    using Disp::BiMaxwellianVDF;
    using Disp::BiLorentzianVDF;

    std::ostringstream os;
    unsigned long const seed = arc4random();
    Utility::NRRandomReal rng(seed);

    try {
        os.str("");
        constexpr double Oc = -1, k = 1000000.1;
        constexpr std::complex<double> op = 1;
        constexpr double th1 = 2, th2 = 3, vd = .5;

        VDF const& bimax = BiMaxwellianVDF(Oc, op, th1, th2, vd);
        VDF const& kappa = BiLorentzianVDF(Oc, op, k, th1, th2, vd);
        printo(std::cout, bimax.description(), "\n", kappa.description(), "\n");

        constexpr double tolerance = 1e-5;
        XCTAssert(std::abs(bimax.Oc() - kappa.Oc()) < tolerance && std::abs(bimax.op() - kappa.op()) < tolerance, @"");
        XCTAssert(std::abs(bimax.vd() - kappa.vd()) < tolerance && std::abs(bimax.T1() - kappa.T1()) < tolerance && std::abs(bimax.T2() - kappa.T2()) < tolerance, @"bimaxT1 = %f, kappaT1 = %f, bimaxT2 = %f, kappaT2 = %f", bimax.T1(), kappa.T1(), bimax.T2(), kappa.T2());
        XCTAssert(std::abs(bimax.f(1, 1) - kappa.f(1, 1)) < tolerance, @"bimax.f = %f, kappa.f = %f", bimax.f(1, 1), kappa.f(1, 1));
        XCTAssert(std::abs(bimax.dfdv1(1, 1) - kappa.dfdv1(1, 1)) < tolerance, @"bimax.dfdv1 = %f, kappa.dfdv1 = %f", bimax.dfdv1(1, 1), kappa.dfdv1(1, 1));
        XCTAssert(std::abs(bimax.dfdv2(1., 1) - kappa.dfdv2(1., 1)) < tolerance, @"bimax.dfdv2 = %f, kappa.dfdv2 = %f", bimax.dfdv2(1., 1), kappa.dfdv2(1., 1));

        if (0) {
            constexpr double k1 = -48.299488, k2 = 94.421125;
            std::complex<double> const o{96.827939, 5.170337};
            int const j = -99;
            auto const biK = bimax.K({k1, k2}, o, j);
            auto const kaK = kappa.K({k1, k2}, o, j);
            printo(os, "bimax = ", biK, ",\nkappa = ", kaK, "\n");
            printo(std::cout, os.str());
            return;
        }

        bool is_same = true;
        for (int j = -100; is_same && j <= 100; ++j) {
            double const k1 = (2.*rng() - 1.)*100, k2 = (2.*rng() - 1.)*100;
            std::complex<double> const o{(2.*rng() - 1.)*100, (2.*rng() - 1.)*10};
            std::complex<double> a, b;

            auto const biK = bimax.K({k1, k2}, o, j);
            auto const kaK = kappa.K({k1, k2}, o, j);

            a = ::get<0, 0>(biK);
            b = ::get<0, 0>(kaK);
            is_same &= std::abs(a - b) < tolerance;
            XCTAssert(std::abs(a - b) < tolerance, @"seed = %ld, k1 = %f, k2 = %f, o = %f %+fi, j = %d, bimax<1,1> = %f %+fi, kappa<1,1> = %f %+fi", seed, k1, k2, o.real(), o.imag(), j, a.real(), a.imag(), b.real(), b.imag());

            a = ::get<0, 1>(biK);
            b = ::get<0, 1>(kaK);
            is_same &= std::abs(a - b) < tolerance;
            XCTAssert(std::abs(a - b) < tolerance, @"seed = %ld, k1 = %f, k2 = %f, o = %f %+fi, j = %d, bimax<1,2> = %f %+fi, kappa<1,2> = %f %+fi", seed, k1, k2, o.real(), o.imag(), j, a.real(), a.imag(), b.real(), b.imag());

            a = ::get<0, 2>(biK);
            b = ::get<0, 2>(kaK);
            is_same &= std::abs(a - b) < tolerance;
            XCTAssert(std::abs(a - b) < tolerance, @"seed = %ld, k1 = %f, k2 = %f, o = %f %+fi, j = %d, bimax<1,3> = %f %+fi, kappa<1,3> = %f %+fi", seed, k1, k2, o.real(), o.imag(), j, a.real(), a.imag(), b.real(), b.imag());

            a = ::get<1, 0>(biK);
            b = ::get<1, 0>(kaK);
            is_same &= std::abs(a - b) < tolerance;
            XCTAssert(std::abs(a - b) < tolerance, @"seed = %ld, k1 = %f, k2 = %f, o = %f %+fi, j = %d, bimax<2,1> = %f %+fi, kappa<2,1> = %f %+fi", seed, k1, k2, o.real(), o.imag(), j, a.real(), a.imag(), b.real(), b.imag());

            a = ::get<1, 1>(biK);
            b = ::get<1, 1>(kaK);
            is_same &= std::abs(a - b) < tolerance*10;
            XCTAssert(std::abs(a - b) < tolerance*10, @"seed = %ld, k1 = %f, k2 = %f, o = %f %+fi, j = %d, bimax<2,2> = %f %+fi, kappa<2,2> = %f %+fi", seed, k1, k2, o.real(), o.imag(), j, a.real(), a.imag(), b.real(), b.imag());

            a = ::get<1, 2>(biK);
            b = ::get<1, 2>(kaK);
            is_same &= std::abs(a - b) < tolerance;
            XCTAssert(std::abs(a - b) < tolerance, @"seed = %ld, k1 = %f, k2 = %f, o = %f %+fi, j = %d, bimax<2,3> = %f %+fi, kappa<2,3> = %f %+fi", seed, k1, k2, o.real(), o.imag(), j, a.real(), a.imag(), b.real(), b.imag());

            a = ::get<2, 0>(biK);
            b = ::get<2, 0>(kaK);
            is_same &= std::abs(a - b) < tolerance;
            XCTAssert(std::abs(a - b) < tolerance, @"seed = %ld, k1 = %f, k2 = %f, o = %f %+fi, j = %d, bimax<3,1> = %f %+fi, kappa<3,1> = %f %+fi", seed, k1, k2, o.real(), o.imag(), j, a.real(), a.imag(), b.real(), b.imag());

            a = ::get<2, 1>(biK);
            b = ::get<2, 1>(kaK);
            is_same &= std::abs(a - b) < tolerance;
            XCTAssert(std::abs(a - b) < tolerance, @"seed = %ld, k1 = %f, k2 = %f, o = %f %+fi, j = %d, bimax<3,2> = %f %+fi, kappa<3,2> = %f %+fi", seed, k1, k2, o.real(), o.imag(), j, a.real(), a.imag(), b.real(), b.imag());

            a = ::get<2, 2>(biK);
            b = ::get<2, 2>(kaK);
            is_same &= std::abs(a - b) < tolerance;
            XCTAssert(std::abs(a - b) < tolerance, @"seed = %ld, k1 = %f, k2 = %f, o = %f %+fi, j = %d, bimax<3,3> = %f %+fi, kappa<3,3> = %f %+fi", seed, k1, k2, o.real(), o.imag(), j, a.real(), a.imag(), b.real(), b.imag());
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testLorentzianVDF_Test {
    using Disp::VDF;
    using Disp::BiMaxwellianVDF;
    using Disp::BiLorentzianVDF;

    std::ostringstream os;
    unsigned long const seed = arc4random();
    Utility::NRRandomReal rng(seed);

    try {
        os.str("");
        constexpr double Oc = -1, k = 2;
        constexpr std::complex<double> op = 1;
        constexpr double th1 = 2, th2 = 3, vd = 0;

        VDF const& bimax = BiMaxwellianVDF(Oc, op, th1, th2, vd);
        VDF const& kappa = BiLorentzianVDF(Oc, op, k, th1, th2, vd);
        printo(std::cout, bimax.description(), "\n", kappa.description(), "\n");

        constexpr double k1 = 8.299488, k2 = 0;
        std::complex<double> const o{6.827939, 1.170337};
        int const j = 1;
        auto const biK = bimax.K({k1, k2}, o, j);
        auto const kaK = kappa.K({k1, k2}, o, j);
        printo(os, "bimax = ", biK, ",\nkappa = ", kaK, "\n");
        printo(std::cout, os.str());
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

@end
