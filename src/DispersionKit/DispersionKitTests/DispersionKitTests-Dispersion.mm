//
//  DispersionKitTests-Dispersion.mm
//  DispersionKit
//
//  Created by KYUNGGUK MIN on 4/25/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#import <XCTest/XCTest.h>
#include <DispersionKit/DISPDispersion.h>
#include <DispersionKit/DISPBiMaxwellianVDF.h>
#include <DispersionKit/DISPMaxwellianRingVDF.h>
#include <DispersionKit/DISPProductBiLorentzianVDF.h>
#include <DispersionKit/DISPBiLorentzianVDF.h>
#include "DISPSquareMatrix.h"
#include <UtilityKit/UtilityKit.h>
#include <sstream>
#include <iostream>
#include <exception>
#include <functional>
#include <deque>
#import <gsl/gsl.h>

@interface DispersionKitTests_Dispersion : XCTestCase

@end

@implementation DispersionKitTests_Dispersion

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    gsl_set_error_handler_off();
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testProperties {
    using Disp::Dispersion;
    using Disp::BiMaxwellianVDF;
    using Disp::MaxwellianRingVDF;
    using Disp::ProductBiLorentzianVDF;
    using Disp::BiLorentzianVDF;

    try {
        constexpr double c = 1;
        const std::pair<double, double> kappa{1000000, 2000000};
        constexpr unsigned jmin = 100;
        Dispersion disp(c);
        disp.set_jmin(jmin);
        XCTAssert(c == disp.c() && jmin == disp.jmin(), @"");

        printo(std::cout, disp.description(), "\n");

        disp.addVDF<BiMaxwellianVDF>(-1836, 17139.4, 1, 1, 0);
        disp.addVDF<MaxwellianRingVDF>(1, 40., 1, 1, 0, 1);
        disp.addVDF<MaxwellianRingVDF>(1, 397.995, 0);
        disp.addVDF<ProductBiLorentzianVDF>(1, 397.995, kappa, 0.001);
        disp.addVDF<BiLorentzianVDF>(1, 397.995, kappa.first, 0.001);
        printo(std::cout, disp.description(), "\n");
        disp.clear_cache();
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testBandedWhistlerDispersionMatrix {
    using Disp::Dispersion;
    using Disp::BiMaxwellianVDF;
    using Disp::MaxwellianRingVDF;
    using Disp::ProductBiLorentzianVDF;
    using Disp::BiLorentzianVDF;

    try {
        const double opeOOce = 4.;
        const double Tp = 1.6;
        const double Tw = 160;
        const double Th = 16000;
        const double e = 1./1836;
        const double c = 1.;
        const double Oce = 1.;
        const std::pair<double, double> kappa{1000000, 2000000};

        Dispersion disp(c);
        disp.set_jmin(10);
        switch (2) {
            case 0: {
                disp.addVDF<MaxwellianRingVDF>(-Oce, opeOOce*std::sqrt(.9), std::sqrt(2.*Tw/511000.), std::sqrt(2.*Tw*5/511000.), 0, .001);
                disp.addVDF<MaxwellianRingVDF>(-Oce, opeOOce*std::sqrt(.1), std::sqrt(2.*Th/511000.), std::sqrt(2.*Th*2/511000.), 0, .001);
            }
                break;
            case 1: {
                disp.addVDF<ProductBiLorentzianVDF>(-Oce, opeOOce*std::sqrt(.9), kappa, std::sqrt(2.*Tw/511000.), std::sqrt(2.*Tw*5/511000.), 0);
                disp.addVDF<ProductBiLorentzianVDF>(-Oce, opeOOce*std::sqrt(.1), kappa, std::sqrt(2.*Th/511000.), std::sqrt(2.*Th*2/511000.), 0);
            }
                break;
            case 2: {
                disp.addVDF<BiLorentzianVDF>(-Oce, opeOOce*std::sqrt(.9), kappa.first, std::sqrt(2.*Tw/511000.), std::sqrt(2.*Tw*5/511000.), 0);
                disp.addVDF<BiLorentzianVDF>(-Oce, opeOOce*std::sqrt(.1), kappa.first, std::sqrt(2.*Th/511000.), std::sqrt(2.*Th*2/511000.), 0);
            }
                break;
            default:
                XCTAssert(false);
                return;
        }
        disp.addVDF<BiMaxwellianVDF>(Oce*e, opeOOce*std::sqrt(1.*e), std::sqrt(2.*Tp/511000.*e));
        printo(std::cout, disp.description(), "\n");

        double const k = 1.33383;
        double const psi = 0.0872665;
        Utility::SpinLock mutex;
        CX::Matrix const D = disp.D({k*std::cos(psi), k*std::sin(psi)}, {.1, 0.}, [&mutex](unsigned j, CX::Matrix const& K)->bool {
            Utility::LockG<Utility::SpinLock> _(mutex);
            printo(std::cout, "j = ", j, ", K = ", K, "\n");
            return true;
        });
        printo(std::cout, "D = ", D, "\n|D| = ", ::det(D), "\n");
        XCTAssert(isfinite(D), @"");
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testBandedWhistlerRootFindParallel {
    using Disp::Dispersion;
    using Disp::BiMaxwellianVDF;
    using Disp::MaxwellianRingVDF;
    using Disp::ProductBiLorentzianVDF;
    using Disp::BiLorentzianVDF;

    try {
        constexpr double c = 4, mpOme = 1836;
        constexpr double nw = .9, b1w = .01, T2OT1w = 5;
        constexpr double nh = 1 - nw, b1h = 1., T2OT1h = 2;
        constexpr double bp = .0001, Oe = -1, Op = 1/mpOme;
        constexpr unsigned jmin = 10;
        const std::pair<double, double> kappa{1000000.1, 2000000};

        Dispersion disp(c);
        disp.set_jmin(jmin);
        switch (2) {
            case 0: {
                disp.addVDF<BiMaxwellianVDF>(Oe, c*std::sqrt(nw), std::sqrt(b1w), std::sqrt(b1w*T2OT1w));
                disp.addVDF<BiMaxwellianVDF>(Oe, c*std::sqrt(nh), std::sqrt(b1h), std::sqrt(b1h*T2OT1h));
                disp.addVDF<BiMaxwellianVDF>(Op, c/std::sqrt(mpOme), std::sqrt(bp/mpOme));
            }
                break;
            case 1: {
                disp.addVDF<ProductBiLorentzianVDF>(Oe, c*std::sqrt(nw), kappa, std::sqrt(b1w), std::sqrt(b1w*T2OT1w));
                disp.addVDF<ProductBiLorentzianVDF>(Oe, c*std::sqrt(nh), kappa, std::sqrt(b1h), std::sqrt(b1h*T2OT1h));
                disp.addVDF<ProductBiLorentzianVDF>(Op, c/std::sqrt(mpOme), kappa, std::sqrt(bp/mpOme));
            }
                break;
            case 2: {
                disp.addVDF<BiLorentzianVDF>(Oe, c*std::sqrt(nw), kappa.first, std::sqrt(b1w), std::sqrt(b1w*T2OT1w));
                disp.addVDF<BiLorentzianVDF>(Oe, c*std::sqrt(nh), kappa.first, std::sqrt(b1h), std::sqrt(b1h*T2OT1h));
                disp.addVDF<BiLorentzianVDF>(Op, c/std::sqrt(mpOme), kappa.first, std::sqrt(bp/mpOme));
            }
                break;
            default:
                XCTAssert(false);
                return;
        }
        printo(std::cout, disp.description(), "\n");

        constexpr double k1 = .64, k2 = 0;
        constexpr CX::Number o{0.294003, 0.0157153};
        unsigned jused;
        CX::Matrix const D = disp.D({k1, k2}, o, [&jused](unsigned j, CX::Matrix const& K)->bool {
            jused = j;
            return true;
        });
        printo(std::cout, "j_used = ", jused, "\nD = ", D, "\n|D| = ", ::det(D), "\n");

        Utility::MullerRootFinder<std::function<CX::Number(CX::Number)>> root([&disp, k1, k2](CX::Number const& o)->CX::Number {
            CX::Matrix const D = disp.o2_x_D({k1, k2}, o, [](unsigned, CX::Matrix const&)->bool { return true; });
            return ::det(D);
        });
        decltype(root)::optional_type opt = root(o - .01, o - .02, o - .03, [](unsigned i, CX::Number const& o)->bool {
            printo(std::cout, "i = ", i, ", o = ", o, "\n");
            return true;
        });
        XCTAssert(opt, @"nil root");
        if (opt) {
            printo(std::cout, "root = ", *opt, "\n");
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testGary_et_al_2010_SubtractedMaxwellian {
    using Disp::Dispersion;
    using Disp::BiMaxwellianVDF;
    using Disp::MaxwellianRingVDF;
    using Disp::ProductBiLorentzianVDF;
    using Disp::BiLorentzianVDF;

    try {
        constexpr double c = 15, mpOme = 100;
        constexpr CX::Number n1 = 6.3, n2 = -5.3;
        constexpr double b1 = .8, T2OT1 = .9, TeOT1 = .01;
        const std::pair<double, double> kappa{1000000.1, 1000000};

        Dispersion disp(c);
        switch (2) {
            case 0: {
                disp.addVDF<BiMaxwellianVDF>(1, c*std::sqrt(n1), std::sqrt(b1));
                disp.addVDF<BiMaxwellianVDF>(1, c*std::sqrt(n2), std::sqrt(b1*T2OT1));
                disp.addVDF<BiMaxwellianVDF>(-mpOme, c*std::sqrt(mpOme), std::sqrt(b1*TeOT1*mpOme));
            }
                break;
            case 1: {
                disp.addVDF<ProductBiLorentzianVDF>(1, c*std::sqrt(n1), kappa, std::sqrt(b1));
                disp.addVDF<ProductBiLorentzianVDF>(1, c*std::sqrt(n2), kappa, std::sqrt(b1*T2OT1));
                disp.addVDF<ProductBiLorentzianVDF>(-mpOme, c*std::sqrt(mpOme), kappa, std::sqrt(b1*TeOT1*mpOme));
            }
                break;
            case 2: {
                disp.addVDF<BiLorentzianVDF>(1, c*std::sqrt(n1), kappa.first, std::sqrt(b1));
                disp.addVDF<BiLorentzianVDF>(1, c*std::sqrt(n2), kappa.first, std::sqrt(b1*T2OT1));
                disp.addVDF<BiLorentzianVDF>(-mpOme, c*std::sqrt(mpOme), kappa.first, std::sqrt(b1*TeOT1*mpOme));
            }
                break;
            default:
                XCTAssert(false);
                return;
        }
        printo(std::cout, disp.description(), "\n");

        unsigned jmin = 15;
        auto disp_step_monitor = [&jmin](unsigned const j, CX::Matrix const&)->bool {
            jmin = j;
            return true;
        };
        auto root_step_monitor = [&jmin](unsigned const i, CX::Number const& o)->bool {
            printo(std::cout, "i = ", i, ", o = ", o, ", jmin = ", jmin, "\n");
            return true;
        };

        double k{}, psi{};
        Utility::MullerRootFinder<std::function<CX::Number(CX::Number)>>
        root([&disp, &k, &psi, disp_step_monitor](CX::Number const& o)->CX::Number {
            CX::Matrix const o2D = disp.o2_x_D({k*std::cos(psi), k*std::sin(psi)}, o, disp_step_monitor);
            return ::det(o2D);
        });

        psi = 86. * M_PI/180;
        std::deque<double> kList;
        std::deque<CX::Matrix> DList;
        std::deque<CX::Number> oList{CX::Number{0.912589, 0.0335244}, CX::Number{0.912589, 0.0335244} - 1./1000, CX::Number{0.912589, 0.0335244} - 2./1000};
        for (k = 4.4; k <= 6; k += 0.01) {
            disp.set_jmin(jmin);
            decltype(root)::optional_type const opt = root(oList[2], oList[1], oList[0], root_step_monitor);
            XCTAssert(opt, @"psi = %f, k = %f", psi, k);
            if (opt) {
                oList.push_front(*opt);
                kList.push_front(k);
                DList.push_front(disp.D({k*std::cos(psi), k*std::sin(psi)}, *opt, [](unsigned, CX::Matrix const&)->bool { return true; }));
            } else {
                break;
            }
            disp.clear_cache();
        }
        oList.pop_back(); oList.pop_back(); oList.pop_back();
        printo(std::cout, disp.description(), "\n");

        std::ostringstream os;
        os.setf(os.fixed);
        os.precision(15);
        printo(os, "{\n");
        if (!oList.empty()) {
            // k
            auto k_it = kList.rbegin();
            printo(os, "\t{", *k_it++);
            while (k_it != kList.rend()) printo(os, ", ", *k_it++);
            printo(os, "},\n");

            // o
            auto o_it = oList.rbegin();
            printo(os, "\t{", *o_it++);
            while (o_it != oList.rend()) printo(os, ", ", *o_it++);
            printo(os, "},\n");

            // D
            auto D_it = DList.rbegin();
            printo(os, "\t{", *D_it++);
            while (D_it != DList.rend()) printo(os, ", ", *D_it++);
            printo(os, "}\n");
        }
        printo(os, "}\n");

        if (0) {
            NSString *str = [NSString stringWithUTF8String:os.str().c_str()];
            str = [str stringByReplacingOccurrencesOfString:@"(" withString:@"Complex["];
            str = [str stringByReplacingOccurrencesOfString:@")" withString:@"]"];

            NSString *filename = [NSString stringWithFormat:@"testGary_et_al_2010_SubtractedMaxwellian.m"];
            NSURL *url = [NSURL fileURLWithPath:[NSHomeDirectory() stringByAppendingPathComponent:@"Downloads"] isDirectory:YES];
            url = [url URLByAppendingPathComponent:filename];
            NSError *error;
            XCTAssert([str writeToURL:url atomically:YES encoding:NSUTF8StringEncoding error:&error], @"%@", error);
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testRingDispersionMatrix {
    using Disp::Dispersion;
    using Disp::BiMaxwellianVDF;
    using Disp::MaxwellianRingVDF;
    using Disp::ProductBiLorentzianVDF;

    try {
        constexpr double c = 400, mpOme = 1836;
        constexpr double nr = 0.01, br = 0.2, vr = 2;
        constexpr double nc = 1 - nr, bc = 0.002;
        const std::pair<double, double> kappa{1000000, 2000000};

        Dispersion disp(c);
        disp.set_jmin(50);
        disp.addVDF<MaxwellianRingVDF>(1, c*std::sqrt(nr), std::sqrt(br), std::sqrt(br), 0., vr);
        if (1) {
            disp.addVDF<ProductBiLorentzianVDF>(1, c*std::sqrt(nc), kappa, std::sqrt(bc));
            disp.addVDF<ProductBiLorentzianVDF>(-mpOme, c*std::sqrt(mpOme), kappa, std::sqrt(bc*mpOme));
        } else {
            disp.addVDF<BiMaxwellianVDF>(1, c*std::sqrt(nc), std::sqrt(bc));
            disp.addVDF<BiMaxwellianVDF>(-mpOme, c*std::sqrt(mpOme), std::sqrt(bc*mpOme));
        }
        printo(std::cout, disp.description(), "\n");

        double k = 3;
        double psi = 86. * M_PI/180;
        CX::Number o{3.02621, 0.0457026};
        Utility::SpinLock mutex;
        CX::Matrix const D = disp.D({k*std::cos(psi), k*std::sin(psi)}, o, [&mutex](unsigned j, CX::Matrix const& K)->bool {
            Utility::LockG<Utility::SpinLock> l(mutex);
            printo(std::cout, "j = ", j, ", K = ", K, "\n");
            return true;
        });
        printo(std::cout, "D = ", D, "\n|D| = ", ::det(D), "\n");
        XCTAssert(isfinite(D), @"");
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testMin_and_Liu_2016_TenuousRing {
    using Disp::Dispersion;
    using Disp::BiMaxwellianVDF;
    using Disp::MaxwellianRingVDF;
    using Disp::ProductBiLorentzianVDF;
    using Disp::BiLorentzianVDF;

    try {
        constexpr double c = 400, mpOme = 1836;
        constexpr double nr = 0.01, br = 0.2, vr = 2;
        constexpr double nc = 1 - nr, bc = 0.002;
        const std::pair<double, double> kappa{1000000.1, 2000000};

        Dispersion disp(c);
        disp.addVDF<MaxwellianRingVDF>(1, c*std::sqrt(nr), std::sqrt(br), std::sqrt(br), 0., vr);
        switch (2) {
            case 0: {
                disp.addVDF<BiMaxwellianVDF>(1, c*std::sqrt(nc), std::sqrt(bc));
                disp.addVDF<BiMaxwellianVDF>(-mpOme, c*std::sqrt(mpOme), std::sqrt(bc*mpOme));
            }
                break;
            case 1: {
                disp.addVDF<ProductBiLorentzianVDF>(1, c*std::sqrt(nc), kappa, std::sqrt(bc));
                disp.addVDF<ProductBiLorentzianVDF>(-mpOme, c*std::sqrt(mpOme), kappa, std::sqrt(bc*mpOme));
            }
                break;
            case 2: {
                disp.addVDF<BiLorentzianVDF>(1, c*std::sqrt(nc), kappa.first, std::sqrt(bc));
                disp.addVDF<BiLorentzianVDF>(-mpOme, c*std::sqrt(mpOme), kappa.first, std::sqrt(bc*mpOme));
            }
                break;
            default:
                XCTAssert(false);
                return;
        }
        printo(std::cout, disp.description(), "\n");

        unsigned jmin = 20;
        auto disp_step_monitor = [&jmin](unsigned const j, CX::Matrix const&)->bool {
            jmin = j;
            return true;
        };
        auto root_step_monitor = [&jmin](unsigned const i, CX::Number const& o)->bool {
            printo(std::cout, "i = ", i, ", o = ", o, ", jmin = ", jmin, "\n");
            return true;
        };

        double k{}, psi{};
        Utility::MullerRootFinder<std::function<CX::Number(CX::Number)>>
        root([&disp, &k, &psi, disp_step_monitor](CX::Number const& o)->CX::Number {
            CX::Matrix const o2D = disp.o2_x_D({k*std::cos(psi), k*std::sin(psi)}, o, disp_step_monitor);
            return ::det(o2D);
        });
        root.max_iterations = 6;

        psi = 86. * M_PI/180;
        std::deque<double> kList;
        std::deque<CX::Matrix> DList;
        std::deque<CX::Number> oList{2.51899, 2.51899 - 1./1000, 2.51899 - 2./1000};
        for (k = 2.5; k <= 3.5; k += 0.01) {
            disp.set_jmin(jmin);
            decltype(root)::optional_type const opt = root(oList[2], oList[1], oList[0], root_step_monitor);
            XCTAssert(opt, @"psi = %f, k = %f", psi, k);
            if (opt) {
                oList.push_front(*opt);
                kList.push_front(k);
                DList.push_front(disp.D({k*std::cos(psi), k*std::sin(psi)}, *opt, [](unsigned, CX::Matrix const&)->bool { return true; }));
            } else {
                break;
            }
            disp.clear_cache();
        }
        oList.pop_back(); oList.pop_back(); oList.pop_back();
        printo(std::cout, disp.description(), "\n");

        std::ostringstream os;
        os.setf(os.fixed);
        os.precision(15);
        printo(os, "{\n");
        if (!oList.empty()) {
            // k
            auto k_it = kList.rbegin();
            printo(os, "\t{", *k_it++);
            while (k_it != kList.rend()) printo(os, ", ", *k_it++);
            printo(os, "},\n");

            // o
            auto o_it = oList.rbegin();
            printo(os, "\t{", *o_it++);
            while (o_it != oList.rend()) printo(os, ", ", *o_it++);
            printo(os, "},\n");

            // D
            auto D_it = DList.rbegin();
            printo(os, "\t{", *D_it++);
            while (D_it != DList.rend()) printo(os, ", ", *D_it++);
            printo(os, "}\n");
        }
        printo(os, "}\n");

        if (0) {
            NSString *str = [NSString stringWithUTF8String:os.str().c_str()];
            str = [str stringByReplacingOccurrencesOfString:@"(" withString:@"Complex["];
            str = [str stringByReplacingOccurrencesOfString:@")" withString:@"]"];

            NSString *filename = [NSString stringWithFormat:@"testMin_and_Liu_2016_TenuousRing.m"];
            NSURL *url = [NSURL fileURLWithPath:[NSHomeDirectory() stringByAppendingPathComponent:@"Downloads"] isDirectory:YES];
            url = [url URLByAppendingPathComponent:filename];
            NSError *error;
            XCTAssert([str writeToURL:url atomically:YES encoding:NSUTF8StringEncoding error:&error], @"%@", error);
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testMin_and_Liu_2016_TenuousShell {
    using Disp::Dispersion;
    using Disp::BiMaxwellianVDF;
    using Disp::MaxwellianRingVDF;

    try {
        constexpr double c = 400, mpOme = 1836;
        constexpr double nc = .99, bc = 0.002, ths = 0.45;
        std::deque<double> const
        nrs{4.23367e-6, 0.000081097, 0.000338869, 0.000646506, 0.000914241, 0.00111971, 0.00124888, 0.00129293, 0.00124888, 0.00111971, 0.000914241, 0.000646506, 0.000338869, 0.000081097, 4.23367e-6},
        vrs{-0.523811, 0., 0.523811, 1.01192, 1.43108, 1.75271, 1.95489, 2.02385, 1.95489, 1.75271, 1.43108, 1.01193, 0.523811, 0., -0.523811},
        vds{1.95489, 2.02385, 1.95489, 1.75271, 1.43108, 1.01193, 0.523811, 0., -0.523811, -1.01192, -1.43108, -1.75271, -1.95489, -2.02385, -1.95489};

        Dispersion disp(c);
        for (unsigned i = 0; i < nrs.size(); ++i) {
            disp.addVDF<MaxwellianRingVDF>(1, c*std::sqrt(nrs.at(i)), ths, ths, vds.at(i), vrs.at(i));
        }
        disp.addVDF<BiMaxwellianVDF>(1, c*std::sqrt(nc), std::sqrt(bc));
        disp.addVDF<BiMaxwellianVDF>(-mpOme, c*std::sqrt(mpOme), std::sqrt(bc*mpOme));
        printo(std::cout, disp.description(), "\n");

        unsigned jmin = 100;
        auto disp_step_monitor = [&jmin](unsigned const j, CX::Matrix const&)->bool {
            jmin = j;
            return true;
        };
        auto root_step_monitor = [&jmin](unsigned const i, CX::Number const& o)->bool {
            printo(std::cout, "i = ", i, ", o = ", o, ", jmin = ", jmin, "\n");
            return true;
        };

        double k1{}, k2{};
        Utility::MullerRootFinder<std::function<CX::Number(CX::Number)>>
        root([&disp, &k1, &k2, disp_step_monitor](CX::Number const& o)->CX::Number {
            CX::Matrix const o2D = disp.o2_x_D({k1, k2}, o, disp_step_monitor);
            return ::det(o2D);
        });
        root.max_iterations = 6;

        k1 = .1;
        std::deque<double> k2List;
        std::deque<CX::Matrix> DList;
        std::deque<CX::Number> oList{CX::Number{8.84736, -0.029527}, CX::Number{8.84736, -0.029527} - 1./1000, CX::Number{8.84736, -0.029527} - 2./1000};
        for (k2 = 8.8; k2 <= 9.6; k2 += 0.01) {
            disp.set_jmin(jmin);
            decltype(root)::optional_type const opt = root(oList[2], oList[1], oList[0], root_step_monitor);
            XCTAssert(opt, @"k1 = %f, k2 = %f", k1, k2);
            if (opt) {
                oList.push_front(*opt);
                k2List.push_front(k2);
                DList.push_front(disp.D({k1, k2}, *opt, [](unsigned, CX::Matrix const&)->bool { return true; }));
            } else {
                break;
            }
            disp.clear_cache();
        }
        oList.pop_back(); oList.pop_back(); oList.pop_back();
        printo(std::cout, disp.description(), "\n");

        std::ostringstream os;
        os.setf(os.fixed);
        os.precision(15);
        printo(os, "{\n");
        if (!oList.empty()) {
            // k2
            auto k2_it = k2List.rbegin();
            printo(os, "\t{", *k2_it++);
            while (k2_it != k2List.rend()) printo(os, ", ", *k2_it++);
            printo(os, "},\n");

            // o
            auto o_it = oList.rbegin();
            printo(os, "\t{", *o_it++);
            while (o_it != oList.rend()) printo(os, ", ", *o_it++);
            printo(os, "},\n");

            // D
            auto D_it = DList.rbegin();
            printo(os, "\t{", *D_it++);
            while (D_it != DList.rend()) printo(os, ", ", *D_it++);
            printo(os, "}\n");
        }
        printo(os, "}\n");

        if (0) {
            NSString *str = [NSString stringWithUTF8String:os.str().c_str()];
            str = [str stringByReplacingOccurrencesOfString:@"(" withString:@"Complex["];
            str = [str stringByReplacingOccurrencesOfString:@")" withString:@"]"];

            NSString *filename = [NSString stringWithFormat:@"testMin_and_Liu_2016_TenuousShell.m"];
            NSURL *url = [NSURL fileURLWithPath:[NSHomeDirectory() stringByAppendingPathComponent:@"Downloads"] isDirectory:YES];
            url = [url URLByAppendingPathComponent:filename];
            NSError *error;
            XCTAssert([str writeToURL:url atomically:YES encoding:NSUTF8StringEncoding error:&error], @"%@", error);
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

@end
