/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include <DispersionKit/DispersionKit.h>
#include <UtilityKit/UtilityKit.h>
#include <memory>
#include <type_traits>
#include <unordered_map>

#include "llLibraryLinkInit.h" // should be included last

namespace {
// MARK: FLR Abstract
//
class FLR {
protected:
    explicit FLR() = default;

public:
    virtual ~FLR() = default;

    template <class SubC, class... Args>
    static std::unique_ptr<FLR> construct(Args &&...args)
    {
        return std::unique_ptr<FLR>{ new SubC(std::forward<Args>(args)...) };
    }

    /**
     @brief Clear the lookup cache.
     */
    virtual void clear_cache() {}

    /**
     @brief Evaluates integral of 2π*J(n, ax)^2*f(x)*x from 0 to inf.
     */
    virtual double A(int const n, double const a) const = 0;
    /**
     @brief Evaluates integral of 2π*J(n, ax)^2*f'(x) from 0 to inf.
     */
    virtual double P(int const n, double const a) const = 0;

    /**
     @brief Evaluates integral of 2π*J(n, ax)*J'(n, ax)*f(x)*x^2 from 0 to inf.
     */
    virtual double B(int const n, double const a) const = 0;
    /**
     @brief Evaluates integral of 2π*J(n, ax)*J'(n, ax)*f'(x)*x from 0 to inf.
     */
    virtual double Q(int const n, double const a) const = 0;

    /**
     @brief Evaluates integral of 2π*J'(n, ax)^2*f(x)*x^3 from 0 to inf.
     */
    virtual double C(int const n, double const a) const = 0;
    /**
     @brief Evaluates integral of 2π*J'(n, ax)^2*f'(x)*x^2 from 0 to inf.
     */
    virtual double R(int const n, double const a) const = 0;

    /**
     @brief Evaluates A(n, a)/a^2 correctly handling the case where |a| -> 0.
     @discussion It is undefined when n = a = 0.
     */
    virtual double AOa2(int const n, double const a) const = 0;
    /**
     @brief Evaluates P(n, a)/a^2 correctly handling the case where |a| -> 0.
     @discussion It is undefined when n = a = 0.
     */
    virtual double POa2(int const n, double const a) const = 0;

    /**
     @brief Evaluates B(n, a)/a correctly handling the case where |a| -> 0.
     */
    virtual double BOa(int const n, double const a) const = 0;
    /**
     @brief Evaluates Q(n, a)/a correctly handling the case where |a| -> 0.
     */
    virtual double QOa(int const n, double const a) const = 0;
};

// MARK: MaxwellianFLR
//
class MaxwellianFLR : private FLR {
    friend class FLR;

    Disp::MaxwellianFLR flr;
    MaxwellianFLR() = default;

    double A(int const n, double const a) const override { return flr.A(n, a); }
    double P(int const n, double const a) const override { return flr.P(n, a); }

    double B(int const n, double const a) const override { return flr.B(n, a); }
    double Q(int const n, double const a) const override { return flr.Q(n, a); }

    double C(int const n, double const a) const override { return flr.C(n, a); }
    double R(int const n, double const a) const override { return flr.R(n, a); }

    double AOa2(int const n, double const a) const override { return flr.AOa2(n, a); }
    double POa2(int const n, double const a) const override { return flr.POa2(n, a); }

    double BOa(int const n, double const a) const override { return flr.BOa(n, a); }
    double QOa(int const n, double const a) const override { return flr.QOa(n, a); }
};

// MARK: RingFLR
//
class RingFLR : private FLR {
    friend class FLR;

    Disp::RingFLR flr;
    RingFLR(double const b)
    : flr(b) {}

    void clear_cache() override { flr.clear_cache(); }

    double A(int const n, double const a) const override { return flr.A(n, a); }
    double P(int const n, double const a) const override { return flr.P(n, a); }

    double B(int const n, double const a) const override { return flr.B(n, a); }
    double Q(int const n, double const a) const override { return flr.Q(n, a); }

    double C(int const n, double const a) const override { return flr.C(n, a); }
    double R(int const n, double const a) const override { return flr.R(n, a); }

    double AOa2(int const n, double const a) const override { return flr.AOa2(n, a); }
    double POa2(int const n, double const a) const override { return flr.POa2(n, a); }

    double BOa(int const n, double const a) const override { return flr.BOa(n, a); }
    double QOa(int const n, double const a) const override { return flr.QOa(n, a); }
};

// MARK: ColdRingFLR
//
class ColdRingFLR : private FLR {
    friend class FLR;

    Disp::ColdRingFLR flr;
    ColdRingFLR(double const vr)
    : flr(vr) {}

    double A(int const n, double const a) const override { return flr.A(n, a); }
    double P(int const n, double const a) const override { return flr.P(n, a); }

    double B(int const n, double const a) const override { return flr.B(n, a); }
    double Q(int const n, double const a) const override { return flr.Q(n, a); }

    double C(int const n, double const a) const override { return flr.C(n, a); }
    double R(int const n, double const a) const override { return flr.R(n, a); }

    double AOa2(int const n, double const a) const override { return flr.AOa2(n, a); }
    double POa2(int const n, double const a) const override { return flr.POa2(n, a); }

    double BOa(int const n, double const a) const override { return flr.BOa(n, a); }
    double QOa(int const n, double const a) const override { return flr.QOa(n, a); }
};

// MARK: LorentzianFLR
//
class LorentzianFLR : private FLR {
    friend class FLR;

    Disp::LorentzianFLR flr;
    LorentzianFLR(double const kappa)
    : flr(kappa) {}

    void clear_cache() override { flr.clear_cache(); }

    double A(int const n, double const a) const override { return flr.A(n, a); }
    double P(int const n, double const a) const override { return flr.P(n, a); }

    double B(int const n, double const a) const override { return flr.B(n, a); }
    double Q(int const n, double const a) const override { return flr.Q(n, a); }

    double C(int const n, double const a) const override { return flr.C(n, a); }
    double R(int const n, double const a) const override { return flr.R(n, a); }

    double AOa2(int const n, double const a) const override { return flr.AOa2(n, a); }
    double POa2(int const n, double const a) const override { return flr.POa2(n, a); }

    double BOa(int const n, double const a) const override { return flr.BOa(n, a); }
    double QOa(int const n, double const a) const override { return flr.QOa(n, a); }
};

// MARK: SplineFLR
//
class SplineFLR : private FLR {
    friend class FLR;

    Disp::SplineFLR flr;
    SplineFLR(Disp::SplineFLR::spline_coefficient_type &&s)
    : flr(std::move(s)) {}

    void clear_cache() override { flr.clear_cache(); }

    double A(int const n, double const a) const override { return flr.A(n, a); }
    double P(int const n, double const a) const override { return flr.P(n, a); }

    double B(int const n, double const a) const override { return flr.B(n, a); }
    double Q(int const n, double const a) const override { return flr.Q(n, a); }

    double C(int const n, double const a) const override { return flr.C(n, a); }
    double R(int const n, double const a) const override { return flr.R(n, a); }

    double AOa2(int const n, double const a) const override { return flr.AOa2(n, a); }
    double POa2(int const n, double const a) const override { return flr.POa2(n, a); }

    double BOa(int const n, double const a) const override { return flr.BOa(n, a); }
    double QOa(int const n, double const a) const override { return flr.QOa(n, a); }
};

// MARK: Persistent Store
//
using _Store = std::unordered_map<mint, std::unique_ptr<FLR>>;
_Store &store()
{
    static _Store store;
    return store;
}
} // namespace

// MARK: LibraryExpressionManager
//
char const *FLRLibraryExpressionMangerName()
{
    static char name[] = "com.kyungguk.DispersionKit.FLRLibraryExpressionManger";
    return name;
}
void FLRLibraryExpressionManger(WolframLibraryData const, mbool const mode, mint const id)
{
    if (mode) { // tear down
        log_info(__FUNCTION__, " - Tearing down id = ", id);
        store().erase(id); // take out for lookup performance
    } else {               // create
        log_info(__FUNCTION__, " - Creating id = ", id);
        store()[id] = nullptr; // this is not necessary, but not harmful
    }
}

// MARK:- LibraryFunctions
//
/**
 @brief LibraryFunction interface that returns FLRLibraryExpressionManger name.
 @discussion It takes no argument and returns a string, "UTF8String".
 */
EXTERN_C int llFLRLibraryExpressionMangerName(WolframLibraryData const libData, mint const argc, MArgument *const, MArgument const res)
{
    constexpr mint n_args = 0;
    if (n_args != argc) {
        log_debug(__FUNCTION__, " - ", n_args, " argument(s) are expected, but ", argc, " arguments are passed");
        return libData->Message("argc"), LIBRARY_FUNCTION_ERROR;
    }
    MArgument_setUTF8String(res, const_cast<char *>(FLRLibraryExpressionMangerName()));
    return LIBRARY_NO_ERROR;
}

/**
 @brief LibraryFunction interface that releases a persistent FLR object associated with the passed id.
 @discussion The input type specification is:
 {id_Integer}, where
 id is the unique identifier returned from ManagedLibraryExpressionQ.

 The output type specification is "Void".
 */
EXTERN_C int llFLRRelease(WolframLibraryData const libData, mint const argc, MArgument *const args, MArgument const)
{
    constexpr mint n_args = 1;
    if (n_args != argc) {
        log_debug(__FUNCTION__, " - ", n_args, " argument(s) are expected, but ", argc, " arguments are passed");
        return libData->Message("argc"), LIBRARY_FUNCTION_ERROR;
    }
    mint const id = MArgument_getInteger(args[0]);
    return libData->releaseManagedLibraryExpression(FLRLibraryExpressionMangerName(), id);
}

/**
 @brief LibraryFunction interface that releases all persistent FLR objects.
 @discussion It takes no argument and does not return.
 */
EXTERN_C int llFLRReleaseAll(WolframLibraryData const libData, mint const argc, MArgument *const, MArgument const)
{
    constexpr mint n_args = 0;
    if (n_args != argc) {
        log_debug(__FUNCTION__, " - ", n_args, " argument(s) are expected, but ", argc, " arguments are passed");
        return libData->Message("argc"), LIBRARY_FUNCTION_ERROR;
    }
    for (auto const &pair : store()) {
        libData->releaseManagedLibraryExpression(FLRLibraryExpressionMangerName(), pair.first);
    }
    return LIBRARY_NO_ERROR;
}

/**
 @brief LibraryFunction interface that clears cached results of the FLR object associated with the passed id.
 @discussion The input type specification is:
 {id_Integer}, where
 id is the unique identifier returned from ManagedLibraryExpressionQ.

 The output type specification is "Void".
 */
EXTERN_C int llFLRClearCache(WolframLibraryData const libData, mint const argc, MArgument *const args, MArgument const)
{
    constexpr mint n_args = 1;
    if (n_args != argc) {
        log_debug(__FUNCTION__, " - ", n_args, " argument(s) are expected, but ", argc, " arguments are passed");
        return libData->Message("argc"), LIBRARY_FUNCTION_ERROR;
    }

    // id
    mint const id = MArgument_getInteger(args[0]);
    if (!store().count(id)) {
        log_debug(__FUNCTION__, " - No persistent object associated with id = ", id);
        return libData->Message("noitem"), LIBRARY_FUNCTION_ERROR;
    }
    auto const &flr = store()[id];
    if (!flr) {
        log_debug(__FUNCTION__, " - Persistent object for id = ", id, " is invalid");
        return libData->Message("noitem"), LIBRARY_FUNCTION_ERROR;
    }

    // evaluate
    try {
        flr->clear_cache();
    } catch (std::exception &e) {
        log_debug(__FUNCTION__, " - ", e.what());
        return libData->Message("exception"), LIBRARY_FUNCTION_ERROR;
    }

    return LIBRARY_NO_ERROR;
}

/**
 @brief LibraryFunction interface that evaluates all integrals A, P, B, Q, C, R, AOa2, POa2, BOa and QOa.
 @discussion The input type specification is:
 {id_Integer, n_Integer, a_Real}, where
 id is the unique identifier returned from ManagedLibraryExpressionQ,
 n is a cyclotron harmonic order, and
 a is the ratio of the Larmor radius to the wave length.

 The output type specification is {Real, 1}, a list of {A, P, B, Q, C, R, AOa2, POa2, BOa, QOa}.
 */
EXTERN_C int llFLREvaluateAll(WolframLibraryData const libData, mint const argc, MArgument *const args, MArgument const res)
{
    constexpr mint n_args = 3;
    if (n_args != argc) {
        log_debug(__FUNCTION__, " - ", n_args, " argument(s) are expected, but ", argc, " arguments are passed");
        return libData->Message("argc"), LIBRARY_FUNCTION_ERROR;
    }

    // id
    mint const id = MArgument_getInteger(args[0]);
    if (!store().count(id)) {
        log_debug(__FUNCTION__, " - No persistent object associated with id = ", id);
        return libData->Message("noitem"), LIBRARY_FUNCTION_ERROR;
    }
    auto const &flr = store()[id];
    if (!flr) {
        log_debug(__FUNCTION__, " - Persistent object for id = ", id, " is invalid");
        return libData->Message("noitem"), LIBRARY_FUNCTION_ERROR;
    }

    // n
    int const n = static_cast<int>(MArgument_getInteger(args[1]));

    // a
    mreal const a = MArgument_getReal(args[2]);

    // results
    struct {
        mreal A, P, B, Q, C, R, AOa2, POa2, BOa, QOa;
    } * results;
    {
        constexpr mint n_dim       = 1;
        mint const     dims[n_dim] = { 10 };
        if (libData->MTensor_new(MType_Real, n_dim, dims, res.tensor)) {
            return LIBRARY_MEMORY_ERROR;
        }
        mreal *data = libData->MTensor_getRealData(*res.tensor);
        results     = reinterpret_cast<decltype(results)>(data);
    }

    // evaluate
    try {
        results->A = flr->A(n, a);
        results->P = flr->P(n, a);

        results->B = flr->B(n, a);
        results->Q = flr->Q(n, a);

        results->C = flr->C(n, a);
        results->R = flr->R(n, a);

        results->AOa2 = flr->AOa2(n, a);
        results->POa2 = flr->POa2(n, a);

        results->BOa = flr->BOa(n, a);
        results->QOa = flr->QOa(n, a);
    } catch (std::exception &e) {
        log_debug(__FUNCTION__, " - ", e.what());
        return libData->Message("exception"), LIBRARY_FUNCTION_ERROR;
    }

    return LIBRARY_NO_ERROR;
}

/**
 @brief LibraryFunction interface that evaluates one of A, P, B, Q, C, R, AOa2, POa2, BOa and QOa.
 @discussion The input type specification is:
 {id_Integer, n_Integer, a_Real, selector_Integer}, where
 id is the unique identifier returned from ManagedLibraryExpressionQ,
 n is a cyclotron harmonic order,
 a is the ratio of the Larmor radius to the wave length, and
 selector is an integer selector:
 switch(selector)
 case 1: A
 case 2: P
 case 3: B
 case 4: Q
 case 5: C
 case 6: R
 case 7: AOa2
 case 8: POa2
 case 9: BOa
 case 10: QOa
 .

 The output type specification is Real of the result.
 */
EXTERN_C int llFLREvaluateOne(WolframLibraryData const libData, mint const argc, MArgument *const args, MArgument const res)
{
    constexpr mint n_args = 4;
    if (n_args != argc) {
        log_debug(__FUNCTION__, " - ", n_args, " argument(s) are expected, but ", argc, " arguments are passed");
        return libData->Message("argc"), LIBRARY_FUNCTION_ERROR;
    }

    // id
    mint const id = MArgument_getInteger(args[0]);
    if (!store().count(id)) {
        log_debug(__FUNCTION__, " - No persistent object associated with id = ", id);
        return libData->Message("noitem"), LIBRARY_FUNCTION_ERROR;
    }
    auto const &flr = store()[id];
    if (!flr) {
        log_debug(__FUNCTION__, " - Persistent object for id = ", id, " is invalid");
        return libData->Message("noitem"), LIBRARY_FUNCTION_ERROR;
    }

    // n
    int const n = static_cast<int>(MArgument_getInteger(args[1]));

    // a
    mreal const a = MArgument_getReal(args[2]);

    // selector
    mint const selecter = MArgument_getInteger(args[3]);

    // result
    mreal *result = res.real;

    // evaluate
    try {
        switch (selecter) {
            case 1:
                *result = flr->A(n, a);
                break;
            case 2:
                *result = flr->P(n, a);
                break;
            case 3:
                *result = flr->B(n, a);
                break;
            case 4:
                *result = flr->Q(n, a);
                break;
            case 5:
                *result = flr->C(n, a);
                break;
            case 6:
                *result = flr->R(n, a);
                break;
            case 7:
                *result = flr->AOa2(n, a);
                break;
            case 8:
                *result = flr->POa2(n, a);
                break;
            case 9:
                *result = flr->BOa(n, a);
                break;
            case 10:
                *result = flr->QOa(n, a);
                break;
            default:
                return libData->Message("noswitchcase"), LIBRARY_FUNCTION_ERROR;
        }
    } catch (std::exception &e) {
        log_debug(__FUNCTION__, " - ", e.what());
        return libData->Message("exception"), LIBRARY_FUNCTION_ERROR;
    }

    return LIBRARY_NO_ERROR;
}

/**
 @brief LibraryFunction interface that constructs a persistent MaxwellianFLR object associated with the passed id.
 @discussion The input type specification is:
 {id_Integer}, where
 id is the unique identifier returned from ManagedLibraryExpressionQ.

 The output type specification is "Void".
 */
EXTERN_C int llMaxwellianFLR(WolframLibraryData const libData, mint const argc, MArgument *const args, MArgument const)
{
    constexpr mint n_args = 1;
    if (n_args != argc) {
        log_debug(__FUNCTION__, " - ", n_args, " argument(s) are expected, but ", argc, " arguments are passed");
        return libData->Message("argc"), LIBRARY_FUNCTION_ERROR;
    }

    // id
    mint const id = MArgument_getInteger(args[0]);

    // construct
    try {
        store()[id] = FLR::construct<MaxwellianFLR>();
    } catch (std::exception &e) {
        log_debug(__FUNCTION__, " - ", e.what());
        return libData->Message("exception"), LIBRARY_FUNCTION_ERROR;
    }

    return LIBRARY_NO_ERROR;
}

/**
 @brief LibraryFunction interface that constructs a persistent RingFLR object associated with the passed id.
 @discussion The input type specification is:
 {id_Integer, b_Real}, where
 id is the unique identifier returned from ManagedLibraryExpressionQ, and
 b is the ratio of the ring speed, vr, to the thermal speed, th.

 The output type specification is "Void".
 */
EXTERN_C int llRingFLR(WolframLibraryData const libData, mint const argc, MArgument *const args, MArgument const)
{
    constexpr mint n_args = 2;
    if (n_args != argc) {
        log_debug(__FUNCTION__, " - ", n_args, " argument(s) are expected, but ", argc, " arguments are passed");
        return libData->Message("argc"), LIBRARY_FUNCTION_ERROR;
    }

    // id
    mint const id = MArgument_getInteger(args[0]);

    // b
    mreal const b = MArgument_getReal(args[1]);

    // construct
    try {
        store()[id] = FLR::construct<RingFLR>(b);
    } catch (std::exception &e) {
        log_debug(__FUNCTION__, " - ", e.what());
        return libData->Message("exception"), LIBRARY_FUNCTION_ERROR;
    }

    return LIBRARY_NO_ERROR;
}

/**
 @brief LibraryFunction interface that constructs a persistent ColdRingFLR object associated with the passed id.
 @discussion The input type specification is:
 {id_Integer, vr_Real}, where
 id is the unique identifier returned from ManagedLibraryExpressionQ, and
 vr is the ring speed.

 The output type specification is "Void".
 */
EXTERN_C int llColdRingFLR(WolframLibraryData const libData, mint const argc, MArgument *const args, MArgument const)
{
    constexpr mint n_args = 2;
    if (n_args != argc) {
        log_debug(__FUNCTION__, " - ", n_args, " argument(s) are expected, but ", argc, " arguments are passed");
        return libData->Message("argc"), LIBRARY_FUNCTION_ERROR;
    }

    // id
    mint const id = MArgument_getInteger(args[0]);

    // vr
    mreal const vr = MArgument_getReal(args[1]);

    // construct
    try {
        store()[id] = FLR::construct<ColdRingFLR>(vr);
    } catch (std::exception &e) {
        log_debug(__FUNCTION__, " - ", e.what());
        return libData->Message("exception"), LIBRARY_FUNCTION_ERROR;
    }

    return LIBRARY_NO_ERROR;
}

/**
 @brief LibraryFunction interface that constructs a persistent LorentzianFLR object associated with the passed id.
 @discussion The input type specification is:
 {id_Integer, kappa_Real}, where
 id is the unique identifier returned from ManagedLibraryExpressionQ, and
 kappa is the kappa index.

 The output type specification is "Void".
 */
EXTERN_C int llLorentzianFLR(WolframLibraryData const libData, mint const argc, MArgument *const args, MArgument const)
{
    constexpr mint n_args = 2;
    if (n_args != argc) {
        log_debug(__FUNCTION__, " - ", n_args, " argument(s) are expected, but ", argc, " arguments are passed");
        return libData->Message("argc"), LIBRARY_FUNCTION_ERROR;
    }

    // id
    mint const id = MArgument_getInteger(args[0]);

    // kappa
    mreal const kappa = MArgument_getReal(args[1]);

    // construct
    try {
        store()[id] = FLR::construct<LorentzianFLR>(kappa);
    } catch (std::exception &e) {
        log_debug(__FUNCTION__, " - ", e.what());
        return libData->Message("exception"), LIBRARY_FUNCTION_ERROR;
    }

    return LIBRARY_NO_ERROR;
}

/**
 @brief LibraryFunction interface that constructs a persistent SplineFLR object associated with the passed id.
 @discussion The input type specification is:
 {id_Integer, {xs_Real, 1}, {ys_Real, 1}}, where
 id is the unique identifier returned from ManagedLibraryExpressionQ,
 xs is a rank-1 tensor of abscissa values with Length[xs] >= 4, and
 ys is a rank-1 tensor of ordinate values with Length[ys] == Length[xs].

 The output type specification is "Void".
 */
EXTERN_C int llSplineFLR(WolframLibraryData const libData, mint const argc, MArgument *const args, MArgument const)
{
    constexpr mint n_args = 3;
    if (n_args != argc) {
        log_debug(__FUNCTION__, " - ", n_args, " argument(s) are expected, but ", argc, " arguments are passed");
        return libData->Message("argc"), LIBRARY_FUNCTION_ERROR;
    }

    // id
    mint const id = MArgument_getInteger(args[0]);

    // xs
    Utility::PaddedArray<double, 0> xs;
    {
        long const idx = 1;
        if (MType_Real != libData->MTensor_getType(MArgument_getMTensor(args[idx]))) {
            return LIBRARY_TYPE_ERROR;
        }
        if (1 != libData->MTensor_getRank(MArgument_getMTensor(args[idx]))) {
            return LIBRARY_RANK_ERROR;
        }
        mint const size = *libData->MTensor_getDimensions(MArgument_getMTensor(args[idx]));
        if (size < 4) {
            return LIBRARY_DIMENSION_ERROR;
        }
        mreal *data = libData->MTensor_getRealData(MArgument_getMTensor(args[idx]));
        xs          = decltype(xs)(static_cast<unsigned long>(size), data);
    }

    // ys
    Utility::PaddedArray<double, 0> ys;
    {
        long const idx = 2;
        if (MType_Real != libData->MTensor_getType(MArgument_getMTensor(args[idx]))) {
            return LIBRARY_TYPE_ERROR;
        }
        if (1 != libData->MTensor_getRank(MArgument_getMTensor(args[idx]))) {
            return LIBRARY_RANK_ERROR;
        }
        mint const size = *libData->MTensor_getDimensions(MArgument_getMTensor(args[idx]));
        if (size < 4) {
            return LIBRARY_DIMENSION_ERROR;
        }
        mreal *data = libData->MTensor_getRealData(MArgument_getMTensor(args[idx]));
        ys          = decltype(ys)(static_cast<unsigned long>(size), data);
    }

    if (xs.size() != ys.size()) {
        return LIBRARY_DIMENSION_ERROR;
    }

    // construct
    try {
        store()[id] = FLR::construct<SplineFLR>(Utility::CubicSplineCoefficient<double>(xs.begin(), xs.end(), ys.begin()));
    } catch (std::exception &e) {
        log_debug(__FUNCTION__, " - ", e.what());
        return libData->Message("exception"), LIBRARY_FUNCTION_ERROR;
    }

    return LIBRARY_NO_ERROR;
}
