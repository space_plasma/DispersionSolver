/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include "WolframLibrary.h"

// MARK: LibraryExpressionManger
//
extern char const *PDFLibraryExpressionManagerName();
extern void        PDFLibraryExpressionManager(WolframLibraryData const libData, mbool const mode, mint const id);
