/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include "WolframLibrary.h"

// MARK: LibraryCallbackManger
//
extern char const *FindRootCallbackManagerName();
extern mbool       FindRootCallbackManager(WolframLibraryData const libData, mint const id, MTensor const argtypes);
