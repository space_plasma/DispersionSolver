/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include <UtilityKit/UtilityKit.h>
#include <complex>
#include <functional>
#include <limits>
#include <utility>

#include "llLibraryLinkInit.h" // should be included last

// MARK: Persistent
//
namespace {
struct MullerCallback {
    mint _id{ 0 };
         operator const bool() const noexcept { return _id > 0; }

    explicit MullerCallback() noexcept = default;
    MullerCallback &operator           =(std::pair<WolframLibraryData const, mint const> const &pair)
    {
        if (_id > 0) {
            pair.first->releaseLibraryCallbackFunction(_id);
        }
        _id = pair.second;
        return *this;
    }
    MullerCallback(MullerCallback const &) = delete;
    MullerCallback &operator=(MullerCallback const &) = delete;

    std::complex<double> operator()(WolframLibraryData const libData, std::complex<double> x) const
    {
        std::complex<double> y{ std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN() };
        constexpr mint       argc = 1;
        MArgument            args[argc];
        args[0].cmplex = reinterpret_cast<mcomplex *>(&x);
        MArgument res;
        res.cmplex = reinterpret_cast<mcomplex *>(&y);
        if (libData->AbortQ()) {
            log_info(__FUNCTION__, " - Aborted");
        } else if (libData->callLibraryCallbackFunction(_id, argc, args, res)) {
            log_debug(__FUNCTION__, " - callLibraryCallbackFunction returned error");
        }
        return y;
    }
} muller_callback;
} // namespace

// MARK: LibraryCallbackManager
//
char const *FindRootCallbackManagerName()
{
    static char name[] = "com.kyungguk.DispersionKit.FindRootCallbackManager";
    return name;
}
mbool FindRootCallbackManager(WolframLibraryData const libData, mint const id, MTensor const argtypes)
{
    mint const *dims = libData->MTensor_getDimensions(argtypes);
    if (dims[0] != 2) { // 1 input, 1 output
        log_debug(__FUNCTION__, " - Incorrect argument count");
        return false;
    }
    // type/rank checks
    std::pair<mint, mint> const *typerank = reinterpret_cast<decltype(typerank)>(libData->MTensor_getIntegerData(argtypes));
    if (typerank[0].first == MType_Complex && typerank[0].second == 0) { // complex scalar argument select muller root finder
        // return type check
        decltype(typerank) last = typerank + dims[0] - 2;
        if (last->first != MType_Complex || last->second != 0) { // return is the same type/rank as the input
            log_debug(__FUNCTION__, " - Muller root finder return type inconsistency");
            return false;
        }
        muller_callback = std::make_pair(libData, id);
        log_info(__FUNCTION__, " - Muller root finder selected");
        return true;
    }
    log_debug(__FUNCTION__, " - No root finder selected");
    return false;
}

// MARK:- LibraryFunctions
//
/**
 @brief LibraryFunction interface that returns FindRootCallbackManagerName name.
 @discussion It takes no argument and returns a string, "UTF8String".
 */
EXTERN_C int llFindRootCallbackManagerName(WolframLibraryData const libData, mint const argc, MArgument *const, MArgument const res)
{
    constexpr mint n_args = 0;
    if (n_args != argc) {
        log_debug(__FUNCTION__, " - ", n_args, " argument(s) are expected, but ", argc, " arguments are passed");
        return libData->Message("argc"), LIBRARY_FUNCTION_ERROR;
    }
    MArgument_setUTF8String(res, const_cast<char *>(FindRootCallbackManagerName()));
    return LIBRARY_NO_ERROR;
}

/**
 @brief LibraryFunction interface that iteratively finds a complex root using Muller's method.
 @discussion The input type specification is:
 {x0_Complex, x1_Complex, x2_Complex, maxit_Integer?Positive, reltol_Real?Positive}, where
 x0, x1 and x2 are three initial guesses, and
 maxit is the maximum iteration count.

 The output type specification is {Real, 1}, a pair of real and imaginary parts of the approximate complex root.

 a pair of NaNs indicates when the library callback function is not set or root finding fails.
 */
EXTERN_C int llMullerFindRoot(WolframLibraryData const libData, mint const argc, MArgument *const args, MArgument const res)
{
    constexpr mint n_args = 5;
    if (n_args != argc) {
        log_debug(__FUNCTION__, " - ", n_args, " argument(s) are expected, but ", argc, " arguments are passed");
        return libData->Message("argc"), LIBRARY_FUNCTION_ERROR;
    }

    // z0
    std::complex<double> const *z0 = reinterpret_cast<decltype(z0)>(args[0].cmplex);

    // z1
    std::complex<double> const *z1 = reinterpret_cast<decltype(z1)>(args[1].cmplex);

    // z2
    std::complex<double> const *z2 = reinterpret_cast<decltype(z2)>(args[2].cmplex);

    // maxit
    mint const maxit = MArgument_getInteger(args[3]);
    if (maxit <= 0) {
        log_debug(__FUNCTION__, " - The maximum iteration count is not positive; maxit = ", maxit);
        return LIBRARY_FUNCTION_ERROR;
    }

    // reltol
    mreal const reltol = MArgument_getReal(args[4]);
    if (reltol <= 0) {
        log_debug(__FUNCTION__, " - The relative tolerance is not positive; reltol = ", reltol);
        return LIBRARY_FUNCTION_ERROR;
    }

    // z
    std::complex<double> *z;
    {
        constexpr mint n_dim       = 1;
        mint const     dims[n_dim] = { 2 };
        int const      err         = libData->MTensor_new(MType_Real, n_dim, dims, res.tensor);
        if (err) {
            return err;
        }
        mreal *data = libData->MTensor_getRealData(*res.tensor);
        z           = reinterpret_cast<decltype(z)>(data);
        z->real(std::numeric_limits<double>::quiet_NaN());
        z->imag(std::numeric_limits<double>::quiet_NaN());
    }

    // find root
    try {
        if (!muller_callback) {
            log_debug(__FUNCTION__, " - No Muller's root finder callback");
            return libData->Message("nocallback"), LIBRARY_FUNCTION_ERROR;
        }
        Utility::MullerRootFinder<std::function<std::complex<double>(std::complex<double>)>>
            root([libData](std::complex<double> const &z) -> std::complex<double> {
                return muller_callback(libData, z);
            });
        root.max_iterations                       = static_cast<unsigned>(maxit);
        root.reltol                               = reltol;
        root.abstol                               = 1e-10;
        decltype(root)::optional_type const z_opt = root(*z0, *z1, *z2);
        if (z_opt) {
            *z = *z_opt;
        } else {
            log_debug(__FUNCTION__, " - Root not found; z0 = ", *z0, ", z1 = ", *z1, ", z2 = ", *z2, ", maxit = ", maxit);
            return LIBRARY_NUMERICAL_ERROR;
        }
    } catch (std::exception &e) {
        log_debug(__FUNCTION__, " - ", e.what());
        return libData->Message("exception"), LIBRARY_FUNCTION_ERROR;
    }

    return LIBRARY_NO_ERROR;
}
