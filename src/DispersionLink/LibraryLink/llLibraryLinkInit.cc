/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "llLibraryLinkInit.h"
#include "llFLR.h"
#include "llFindRoot.h"
#include "llPDF.h"
#include <gsl/gsl_errno.h>

EXTERN_C mint WolframLibrary_getVersion(void)
{
    return WolframLibraryVersion;
}

// MARK: LibraryLink init/deinit
//
EXTERN_C int WolframLibrary_initialize(WolframLibraryData libData)
{
    // disable gsl error handler
    //
    gsl_set_error_handler_off();

    // register managers
    //
    libData->registerLibraryExpressionManager(PDFLibraryExpressionManagerName(), &PDFLibraryExpressionManager);
    libData->registerLibraryExpressionManager(FLRLibraryExpressionMangerName(), &FLRLibraryExpressionManger);
    libData->registerLibraryCallbackManager(FindRootCallbackManagerName(), &FindRootCallbackManager);

    return 0;
}

EXTERN_C void WolframLibrary_uninitialize(WolframLibraryData libData)
{
    // unregister managers
    //
    libData->unregisterLibraryExpressionManager(PDFLibraryExpressionManagerName());
    libData->unregisterLibraryExpressionManager(FLRLibraryExpressionMangerName());
    libData->unregisterLibraryCallbackManager(FindRootCallbackManagerName());

    return;
}
