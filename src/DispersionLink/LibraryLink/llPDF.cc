/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include <DispersionKit/DispersionKit.h>
#include <UtilityKit/UtilityKit.h>
#include <memory>
#include <type_traits>
#include <unordered_map>

#include "llLibraryLinkInit.h" // should be included last

namespace {
// MARK: PDF Abstract
//
class PDF {
public:
    virtual ~PDF() = default;

    template <class SubC, class... Args>
    static std::unique_ptr<PDF> construct(Args &&...args)
    {
        return std::unique_ptr<PDF>{ new SubC(std::forward<Args>(args)...) };
    }

    using triplet_type = std::tuple<std::complex<double>, std::complex<double>, std::complex<double>>;

    /**
     @brief Evaluates Z<0>, Z<1> and Z<2> using the recurring algorithm.
     */
    virtual triplet_type Ztriplet(std::complex<double> const &) const = 0;
    /**
     @brief Evaluates W<0>, W<1> and W<2> using the recurring algorithm.
     */
    virtual triplet_type Wtriplet(std::complex<double> const &) const = 0;

    //@{
    /**
     @brief Evaluates integral of f(x)*x^N/(x - z).
     */
    virtual std::tuple_element<0, triplet_type>::type Z0(std::complex<double> const &z) const { return std::get<0>(Ztriplet(z)); }
    virtual std::tuple_element<1, triplet_type>::type Z1(std::complex<double> const &z) const { return std::get<1>(Ztriplet(z)); }
    virtual std::tuple_element<2, triplet_type>::type Z2(std::complex<double> const &z) const { return std::get<2>(Ztriplet(z)); }
    //@}

    //@{
    /**
     @brief Evaluates integral of f'(x)*x^N/(x - z).
     */
    virtual std::tuple_element<0, triplet_type>::type W0(std::complex<double> const &z) const { return std::get<0>(Wtriplet(z)); }
    virtual std::tuple_element<1, triplet_type>::type W1(std::complex<double> const &z) const { return std::get<1>(Wtriplet(z)); }
    virtual std::tuple_element<2, triplet_type>::type W2(std::complex<double> const &z) const { return std::get<2>(Wtriplet(z)); }
    //@}

protected:
    explicit PDF() = default;
};

// MARK: MaxwellianPDF
//
class MaxwellianPDF : private PDF {
    friend class PDF;

    Disp::MaxwellianPDF pdf;
    MaxwellianPDF() = default;

    triplet_type Ztriplet(std::complex<double> const &z) const override
    {
        return pdf.Ztriplet(z);
    }
    triplet_type Wtriplet(std::complex<double> const &z) const override
    {
        return pdf.Wtriplet(z);
    }
};

// MARK: SplinePDF
//
class SplinePDF : private PDF {
    friend class PDF;

    Disp::SplinePDF pdf;
    bool            analytic_continuation;
    SplinePDF(Disp::SplinePDF::spline_coefficient_type &&s, bool ac)
    : pdf(std::move(s)), analytic_continuation(ac)
    {
    }

    triplet_type Ztriplet(std::complex<double> const &z) const override
    {
        return pdf.Ztriplet(z, analytic_continuation);
    }
    triplet_type Wtriplet(std::complex<double> const &z) const override
    {
        return pdf.Wtriplet(z, analytic_continuation);
    }

    std::tuple_element<0, triplet_type>::type Z0(std::complex<double> const &z) const override
    {
        return pdf.Z<0>(z, analytic_continuation);
    }
    std::tuple_element<1, triplet_type>::type Z1(std::complex<double> const &z) const override
    {
        return pdf.Z<1>(z, analytic_continuation);
    }
    std::tuple_element<2, triplet_type>::type Z2(std::complex<double> const &z) const override
    {
        return pdf.Z<2>(z, analytic_continuation);
    }

    std::tuple_element<0, triplet_type>::type W0(std::complex<double> const &z) const override
    {
        return pdf.W<0>(z, analytic_continuation);
    }
    std::tuple_element<1, triplet_type>::type W1(std::complex<double> const &z) const override
    {
        return pdf.W<1>(z, analytic_continuation);
    }
    std::tuple_element<2, triplet_type>::type W2(std::complex<double> const &z) const override
    {
        return pdf.W<2>(z, analytic_continuation);
    }
};

// MARK: IntegerKappaPDF
//
class IntegerKappaPDF : private PDF {
    friend class PDF;

    Disp::IntegerKappaPDF pdf;
    IntegerKappaPDF(int const kappa)
    : pdf(kappa) {}

    triplet_type Ztriplet(std::complex<double> const &z) const override
    {
        return pdf.Ztriplet(z);
    }
    triplet_type Wtriplet(std::complex<double> const &z) const override
    {
        return pdf.Wtriplet(z);
    }
};

// MARK: SplineKappaPDF
//
class SplineKappaPDF : private PDF {
    friend class PDF;

    Disp::SplineKappaPDF pdf;
    SplineKappaPDF(double const kappa)
    : pdf(kappa) {}

    triplet_type Ztriplet(std::complex<double> const &z) const override
    {
        return pdf.Ztriplet(z);
    }
    triplet_type Wtriplet(std::complex<double> const &z) const override
    {
        return pdf.Wtriplet(z);
    }

    std::tuple_element<0, triplet_type>::type Z0(std::complex<double> const &z) const override
    {
        return pdf.Z<0>(z);
    }
    std::tuple_element<1, triplet_type>::type Z1(std::complex<double> const &z) const override
    {
        return pdf.Z<1>(z);
    }
    std::tuple_element<2, triplet_type>::type Z2(std::complex<double> const &z) const override
    {
        return pdf.Z<2>(z);
    }

    std::tuple_element<0, triplet_type>::type W0(std::complex<double> const &z) const override
    {
        return pdf.W<0>(z);
    }
    std::tuple_element<1, triplet_type>::type W1(std::complex<double> const &z) const override
    {
        return pdf.W<1>(z);
    }
    std::tuple_element<2, triplet_type>::type W2(std::complex<double> const &z) const override
    {
        return pdf.W<2>(z);
    }
};

// MARK: Persistent Store
//
using _Store = std::unordered_map<mint, std::unique_ptr<PDF>>;
_Store &store()
{
    static _Store store;
    return store;
}
} // namespace

// MARK: LibraryExpressionManager
//
char const *PDFLibraryExpressionManagerName()
{
    static char name[] = "com.kyungguk.DispersionKit.PDFLibraryExpressionManager";
    return name;
}
void PDFLibraryExpressionManager(WolframLibraryData const, mbool const mode, mint const id)
{
    if (mode) { // tear down
        log_info(__FUNCTION__, " - Tearing down id = ", id);
        store().erase(id); // take out for lookup performance
    } else {               // create
        log_info(__FUNCTION__, " - Creating id = ", id);
        store()[id] = nullptr; // this is not necessary, but not harmful
    }
}

// MARK:- LibraryFunctions
//
/**
 @brief LibraryFunction interface that returns PDFLibraryExpressionManager name.
 @discussion It takes no argument and returns a string, "UTF8String".
 */
EXTERN_C int llPDFLibraryExpressionManagerName(WolframLibraryData const libData, mint const argc, MArgument *const, MArgument const res)
{
    constexpr mint n_args = 0;
    if (n_args != argc) {
        log_debug(__FUNCTION__, " - ", n_args, " argument(s) are expected, but ", argc, " arguments are passed");
        return libData->Message("argc"), LIBRARY_FUNCTION_ERROR;
    }
    MArgument_setUTF8String(res, const_cast<char *>(PDFLibraryExpressionManagerName()));
    return LIBRARY_NO_ERROR;
}

/**
 @brief LibraryFunction interface that releases a persistent PDF object associated with the passed id.
 @discussion The input type specification is:
 {id_Integer}, where
 id is the unique identifier returned from ManagedLibraryExpressionQ.

 The output type specification is "Void".
 */
EXTERN_C int llPDFRelease(WolframLibraryData const libData, mint const argc, MArgument *const args, MArgument const)
{
    constexpr mint n_args = 1;
    if (n_args != argc) {
        log_debug(__FUNCTION__, " - ", n_args, " argument(s) are expected, but ", argc, " arguments are passed");
        return libData->Message("argc"), LIBRARY_FUNCTION_ERROR;
    }
    mint const id = MArgument_getInteger(args[0]);
    return libData->releaseManagedLibraryExpression(PDFLibraryExpressionManagerName(), id);
}

/**
 @brief LibraryFunction interface that releases all persistent PDF objects.
 @discussion It takes no argument and does not return.
 */
EXTERN_C int llPDFReleaseAll(WolframLibraryData const libData, mint const argc, MArgument *const, MArgument const)
{
    constexpr mint n_args = 0;
    if (n_args != argc) {
        log_debug(__FUNCTION__, " - ", n_args, " argument(s) are expected, but ", argc, " arguments are passed");
        return libData->Message("argc"), LIBRARY_FUNCTION_ERROR;
    }
    for (auto const &pair : store()) {
        libData->releaseManagedLibraryExpression(PDFLibraryExpressionManagerName(), pair.first);
    }
    return LIBRARY_NO_ERROR;
}

/**
 @brief LibraryFunction interface that evaluates a Z triplet associated with the passed id.
 @discussion The input type specification is:
 {id_Integer, z_Complex}, where
 id is the unique identifier returned from ManagedLibraryExpressionQ, and
 z is a complex number.

 The output type specification is {res_Real, 2}, which stores {Z<0>, Z<1>, Z<2>} as a list of real-imaginary pairs.
 */
EXTERN_C int llPDFZtriplet(WolframLibraryData const libData, mint const argc, MArgument *const args, MArgument const res)
{
    constexpr mint n_args = 2;
    if (n_args != argc) {
        log_debug(__FUNCTION__, " - ", n_args, " argument(s) are expected, but ", argc, " arguments are passed");
        return libData->Message("argc"), LIBRARY_FUNCTION_ERROR;
    }

    // id
    mint const id = MArgument_getInteger(args[0]);
    if (!store().count(id)) {
        log_debug(__FUNCTION__, " - No persistent object associated with id = ", id);
        return libData->Message("noitem"), LIBRARY_FUNCTION_ERROR;
    }
    auto const &pdf = store()[id];
    if (!pdf) {
        log_debug(__FUNCTION__, " - Persistent object for id = ", id, " is invalid");
        return libData->Message("noitem"), LIBRARY_FUNCTION_ERROR;
    }

    // z
    std::complex<double> const *z = reinterpret_cast<decltype(z)>(args[1].cmplex);

    // Ztriplet
    std::complex<double> *Zs;
    {
        constexpr mint n_dim       = 2;
        mint const     dims[n_dim] = { 3, 2 };
        if (libData->MTensor_new(MType_Real, n_dim, dims, res.tensor)) {
            return LIBRARY_MEMORY_ERROR;
        }
        mreal *data = libData->MTensor_getRealData(*res.tensor);
        Zs          = reinterpret_cast<decltype(Zs)>(data);
    }

    // evaluate
    try {
        std::tie(Zs[0], Zs[1], Zs[2]) = pdf->Ztriplet(*z);
    } catch (std::exception &e) {
        log_debug(__FUNCTION__, " - ", e.what());
        return libData->Message("exception"), LIBRARY_FUNCTION_ERROR;
    }

    return LIBRARY_NO_ERROR;
}

/**
 @brief LibraryFunction interface that evaluates a W triplet associated with the passed id.
 @discussion The input type specification is:
 {id_Integer, z_Complex}, where
 id is the unique identifier returned from ManagedLibraryExpressionQ, and
 z is a complex number.

 The output type specification is {res_Real, 2}, which stores {W<0>, W<1>, W<2>} as a list of real-imaginary pairs.
 */
EXTERN_C int llPDFWtriplet(WolframLibraryData const libData, mint const argc, MArgument *const args, MArgument const res)
{
    constexpr mint n_args = 2;
    if (n_args != argc) {
        log_debug(__FUNCTION__, " - ", n_args, " argument(s) are expected, but ", argc, " arguments are passed");
        return libData->Message("argc"), LIBRARY_FUNCTION_ERROR;
    }

    // id
    mint const id = MArgument_getInteger(args[0]);
    if (!store().count(id)) {
        log_debug(__FUNCTION__, " - No persistent object associated with id = ", id);
        return libData->Message("noitem"), LIBRARY_FUNCTION_ERROR;
    }
    auto const &pdf = store()[id];
    if (!pdf) {
        log_debug(__FUNCTION__, " - Persistent object for id = ", id, " is invalid");
        return libData->Message("noitem"), LIBRARY_FUNCTION_ERROR;
    }

    // z
    std::complex<double> const *z = reinterpret_cast<decltype(z)>(args[1].cmplex);

    // Wtriplet
    std::complex<double> *Ws;
    {
        constexpr mint n_dim       = 2;
        mint const     dims[n_dim] = { 3, 2 };
        if (libData->MTensor_new(MType_Real, n_dim, dims, res.tensor)) {
            return LIBRARY_MEMORY_ERROR;
        }
        mreal *data = libData->MTensor_getRealData(*res.tensor);
        Ws          = reinterpret_cast<decltype(Ws)>(data);
    }

    // evaluate
    try {
        std::tie(Ws[0], Ws[1], Ws[2]) = pdf->Wtriplet(*z);
    } catch (std::exception &e) {
        log_debug(__FUNCTION__, " - ", e.what());
        return libData->Message("exception"), LIBRARY_FUNCTION_ERROR;
    }

    return LIBRARY_NO_ERROR;
}

/**
 @brief LibraryFunction interface that evaluates Z<n> associated with the passed id.
 @discussion The input type specification is:
 {id_Integer, n_Integer, z_Complex}, where
 id is the unique identifier returned from ManagedLibraryExpressionQ,
 n is either 0, 1 or 2, and
 z is a complex number.

 The output type specification is {res_Real, 1}, a pair of real and imaginary parts.
 */
EXTERN_C int llPDFIntegralZ(WolframLibraryData const libData, mint const argc, MArgument *const args, MArgument const res)
{
    constexpr mint n_args = 3;
    if (n_args != argc) {
        log_debug(__FUNCTION__, " - ", n_args, " argument(s) are expected, but ", argc, " arguments are passed");
        return libData->Message("argc"), LIBRARY_FUNCTION_ERROR;
    }

    // id
    mint const id = MArgument_getInteger(args[0]);
    if (!store().count(id)) {
        log_debug(__FUNCTION__, " - No persistent object associated with id = ", id);
        return libData->Message("noitem"), LIBRARY_FUNCTION_ERROR;
    }
    auto const &pdf = store()[id];
    if (!pdf) {
        log_debug(__FUNCTION__, " - Persistent object for id = ", id, " is invalid");
        return libData->Message("noitem"), LIBRARY_FUNCTION_ERROR;
    }

    // n
    mint const n = MArgument_getInteger(args[1]);

    // z
    std::complex<double> const *z = reinterpret_cast<decltype(z)>(args[2].cmplex);

    // Z<n>
    std::complex<double> *Z_n;
    {
        constexpr mint n_dim       = 1;
        mint const     dims[n_dim] = { 2 };
        if (libData->MTensor_new(MType_Real, n_dim, dims, res.tensor)) {
            return LIBRARY_MEMORY_ERROR;
        }
        mreal *data = libData->MTensor_getRealData(*res.tensor);
        Z_n         = reinterpret_cast<decltype(Z_n)>(data);
    }

    // evaluate
    try {
        switch (n) {
            case 0:
                *Z_n = pdf->Z0(*z);
                break;
            case 1:
                *Z_n = pdf->Z1(*z);
                break;
            case 2:
                *Z_n = pdf->Z2(*z);
                break;
            default:
                return libData->Message("noswitchcase"), LIBRARY_FUNCTION_ERROR;
        }
    } catch (std::exception &e) {
        log_debug(__FUNCTION__, " - ", e.what());
        return libData->Message("exception"), LIBRARY_FUNCTION_ERROR;
    }

    return LIBRARY_NO_ERROR;
}

/**
 @brief LibraryFunction interface that evaluates W<n> associated with the passed id.
 @discussion The input type specification is:
 {id_Integer, n_Integer, z_Complex}, where
 id is the unique identifier returned from ManagedLibraryExpressionQ,
 n is either 0, 1 or 2, and
 z is a complex number.

 The output type specification is {res_Real, 1}, a pair of real and imaginary parts.
 */
EXTERN_C int llPDFIntegralW(WolframLibraryData const libData, mint const argc, MArgument *const args, MArgument const res)
{
    constexpr mint n_args = 3;
    if (n_args != argc) {
        log_debug(__FUNCTION__, " - ", n_args, " argument(s) are expected, but ", argc, " arguments are passed");
        return libData->Message("argc"), LIBRARY_FUNCTION_ERROR;
    }

    // id
    mint const id = MArgument_getInteger(args[0]);
    if (!store().count(id)) {
        log_debug(__FUNCTION__, " - No persistent object associated with id = ", id);
        return libData->Message("noitem"), LIBRARY_FUNCTION_ERROR;
    }
    auto const &pdf = store()[id];
    if (!pdf) {
        log_debug(__FUNCTION__, " - Persistent object for id = ", id, " is invalid");
        return libData->Message("noitem"), LIBRARY_FUNCTION_ERROR;
    }

    // n
    mint const n = MArgument_getInteger(args[1]);

    // z
    std::complex<double> const *z = reinterpret_cast<decltype(z)>(args[2].cmplex);

    // W<n>
    std::complex<double> *W_n;
    {
        constexpr mint n_dim       = 1;
        mint const     dims[n_dim] = { 2 };
        if (libData->MTensor_new(MType_Real, n_dim, dims, res.tensor)) {
            return LIBRARY_MEMORY_ERROR;
        }
        mreal *data = libData->MTensor_getRealData(*res.tensor);
        W_n         = reinterpret_cast<decltype(W_n)>(data);
    }

    // evaluate
    try {
        switch (n) {
            case 0:
                *W_n = pdf->W0(*z);
                break;
            case 1:
                *W_n = pdf->W1(*z);
                break;
            case 2:
                *W_n = pdf->W2(*z);
                break;
            default:
                return libData->Message("noswitchcase"), LIBRARY_FUNCTION_ERROR;
        }
    } catch (std::exception &e) {
        log_debug(__FUNCTION__, " - ", e.what());
        return libData->Message("exception"), LIBRARY_FUNCTION_ERROR;
    }

    return LIBRARY_NO_ERROR;
}

/**
 @brief LibraryFunction interface that constructs a persistent MaxwellianPDF object associated with the passed id.
 @discussion The input type specification is:
 {id_Integer}, where
 id is the unique identifier returned from ManagedLibraryExpressionQ.

 The output type specification is "Void".
 */
EXTERN_C int llMaxwellianPDF(WolframLibraryData const libData, mint const argc, MArgument *const args, MArgument const)
{
    constexpr mint n_args = 1;
    if (n_args != argc) {
        log_debug(__FUNCTION__, " - ", n_args, " argument(s) are expected, but ", argc, " arguments are passed");
        return libData->Message("argc"), LIBRARY_FUNCTION_ERROR;
    }

    // id
    mint const id = MArgument_getInteger(args[0]);

    // construct
    try {
        store()[id] = PDF::construct<MaxwellianPDF>();
    } catch (std::exception &e) {
        log_debug(__FUNCTION__, " - ", e.what());
        return libData->Message("exception"), LIBRARY_FUNCTION_ERROR;
    }

    return LIBRARY_NO_ERROR;
}

/**
 @brief LibraryFunction interface that constructs a persistent LorentzianPDF object associated with the passed id.
 @discussion The input type specification is:
 {id_Integer, kappa_Integer}, where
 id is the unique identifier returned from ManagedLibraryExpressionQ, and
 kappa is an integer power index.

 The output type specification is "Void".
 */
EXTERN_C int llIntegerKappaPDF(WolframLibraryData const libData, mint const argc, MArgument *const args, MArgument const)
{
    constexpr mint n_args = 2;
    if (n_args != argc) {
        log_debug(__FUNCTION__, " - ", n_args, " argument(s) are expected, but ", argc, " arguments are passed");
        return libData->Message("argc"), LIBRARY_FUNCTION_ERROR;
    }

    // id
    mint const id = MArgument_getInteger(args[0]);

    // kappa
    mint const kappa = MArgument_getInteger(args[1]);

    // construct
    try {
        store()[id] = PDF::construct<IntegerKappaPDF>(static_cast<int>(kappa));
    } catch (std::exception &e) {
        log_debug(__FUNCTION__, " - ", e.what());
        return libData->Message("exception"), LIBRARY_FUNCTION_ERROR;
    }

    return LIBRARY_NO_ERROR;
}

/**
 @brief LibraryFunction interface that constructs a persistent LorentzianPDF object with associated with the passed id.
 @discussion The input type specification is:
 {id_Integer, kappa_Real}, where
 id is the unique identifier returned from ManagedLibraryExpressionQ, and
 kappa is a real power index.

 The output type specification is "Void".
 */
EXTERN_C int llRealKappaPDF(WolframLibraryData const libData, mint const argc, MArgument *const args, MArgument const)
{
    constexpr mint n_args = 2;
    if (n_args != argc) {
        log_debug(__FUNCTION__, " - ", n_args, " argument(s) are expected, but ", argc, " arguments are passed");
        return libData->Message("argc"), LIBRARY_FUNCTION_ERROR;
    }

    // id
    mint const id = MArgument_getInteger(args[0]);

    // kappa
    mreal const kappa = MArgument_getReal(args[1]);

    // construct
    try {
        store()[id] = PDF::construct<SplineKappaPDF>(kappa);
    } catch (std::exception &e) {
        log_debug(__FUNCTION__, " - ", e.what());
        return libData->Message("exception"), LIBRARY_FUNCTION_ERROR;
    }

    return LIBRARY_NO_ERROR;
}

/**
 @brief LibraryFunction interface that constructs a persistent SplinePDF object associated with the passed id.
 @discussion The input type specification is:
 {id_Integer, ac_"Boolean", {xs_Real, 1}, {ys_Real, 1}}, where
 id is the unique identifier returned from ManagedLibraryExpressionQ,
 ac is a flag for analytic continuation,
 xs is a rank-1 tensor of abscissa values with Length[xs] >= 4, and
 ys is a rank-1 tensor of ordinate values with Length[ys] == Length[xs].

 The output type specification is "Void".
 */
EXTERN_C int llSplinePDF(WolframLibraryData const libData, mint const argc, MArgument *const args, MArgument const)
{
    constexpr mint n_args = 4;
    if (n_args != argc) {
        log_debug(__FUNCTION__, " - ", n_args, " argument(s) are expected, but ", argc, " arguments are passed");
        return libData->Message("argc"), LIBRARY_FUNCTION_ERROR;
    }

    // id
    mint const id = MArgument_getInteger(args[0]);

    // ac
    mbool const ac = MArgument_getBoolean(args[1]);

    // xs
    Utility::PaddedArray<double, 0> xs;
    {
        long const idx = 2;
        if (MType_Real != libData->MTensor_getType(MArgument_getMTensor(args[idx]))) {
            return LIBRARY_TYPE_ERROR;
        }
        if (1 != libData->MTensor_getRank(MArgument_getMTensor(args[idx]))) {
            return LIBRARY_RANK_ERROR;
        }
        mint const size = *libData->MTensor_getDimensions(MArgument_getMTensor(args[idx]));
        if (size < 4) {
            return LIBRARY_DIMENSION_ERROR;
        }
        mreal *data = libData->MTensor_getRealData(MArgument_getMTensor(args[idx]));
        xs          = decltype(xs)(static_cast<unsigned long>(size), data);
    }

    // ys
    Utility::PaddedArray<double, 0> ys;
    {
        long const idx = 3;
        if (MType_Real != libData->MTensor_getType(MArgument_getMTensor(args[idx]))) {
            return LIBRARY_TYPE_ERROR;
        }
        if (1 != libData->MTensor_getRank(MArgument_getMTensor(args[idx]))) {
            return LIBRARY_RANK_ERROR;
        }
        mint const size = *libData->MTensor_getDimensions(MArgument_getMTensor(args[idx]));
        if (size < 4) {
            return LIBRARY_DIMENSION_ERROR;
        }
        mreal *data = libData->MTensor_getRealData(MArgument_getMTensor(args[idx]));
        ys          = decltype(ys)(static_cast<unsigned long>(size), data);
    }

    if (xs.size() != ys.size()) {
        return LIBRARY_DIMENSION_ERROR;
    }

    // construct
    try {
        store()[id] = PDF::construct<SplinePDF>(Utility::CubicSplineCoefficient<double>(xs.begin(), xs.end(), ys.begin()), ac);
    } catch (std::exception &e) {
        log_debug(__FUNCTION__, " - ", e.what());
        return libData->Message("exception"), LIBRARY_FUNCTION_ERROR;
    }

    return LIBRARY_NO_ERROR;
}
