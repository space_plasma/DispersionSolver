/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <iostream>
#include <type_traits>

// NOTE: This header should be imported after all c++ headers.
//
//#include <wstp/wstp.h>
#include "WolframLibrary.h"

// debug print
//
namespace {
template <class... Args>
inline void no_op(Args &&...) noexcept
{
}
template <class... Args>
inline void log_debug(Args &&...args)
{
    //#if defined(DEBUG)
    printo(std::cerr, "llDispersionKit::debug : ", std::forward<Args>(args)...) << std::endl;
    //#endif
}
template <class... Args>
inline void log_info(Args &&...args)
{
#if defined(DEBUG)
    printo(std::cout, "llDispersionKit::info : ", std::forward<Args>(args)...) << std::endl;
#else
    no_op(args...);
#endif
}
} // namespace
