(* ::Package:: *)

(*
 * Copyright (c) 2017-2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 *)


BeginPackage["DispersionLink`"];


(* :Code Section (Call Unprotect and ClearAll): *)


Unprotect[
DLFLRReleaseAll,DLFLRQ,DLFLRClearCache,
DLFLRIntegralA,DLFLRIntegralP,DLFLRIntegralB,DLFLRIntegralQ,DLFLRIntegralC,DLFLRIntegralR,
DLFLRIntegralAOa2,DLFLRIntegralPOa2,DLFLRIntegralBOa,DLFLRIntegralQOa,
DLMaxwellianFLR,DLRingFLR,DLLorentzianFLR,DLSplineFLR
]
ClearAll[DLFLRReleaseAll,DLFLRQ,DLFLRClearCache,
DLFLRIntegralA,DLFLRIntegralP,DLFLRIntegralB,DLFLRIntegralQ,DLFLRIntegralC,DLFLRIntegralR,
DLFLRIntegralAOa2,DLFLRIntegralPOa2,DLFLRIntegralBOa,DLFLRIntegralQOa,
DLMaxwellianFLR,DLRingFLR,DLLorentzianFLR,DLSplineFLR
]


(* :Usage Messages: *)


DLFLRReleaseAll::usage="DLFLRReleaseAll[] releases all managed objects."


DLFLRQ::usage="DLFLRQ[e] tests whether e is a valid FLR identifier expression."


DLFLRClearCache::usage="DLFLRClearCache[e] clears cached results of the managed library expression, e."


DLFLRIntegralA::usage="DLFLRIntegralA[e,n,a] evaluates \!\(\*FormBox[\(2  \[Pi] \(\*SubsuperscriptBox[\(\[Integral]\), \(0\), \(\[Infinity]\)]\*SuperscriptBox[\(\(\*SubscriptBox[\(J\), \(n\)]\)[a\\\ x]\), \(2\)] f[x] x \[DifferentialD]x\)\),
TraditionalForm]\) for f[x] associated with the managed library expression, e.
It automatically threads over lists."


DLFLRIntegralP::usage="DLFLRIntegralP[e,n,a] evaluates \!\(\*FormBox[\(2  \[Pi] \(\*SubsuperscriptBox[\(\[Integral]\), \(0\), \(\[Infinity]\)]\*SuperscriptBox[\(\(\*SubscriptBox[\(J\), \(n\)]\)[a\\\ x]\), \(2\)] \(f'\)[x] \[DifferentialD]x\)\),
TraditionalForm]\) for f[x] associated with the managed library expression, e.
It automatically threads over lists."


DLFLRIntegralB::usage="DLFLRIntegralB[e,n,a] evaluates \!\(\*FormBox[\(2  \[Pi] \(\*SubsuperscriptBox[\(\[Integral]\), \(0\), \(\[Infinity]\)]\(\*SubscriptBox[\(J\), \(n\)]\)[a\\\ x] \(\*SubscriptBox[\(J\), \(n\)]'\)[a\\\ x] f[x] \*SuperscriptBox[\(x\), \(2\)] \[DifferentialD]x\)\),
TraditionalForm]\) for f[x] associated with the managed library expression, e.
It automatically threads over lists."


DLFLRIntegralQ::usage="DLFLRIntegralQ[e,n,a] evaluates \!\(\*FormBox[\(2  \[Pi] \(\*SubsuperscriptBox[\(\[Integral]\), \(0\), \(\[Infinity]\)]\(\*SubscriptBox[\(J\), \(n\)]\)[a\\\ x] \(\*SubscriptBox[\(J\), \(n\)]'\)[a\\\ x] \(f'\)[x] x \[DifferentialD]x\)\),
TraditionalForm]\) for f[x] associated with the managed library expression, e.
It automatically threads over lists."


DLFLRIntegralC::usage="DLFLRIntegralC[e,n,a] evaluates \!\(\*FormBox[\(2  \[Pi] \(\*SubsuperscriptBox[\(\[Integral]\), \(0\), \(\[Infinity]\)]\*SuperscriptBox[\((\(\*SubscriptBox[\(J\), \(n\)]'\)[a\\\ x])\), \(2\)] f[x] \*SuperscriptBox[\(x\), \(3\)] \[DifferentialD]x\)\),
TraditionalForm]\) for f[x] associated with the managed library expression, e.
It automatically threads over lists."


DLFLRIntegralR::usage="DLFLRIntegralR[e,n,a] evaluates \!\(\*FormBox[\(2  \[Pi] \(\*SubsuperscriptBox[\(\[Integral]\), \(0\), \(\[Infinity]\)]\*SuperscriptBox[\((\(\*SubscriptBox[\(J\), \(n\)]'\)[a\\\ x])\), \(2\)] \(f'\)[x] \*SuperscriptBox[\(x\), \(2\)] \[DifferentialD]x\)\),
TraditionalForm]\) for f[x] associated with the managed library expression, e.
It automatically threads over lists."


DLFLRIntegralAOa2::usage="DLFLRIntegralAOa2[e,n,a] evaluates DLFLRIntegralA[e,n,a]/\!\(\*SuperscriptBox[\(a\), \(2\)]\) valid for a=0.
It automatically threads over lists."


DLFLRIntegralPOa2::usage="DLFLRIntegralPOa2[e,n,a] evaluates DLFLRIntegralP[e,n,a]/\!\(\*SuperscriptBox[\(a\), \(2\)]\) valid for a=0.
It automatically threads over lists."


DLFLRIntegralBOa::usage="DLFLRIntegralBOa[e,n,a] evaluates DLFLRIntegralB[e,n,a]/a valid for a=0.
It automatically threads over lists."


DLFLRIntegralQOa::usage="DLFLRIntegralQOa[e,n,a] evaluates DLFLRIntegralQ[e,n,a]/a valid for a=0.
It automatically threads over lists."


DLMaxwellianFLR::usage="DLMaxwellianFLR[] creates a managed object of Maxwellian FLR, f[x]=\!\(\*SuperscriptBox[\(\[ExponentialE]\), \(-\*SuperscriptBox[\(x\), \(2\)]\)]\)/\[Pi], and returns an identifier expression.
DLMaxwellianFLR[\[Theta]] creates a managed object of Maxwellian PDF, f[v]=\!\(\*FractionBox[\(Exp[\(-\*SuperscriptBox[\(v\), \(2\)]\)/\*SuperscriptBox[\(\[Theta]\), \(2\)]]\), \(\[Pi]\\\ \*SuperscriptBox[\(\[Theta]\), \(2\)]\)]\) with \[Theta]>0."


DLRingFLR::usage="DLRingFLR[b] creates a managed object of ring FLR, f[x]=\!\(\*FractionBox[\(Exp[\(-\*SuperscriptBox[\((x - b)\), \(2\)]\)]\), \(\[Pi]\\\ C[b]\)]\) with C[x]=\!\(\*SuperscriptBox[\(\[ExponentialE]\), \(-\*SuperscriptBox[\(x\), \(2\)]\)]\)+\!\(\*SqrtBox[\(\[Pi]\)]\)x Erfc[-x] and b\[Element]Reals, and returns an identifier expression.
DLRingFLR[vr,\[Theta]] creates a managed object of ring FLR, f[v]=\!\(\*FractionBox[\(Exp[\(-\*SuperscriptBox[\((v - vr)\), \(2\)]\)/\*SuperscriptBox[\(\[Theta]\), \(2\)]]\), \(\[Pi]\\\ \*SuperscriptBox[\(\[Theta]\), \(2\)] C[vr/\[Theta]]\)]\) with \[Theta]>0 and vr\[Element]Reals.
If \[Theta]=0, f[v] becomes a cold ring, f[v]=\!\(\*FractionBox[\(\[Delta][v - vr]\), \(2  \[Pi]\\\ vr\)]\), and the cold ring version is used. In this case, vr cannot be negative."


DLLorentzianFLR::usage="DLLorentzianFLR[\[Kappa]] creates a managed object of kappa FLR, f[x]=(1+\!\(\*SuperscriptBox[\(x\), \(2\)]\)/\[Kappa]\!\(\*SuperscriptBox[\()\), \(-\((\[Kappa] + 1)\)\)]\)/\[Pi] where \[Kappa]>1, and returns an identifier expression.
DLLorentzianFLR[\[Kappa],\[Theta]] creates a managed object of kappa FLR, f[v]=\!\(\*FractionBox[SuperscriptBox[\((1 + \*SuperscriptBox[\(v\), \(2\)]/\*SuperscriptBox[\(\[Theta]\), \(2\)] \[Kappa])\), \(-\((\[Kappa] + 1)\)\)], \(\[Pi]\\\ \*SuperscriptBox[\(\[Theta]\), \(2\)]\)]\) with \[Theta]>0."


DLSplineFLR::usage="DLSplineFLR[xs,ys] creates a managed object of cubic spline FLR, where f[x] is approximated by a cubic spline interpolation, and returns an identifier expression.
xs and ys are a list of real abscissa and ordinate values such that ys=f[xs]; Length[xs]>=4.
Min[xs] can be negative, but Max[xs] should be positive."


(* :Error Messages: *)


SetAttributes[#,Listable]&/@{
DLFLRIntegralA,DLFLRIntegralP,DLFLRIntegralB,DLFLRIntegralQ,DLFLRIntegralC,DLFLRIntegralR,
DLFLRIntegralAOa2,DLFLRIntegralPOa2,DLFLRIntegralBOa,DLFLRIntegralQOa
}


DLRingFLR::negvr="vr cannot be negative when Chop[\[Theta]]=0."


DLSplineFLR::argx="`1`"


(* :Begin Private: *)


Begin["`Private`"];


libPath=FileNameJoin[{FileNameDrop[FindFile["DispersionLink`"],-1](*,"LibraryResources",$SystemID*)}]


(* llFLRLibraryExpressionMangerName *)


Switch[LibraryFunctionInformation[llFLRLibraryExpressionMangerName],
{__},LibraryFunctionUnload[llFLRLibraryExpressionMangerName]
]


llFLRLibraryExpressionMangerName=Block[{$LibraryPath=libPath},
LibraryFunctionLoad["DispersionLink","llFLRLibraryExpressionMangerName",{},"UTF8String"]
]


DLFLRQ[e_]:=ManagedLibraryExpressionQ[e,llFLRLibraryExpressionMangerName[]]


(* DLFLRReleaseAll *)


Switch[LibraryFunctionInformation[llFLRReleaseAll],
{__},LibraryFunctionUnload[llFLRReleaseAll]
]


llFLRReleaseAll=Block[{$LibraryPath=libPath},
LibraryFunctionLoad["DispersionLink","llFLRReleaseAll",{},"Void"]
]


DLFLRReleaseAll[]:=llPDFReleaseAll[]


(* DLFLRClearCache *)


Switch[LibraryFunctionInformation[llFLRClearCache],
{__},LibraryFunctionUnload[llFLRClearCache]
]


llFLRClearCache=Block[{$LibraryPath=libPath},
LibraryFunctionLoad["DispersionLink","llFLRClearCache",{Integer},"Void"]
]


DLFLRClearCache[e_?DLFLRQ]:=llFLRClearCache[ManagedLibraryExpressionID[e,llFLRLibraryExpressionMangerName[]]]


(* llFLREvaluateOne *)
Switch[LibraryFunctionInformation[llFLREvaluateOne],
{__},LibraryFunctionUnload[llFLREvaluateOne]
]


llFLREvaluateOne=Block[{$LibraryPath=libPath},
LibraryFunctionLoad["DispersionLink","llFLREvaluateOne",{(*id*)Integer,(*n*)Integer,(*a*)Real,(*selector*)Integer},Real]
]


DLFLRIntegralA[e:(llFLRLibraryExpression[_]?DLFLRQ),n_Integer,a_Real]:=llFLREvaluateOne[ManagedLibraryExpressionID[e,llFLRLibraryExpressionMangerName[]],n,a,1]
DLFLRIntegralP[e:(llFLRLibraryExpression[_]?DLFLRQ),n_Integer,a_Real]:=llFLREvaluateOne[ManagedLibraryExpressionID[e,llFLRLibraryExpressionMangerName[]],n,a,2]
DLFLRIntegralB[e:(llFLRLibraryExpression[_]?DLFLRQ),n_Integer,a_Real]:=llFLREvaluateOne[ManagedLibraryExpressionID[e,llFLRLibraryExpressionMangerName[]],n,a,3]
DLFLRIntegralQ[e:(llFLRLibraryExpression[_]?DLFLRQ),n_Integer,a_Real]:=llFLREvaluateOne[ManagedLibraryExpressionID[e,llFLRLibraryExpressionMangerName[]],n,a,4]
DLFLRIntegralC[e:(llFLRLibraryExpression[_]?DLFLRQ),n_Integer,a_Real]:=llFLREvaluateOne[ManagedLibraryExpressionID[e,llFLRLibraryExpressionMangerName[]],n,a,5]
DLFLRIntegralR[e:(llFLRLibraryExpression[_]?DLFLRQ),n_Integer,a_Real]:=llFLREvaluateOne[ManagedLibraryExpressionID[e,llFLRLibraryExpressionMangerName[]],n,a,6]
DLFLRIntegralAOa2[e:(llFLRLibraryExpression[_]?DLFLRQ),n_Integer,a_Real]:=llFLREvaluateOne[ManagedLibraryExpressionID[e,llFLRLibraryExpressionMangerName[]],n,a,7]
DLFLRIntegralPOa2[e:(llFLRLibraryExpression[_]?DLFLRQ),n_Integer,a_Real]:=llFLREvaluateOne[ManagedLibraryExpressionID[e,llFLRLibraryExpressionMangerName[]],n,a,8]
DLFLRIntegralBOa[e:(llFLRLibraryExpression[_]?DLFLRQ),n_Integer,a_Real]:=llFLREvaluateOne[ManagedLibraryExpressionID[e,llFLRLibraryExpressionMangerName[]],n,a,9]
DLFLRIntegralQOa[e:(llFLRLibraryExpression[_]?DLFLRQ),n_Integer,a_Real]:=llFLREvaluateOne[ManagedLibraryExpressionID[e,llFLRLibraryExpressionMangerName[]],n,a,10]


DLFLRIntegralA[e:(llFLRLibraryExpression[_,\[Theta]_Real?NonNegative]?DLFLRQ),n_Integer,a_Real]:=llFLREvaluateOne[ManagedLibraryExpressionID[e,llFLRLibraryExpressionMangerName[]],n,a \[Theta],1]
DLFLRIntegralP[e:(llFLRLibraryExpression[_,\[Theta]_Real?NonNegative]?DLFLRQ),n_Integer,a_Real]:=llFLREvaluateOne[ManagedLibraryExpressionID[e,llFLRLibraryExpressionMangerName[]],n,a \[Theta],2]/\[Theta]^2
DLFLRIntegralB[e:(llFLRLibraryExpression[_,\[Theta]_Real?NonNegative]?DLFLRQ),n_Integer,a_Real]:=llFLREvaluateOne[ManagedLibraryExpressionID[e,llFLRLibraryExpressionMangerName[]],n,a \[Theta],3]\[Theta]
DLFLRIntegralQ[e:(llFLRLibraryExpression[_,\[Theta]_Real?NonNegative]?DLFLRQ),n_Integer,a_Real]:=llFLREvaluateOne[ManagedLibraryExpressionID[e,llFLRLibraryExpressionMangerName[]],n,a \[Theta],4]/\[Theta]
DLFLRIntegralC[e:(llFLRLibraryExpression[_,\[Theta]_Real?NonNegative]?DLFLRQ),n_Integer,a_Real]:=llFLREvaluateOne[ManagedLibraryExpressionID[e,llFLRLibraryExpressionMangerName[]],n,a \[Theta],5]\[Theta]^2
DLFLRIntegralR[e:(llFLRLibraryExpression[_,\[Theta]_Real?NonNegative]?DLFLRQ),n_Integer,a_Real]:=llFLREvaluateOne[ManagedLibraryExpressionID[e,llFLRLibraryExpressionMangerName[]],n,a \[Theta],6]
DLFLRIntegralAOa2[e:(llFLRLibraryExpression[_,\[Theta]_Real?NonNegative]?DLFLRQ),n_Integer,a_Real]:=llFLREvaluateOne[ManagedLibraryExpressionID[e,llFLRLibraryExpressionMangerName[]],n,a \[Theta],7]\[Theta]^2
DLFLRIntegralPOa2[e:(llFLRLibraryExpression[_,\[Theta]_Real?NonNegative]?DLFLRQ),n_Integer,a_Real]:=llFLREvaluateOne[ManagedLibraryExpressionID[e,llFLRLibraryExpressionMangerName[]],n,a \[Theta],8]
DLFLRIntegralBOa[e:(llFLRLibraryExpression[_,\[Theta]_Real?NonNegative]?DLFLRQ),n_Integer,a_Real]:=llFLREvaluateOne[ManagedLibraryExpressionID[e,llFLRLibraryExpressionMangerName[]],n,a \[Theta],9]\[Theta]^2
DLFLRIntegralQOa[e:(llFLRLibraryExpression[_,\[Theta]_Real?NonNegative]?DLFLRQ),n_Integer,a_Real]:=llFLREvaluateOne[ManagedLibraryExpressionID[e,llFLRLibraryExpressionMangerName[]],n,a \[Theta],10]


(* DLMaxwellianFLR *)


Switch[LibraryFunctionInformation[llMaxwellianFLR],
{__},LibraryFunctionUnload[llMaxwellianFLR]
]


llMaxwellianFLR=Block[{$LibraryPath=libPath},
LibraryFunctionLoad["DispersionLink","llMaxwellianFLR",{Integer},"Void"]
]


DLMaxwellianFLR[]:=Module[{e},
e=CreateManagedLibraryExpression[llFLRLibraryExpressionMangerName[],llFLRLibraryExpression];
llMaxwellianFLR[ManagedLibraryExpressionID[e,llFLRLibraryExpressionMangerName[]]];
e
]
DLMaxwellianFLR[\[Theta]_?Positive]:=Module[{e},
e=CreateManagedLibraryExpression[llFLRLibraryExpressionMangerName[],llFLRLibraryExpression[#,N[\[Theta]]]&];
llMaxwellianFLR[ManagedLibraryExpressionID[e,llFLRLibraryExpressionMangerName[]]];
e
]


(* DLRingFLR *)


Switch[LibraryFunctionInformation[llRingFLR],
{__},LibraryFunctionUnload[llRingFLR]
]
Switch[LibraryFunctionInformation[llColdRingFLR],
{__},LibraryFunctionUnload[llColdRingFLR]
]


llRingFLR=Block[{$LibraryPath=libPath},
LibraryFunctionLoad["DispersionLink","llRingFLR",{Integer,Real},"Void"]
]
llColdRingFLR=Block[{$LibraryPath=libPath},
LibraryFunctionLoad["DispersionLink","llColdRingFLR",{Integer,Real},"Void"]
]


DLRingFLR[b_Real]:=Module[{e},
e=CreateManagedLibraryExpression[llFLRLibraryExpressionMangerName[],llFLRLibraryExpression];
llRingFLR[ManagedLibraryExpressionID[e,llFLRLibraryExpressionMangerName[]],b];
e
]
DLRingFLR[vr_Real,\[Theta]_?NumberQ/;0==Chop[\[Theta]]]/;And[(*Cold ring*)
If[Negative[Chop[vr]],(Message[DLRingFLR::negvr];Abort[]),True]
]:=Module[{e},
e=CreateManagedLibraryExpression[llFLRLibraryExpressionMangerName[],llFLRLibraryExpression];
llColdRingFLR[ManagedLibraryExpressionID[e,llFLRLibraryExpressionMangerName[]],vr];
e
]
DLRingFLR[vr_Real,\[Theta]_?Positive]:=Module[{e},(*Thermal ring*)
e=CreateManagedLibraryExpression[llFLRLibraryExpressionMangerName[],llFLRLibraryExpression[#,N[\[Theta]]]&];
llRingFLR[ManagedLibraryExpressionID[e,llFLRLibraryExpressionMangerName[]],vr/\[Theta]];
e
]


(* DLLorentzianFLR *)


Switch[LibraryFunctionInformation[llLorentzianFLR],
{__},LibraryFunctionUnload[llLorentzianFLR]
]


llLorentzianFLR=Block[{$LibraryPath=libPath},
LibraryFunctionLoad["DispersionLink","llLorentzianFLR",{Integer,Real},"Void"]
]


DLLorentzianFLR[kappa_?NumberQ/;kappa>1]:=Module[{e},
e=CreateManagedLibraryExpression[llFLRLibraryExpressionMangerName[],llFLRLibraryExpression];
llLorentzianFLR[ManagedLibraryExpressionID[e,llFLRLibraryExpressionMangerName[]],kappa];
e
]
DLLorentzianFLR[kappa_?NumberQ/;kappa>1,\[Theta]_?Positive]:=Module[{e},
e=CreateManagedLibraryExpression[llFLRLibraryExpressionMangerName[],llFLRLibraryExpression[#,N[\[Theta]]]&];
llLorentzianFLR[ManagedLibraryExpressionID[e,llFLRLibraryExpressionMangerName[]],kappa];
e
]


(* DLSplineFLR *)


Switch[LibraryFunctionInformation[llSplineFLR],
{__},LibraryFunctionUnload[llSplineFLR]
]


llSplineFLR=Block[{$LibraryPath=libPath},
LibraryFunctionLoad["DispersionLink","llSplineFLR",{(*id*)Integer,{(*xs*)Real,1},{(*ys*)Real,1}},"Void"]
]


DLSplineFLR[xs:{__Real}/;Length[xs]>=4,ys:{__Real}]/;And[
If[Length[ys]!=Length[xs],(Message[DLSplineFLR::argx,"The length of ys is not the same as the length of xs."];Abort[]),True],
If[Not[Equal@@Append[Chop[#-Median[#]&@Differences[xs]],0]],(Message[DLSplineFLR::argx,"xs is not regularly spaced."];Abort[]),
True],
If[Last[xs]-First[xs]<=0,(Message[DLSplineFLR::argx,"xs is not in ascending order."];Abort[]),True],
If[Last[xs]<=0,(Message[DLSplineFLR::argx,"Max[xs] is not positive."];Abort[]),True]
]:=Module[{e},
e=CreateManagedLibraryExpression[llFLRLibraryExpressionMangerName[],llFLRLibraryExpression];
llSplineFLR[ManagedLibraryExpressionID[e,llFLRLibraryExpressionMangerName[]],xs,N[Chop[ys]]];
e
]


End[];


(* :End Private: *)


(* :Code Section (Call Protect): *)


Protect[DLFLRReleaseAll,DLFLRQ,DLFLRClearCache,
DLFLRIntegralA,DLFLRIntegralP,DLFLRIntegralB,DLFLRIntegralQ,DLFLRIntegralC,DLFLRIntegralR,
DLFLRIntegralAOa2,DLFLRIntegralPOa2,DLFLRIntegralBOa,DLFLRIntegralQOa,
DLMaxwellianFLR,DLRingFLR,DLLorentzianFLR,DLSplineFLR
]


EndPackage[];
