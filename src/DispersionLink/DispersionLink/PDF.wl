(* ::Package:: *)

(*
 * Copyright (c) 2017-2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 *)


BeginPackage["DispersionLink`"];


(* :Code Section (Call Unprotect and ClearAll): *)


Unprotect[DLPDFReleaseAll,DLPDFQ,DLPDFIntegralZ,DLPDFIntegralW,DLMaxwellianPDF,DLLorentzianPDF,DLSplinePDF]
ClearAll[DLPDFReleaseAll,DLPDFQ,DLPDFIntegralZ,DLPDFIntegralW,DLMaxwellianPDF,DLLorentzianPDF,DLSplinePDF]


(* :Usage Messages: *)


DLPDFReleaseAll::usage="DLPDFReleaseAll[] releases all managed objects."


DLPDFQ::usage="DLPDFQ[e] tests whether e is a valid PDF identifier expression."


DLPDFIntegralZ::usage="DLPDFIntegralZ[e,n,\[Zeta]] evaluates \!\(\*SubsuperscriptBox[\(\[Integral]\), \(-\[Infinity]\), \(\[Infinity]\)]\)\!\(\*FractionBox[\(\*SuperscriptBox[\(x\), \(n\)] f[x]\), \(x - \[Zeta]\)]\)\[DifferentialD]x, where e is a valid identifier expression (i.e., DLPDFQ[e]==True), \[Zeta] is a complex-convertible number, and n is either 0, 1 or 2.
DLPDFIntegralZ[e,All,\[Zeta]] is equivalent to DLPDFIntegralZ[e,\[Zeta],{0,1,2}]. It can be much faster for the spline-based PDFs.
It automatically threads over lists."


DLPDFIntegralW::usage="DLPDFIntegralW[e,n,\[Zeta]] evaluates \!\(\*SubsuperscriptBox[\(\[Integral]\), \(-\[Infinity]\), \(\[Infinity]\)]\)\!\(\*FractionBox[\(\*SuperscriptBox[\(x\), \(n\)] \*SubscriptBox[\(\[PartialD]\), \(x\)]f[x]\), \(x - \[Zeta]\)]\)\[DifferentialD]x, where e is a valid identifier expression (i.e., DLPDFQ[e]==True), \[Zeta] is a complex-convertible number, and n is either 0, 1 or 2.
DLPDFIntegralW[e,All,\[Zeta]] is equivalent to DLPDFIntegralW[e,\[Zeta],{0,1,2}]. It can be much faster for the spline-based PDFs.
It automatically threads over lists."


DLMaxwellianPDF::usage="DLMaxwellianPDF[] creates a managed object of Maxwellian PDF, f[x]=\!\(\*SuperscriptBox[\(\[ExponentialE]\), \(-\*SuperscriptBox[\(x\), \(2\)]\)]\)/\!\(\*SqrtBox[\(\[Pi]\)]\), and returns an identifier expression.
DLMaxwellianPDF[\[Theta]] creates a managed object of Maxwellian PDF, f[v]=\!\(\*FractionBox[\(Exp[\(-\*SuperscriptBox[\(v\), \(2\)]\)/\*SuperscriptBox[\(\[Theta]\), \(2\)]]\), \(\*SqrtBox[\(\[Pi]\)] \[Theta]\)]\) with \[Theta]>0.
DLMaxwellianPDF[\[Theta],vd] creates a managed object of Maxwellian PDF, f[v]=\!\(\*FractionBox[\(Exp[\(-\*SuperscriptBox[\((v - vd)\), \(2\)]\)/\*SuperscriptBox[\(\[Theta]\), \(2\)]]\), \(\*SqrtBox[\(\[Pi]\)] \[Theta]\)]\) with vd\[Element]Reals."


DLLorentzianPDF::usage="DLLorentzianPDF[\[Kappa]] creates a managed object of Lorentzian PDF, f[x]=\!\(\*FractionBox[\(C[\[Kappa]]\), SqrtBox[\(\[Pi]\)]]\)\!\(\*FractionBox[\(1\), SuperscriptBox[\((1 + \*SuperscriptBox[\(x\), \(2\)]/\[Kappa])\), \(\[Kappa] + 1\)]]\) with \[Kappa]>1/2 and C[x]=\!\(\*FractionBox[\(\*SqrtBox[\(x\)] \[CapitalGamma][x]\), \(\[CapitalGamma][x + 1/2]\)]\), and returns an identifier expression.
DLLorentzianPDF[\[Kappa],\[Theta]] creates a managed object of Lorentzian PDF, f[v]=\!\(\*FractionBox[\(C[\[Kappa]]\), \(\*SqrtBox[\(\[Pi]\)] \[Theta]\)]\)\!\(\*FractionBox[\(1\), SuperscriptBox[\((1 + \*SuperscriptBox[\(v\), \(2\)]/\*SuperscriptBox[\(\[Theta]\), \(2\)] \[Kappa])\), \(\[Kappa] + 1\)]]\) with \[Theta]>0.
DLLorentzianPDF[\[Kappa],\[Theta],vd] creates a managed object of Lorentzian PDF, f[v]=\!\(\*FractionBox[\(C[\[Kappa]]\), \(\*SqrtBox[\(\[Pi]\)] \[Theta]\)]\)\!\(\*FractionBox[\(1\), SuperscriptBox[\((1 + \*SuperscriptBox[\((v - vd)\), \(2\)]/\*SuperscriptBox[\(\[Theta]\), \(2\)] \[Kappa])\), \(\[Kappa] + 1\)]]\) with vd\[Element]Reals.
If \[Kappa] is an integer (real), then the integer (real) version is initialized."


DLSplinePDF::usage="DLSplinePDF[xs,ys,ac] creates a managed object of cubic spline PDF, where f[x] is approximated by a cubic spline interpolation, and returns an identifier expression.
xs and ys are a list of real abscissa and ordinate values such that ys=f[xs]; Length[xs]>=4.
If ac is True, the results of {Z,W}Triplets are analytically continuous across the imaginary axis."


(* :Error Messages: *)


SetAttributes[#,Listable]&/@{DLPDFIntegralZ,DLPDFIntegralW}


DLSplinePDF::argx="`1`"


(* :Begin Private: *)


Begin["`Private`"];


libPath=FileNameJoin[{FileNameDrop[FindFile["DispersionLink`"],-2](*,"LibraryResources",$SystemID*)}]


(* llPDFLibraryExpressionManagerName *)


Switch[LibraryFunctionInformation[llPDFLibraryExpressionManagerName],
{__},LibraryFunctionUnload[llPDFLibraryExpressionManagerName]
]


llPDFLibraryExpressionManagerName=Block[{$LibraryPath=libPath},
LibraryFunctionLoad["DispersionLink","llPDFLibraryExpressionManagerName",{},"UTF8String"]
]


DLPDFQ[e_]:=ManagedLibraryExpressionQ[e,llPDFLibraryExpressionManagerName[]]


(* DLPDFReleaseAll *)


Switch[LibraryFunctionInformation[llPDFReleaseAll],
{__},LibraryFunctionUnload[llPDFReleaseAll]
]


llPDFReleaseAll=Block[{$LibraryPath=libPath},
LibraryFunctionLoad["DispersionLink","llPDFReleaseAll",{},"Void"]
]


DLPDFReleaseAll[]:=llPDFReleaseAll[]


(* DLPDFIntegralZ *)


Switch[LibraryFunctionInformation[llPDFZtriplet],
{__},LibraryFunctionUnload[llPDFZtriplet]
]
Switch[LibraryFunctionInformation[llPDFIntegralZ],
{__},LibraryFunctionUnload[llPDFIntegralZ]
]


llPDFZtriplet=Block[{$LibraryPath=libPath},
LibraryFunctionLoad["DispersionLink","llPDFZtriplet",{Integer,Complex},{Real,2}]
]
llPDFIntegralZ=Block[{$LibraryPath=libPath},
LibraryFunctionLoad["DispersionLink","llPDFIntegralZ",{(*id*)Integer,(*n*)Integer,(*z*)Complex},{Real,1}]
]


DLPDFIntegralZ[e:(llPDFLibraryExpression[_]?DLPDFQ),n:0|1|2,\[Zeta]_?NumberQ]:=With[{},
#1+I #2&@@llPDFIntegralZ[ManagedLibraryExpressionID[e,llPDFLibraryExpressionManagerName[]],n,\[Zeta]]
]
DLPDFIntegralZ[e:(llPDFLibraryExpression[id_,\[Theta]_Real?Positive]?DLPDFQ),n:0|1|2,\[Zeta]_?NumberQ]:=With[{
\[ScriptCapitalZ]=#+I #2&@@llPDFIntegralZ[ManagedLibraryExpressionID[e,llPDFLibraryExpressionManagerName[]],n,\[Zeta]/\[Theta]]
},
Power[\[Theta],n-1]\[ScriptCapitalZ]
]
DLPDFIntegralZ[e:(llPDFLibraryExpression[id_,\[Theta]_Real?Positive,vd_Real]?DLPDFQ),0,\[Zeta]_?NumberQ]:=With[{
\[ScriptCapitalZ]0=#+I #2&@@llPDFIntegralZ[ManagedLibraryExpressionID[e,llPDFLibraryExpressionManagerName[]],0,(\[Zeta]-vd)/\[Theta]]
},
\[ScriptCapitalZ]0/\[Theta]
]
DLPDFIntegralZ[e:(llPDFLibraryExpression[id_,\[Theta]_Real?Positive,vd_Real]?DLPDFQ),1,\[Zeta]_?NumberQ]:=With[{
\[ScriptCapitalZ]0=#+I #2&@@llPDFIntegralZ[ManagedLibraryExpressionID[e,llPDFLibraryExpressionManagerName[]],0,(\[Zeta]-vd)/\[Theta]],
\[ScriptCapitalZ]1=#+I #2&@@llPDFIntegralZ[ManagedLibraryExpressionID[e,llPDFLibraryExpressionManagerName[]],1,(\[Zeta]-vd)/\[Theta]]
},
\[ScriptCapitalZ]1+\[ScriptCapitalZ]0 vd/\[Theta]
]
DLPDFIntegralZ[e:(llPDFLibraryExpression[id_,\[Theta]_Real?Positive,vd_Real]?DLPDFQ),2,\[Zeta]_?NumberQ]:=With[{
\[ScriptCapitalZ]0=#+I #2&@@llPDFIntegralZ[ManagedLibraryExpressionID[e,llPDFLibraryExpressionManagerName[]],0,(\[Zeta]-vd)/\[Theta]],
\[ScriptCapitalZ]1=#+I #2&@@llPDFIntegralZ[ManagedLibraryExpressionID[e,llPDFLibraryExpressionManagerName[]],1,(\[Zeta]-vd)/\[Theta]],
\[ScriptCapitalZ]2=#+I #2&@@llPDFIntegralZ[ManagedLibraryExpressionID[e,llPDFLibraryExpressionManagerName[]],2,(\[Zeta]-vd)/\[Theta]]
},
\[ScriptCapitalZ]2 \[Theta]+2\[ScriptCapitalZ]1 vd+\[ScriptCapitalZ]0 vd^2/\[Theta]
]


DLPDFIntegralZ[e:(llPDFLibraryExpression[_]?DLPDFQ),All,\[Zeta]_?NumberQ]:=With[{},
#+I #2&@@@llPDFZtriplet[ManagedLibraryExpressionID[e,llPDFLibraryExpressionManagerName[]],\[Zeta]]
]
DLPDFIntegralZ[e:(llPDFLibraryExpression[id_,\[Theta]_Real?Positive]?DLPDFQ),All,\[Zeta]_?NumberQ]:=With[{
\[ScriptCapitalZ]=#+I #2&@@@llPDFZtriplet[ManagedLibraryExpressionID[e,llPDFLibraryExpressionManagerName[]],\[Zeta]/\[Theta]]
},
\[ScriptCapitalZ]{1/\[Theta],1,\[Theta]}
]
DLPDFIntegralZ[e:(llPDFLibraryExpression[id_,\[Theta]_Real?Positive,vd_Real]?DLPDFQ),All,\[Zeta]_?NumberQ]:=With[{
\[ScriptCapitalZ]=#+I #2&@@@llPDFZtriplet[ManagedLibraryExpressionID[e,llPDFLibraryExpressionManagerName[]],(\[Zeta]-vd)/\[Theta]]
},
{\[ScriptCapitalZ][[1]]/\[Theta],\[ScriptCapitalZ][[2]]+\[ScriptCapitalZ][[1]]vd/\[Theta],\[ScriptCapitalZ][[3]]\[Theta]+2\[ScriptCapitalZ][[2]]vd+\[ScriptCapitalZ][[1]]vd^2/\[Theta]}
]


(* DLPDFIntegralW *)


Switch[LibraryFunctionInformation[llPDFWtriplet],
{__},LibraryFunctionUnload[llPDFWtriplet]
]
Switch[LibraryFunctionInformation[llPDFIntegralW],
{__},LibraryFunctionUnload[llPDFIntegralW]
]


llPDFWtriplet=Block[{$LibraryPath=libPath},
LibraryFunctionLoad["DispersionLink","llPDFWtriplet",{Integer,Complex},{Real,2}]
]
llPDFIntegralW=Block[{$LibraryPath=libPath},
LibraryFunctionLoad["DispersionLink","llPDFIntegralW",{(*id*)Integer,(*n*)Integer,(*z*)Complex},{Real,1}]
]


DLPDFIntegralW[e:(llPDFLibraryExpression[_]?DLPDFQ),n:0|1|2,\[Zeta]_?NumberQ]:=With[{},
#+I #2&@@llPDFIntegralW[ManagedLibraryExpressionID[e,llPDFLibraryExpressionManagerName[]],n,\[Zeta]]
]
DLPDFIntegralW[e:(llPDFLibraryExpression[id_,\[Theta]_Real?Positive]?DLPDFQ),n:0|1|2,\[Zeta]_?NumberQ]:=With[{
\[ScriptCapitalW]=#+I #2&@@llPDFIntegralW[ManagedLibraryExpressionID[e,llPDFLibraryExpressionManagerName[]],n,\[Zeta]/\[Theta]]
},
Power[\[Theta],n-2]\[ScriptCapitalW]
]
DLPDFIntegralW[e:(llPDFLibraryExpression[id_,\[Theta]_Real?Positive,vd_Real]?DLPDFQ),0,\[Zeta]_?NumberQ]:=With[{
\[ScriptCapitalW]0=#+I #2&@@llPDFIntegralW[ManagedLibraryExpressionID[e,llPDFLibraryExpressionManagerName[]],0,(\[Zeta]-vd)/\[Theta]]
},
\[ScriptCapitalW]0/\[Theta]^2
]
DLPDFIntegralW[e:(llPDFLibraryExpression[id_,\[Theta]_Real?Positive,vd_Real]?DLPDFQ),1,\[Zeta]_?NumberQ]:=With[{
\[ScriptCapitalW]0=#+I #2&@@llPDFIntegralW[ManagedLibraryExpressionID[e,llPDFLibraryExpressionManagerName[]],0,(\[Zeta]-vd)/\[Theta]],
\[ScriptCapitalW]1=#+I #2&@@llPDFIntegralW[ManagedLibraryExpressionID[e,llPDFLibraryExpressionManagerName[]],1,(\[Zeta]-vd)/\[Theta]]
},
Divide[\[ScriptCapitalW]1+\[ScriptCapitalW]0 vd/\[Theta],\[Theta]]
]
DLPDFIntegralW[e:(llPDFLibraryExpression[id_,\[Theta]_Real?Positive,vd_Real]?DLPDFQ),2,\[Zeta]_?NumberQ]:=With[{
\[ScriptCapitalW]0=#+I #2&@@llPDFIntegralW[ManagedLibraryExpressionID[e,llPDFLibraryExpressionManagerName[]],0,(\[Zeta]-vd)/\[Theta]],
\[ScriptCapitalW]1=#+I #2&@@llPDFIntegralW[ManagedLibraryExpressionID[e,llPDFLibraryExpressionManagerName[]],1,(\[Zeta]-vd)/\[Theta]],
\[ScriptCapitalW]2=#+I #2&@@llPDFIntegralW[ManagedLibraryExpressionID[e,llPDFLibraryExpressionManagerName[]],2,(\[Zeta]-vd)/\[Theta]]
},
Divide[\[ScriptCapitalW]2 \[Theta]+2\[ScriptCapitalW]1 vd+\[ScriptCapitalW]0 vd^2/\[Theta],\[Theta]]
]


DLPDFIntegralW[e:(llPDFLibraryExpression[_]?DLPDFQ),All,\[Zeta]_?NumberQ]:=With[{},
#+I #2&@@@llPDFWtriplet[ManagedLibraryExpressionID[e,llPDFLibraryExpressionManagerName[]],\[Zeta]]
]
DLPDFIntegralW[e:(llPDFLibraryExpression[id_,\[Theta]_Real?Positive]?DLPDFQ),All,\[Zeta]_?NumberQ]:=With[{
\[ScriptCapitalW]=#+I #2&@@@llPDFWtriplet[ManagedLibraryExpressionID[e,llPDFLibraryExpressionManagerName[]],\[Zeta]/\[Theta]]
},
\[ScriptCapitalW] {1/\[Theta],1,\[Theta]}/\[Theta]
]
DLPDFIntegralW[e:(llPDFLibraryExpression[id_,\[Theta]_Real?Positive,vd_Real]?DLPDFQ),All,\[Zeta]_?NumberQ]:=With[{
\[ScriptCapitalW]=#+I #2&@@@llPDFWtriplet[ManagedLibraryExpressionID[e,llPDFLibraryExpressionManagerName[]],(\[Zeta]-vd)/\[Theta]]
},
{\[ScriptCapitalW][[1]]/\[Theta],\[ScriptCapitalW][[2]]+\[ScriptCapitalW][[1]]vd/\[Theta],\[ScriptCapitalW][[3]]\[Theta]+2\[ScriptCapitalW][[2]]vd+\[ScriptCapitalW][[1]]vd^2/\[Theta]}/\[Theta]
]


(* DLMaxwellianPDF *)


Switch[LibraryFunctionInformation[llMaxwellianPDF],
{__},LibraryFunctionUnload[llMaxwellianPDF]
]


llMaxwellianPDF=Block[{$LibraryPath=libPath},
LibraryFunctionLoad["DispersionLink","llMaxwellianPDF",{Integer},"Void"]
]


DLMaxwellianPDF[]:=Module[{e},
e=CreateManagedLibraryExpression[llPDFLibraryExpressionManagerName[],llPDFLibraryExpression];
llMaxwellianPDF[ManagedLibraryExpressionID[e,llPDFLibraryExpressionManagerName[]]];
e
]
DLMaxwellianPDF[\[Theta]_?Positive]:=Module[{e},
e=CreateManagedLibraryExpression[llPDFLibraryExpressionManagerName[],llPDFLibraryExpression[#,N[\[Theta]]]&];
llMaxwellianPDF[ManagedLibraryExpressionID[e,llPDFLibraryExpressionManagerName[]]];
e
]
DLMaxwellianPDF[\[Theta]_?Positive,vd_Real]:=Module[{e},
e=CreateManagedLibraryExpression[llPDFLibraryExpressionManagerName[],llPDFLibraryExpression[#,N[\[Theta]],N[vd]]&];
llMaxwellianPDF[ManagedLibraryExpressionID[e,llPDFLibraryExpressionManagerName[]]];
e
]


(* DLLorentzianPDF *)


Switch[LibraryFunctionInformation[llIntegerKappaPDF],
{__},LibraryFunctionUnload[llIntegerKappaPDF]
]
Switch[LibraryFunctionInformation[llRealKappaPDF],
{__},LibraryFunctionUnload[llRealKappaPDF]
]


llIntegerKappaPDF=Block[{$LibraryPath=libPath},
LibraryFunctionLoad["DispersionLink","llIntegerKappaPDF",{Integer,Integer},"Void"]
]
llRealKappaPDF=Block[{$LibraryPath=libPath},
LibraryFunctionLoad["DispersionLink","llRealKappaPDF",{Integer,Real},"Void"]
]


DLLorentzianPDF[\[Kappa]_Integer/;\[Kappa]>1/2]:=Module[{e},
e=CreateManagedLibraryExpression[llPDFLibraryExpressionManagerName[],llPDFLibraryExpression];
llIntegerKappaPDF[ManagedLibraryExpressionID[e,llPDFLibraryExpressionManagerName[]],\[Kappa]];
e
]
DLLorentzianPDF[\[Kappa]_Integer/;\[Kappa]>1/2,\[Theta]_?Positive]:=Module[{e},
e=CreateManagedLibraryExpression[llPDFLibraryExpressionManagerName[],llPDFLibraryExpression[#,N[\[Theta]]]&];
llIntegerKappaPDF[ManagedLibraryExpressionID[e,llPDFLibraryExpressionManagerName[]],\[Kappa]];
e
]
DLLorentzianPDF[\[Kappa]_Integer/;\[Kappa]>1/2,\[Theta]_?Positive,vd_Real]:=Module[{e},
e=CreateManagedLibraryExpression[llPDFLibraryExpressionManagerName[],llPDFLibraryExpression[#,N[\[Theta]],N[vd]]&];
llIntegerKappaPDF[ManagedLibraryExpressionID[e,llPDFLibraryExpressionManagerName[]],\[Kappa]];
e
]


DLLorentzianPDF[\[Kappa]_Real/;\[Kappa]>1/2]:=Module[{e},
e=CreateManagedLibraryExpression[llPDFLibraryExpressionManagerName[],llPDFLibraryExpression];
llRealKappaPDF[ManagedLibraryExpressionID[e,llPDFLibraryExpressionManagerName[]],\[Kappa]];
e
]
DLLorentzianPDF[\[Kappa]_Real/;\[Kappa]>1/2,\[Theta]_?Positive]:=Module[{e},
e=CreateManagedLibraryExpression[llPDFLibraryExpressionManagerName[],llPDFLibraryExpression[#,N[\[Theta]]]&];
llRealKappaPDF[ManagedLibraryExpressionID[e,llPDFLibraryExpressionManagerName[]],\[Kappa]];
e
]
DLLorentzianPDF[\[Kappa]_Real/;\[Kappa]>1/2,\[Theta]_?Positive,vd_Real]:=Module[{e},
e=CreateManagedLibraryExpression[llPDFLibraryExpressionManagerName[],llPDFLibraryExpression[#,N[\[Theta]],N[vd]]&];
llRealKappaPDF[ManagedLibraryExpressionID[e,llPDFLibraryExpressionManagerName[]],\[Kappa]];
e
]


(* DLSplinePDF *)


Switch[LibraryFunctionInformation[llSplinePDF],
{__},LibraryFunctionUnload[llSplinePDF]
]


llSplinePDF=Block[{$LibraryPath=libPath},
LibraryFunctionLoad["DispersionLink","llSplinePDF",{Integer,"Boolean",{Real,1},{Real,1}},"Void"]
]


DLSplinePDF[xs:{__Real}/;Length[xs]>=4,ys:{__Real},ac:True|False]/;And[
If[Length[ys]!=Length[xs],(Message[DLSplinePDF::argx,"The length of ys is not the same as the length of xs."];Abort[]),True],
If[Not[Equal@@Append[Chop[#-Median[#]&@Differences[xs]],0]],(Message[DLSplinePDF::argx,"xs is not regularly spaced."];Abort[]),
True],
If[Last[xs]-First[xs]<=0,(Message[DLSplinePDF::argx,"xs is not in ascending order."];Abort[]),True]
]:=Module[{e},
e=CreateManagedLibraryExpression[llPDFLibraryExpressionManagerName[],llPDFLibraryExpression];
llSplinePDF[ManagedLibraryExpressionID[e,llPDFLibraryExpressionManagerName[]],ac,xs,N[Chop[ys]]];
e
]


End[];


(* :End Private: *)


(* :Code Section (Call Protect): *)


Protect[DLPDFReleaseAll,DLPDFQ,DLPDFIntegralZ,DLPDFIntegralW,DLMaxwellianPDF,DLLorentzianPDF,DLSplinePDF]


EndPackage[];
