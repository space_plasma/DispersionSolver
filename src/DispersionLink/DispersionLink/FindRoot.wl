(* ::Package:: *)

(*
 * Copyright (c) 2017-2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 *)


BeginPackage["DispersionLink`"];


(* :Code Section (Call Unprotect and ClearAll): *)


Unprotect[DLMullerFindRoot]
ClearAll[DLMullerFindRoot]


(* :Usage Messages: *)


Options[DLMullerFindRoot]={MaxIterations->10,AccuracyGoal->4}
DLMullerFindRoot::usage="DLMullerFindRoot[cf,inits] finds a complex root, z, of equation f[z]==0 using Muller's method. cf is a compiled function with the argument type {{z,_Complex}} and which returns a complex value of f[z]. inits are three complex-convertible initial guesses. It returns Indeterminate on failure.
The MaxIterations option sets the maximum number of iterations; default is 10.
The AccuracyGoal option sets the relative tolerance (10^-AccuracyGoal) of the root convergence; default is 4."


(* :Error Messages: *)


DLMullerFindRoot::callback="Failed to connect library callback function."
DLMullerFindRoot::optx="Invalid value for option: `1`."


(* :Begin Private: *)


Begin["`Private`"];


libPath=FileNameJoin[{FileNameDrop[FindFile["DispersionLink`"],-1](*,"LibraryResources",$SystemID*)}]


(* llFindRootCallbackManagerName *)


Switch[LibraryFunctionInformation[llFindRootCallbackManagerName],
{__},LibraryFunctionUnload[llFindRootCallbackManagerName]
]


llFindRootCallbackManagerName=Block[{$LibraryPath=libPath},
LibraryFunctionLoad["DispersionLink","llFindRootCallbackManagerName",{},"UTF8String"]
]


(* DLMullerFindRoot *)


Switch[LibraryFunctionInformation[llMullerFindRoot],
{__},LibraryFunctionUnload[llMullerFindRoot]
]


llMullerFindRoot=Block[{$LibraryPath=libPath},
LibraryFunctionLoad["DispersionLink","llMullerFindRoot",{(*x0*)Complex,(*x1*)Complex,(*x2*)Complex,(*maxit*)Integer(*?Positive*),(*reltol*)Real(*?Positive*)},{Real,1}]
]


DLMullerFindRoot[cf_CompiledFunction,inits:{z0_?NumberQ,z1_?NumberQ,z2_?NumberQ},OptionsPattern[DLMullerFindRoot]]:=With[{
res=llMullerFindRoot[Sequence@@inits,OptionValue[MaxIterations],Power[10.,-N@OptionValue[AccuracyGoal]]]
},
Switch[res,{_Real,_Real},Apply[#+I #2&,res],_,Indeterminate]
]/;And[(*this must be at the end to evaluate OptionValue*)
If[!ConnectLibraryCallbackFunction[llFindRootCallbackManagerName[],cf],(Message[DLMullerFindRoot::callback];Abort[]),True],
If[!Positive[OptionValue[MaxIterations]],(Message[DLMullerFindRoot::optx,MaxIterations];Abort[]),True],
If[!SameQ[Head[N[OptionValue[AccuracyGoal]]],Real],(Message[DLMullerFindRoot::optx,AccuracyGoal];Abort[]),True]
]


End[];


(* :End Private: *)


(* :Code Section (Call Protect): *)


Protect[DLMullerFindRoot]


EndPackage[];
