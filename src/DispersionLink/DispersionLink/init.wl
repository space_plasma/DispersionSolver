(* ::Package:: *)

(*
 * Copyright (c) 2017-2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 *)


LibraryFunction::exception = "A std::exception thrown."
LibraryFunction::argc = "Incorrect argument count."
LibraryFunction::noitem = "No persistent object."
LibraryFunction::noswitchcase = "No case for switch selector."
LibraryFunction::nocallback = "No callback function registered."

Get[FileNameJoin[{FileNameDrop[FindFile["DispersionLink`"],-1],"PDF.wl"}]]
Get[FileNameJoin[{FileNameDrop[FindFile["DispersionLink`"],-1],"FLR.wl"}]]
Get[FileNameJoin[{FileNameDrop[FindFile["DispersionLink`"],-1],"FindRoot.wl"}]]
