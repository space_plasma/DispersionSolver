/*
 * Copyright (c) 2016-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <UtilityKit/UtilityKit-config.h>

// MARK: Version 3
//
#include <UtilityKit/Expression.h>
#include <UtilityKit/FixedSizedNumericVector.h>
#include <sstream>

UTILITYKIT_BEGIN_NAMESPACE

// MARK:- Vector Expressions
#if defined(ARRAYKIT_USE_VECTOR_EXPRESSION) && ARRAYKIT_USE_VECTOR_EXPRESSION
namespace _VecExp_ {
using Expression::Pair

    // plus:
    template <class T, class E1, class E2>
    struct Plus;
// E1 + E2:
template <class T, class E1, class E2>
struct Plus<T, E1 const &, E2 const &> : public Pair<E1 const &, E2 const &>
, public _VE<T, Plus<T, E1 const &, E2 const &>> {
    typedef Pair<E1 const &, E2 const &> __pair;
    Plus(_VE<T, E1> const &x, _VE<T, E2> const &y) noexcept
    : __pair(x, y) {}

    T operator[](unsigned long i) const noexcept { return __pair::x[i] + __pair::y[i]; }
};
// E + S:
template <class T, class E>
struct Plus<T, E const &, T> : public Pair<E const &, T>
, public _VE<T, Plus<T, E const &, T>> {
    typedef Pair<E const &, T> __pair;
    Plus(_VE<T, E> const &x, T const &y) noexcept
    : __pair(x, y) {}

    T operator[](unsigned long i) const noexcept { return __pair::x[i] + __pair::y; }
};

// minus:
template <class T, class E1, class E2>
struct Minus;
// E1 - E2:
template <class T, class E1, class E2>
struct Minus<T, E1 const &, E2 const &> : public Pair<E1 const &, E2 const &>
, public _VE<T, Minus<T, E1 const &, E2 const &>> {
    typedef Pair<E1 const &, E2 const &> __pair;
    Minus(_VE<T, E1> const &x, _VE<T, E2> const &y) noexcept
    : __pair(x, y) {}

    T operator[](unsigned long i) const noexcept { return __pair::x[i] - __pair::y[i]; }
};
// S - E:
template <class T, class E>
struct Minus<T, T, E const &> : public Pair<T, E const &>
, public _VE<T, Minus<T, T, E const &>> {
    typedef Pair<T, E const &> __pair;
    Minus(T const &x, _VE<T, E> const &y) noexcept
    : __pair(x, y) {}

    T operator[](unsigned long i) const noexcept { return __pair::x - __pair::y[i]; }
};

// times:
template <class T, class E1, class E2>
struct Times;
// E1 * E2:
template <class T, class E1, class E2>
struct Times<T, E1 const &, E2 const &> : public Pair<E1 const &, E2 const &>
, public _VE<T, Times<T, E1 const &, E2 const &>> {
    typedef Pair<E1 const &, E2 const &> __pair;
    Times(_VE<T, E1> const &x, _VE<T, E2> const &y) noexcept
    : __pair(x, y) {}

    T operator[](unsigned long i) const noexcept { return __pair::x[i] * __pair::y[i]; }
};
// E * S:
template <class T, class E>
struct Times<T, E const &, T> : public Pair<E const &, T>
, public _VE<T, Times<T, E const &, T>> {
    typedef Pair<E const &, T> __pair;
    Times(_VE<T, E> const &x, T const &y) noexcept
    : __pair(x, y) {}

    T operator[](unsigned long i) const noexcept { return __pair::x[i] * __pair::y; }
};

// divide:
template <class T, class E1, class E2>
struct Divide;
// E1 / E2:
template <class T, class E1, class E2>
struct Divide<T, E1 const &, E2 const &> : public Pair<E1 const &, E2 const &>
, public _VE<T, Divide<T, E1 const &, E2 const &>> {
    typedef Pair<E1 const &, E2 const &> __pair;
    constexpr Divide(_VE<T, E1> const &x, _VE<T, E2> const &y) noexcept
    : __pair(x, y) {}

    T operator[](unsigned long i) const noexcept { return __pair::x[i] / __pair::y[i]; }
};
// S / E:
template <class T, class E>
struct Divide<T, T, E const &> : public Pair<T, E const &>
, public _VE<T, Divide<T, T, E const &>> {
    typedef Pair<T, E const &> __pair;
    Divide(T const &x, _VE<T, E> const &y) noexcept
    : __pair(x, y) {}

    T operator[](unsigned long i) const noexcept { return __pair::x / __pair::y[i]; }
};

} // namespace _VecExp_
#endif

// MARK:- Arithematic Operators
// MARK: Compounds
// compound - vector:
template <class T, long S>
inline Vector<T, S> &operator+=(Vector<T, S> &a, Vector<T, S> const &b)
{
    for (unsigned i = 0; i < S; ++i)
        a[i] += b[i];
    return a;
}
template <class T, long S>
inline Vector<T, S> &operator-=(Vector<T, S> &a, Vector<T, S> const &b)
{
    for (unsigned i = 0; i < S; ++i)
        a[i] -= b[i];
    return a;
}
template <class T, long S>
inline Vector<T, S> &operator*=(Vector<T, S> &a, Vector<T, S> const &b)
{
    for (unsigned i = 0; i < S; ++i)
        a[i] *= b[i];
    return a;
}
template <class T, long S>
inline Vector<T, S> &operator/=(Vector<T, S> &a, Vector<T, S> const &b)
{
    for (unsigned i = 0; i < S; ++i)
        a[i] /= b[i];
    return a;
}
// compound - scalar:
template <class T, long S>
inline Vector<T, S> &operator+=(Vector<T, S> &a, T const &s)
{
    for (unsigned i = 0; i < S; ++i)
        a[i] += s;
    return a;
}
template <class T, long S>
inline Vector<T, S> &operator-=(Vector<T, S> &a, T const &s)
{
    for (unsigned i = 0; i < S; ++i)
        a[i] -= s;
    return a;
}
template <class T, long S>
inline Vector<T, S> &operator*=(Vector<T, S> &a, T const &s)
{
    for (unsigned i = 0; i < S; ++i)
        a[i] *= s;
    return a;
}
template <class T, long S>
inline Vector<T, S> &operator/=(Vector<T, S> &a, T const &s)
{
    for (unsigned i = 0; i < S; ++i)
        a[i] /= s;
    return a;
}
#if defined(ARRAYKIT_USE_VECTOR_EXPRESSION) && ARRAYKIT_USE_VECTOR_EXPRESSION
// compound - expression:
template <class T, long S, class E>
inline Vector<T, S> &operator+=(Vector<T, S> &a, _VE<T, E> const &b)
{
    for (unsigned i = 0; i < S; ++i)
        a[i] += b[i];
    return a;
}
template <class T, long S, class E>
inline Vector<T, S> &operator-=(Vector<T, S> &a, _VE<T, E> const &b)
{
    for (unsigned i = 0; i < S; ++i)
        a[i] -= b[i];
    return a;
}
template <class T, long S, class E>
inline Vector<T, S> &operator*=(Vector<T, S> &a, _VE<T, E> const &b)
{
    for (unsigned i = 0; i < S; ++i)
        a[i] *= b[i];
    return a;
}
template <class T, long S, class E>
inline Vector<T, S> &operator/=(Vector<T, S> &a, _VE<T, E> const &b)
{
    for (unsigned i = 0; i < S; ++i)
        a[i] /= b[i];
    return a;
}
#endif

// MARK: Unary/Binary
// unary:
template <class T, long S>
inline Vector<T, S> const &operator+(Vector<T, S> const &a)
{
    return a;
}

#if defined(ARRAYKIT_USE_VECTOR_EXPRESSION) && ARRAYKIT_USE_VECTOR_EXPRESSION
template <class T, class E>
inline _VecExp_::Minus<T, T, E const &> const operator-(_VE<T, E> const &a)
{
    return { T{}, a };
}

// binary:
// +:
template <class T, class E1, class E2>
inline _VecExp_::Plus<T, E1 const &, E2 const &> const operator+(_VE<T, E1> const &a, _VE<T, E2> const &b)
{
    return { a, b };
}
template <class T, class E>
inline _VecExp_::Plus<T, E const &, T> const operator+(_VE<T, E> const &a, T const &s)
{
    return { a, s };
}
template <class T, class E>
inline _VecExp_::Plus<T, E const &, T> const operator+(T const &s, _VE<T, E> const &a)
{
    return { a, s };
}
// *:
template <class T, class E1, class E2>
inline _VecExp_::Times<T, E1 const &, E2 const &> const operator*(_VE<T, E1> const &a, _VE<T, E2> const &b)
{
    return { a, b };
}
template <class T, class E>
inline _VecExp_::Times<T, E const &, T> const operator*(_VE<T, E> const &a, T const &s)
{
    return { a, s };
}
template <class T, class E>
inline _VecExp_::Times<T, E const &, T> const operator*(T const &s, _VE<T, E> const &a)
{
    return { a, s };
}
// -:
template <class T, class E1, class E2>
inline _VecExp_::Minus<T, E1 const &, E2 const &> const operator-(_VE<T, E1> const &a, _VE<T, E2> const &b)
{
    return { a, b };
}
template <class T, class E>
inline _VecExp_::Plus<T, E const &, T> const operator-(_VE<T, E> const &a, T const &s)
{
    return { a, -s };
}
template <class T, class E>
inline _VecExp_::Minus<T, T, E const &> const operator-(T const &s, _VE<T, E> const &a)
{
    return { s, a };
}
// /:
template <class T, class E1, class E2>
inline _VecExp_::Divide<T, E1 const &, E2 const &> const operator/(_VE<T, E1> const &a, _VE<T, E2> const &b)
{
    return { a, b };
}
template <class T, class E>
inline _VecExp_::Times<T, E const &, T> const operator/(_VE<T, E> const &a, T const &s)
{
    return { a, T(1) / s };
}
template <class T, class E>
inline _VecExp_::Divide<T, T, E const &> const operator/(T const &s, _VE<T, E> const &a)
{
    return { s, a };
}
#else
template <class T, long S>
inline Vector<T, S> operator-(Vector<T, S> const &b)
{
    Vector<T, S> a{};
    return a -= b;
}

// binary:
// +:
template <class T, long S>
inline Vector<T, S> operator+(Vector<T, S> a, Vector<T, S> const &b)
{
    return a += b;
}
template <class T, long S>
inline Vector<T, S> operator+(Vector<T, S> a, T const &s)
{
    return a += s;
}
template <class T, long S>
inline Vector<T, S> operator+(T const &s, Vector<T, S> a)
{
    return a += s;
}
// -:
template <class T, long S>
inline Vector<T, S> operator-(Vector<T, S> a, Vector<T, S> const &b)
{
    return a -= b;
}
template <class T, long S>
inline Vector<T, S> operator-(Vector<T, S> a, T const &s)
{
    return a -= s;
}
template <class T, long S>
inline Vector<T, S> operator-(T const &s, Vector<T, S> const &b)
{
    Vector<T, S> a(s);
    return a -= b;
}
// *:
template <class T, long S>
inline Vector<T, S> operator*(Vector<T, S> a, Vector<T, S> const &b)
{
    return a *= b;
}
template <class T, long S>
inline Vector<T, S> operator*(Vector<T, S> a, T const &s)
{
    return a *= s;
}
template <class T, long S>
inline Vector<T, S> operator*(T const &s, Vector<T, S> a)
{
    return a *= s;
}
// /:
template <class T, long S>
inline Vector<T, S> operator/(Vector<T, S> a, Vector<T, S> const &b)
{
    return a /= b;
}
template <class T, long S>
inline Vector<T, S> operator/(Vector<T, S> a, T const &s)
{
    return a /= s;
}
template <class T, long S>
inline Vector<T, S> operator/(T const &s, Vector<T, S> const &b)
{
    Vector<T, S> a(s);
    return a /= b;
}
#endif

// MARK:- Output Stream Operator
template <class CharT, class Traits, class Tp, long Size>
std::basic_ostream<CharT, Traits> &
operator<<(std::basic_ostream<CharT, Traits> &ostream, Vector<Tp, Size> const &vector)
{
    std::basic_ostringstream<CharT, Traits> ss;
    {
        ss.flags(ostream.flags());
        ss.imbue(ostream.getloc());
        ss.precision(ostream.precision());
    }
    unsigned long i = 0;
    ss << '{' << vector[i++];
    while (i < Size)
        ss << ", " << vector[i++];
    ss << '}';
    return ostream << ss.str();
}

UTILITYKIT_END_NAMESPACE
