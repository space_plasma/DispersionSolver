/*
 * Copyright (c) 2017-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <UtilityKit/UtilityKit-config.h>

// MARK: Version 3
//
#include <array>
#include <cmath>
#include <stdexcept>
#include <string>
#include <type_traits>

UTILITYKIT_BEGIN_NAMESPACE

/**
 @brief Fixed-point Gauss-Legendre quadrature; implementation from Numerical Recipe, Ch. 4.
 @discussion At construction, it calculates N abscissa points and weights.
 The call operator facilitates the integration given the function object and the integral interval.
 @tparam X Abscissa type; either float or double.
 @tparam N The number of abscissa points, i.e., the order of Legendre polynomial from which the abscissas and weights are calculated.
 */
template <class X, long N>
struct GaussLegendreIntegrator {
    static_assert(std::is_floating_point<X>::value, "The abscissa type should be of a floating point type");
    static_assert(N > 0, "The number of points N should be greater than or equal to 1");

    /**
     @brief Calculates abscissas and weights.
     @discussion The roots of the N-degree Legendre polynomial are the abscissas, which are found with Newton's method.
     An std::runtime_error exception will be thrown when the root finding fails.
     */
    explicit GaussLegendreIntegrator();

    /**
     @brief Returns abscissas scaled between -1 and 1.
     */
    std::array<X, N> const &abscissas() const noexcept { return _x; }
    /**
     @brief Returns weights for the abscissas scaled between -1 and 1.
     */
    std::array<X, N> const &weights() const noexcept { return _w; }

    /**
     @brief Returns integral of the function `f'.
     @tparam F Function type whose return type should be convertible to the return type Y.
     @tparam Y Return type.
     @param f A function object/pointer whose signature is Y(X).
     @param a From which to integrate f.
     @param b To which to integrate f.
     @param s_init An initial value to which the result will be added.
     @return s_init + the integral of f.
     */
    template <class F /*Y(X)*/, class Y>
    typename std::decay<Y>::type operator()(F f, X const a, X const b, Y &&s_init) const noexcept;

    /**
     A short hand for (*this)(f, a, b, 0).
     */
    template <class F /*Y(X)*/>
    auto operator()(F f, X const a, X const b) const noexcept -> decltype(f(a))
    {
        return this->template operator()<F const &>(f, a, b, decltype(f(a)){});
    }

private:
    std::array<X, N> _x, _w; //!< abscissas and weights sampled for interval -1 <= x <= 1.
};

// MARK: Out-of-line Implementations
//
template <class X, long n>
GaussLegendreIntegrator<X, n>::GaussLegendreIntegrator()
{
    for (unsigned long i = 0, m = (n + 1) / 2; i < m; ++i) {
        // iterative root finding by Newton's method, which will be the abscissa
        // proceed with double for accuracy
        //
        constexpr double   eps = 1e-14;
        double             z   = std::cos(M_PI * (i + .75) / (n + .5)); // initial guess
        double             pp{};
        unsigned           iter;
        constexpr unsigned max_iterations = 10;
        for (iter = 0; iter < max_iterations; ++iter) {
            double p1 = 1, p2 = 0;
            for (unsigned long j = 0; j < n; ++j) { // recurrence relation to valuate Legendre polynomial
                double const p3 = p2;
                p2              = p1;
                p1              = ((X{ 2 } * j + 1) * z * p2 - j * p3) / (j + X{ 1 });
            }
            pp              = n * (z * p1 - p2) / (z * z - 1); // first derivative of Legendre polynomial
            double const z1 = z;
            z               = z1 - p1 / pp;
            if (std::abs(z - z1) < eps)
                break;
        }
        if (iter >= max_iterations) {
            throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - finding roots of Legendre polynomial failed");
        } else {
            _x[i]         = X(-z);                           // lower half
            _x[n - 1 - i] = -_x[i];                          // upper half; roots are symmetric
            _w[i]         = X(2. / ((1 - z * z) * pp * pp)); // note that `xl' in NR here is 1
            _w[n - 1 - i] = _w[i];                           // symmetric
        }
    }
}

template <class X, long n>
template <class F, class Y>
typename std::decay<Y>::type GaussLegendreIntegrator<X, n>::operator()(F f, X const a, X const b, Y &&s_init) const noexcept
{
    X const                      xr = X{ .5 } * (b - a);
    typename std::decay<Y>::type s{};
    for (unsigned long i = 0; i < n; ++i) {
        X const                      t = _x[i];
        X const                      w = _w[i];
        X const                      x = (t + 1) * xr + a;
        typename std::decay<Y>::type y = f(x);
        s += (y *= w);
    }
    s *= xr;
    return s += s_init;
}

UTILITYKIT_END_NAMESPACE
