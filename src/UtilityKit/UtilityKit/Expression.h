/*
 * Copyright (c) 2015-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <UtilityKit/UtilityKit-config.h>

// MARK: Version 3
//

UTILITYKIT_BEGIN_NAMESPACE
namespace Expression {

/**
 @brief Pair expression.
 */
template <class X, class Y>
struct Pair {
    inline X _1() const noexcept { return x; }
    inline Y _2() const noexcept { return y; }

    constexpr Pair(X _x, Y _y) noexcept
    : x(_x), y(_y) {}

protected:
    X x;
    Y y;
};

} // namespace Expression
UTILITYKIT_END_NAMESPACE
