/*
 * Copyright (c) 2016, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

// root namespace
//
#ifndef UTILITYKIT_NAMESPACE
#define UTILITYKIT_NAMESPACE       Utility
#define UTILITYKIT_BEGIN_NAMESPACE namespace UTILITYKIT_NAMESPACE {
#define UTILITYKIT_END_NAMESPACE   }
#endif

// expression-based Vector operations
//
#ifndef ARRAYKIT_USE_VECTOR_EXPRESSION
#define ARRAYKIT_USE_VECTOR_EXPRESSION 0
#endif

// GCC Deprecated attribute
//
#ifndef UTILITYKIT_DEPRECATED_ATTRIBUTE
#define UTILITYKIT_DEPRECATED_ATTRIBUTE __attribute__((deprecated))
#endif
