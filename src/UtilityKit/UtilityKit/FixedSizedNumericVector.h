/*
 * Copyright (c) 2016-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <UtilityKit/UtilityKit-config.h>

// MARK: Version 3
//
#include <algorithm>
#include <array>
#include <iterator>
#include <type_traits>
#include <utility>

UTILITYKIT_BEGIN_NAMESPACE

template <class _Tp, long _Size>
struct Vector;

// MARK:- Vector Expression
template <class T, class E>
struct _VE {
    typedef T             value_type;
    typedef unsigned long size_type;

    value_type operator[](size_type i) const noexcept { return static_cast<E const &>(*this)[i]; }

    operator E &() noexcept { return static_cast<E &>(*this); }
    operator E const &() const noexcept { return static_cast<const E &>(*this); }
};

// MARK:- Vector<1>
template <class _Tp>
struct Vector<_Tp, 1>
#if defined(ARRAYKIT_USE_VECTOR_EXPRESSION) && ARRAYKIT_USE_VECTOR_EXPRESSION
: public _VE<_Tp, Vector<_Tp, 1>>
#endif
{
    // capacity:
    static constexpr unsigned long size() noexcept { return 1; }
    static constexpr unsigned long max_size() noexcept { return size(); }

    // types:
    typedef _Tp                                   value_type;
    typedef value_type                           &reference;
    typedef value_type const                     &const_reference;
    typedef value_type                           *iterator;
    typedef value_type const                     *const_iterator;
    typedef value_type                           *pointer;
    typedef value_type const                     *const_pointer;
    typedef unsigned long                         size_type;
    typedef long                                  difference_type;
    typedef std::reverse_iterator<iterator>       reverse_iterator;
    typedef std::reverse_iterator<const_iterator> const_reverse_iterator;

    value_type x;

    // constructors:
    constexpr explicit Vector() noexcept(noexcept(value_type())) = default;
    constexpr Vector(value_type const &a) noexcept(noexcept(value_type(a)))
    : x(a) {}

    constexpr Vector(Vector const &) noexcept(noexcept(value_type(x))) = default;
    Vector &operator=(Vector const &o) noexcept(noexcept(x = o.x)) = default;

#if defined(ARRAYKIT_USE_VECTOR_EXPRESSION) && ARRAYKIT_USE_VECTOR_EXPRESSION
    template <class E>
    Vector(_VE<value_type, E> const &e) noexcept(noexcept(value_type(e[0])))
    : x(e[0])
    {
    }
    template <class E>
    Vector &operator=(_VE<value_type, E> const &e) noexcept(noexcept(x = e[0]))
    {
        x = e[0];
        return *this;
    }
#endif

    // modifiers:
    void fill(value_type const &__u) noexcept(noexcept(x = __u)) { x = __u; }
    void swap(Vector &o) noexcept(noexcept(std::swap(x, o.x))) { std::swap(x, o.x); }

    // element access:
    reference       back() noexcept { return x; }
    const_reference back() const noexcept { return x; }
    reference       front() noexcept { return x; }
    const_reference front() const noexcept { return x; }

    reference       operator[](size_type __n) noexcept { return (&x)[__n]; }
    const_reference operator[](size_type __n) const noexcept { return (&x)[__n]; }

    pointer       data() noexcept { return &x; }
    const_pointer data() const noexcept { return &x; }

    // iterators:
    iterator       begin() noexcept { return iterator(&x); }
    const_iterator begin() const noexcept { return iterator(&x); }
    iterator       end() noexcept { return iterator(&x + size()); }
    const_iterator end() const noexcept { return iterator(&x + size()); }

    reverse_iterator       rbegin() noexcept { return reverse_iterator(end()); }
    const_reverse_iterator rbegin() const noexcept { return const_reverse_iterator(end()); }
    reverse_iterator       rend() noexcept { return reverse_iterator(begin()); }
    const_reverse_iterator rend() const noexcept { return const_reverse_iterator(begin()); }
};

// MARK:- Vector<2>
template <class _Tp>
struct Vector<_Tp, 2>
#if defined(ARRAYKIT_USE_VECTOR_EXPRESSION) && ARRAYKIT_USE_VECTOR_EXPRESSION
: public _VE<_Tp, Vector<_Tp, 2>>
#endif
{
    // capacity:
    static constexpr unsigned long size() noexcept { return 2; }
    static constexpr unsigned long max_size() noexcept { return size(); }

    // types:
    typedef Vector<_Tp, 1>                        __V1;
    typedef typename __V1::value_type             value_type;
    typedef typename __V1::reference              reference;
    typedef typename __V1::const_reference        const_reference;
    typedef typename __V1::iterator               iterator;
    typedef typename __V1::const_iterator         const_iterator;
    typedef typename __V1::pointer                pointer;
    typedef typename __V1::const_pointer          const_pointer;
    typedef typename __V1::size_type              size_type;
    typedef typename __V1::difference_type        difference_type;
    typedef typename __V1::reverse_iterator       reverse_iterator;
    typedef typename __V1::const_reverse_iterator const_reverse_iterator;

    value_type x, y;

    // constructors:
    constexpr explicit Vector() noexcept(noexcept(value_type())) = default;
    constexpr explicit Vector(value_type const &a) noexcept(noexcept(value_type(a)))
    : x(a), y(a) {}
    constexpr Vector(value_type const &a, value_type const &b) noexcept(noexcept(value_type(a)))
    : x(a), y(b) {}

    constexpr Vector(Vector const &) noexcept(noexcept(value_type(x))) = default;
    Vector &operator=(Vector const &o) noexcept(noexcept(x = o.x)) = default;

#if defined(ARRAYKIT_USE_VECTOR_EXPRESSION) && ARRAYKIT_USE_VECTOR_EXPRESSION
    template <class E>
    Vector(_VE<value_type, E> const &e) noexcept(noexcept(value_type(e[0])))
    : x(e[0]), y(e[1])
    {
    }
    template <class E>
    Vector &operator=(_VE<value_type, E> const &e) noexcept(noexcept(x = e[0]))
    {
        x = e[0];
        y = e[1];
        return *this;
    }
#endif

    // modifiers:
    void fill(value_type const &__u) noexcept(noexcept(x = __u)) { y = x = __u; }
    void swap(Vector &o) noexcept(noexcept(std::swap(x, o.x)))
    {
        std::swap(x, o.x);
        std::swap(y, o.y);
    }

    // element access:
    reference       back() noexcept { return y; }
    const_reference back() const noexcept { return y; }
    reference       front() noexcept { return x; }
    const_reference front() const noexcept { return x; }

    reference       operator[](size_type __n) noexcept { return (&x)[__n]; }
    const_reference operator[](size_type __n) const noexcept { return (&x)[__n]; }

    pointer       data() noexcept { return &x; }
    const_pointer data() const noexcept { return &x; }

    // iterators:
    iterator       begin() noexcept { return iterator(&x); }
    const_iterator begin() const noexcept { return iterator(&x); }
    iterator       end() noexcept { return iterator(&x + size()); }
    const_iterator end() const noexcept { return iterator(&x + size()); }

    reverse_iterator       rbegin() noexcept { return reverse_iterator(end()); }
    const_reverse_iterator rbegin() const noexcept { return const_reverse_iterator(end()); }
    reverse_iterator       rend() noexcept { return reverse_iterator(begin()); }
    const_reverse_iterator rend() const noexcept { return const_reverse_iterator(begin()); }
};

// MARK:- Vector<3>
template <class _Tp>
struct Vector<_Tp, 3>
#if defined(ARRAYKIT_USE_VECTOR_EXPRESSION) && ARRAYKIT_USE_VECTOR_EXPRESSION
: public _VE<_Tp, Vector<_Tp, 3>>
#endif
{
    // capacity:
    static constexpr unsigned long size() noexcept { return 3; }
    static constexpr unsigned long max_size() noexcept { return size(); }

    // types:
    typedef Vector<_Tp, 1>                        __V1;
    typedef typename __V1::value_type             value_type;
    typedef typename __V1::reference              reference;
    typedef typename __V1::const_reference        const_reference;
    typedef typename __V1::iterator               iterator;
    typedef typename __V1::const_iterator         const_iterator;
    typedef typename __V1::pointer                pointer;
    typedef typename __V1::const_pointer          const_pointer;
    typedef typename __V1::size_type              size_type;
    typedef typename __V1::difference_type        difference_type;
    typedef typename __V1::reverse_iterator       reverse_iterator;
    typedef typename __V1::const_reverse_iterator const_reverse_iterator;

    value_type x, y, z;

    // constructors:
    constexpr explicit Vector() noexcept(noexcept(value_type())) = default;
    constexpr explicit Vector(value_type const &a) noexcept(noexcept(value_type(a)))
    : x(a), y(a), z(a) {}
    constexpr Vector(value_type const &a, value_type const &b, value_type const &c) noexcept(noexcept(value_type(a)))
    : x(a), y(b), z(c) {}

    constexpr Vector(Vector const &) noexcept(noexcept(value_type(x))) = default;
    Vector &operator=(Vector const &o) noexcept(noexcept(x = o.x)) = default;

#if defined(ARRAYKIT_USE_VECTOR_EXPRESSION) && ARRAYKIT_USE_VECTOR_EXPRESSION
    template <class E>
    Vector(_VE<value_type, E> const &e) noexcept(noexcept(value_type(e[0])))
    : x(e[0]), y(e[1]), z(e[2])
    {
    }
    template <class E>
    Vector &operator=(_VE<value_type, E> const &e) noexcept(noexcept(x = e[0]))
    {
        x = e[0];
        y = e[1];
        z = e[2];
        return *this;
    }
#endif

    // modifiers:
    void fill(value_type const &__u) noexcept(noexcept(x = __u)) { z = y = x = __u; }
    void swap(Vector &o) noexcept(noexcept(std::swap(x, o.x)))
    {
        std::swap(x, o.x);
        std::swap(y, o.y);
        std::swap(z, o.z);
    }

    // element access:
    reference       back() noexcept { return z; }
    const_reference back() const noexcept { return z; }
    reference       front() noexcept { return x; }
    const_reference front() const noexcept { return x; }

    reference       operator[](size_type __n) noexcept { return (&x)[__n]; }
    const_reference operator[](size_type __n) const noexcept { return (&x)[__n]; }

    pointer       data() noexcept { return &x; }
    const_pointer data() const noexcept { return &x; }

    // iterators:
    iterator       begin() noexcept { return iterator(&x); }
    const_iterator begin() const noexcept { return iterator(&x); }
    iterator       end() noexcept { return iterator(&x + size()); }
    const_iterator end() const noexcept { return iterator(&x + size()); }

    reverse_iterator       rbegin() noexcept { return reverse_iterator(end()); }
    const_reverse_iterator rbegin() const noexcept { return const_reverse_iterator(end()); }
    reverse_iterator       rend() noexcept { return reverse_iterator(begin()); }
    const_reverse_iterator rend() const noexcept { return const_reverse_iterator(begin()); }
};

// MARK:- Vector<4>
template <class _Tp>
struct Vector<_Tp, 4>
#if defined(ARRAYKIT_USE_VECTOR_EXPRESSION) && ARRAYKIT_USE_VECTOR_EXPRESSION
: public _VE<_Tp, Vector<_Tp, 4>>
#endif
{
    // capacity:
    static constexpr unsigned long size() noexcept { return 4; }
    static constexpr unsigned long max_size() noexcept { return size(); }

    // types:
    typedef Vector<_Tp, 1>                        __V1;
    typedef typename __V1::value_type             value_type;
    typedef typename __V1::reference              reference;
    typedef typename __V1::const_reference        const_reference;
    typedef typename __V1::iterator               iterator;
    typedef typename __V1::const_iterator         const_iterator;
    typedef typename __V1::pointer                pointer;
    typedef typename __V1::const_pointer          const_pointer;
    typedef typename __V1::size_type              size_type;
    typedef typename __V1::difference_type        difference_type;
    typedef typename __V1::reverse_iterator       reverse_iterator;
    typedef typename __V1::const_reverse_iterator const_reverse_iterator;

    value_type x, y, z, u;

    // constructors:
    constexpr explicit Vector() noexcept(noexcept(value_type())) = default;
    constexpr explicit Vector(value_type const &a) noexcept(noexcept(value_type(a)))
    : x(a), y(a), z(a), u(a) {}
    constexpr Vector(value_type const &a, value_type const &b, value_type const &c, value_type const &d) noexcept(noexcept(value_type(a)))
    : x(a), y(b), z(c), u(d) {}

    constexpr Vector(Vector const &) noexcept(noexcept(value_type(x))) = default;
    Vector &operator=(Vector const &o) noexcept(noexcept(x = o.x)) = default;

#if defined(ARRAYKIT_USE_VECTOR_EXPRESSION) && ARRAYKIT_USE_VECTOR_EXPRESSION
    template <class E>
    Vector(_VE<value_type, E> const &e) noexcept(noexcept(value_type(e[0])))
    : x(e[0]), y(e[1]), z(e[2]), u(e[3])
    {
    }
    template <class E>
    Vector &operator=(_VE<value_type, E> const &e) noexcept(noexcept(x = e[0]))
    {
        x = e[0];
        y = e[1];
        z = e[2];
        u = e[3];
        return *this;
    }
#endif

    // modifiers:
    void fill(value_type const &__u) noexcept(noexcept(x = __u)) { u = z = y = x = __u; }
    void swap(Vector &o) noexcept(noexcept(std::swap(x, o.x))) { std::swap_ranges(o.begin(), o.end(), begin()); }

    // element access:
    reference       back() noexcept { return u; }
    const_reference back() const noexcept { return u; }
    reference       front() noexcept { return x; }
    const_reference front() const noexcept { return x; }

    reference       operator[](size_type __n) noexcept { return (&x)[__n]; }
    const_reference operator[](size_type __n) const noexcept { return (&x)[__n]; }

    pointer       data() noexcept { return &x; }
    const_pointer data() const noexcept { return &x; }

    // iterators:
    iterator       begin() noexcept { return iterator(&x); }
    const_iterator begin() const noexcept { return iterator(&x); }
    iterator       end() noexcept { return iterator(&x + size()); }
    const_iterator end() const noexcept { return iterator(&x + size()); }

    reverse_iterator       rbegin() noexcept { return reverse_iterator(end()); }
    const_reverse_iterator rbegin() const noexcept { return const_reverse_iterator(end()); }
    reverse_iterator       rend() noexcept { return reverse_iterator(begin()); }
    const_reverse_iterator rend() const noexcept { return const_reverse_iterator(begin()); }
};

// MARK:- Vector<5>
template <class _Tp>
struct Vector<_Tp, 5>
#if defined(ARRAYKIT_USE_VECTOR_EXPRESSION) && ARRAYKIT_USE_VECTOR_EXPRESSION
: public _VE<_Tp, Vector<_Tp, 5>>
#endif
{
    // capacity:
    static constexpr unsigned long size() noexcept { return 5; }
    static constexpr unsigned long max_size() noexcept { return size(); }

    // types:
    typedef Vector<_Tp, 1>                        __V1;
    typedef typename __V1::value_type             value_type;
    typedef typename __V1::reference              reference;
    typedef typename __V1::const_reference        const_reference;
    typedef typename __V1::iterator               iterator;
    typedef typename __V1::const_iterator         const_iterator;
    typedef typename __V1::pointer                pointer;
    typedef typename __V1::const_pointer          const_pointer;
    typedef typename __V1::size_type              size_type;
    typedef typename __V1::difference_type        difference_type;
    typedef typename __V1::reverse_iterator       reverse_iterator;
    typedef typename __V1::const_reverse_iterator const_reverse_iterator;

    value_type x, y, z, u, v;

    // constructors:
    constexpr explicit Vector() noexcept(noexcept(value_type())) = default;
    constexpr explicit Vector(value_type const &a) noexcept(noexcept(value_type(a)))
    : x(a), y(a), z(a), u(a), v(a) {}
    constexpr Vector(value_type const &a, value_type const &b, value_type const &c, value_type const &d, value_type const &e) noexcept(noexcept(value_type(a)))
    : x(a), y(b), z(c), u(d), v(e) {}

    constexpr Vector(Vector const &) noexcept(noexcept(value_type(x))) = default;
    Vector &operator=(Vector const &o) noexcept(noexcept(x = o.x)) = default;

#if defined(ARRAYKIT_USE_VECTOR_EXPRESSION) && ARRAYKIT_USE_VECTOR_EXPRESSION
    template <class E>
    Vector(_VE<value_type, E> const &e) noexcept(noexcept(value_type(e[0])))
    : x(e[0]), y(e[1]), z(e[2]), u(e[3]), v(e[4])
    {
    }
    template <class E>
    Vector &operator=(_VE<value_type, E> const &e) noexcept(noexcept(x = e[0]))
    {
        x = e[0];
        y = e[1];
        z = e[2];
        u = e[3];
        v = e[4];
        return *this;
    }
#endif

    // modifiers:
    void fill(value_type const &__u) noexcept(noexcept(x = __u)) { v = u = z = y = x = __u; }
    void swap(Vector &o) noexcept(noexcept(std::swap(x, o.x))) { std::swap_ranges(o.begin(), o.end(), begin()); }

    // element access:
    reference       back() noexcept { return v; }
    const_reference back() const noexcept { return v; }
    reference       front() noexcept { return x; }
    const_reference front() const noexcept { return x; }

    reference       operator[](size_type __n) noexcept { return (&x)[__n]; }
    const_reference operator[](size_type __n) const noexcept { return (&x)[__n]; }

    pointer       data() noexcept { return &x; }
    const_pointer data() const noexcept { return &x; }

    // iterators:
    iterator       begin() noexcept { return iterator(&x); }
    const_iterator begin() const noexcept { return iterator(&x); }
    iterator       end() noexcept { return iterator(&x + size()); }
    const_iterator end() const noexcept { return iterator(&x + size()); }

    reverse_iterator       rbegin() noexcept { return reverse_iterator(end()); }
    const_reverse_iterator rbegin() const noexcept { return const_reverse_iterator(end()); }
    reverse_iterator       rend() noexcept { return reverse_iterator(begin()); }
    const_reverse_iterator rend() const noexcept { return const_reverse_iterator(begin()); }
};

// MARK:- Vector<6>
template <class _Tp>
struct Vector<_Tp, 6>
#if defined(ARRAYKIT_USE_VECTOR_EXPRESSION) && ARRAYKIT_USE_VECTOR_EXPRESSION
: public _VE<_Tp, Vector<_Tp, 6>>
#endif
{
    // capacity:
    static constexpr unsigned long size() noexcept { return 6; }
    static constexpr unsigned long max_size() noexcept { return size(); }

    // types:
    typedef Vector<_Tp, 1>                        __V1;
    typedef typename __V1::value_type             value_type;
    typedef typename __V1::reference              reference;
    typedef typename __V1::const_reference        const_reference;
    typedef typename __V1::iterator               iterator;
    typedef typename __V1::const_iterator         const_iterator;
    typedef typename __V1::pointer                pointer;
    typedef typename __V1::const_pointer          const_pointer;
    typedef typename __V1::size_type              size_type;
    typedef typename __V1::difference_type        difference_type;
    typedef typename __V1::reverse_iterator       reverse_iterator;
    typedef typename __V1::const_reverse_iterator const_reverse_iterator;

    value_type x, y, z, u, v, w;

    // constructors:
    constexpr explicit Vector() noexcept(noexcept(value_type())) = default;
    constexpr explicit Vector(value_type const &a) noexcept(noexcept(value_type(a)))
    : x(a), y(a), z(a), u(a), v(a), w(a) {}
    constexpr Vector(value_type const &a, value_type const &b, value_type const &c, value_type const &d, value_type const &e, value_type const &f) noexcept(noexcept(value_type(a)))
    : x(a), y(b), z(c), u(d), v(e), w(f) {}

    constexpr Vector(Vector const &) noexcept(noexcept(value_type(x))) = default;
    Vector &operator=(Vector const &o) noexcept(noexcept(x = o.x)) = default;

#if defined(ARRAYKIT_USE_VECTOR_EXPRESSION) && ARRAYKIT_USE_VECTOR_EXPRESSION
    template <class E>
    Vector(_VE<value_type, E> const &e) noexcept(noexcept(value_type(e[0])))
    : x(e[0]), y(e[1]), z(e[2]), u(e[3]), v(e[4]), w(e[5])
    {
    }
    template <class E>
    Vector &operator=(_VE<value_type, E> const &e) noexcept(noexcept(x = e[0]))
    {
        x = e[0];
        y = e[1];
        z = e[2];
        u = e[3];
        v = e[4];
        w = e[5];
        return *this;
    }
#endif

    // modifiers:
    void fill(value_type const &__u) noexcept(noexcept(x = __u)) { w = v = u = z = y = x = __u; }
    void swap(Vector &o) noexcept(noexcept(std::swap(x, o.x))) { std::swap_ranges(o.begin(), o.end(), begin()); }

    // element access:
    reference       back() noexcept { return w; }
    const_reference back() const noexcept { return w; }
    reference       front() noexcept { return x; }
    const_reference front() const noexcept { return x; }

    reference       operator[](size_type __n) noexcept { return (&x)[__n]; }
    const_reference operator[](size_type __n) const noexcept { return (&x)[__n]; }

    pointer       data() noexcept { return &x; }
    const_pointer data() const noexcept { return &x; }

    // iterators:
    iterator       begin() noexcept { return iterator(&x); }
    const_iterator begin() const noexcept { return iterator(&x); }
    iterator       end() noexcept { return iterator(&x + size()); }
    const_iterator end() const noexcept { return iterator(&x + size()); }

    reverse_iterator       rbegin() noexcept { return reverse_iterator(end()); }
    const_reverse_iterator rbegin() const noexcept { return const_reverse_iterator(end()); }
    reverse_iterator       rend() noexcept { return reverse_iterator(begin()); }
    const_reverse_iterator rend() const noexcept { return const_reverse_iterator(begin()); }

    // symmetric/anti-symmetric tensor elements:
    reference xx() noexcept { return x; }
    reference yy() noexcept { return y; }
    reference zz() noexcept { return z; }
    reference xy() noexcept { return u; }
    reference yz() noexcept { return v; }
    reference zx() noexcept { return w; }

    const_reference xx() const noexcept { return x; }
    const_reference yy() const noexcept { return y; }
    const_reference zz() const noexcept { return z; }
    const_reference xy() const noexcept { return u; }
    const_reference yz() const noexcept { return v; }
    const_reference zx() const noexcept { return w; }

    // lo/hi:
    typedef Vector<value_type, size() / 2> half_vector;

    half_vector &lo() noexcept { return *reinterpret_cast<half_vector *>(data() + 0); }
    half_vector &hi() noexcept { return *reinterpret_cast<half_vector *>(data() + half_vector::size()); }

    half_vector const &lo() const noexcept { return *reinterpret_cast<half_vector const *>(data() + 0); }
    half_vector const &hi() const noexcept { return *reinterpret_cast<half_vector const *>(data() + half_vector::size()); }
};

// MARK:- Vector<N>
template <class _Tp, long _Size>
struct Vector
#if defined(ARRAYKIT_USE_VECTOR_EXPRESSION) && ARRAYKIT_USE_VECTOR_EXPRESSION
: public _VE<_Tp, Vector<_Tp, _Size>>
#endif
{
    static_assert(_Size > 0, "invalid vector size");

    // capacity:
    static constexpr unsigned long size() noexcept { return _Size; }
    static constexpr unsigned long max_size() noexcept { return size(); }

    // types:
    typedef Vector<_Tp, 1>                        __V1;
    typedef typename __V1::value_type             value_type;
    typedef typename __V1::reference              reference;
    typedef typename __V1::const_reference        const_reference;
    typedef typename __V1::iterator               iterator;
    typedef typename __V1::const_iterator         const_iterator;
    typedef typename __V1::pointer                pointer;
    typedef typename __V1::const_pointer          const_pointer;
    typedef typename __V1::size_type              size_type;
    typedef typename __V1::difference_type        difference_type;
    typedef typename __V1::reverse_iterator       reverse_iterator;
    typedef typename __V1::const_reverse_iterator const_reverse_iterator;

    std::array<_Tp, _Size> _a;

    // constructors:
    constexpr explicit Vector() noexcept(noexcept(decltype(_a){})) = default;
    explicit Vector(value_type const &x) noexcept(noexcept(decltype(_a){}) &&noexcept(_a.fill(x))) { _a.fill(x); }

    constexpr Vector(Vector const &) noexcept(noexcept(decltype(_a)(_a))) = default;
    Vector &operator=(Vector const &o) noexcept(noexcept(_a = o._a)) = default;

#if defined(ARRAYKIT_USE_VECTOR_EXPRESSION) && ARRAYKIT_USE_VECTOR_EXPRESSION
    template <class E>
    Vector(_VE<value_type, E> const &e) noexcept(noexcept(decltype(_a){}) &&noexcept(_a[0] = e[0]))
    {
        for (size_type i = 0; i < size(); ++i) {
            _a[i] = e[i];
        }
    }
    template <class E>
    Vector &operator=(_VE<value_type, E> const &e) noexcept(noexcept(_a[0] = e[0]))
    {
        for (size_type i = 0; i < size(); ++i) {
            _a[i] = e[i];
        }
        return *this;
    }
#endif

    // modifiers:
    void fill(value_type const &x) noexcept(noexcept(_a.fill(x))) { _a.fill(x); }
    void swap(Vector &o) noexcept(noexcept(std::swap(_a, o._a))) { std::swap(_a, o._a); }

    // element access:
    reference       back() noexcept { return _a.back(); }
    const_reference back() const noexcept { return _a.back(); }
    reference       front() noexcept { return _a.front(); }
    const_reference front() const noexcept { return _a.front(); }

    reference       operator[](size_type __n) noexcept { return _a[__n]; }
    const_reference operator[](size_type __n) const noexcept { return _a[__n]; }

    pointer       data() noexcept { return _a.data(); }
    const_pointer data() const noexcept { return _a.data(); }

    // iterators:
    iterator       begin() noexcept { return iterator(data()); }
    const_iterator begin() const noexcept { return iterator(data()); }
    iterator       end() noexcept { return iterator(data() + size()); }
    const_iterator end() const noexcept { return iterator(data() + size()); }

    reverse_iterator       rbegin() noexcept { return reverse_iterator(end()); }
    const_reverse_iterator rbegin() const noexcept { return const_reverse_iterator(end()); }
    reverse_iterator       rend() noexcept { return reverse_iterator(begin()); }
    const_reverse_iterator rend() const noexcept { return const_reverse_iterator(begin()); }
};

UTILITYKIT_END_NAMESPACE

// MARK: std specializations
namespace std {
template <class T, long Size>
void swap(UTILITYKIT_NAMESPACE::Vector<T, Size> &a,
          UTILITYKIT_NAMESPACE::Vector<T, Size> &b)
{
    a.swap(b);
}

template <unsigned long I, class T, long Size>
constexpr typename std::enable_if<Size <= 6, T>::type &get(UTILITYKIT_NAMESPACE::Vector<T, Size> &v)
{
    static_assert(I >= 0 && I < Size, "Index out-of-bound");
    return (&v.x)[I];
}
template <unsigned long I, class T, long Size>
constexpr typename std::enable_if<Size <= 6, T>::type const &get(UTILITYKIT_NAMESPACE::Vector<T, Size> const &v)
{
    static_assert(I >= 0 && I < Size, "Index out-of-bound");
    return (&v.x)[I];
}

template <unsigned long I, class T, long Size>
constexpr typename std::enable_if<Size >= 7, T>::type &get(UTILITYKIT_NAMESPACE::Vector<T, Size> &v)
{
    static_assert(I >= 0 && I < Size, "Index out-of-bound");
    return std::get<I>(v._a);
}
template <unsigned long I, class T, long Size>
constexpr typename std::enable_if<Size >= 7, T>::type const &get(UTILITYKIT_NAMESPACE::Vector<T, Size> const &v)
{
    static_assert(I >= 0 && I < Size, "Index out-of-bound");
    return std::get<I>(v._a);
}
} // namespace std
