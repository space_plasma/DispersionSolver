/*
 * Copyright (c) 2015-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "Mutex.h"
#include "printo.h"

// MARK: Version 3
//
#include <iostream>
#include <mutex>
#include <sched.h>
#include <stdexcept>
#include <string>
#include <sys/time.h>

UTILITYKIT_BEGIN_NAMESPACE

// MARK: Mutex
//
Mutex::~Mutex()
try {
    if (pthread_mutex_destroy(&__m_)) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + "pthread_mutex_destroy(...) returned error");
    }
} catch (std::exception &e) {
    printo(std::cerr, e.what(), '\n');
    std::terminate();
}

Mutex::Mutex()
try {
    if (pthread_mutex_init(&__m_, nullptr)) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + "pthread_mutex_init(...) returned error");
    }
} catch (std::exception &) {
    throw;
}

void Mutex::lock()
{
    if (pthread_mutex_lock(&__m_)) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + "pthread_mutex_lock(...) returned error");
    }
}
bool Mutex::try_lock() noexcept
{
    return 0 == pthread_mutex_trylock(&__m_); // lock was aquired
}
void Mutex::unlock() noexcept
try {
    if (pthread_mutex_unlock(&__m_)) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + "pthread_mutex_unlock(...) returned error");
    }
} catch (std::exception &e) {
    printo(std::cerr, e.what(), '\n');
    // throw;
}

// MARK: SpinLock
//
void SpinLock::lock() noexcept
{
    while (__s_.test_and_set(std::memory_order_acquire)) {
        // sched_yield();
    }
}
bool SpinLock::try_lock() noexcept
{
    return __s_.test_and_set(std::memory_order_acquire);
}
void SpinLock::unlock() noexcept
{
    __s_.clear(std::memory_order_release);
}

// MARK: CondV<Mutex>
//
CondV<Mutex>::~CondV()
try {
    if (pthread_cond_destroy(&__c_)) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + "pthread_cond_destroy(...) returned error");
    }
} catch (std::exception &e) {
    printo(std::cerr, e.what(), '\n');
    std::terminate();
}

CondV<Mutex>::CondV()
try {
    if (pthread_cond_init(&__c_, nullptr)) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + "pthread_cond_init(...) returned error");
    }
} catch (std::exception &) {
    throw;
}

// notifications:
void CondV<Mutex>::notify_one() noexcept
try {
    if (pthread_cond_signal(&__c_)) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + "pthread_cond_signal(...) returned error");
    }
} catch (std::exception &e) {
    printo(std::cerr, e.what(), '\n');
    // throw;
}
void CondV<Mutex>::notify_all() noexcept
try {
    if (pthread_cond_broadcast(&__c_)) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + "pthread_cond_broadcast(...) returned error");
    }
} catch (std::exception &e) {
    printo(std::cerr, e.what(), '\n');
    // throw;
}

// waiting:
void CondV<Mutex>::wait(lock_type &__mx)
{
    if (pthread_cond_wait(&__c_, __mx.native_handle())) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + "pthread_cond_wait(...) returned error");
    }
}

auto CondV<Mutex>::wait_for(lock_type &__mx, unsigned __dur_in_sec) -> timed_status
{
    // calculate abstime in future
    struct timespec ts {
        0, 0
    };
    {
        struct timeval tv;
        if (gettimeofday(&tv, nullptr)) {
            throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + "gettimeofday(...) returned error");
        }
        ts.tv_sec = tv.tv_sec + __dur_in_sec;
    }

    // timed wait
    int ret;
    if ((ret = pthread_cond_timedwait(&__c_, __mx.native_handle(), &ts))) {
        return ETIMEDOUT == ret ? timeout
                                : throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + "pthread_cond_timedwait(...) returned error");
    }
    return no_timeout;
}

// MARK: CondV<SpinLock>
//
CondV<SpinLock>::~CondV()
try {
    LockG<lock_type> l(_master_key);
    if (!_wait_list.empty()) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + "there are threads still waiting");
    }
} catch (std::exception &e) {
    printo(std::cerr, e.what(), '\n');
    // std::terminate();
}

CondV<SpinLock>::CondV()
: _wait_list(), _master_key()
{
}

// notifications:
void CondV<SpinLock>::notify_one() noexcept
{
    LockG<lock_type> l(_master_key);
    if (!_wait_list.empty()) {
        _wait_list.back()->unlock();
        _wait_list.pop_back();
    }
}
void CondV<SpinLock>::notify_all() noexcept
{
    LockG<lock_type> l(_master_key);
    for (lock_type *lock : _wait_list) {
        lock->unlock();
    }
    _wait_list.clear();
}

// waiting:
void CondV<SpinLock>::wait(lock_type &mx)
{
    lock_type wait(SpinLock::Locked);
    {
        LockG<lock_type> _(_master_key); // first, acquire master lock
        mx.unlock();                     // then, release mutex
        _wait_list.push_back(&wait);     // be wait-listed
        // release master key
    }
    wait.lock(); // acquire wait lock
    mx.lock();   // acquire mutex
}

UTILITYKIT_END_NAMESPACE
