/*
 * Copyright (c) 2017-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <UtilityKit/UtilityKit-config.h>

// MARK: Version 3
//
#include <cmath>
#include <complex> // This should be preceed the definition, otherwise std::abs are not specialized for std::complex.
#include <type_traits>
#include <utility>

UTILITYKIT_BEGIN_NAMESPACE

/**
 @brief Trapezoidal integration algorithm.
 */
template <class X, class Y = X>
struct TrapzIntegrator {
    static_assert(std::is_floating_point<X>::value, "The abscissa type should be of a floating point type");

    unsigned accuracyGoal{ 7 };  //!< Accuracy tolerence which is 10^-accuracyGoal.
    unsigned maxRecursion{ 20 }; //!< Maximum number of bisection.
    unsigned minRecursion{ 5 };  //!< Minimum number of bisection.

    /**
     @brief Trapezoidal integration.
     @param f A function of type Y(X) to integrate. It should return a finite number convertible to Y.
     @param a Lower limit.
     @param b Upper limit.
     @param norm A function that returns a norm of the updated value, which is used for convergence test. The function signature is X(Y).
     @return A pair of integrated value and number of recursions.
     If not converged, the returned number of recursions are the same as maxRecursion.
     */
    template <class F /*Y(X)*/, class Norm /*X(Y)*/>
    std::pair<Y, unsigned> qtrap(F f, X const a, X const b, Norm norm) const
    {
        X const  tolerance = std::pow(X{ 10 }, -X(accuracyGoal));
        Y        s;
        unsigned n, i;
        std::tie(s, n) = _trapzd(f, a, b);
        for (i = 1; i < minRecursion; ++i) {
            std::tie(s, n) = _trapzd(f, a, b, s, n);
        }
        for (; i <= maxRecursion; ++i) {
            Y const sold   = s;
            std::tie(s, n) = _trapzd(f, a, b, s, n);

            if (norm(sold - s) < norm(sold) * tolerance) {
                return { s, n };
            }
        }

        // not converged
        return { s, n };
    }
    /**
     @brief Trapezoidal integration with simpson's rule.
     @param f A function of type Y(X) to integrate. It should return a finite number convertible to Y.
     @param a Lower limit.
     @param b Upper limit.
     @param norm A function that returns a norm of the updated value, which is used for convergence test. The function signature is X(Y).
     @return A pair of integrated value and number of recursions.
     If not converged, the returned number of recursions are the same as maxRecursion.
     */
    template <class F /*Y(X)*/, class Norm /*X(Y)*/>
    std::pair<Y, unsigned> qsimp(F f, X const a, X const b, Norm norm) const
    {
        X const  tolerance = std::pow(X{ 10 }, -X(accuracyGoal));
        Y        st;
        unsigned n, i;
        std::tie(st, n) = _trapzd(f, a, b);
        Y s             = st;
        for (i = 1; i < minRecursion; ++i) {
            std::tie(st, n) = _trapzd(f, a, b, s, n);
            s               = (X(4.) * st - s) / X(3.);
        }
        for (; i <= maxRecursion; ++i) {
            Y const sold    = s;
            std::tie(st, n) = _trapzd(f, a, b, s, n);
            s               = (X(4.) * st - s) / X(3.);

            if (norm(sold - s) < norm(sold) * tolerance) {
                return { s, n };
            }
        }

        // not converged
        return { s, n };
    }

    /**
     @brief Trapezoidal integration.
     @param f A function of type Y(X) to integrate. It should return a finite number convertible to Y.
     @param a Lower limit.
     @param b Upper limit.
     @return A pair of integrated value and number of recursions.
     If not converged, the returned number of recursions are the same as maxRecursion.
     */
    template <class F /*Y(X)*/>
    std::pair<Y, unsigned> qtrap(F f, X const a, X const b) const
    {
        return qtrap<F const &>(f, a, b, &std::abs<X>);
    }
    /**
     @brief Trapezoidal integration with simpson's rule.
     @param f A function of type Y(X) to integrate. It should return a finite number convertible to Y.
     @param a Lower limit.
     @param b Upper limit.
     @return A pair of integrated value and number of recursions.
     If not converged, the returned number of recursions are the same as maxRecursion.
     */
    template <class F /*Y(X)*/>
    std::pair<Y, unsigned> qsimp(F f, X const a, X const b) const
    {
        return qsimp<F const &>(f, a, b, &std::abs<X>);
    }

    /**
     @brief Shorthand for qsimp(f, a, b).
     */
    template <class F /*Y(X)*/>
    std::pair<Y, unsigned> operator()(F f, X const a, X const b) const { return qsimp<F const &>(f, a, b); }

    /**
     @brief Shorthand for qsimp(f, a, b, test).
     */
    template <class F /*Y(X)*/, class Norm /*X(Y)*/>
    std::pair<Y, unsigned> operator()(F f, X const a, X const b, Norm norm) const { return qsimp<F const &, Norm const &>(f, a, b, norm); }

    /**
     @brief Simple trapezoidal sum.
     @param x_first The beginning of the abscissa iterator. The value type should be convertible to X.
     @param x_last The end of the abscissa iterator.
     @param y_first The beginning of the ordinate iterator. The value type should be convertible to Y.
     @return Integrated value of type Y.
     */
    template <class XIt, class YIt>
    static Y trapzd(XIt x_first, XIt x_last, YIt y_first)
    {
        Y sum(X(0.));
        if (x_first != x_last) {
            XIt x1 = x_first, x0 = x1++;
            YIt y1 = y_first, y0 = y1++;
            for (; x1 != x_last; x0 = x1++, y0 = y1++) {
                sum += _trapzd(*x0, *y0, *x1, *y1);
            }
        }
        return sum;
    }

private:
    static Y _trapzd(X const x0, Y const &y0, X const x1, Y const &y1)
    {
        return X(.5) * (x1 - x0) * (y0 + y1);
    }
    template <class F>
    static std::pair<Y, unsigned> _trapzd(F const &f, X const a, X const b)
    {
        return { _trapzd(a, f(a), b, f(b)), 0 };
    }
    template <class F>
    static std::pair<Y, unsigned> _trapzd(F const &f, X const a, X const b, Y const &sold, unsigned const _n)
    {
        unsigned const n = 01 << _n;
        Y              sumf(X(0.));
        X const        dx = (b - a) / X(n);
        for (unsigned i = 0; i < n; ++i) {
            X const x = a + (X(i) + X(.5)) * dx;
            sumf += f(x);
        }

        return { X(.5) * (sold + dx * sumf), _n + 1 };
    }
};

UTILITYKIT_END_NAMESPACE
