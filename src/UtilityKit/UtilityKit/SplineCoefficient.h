/*
 * Copyright (c) 2017-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <UtilityKit/UtilityKit-config.h>

// MARK: Version 3
//
#include <UtilityKit/Optional.h>
#include <algorithm>
#include <array>
#include <cmath>
#include <limits>
#include <map>
#include <numeric>
#include <stdexcept>
#include <type_traits>
#include <utility>
#include <vector>

UTILITYKIT_BEGIN_NAMESPACE

// MARK: Forward Declarations
//
template <class RealType, long Order>
class SplineInterpolator;
template <class RealType, long Order>
class SplineCoefficient;
template <class RealType>
class LinearSplineCoefficient;
template <class RealType>
class CubicSplineCoefficient;
template <class RealType>
class MonotoneCubicCoefficient;

// MARK: SplineCoefficient
//
/**
 @brief Encapsulation of nth order spline coefficients.
 @discussion Contains abscissa values and nth order spline coefficients in a std::vector container.
 Each element of the vector is a pair of an abscissa value and an array of n+1 element polynomial coefficients.
 The first element of this coefficient array is the ordinate value corresponding to the abscissa value.

 If p+1 points are given to the constructor, the vector container will have p+1 entries.
 However, interpolation uses only the first p entries.
 The last entry of the container only saves the abscissa and ordinate of the p+1'th point.

 When constructed, any duplicate abscissa points are removed, and the input points are stored in an ascending order.

 Use one of the concrete subclasses for construction.
 */
template <class RealType, long Order>
class SplineCoefficient {
    static_assert(std::is_floating_point<RealType>::value, "The template parameter should be a floating point type.");
    static_assert(Order >= 1, "The spline order should be a positive integer.");

public:
    // types:
    using value_type               = RealType;
    using coefficient_type         = std::array<value_type, Order + 1>; //!< The first element is the 0th order coefficient.
    using coefficient_table_type   = std::vector<std::pair<value_type, coefficient_type>>;
    using spline_interpolator_type = SplineInterpolator<RealType, Order>;

public:
    /**
     @brief Polynomial order of the spline segments.
     */
    constexpr static unsigned spline_order() noexcept { return Order; }

    /**
     @brief Query if the receiver is valid.
     */
    explicit operator bool() const noexcept { return !_table.empty(); }

    /**
     @brief Check whether abscissa values are regularly spaced.
     Effectively, bool(delta()).
     */
    bool is_regular_grid() const noexcept { return bool(_delta); }

    /**
     @brief Return polynomial coefficients as an array of a pair of abscissa and coefficients.
     Sorted in ascending order.
     */
    coefficient_table_type const &table() const noexcept { return _table; }

    /**
     @brief If the abscissa values are regularly spaced, returns an optional wrapping the interval.
     Otherwise, return a nil optional.
     */
    Optional<value_type> const &delta() const noexcept { return _delta; }

    /**
     @brief Minimum abscissa value.
     */
    value_type const &min_abscissa() const noexcept { return _table.front().first; }
    /**
     @brief Maximum abscissa value.
     */
    value_type const &max_abscissa() const noexcept { return _table.back().first; }

    /**
     @brief Return an order-1 SplineCoefficient instance representing a first order derivative.
     @exception An exception is thrown if spline_order() call to the receiver returns 1.
     */
    // SplineCoefficient<RealType, Order-1> derivative() const;

    /**
     @brief Integrate the approximating function in the entire interval.
     */
    value_type integrate() const;

    /**
     @brief Returns an interpolator instance.
     @discussion The receiver is copied.
     */
    spline_interpolator_type interpolator() const & { return spline_interpolator_type(*this); }
    /**
     @brief Returns an interpolator instance.
     @discussion The receiver is moved and becomes invalid.
     */
    spline_interpolator_type interpolator() && { return spline_interpolator_type(std::move(*this)); }

    /**
     @brief Swap the receiver's content with other's.
     */
    void swap(SplineCoefficient &other) { _table.swap(other._table), _delta.swap(other._delta); }

    /**
     @brief Default constructor.
     @discussion The state is invalid.
     */
    explicit SplineCoefficient() noexcept = default;

    // copy/move:
    SplineCoefficient(SplineCoefficient const &) = default;
    SplineCoefficient(SplineCoefficient &&)      = default;
    SplineCoefficient &operator=(SplineCoefficient const &) = default;
    SplineCoefficient &operator=(SplineCoefficient &&) = default;

protected:
    Optional<value_type>   _delta{};
    coefficient_table_type _table{};

    template <class It>
    SplineCoefficient(It first, It last)
    {
        // Copy data
        // Use std::map to remove duplicate points, and automatically sort
        // At least size == Order+1 (note that last is not used, so effectively 1)
        //
        std::map<value_type, coefficient_type> table;
        for (; first != last; ++first) {
            coefficient_type &c = table[first->first];
            c.fill(std::numeric_limits<RealType>::quiet_NaN());
            std::get<0>(c) = first->second;
        }
        _init_common(table);
    }
    template <class It1, class It2>
    SplineCoefficient(It1 first1, It1 last1, It2 first2)
    {
        // Copy data
        // Use std::map to remove duplicate points, and automatically sort
        // At least size == Order+1 (note that last is not used, so effectively 1)
        //
        std::map<value_type, coefficient_type> table;
        while (first1 != last1) {
            coefficient_type &c = table[*first1++];
            c.fill(std::numeric_limits<RealType>::quiet_NaN());
            std::get<0>(c) = *first2++;
        }
        _init_common(table);
    }

private:
    void _init_common(std::map<value_type, coefficient_type> const &map_table)
    {
        if (map_table.size() < 2)
            throw std::invalid_argument(__PRETTY_FUNCTION__); // at least two entries are needed

        coefficient_table_type &vec_table = _table;
        // Copy to vector
        //
        vec_table.resize(map_table.size());
        std::copy(map_table.begin(), map_table.end(), vec_table.begin());

        // Check if regularly spaced
        //
        constexpr value_type tolerance = static_cast<value_type>(1e-10);
        auto                 _1 = vec_table.begin(), _0 = _1++;
        value_type const     d0         = _1->first - _0->first;
        bool                 is_regular = true;
        for (auto const end = vec_table.end(); _1 != end; ++_1, ++_0) {
            value_type const d1 = std::get<1>(_0->second) = _1->first - _0->first;
            is_regular &= std::abs(d1 - d0) < tolerance;
        }
        if (is_regular) {
            _delta = d0;
        }
        // After this call, array[0] is filled with ys, and array[1] is filled with x_j+1 - x_j.
        // Other elements are filled with nan.
    }

    static value_type _integrate_helper(coefficient_type const &coef, value_type const d)
    {
        value_type result(0.);
        for (unsigned i = 0; i < coef.size(); ++i) {
            result += coef[i] * d / value_type(i + 1);
        }
        return result;
    }
};

template <class RealType, long Order>
auto SplineCoefficient<RealType, Order>::integrate() const -> value_type
{
    auto       _1 = _table.begin(), _0 = _1++;
    value_type result(0.);
    for (auto const last = _table.end(); _1 != last; ++_0, ++_1) {
        result += _integrate_helper(_0->second, _1->first - _0->first);
    }
    return result;
}

// MARK: LinearSplineCoefficient
//
/**
 @brief Construct linear (1st order) spline coefficients.
 */
template <class RealType>
class LinearSplineCoefficient : public SplineCoefficient<RealType, 1> {
    using Base = SplineCoefficient<RealType, 1>;

public:
    template <class It>
    LinearSplineCoefficient(It first, It last)
    : Base(first, last)
    {
        _init_common();
    }
    template <class It1, class It2>
    LinearSplineCoefficient(It1 first1, It1 last1, It2 first2)
    : Base(first1, last1, first2)
    {
        _init_common();
    }

private:
    void _init_common()
    {
        // S_j(x) = y_j + (y_j+1 - y_j)*t, where t = (x - x_j)/(x_j+1 - x_j).
        //
        auto _1 = Base::_table.begin(), _0 = _1++;
        for (auto const end = Base::_table.end(); _1 != end; ++_1, ++_0) {
            std::get<1>(_0->second) = std::get<0>(_1->second) - std::get<0>(_0->second);
        }
    }
};

// MARK: CubicSplineCoefficient
//
/**
 @brief Construct cubic (3rd order) spline coefficients.
 Implementation from Numerical Recipe.
 */
template <class RealType>
class CubicSplineCoefficient : public SplineCoefficient<RealType, 3> {
    using Base = SplineCoefficient<RealType, 3>;

public:
    template <class It>
    CubicSplineCoefficient(It first, It last)
    : Base(first, last)
    {
        _init_common();
    }
    template <class It1, class It2>
    CubicSplineCoefficient(It1 first1, It1 last1, It2 first2)
    : Base(first1, last1, first2)
    {
        _init_common();
    }

private:
    void _init_common()
    {
        constexpr RealType                     zero(0.), one(1.), two(2.), three(3.), _1_3 = one / three, _1_6 = one / (two * three);
        typename Base::coefficient_table_type &table = Base::_table;
        if (table.size() < 3)
            throw std::invalid_argument(__PRETTY_FUNCTION__); // at least three entries are needed

        // Get y_j'' using a natural boundary condition
        // In coefficient array, {y_j, M(j,j), M(j,j+1), b_j}
        // Upon solving tridiag, {y_j, _, _, y_j''}
        //
        {
            // solve tridiag
            typename Base::coefficient_table_type tmp_table(table.size() - 2);
            for (unsigned long i = 0, n = tmp_table.size(); i < n; ++i) {
                typename Base::coefficient_type &c  = tmp_table[i].second;
                RealType const                   y0 = std::get<0>(table[i + 0].second), y1 = std::get<0>(table[i + 1].second), y2 = std::get<0>(table[i + 2].second);
                RealType const                   d0 = std::get<1>(table[i + 0].second), d1 = std::get<1>(table[i + 1].second);
                std::get<3>(c) = (y2 - y1) / d1 - (y1 - y0) / d0;
                std::get<0>(c) = d0 * _1_6;
                std::get<1>(c) = (d1 + d0) * _1_3;
                std::get<2>(c) = d1 * _1_6;
            }
            _solve_tridiag(tmp_table);
            // copy back results
            for (unsigned long i = 0, n = tmp_table.size(); i < n; ++i) {
                std::get<3>(table[i + 1].second) = std::get<3>(tmp_table[i].second);
            }
            // put boundary y_j'':
            std::get<3>(table.front().second) = zero;
            std::get<3>(table.back().second)  = zero;
        }

        // Get coefficients
        // Note that the first element is already filled.
        //
        {
            auto _1 = table.begin(), _0 = _1++;
            for (auto const last = table.end(); _1 != last; ++_0, ++_1) {
                typename Base::coefficient_type &c0 = _0->second, &c1 = _1->second;
                RealType const                   dx = _1->first - _0->first;
#if 1 // normalized; t = (x - x_j)/(x_j + 1-x_j)
                RealType const dx2 = dx * dx;
                std::get<1>(c0)    = (c1.front() - c0.front()) - dx2 * (two * c0.back() + c1.back()) * _1_6;
                std::get<2>(c0)    = dx2 * c0.back() / two;
                std::get<3>(c0)    = dx2 * (c1.back() - c0.back()) * _1_6;
#else // non-normalized; t = (x - x_j)
                std::get<1>(c0) = (c1.front() - c0.front()) / dx - dx * (two * c0.back() + c1.back()) * _1_6;
                std::get<2>(c0) = c0.back() / two;
                std::get<3>(c0) = (c1.back() - c0.back()) / dx * _1_6;
#endif
            }
            // Note that the last entry is not filled except c[0].
        }
    }

    // Tridiagonal linear solver (in-place):
    static void _solve_tridiag(typename Base::coefficient_table_type &table)
    {
        if (table.empty())
            return;

        // Decomposition and forward substitution:
        // In in-place operation, {y_j, M(j,j), M(j,j+1), b_j} -> {y_j, gam, M(j,j+1), y_j''}
        //
        constexpr RealType zero(0.);
        {
            RealType bet, gam;
            auto     _1 = table.begin(), _0 = _1++;
            std::get<3>(_0->second) /= (bet = std::get<1>(_0->second));
            for (auto const last = table.end(); _1 != last; ++_0, ++_1) {
                gam                     = std::get<2>(_0->second) / bet;
                bet                     = std::get<1>(_1->second) - std::get<0>(_1->second) * gam;
                std::get<1>(_1->second) = gam;
                if (zero == bet) {
                    throw std::runtime_error(__PRETTY_FUNCTION__);
                }
                std::get<3>(_1->second) = (std::get<3>(_1->second) - std::get<0>(_1->second) * std::get<3>(_0->second)) / bet;
            }
        }

        // Backsubstitution:
        {
            auto _1 = table.rbegin(), _0 = _1++;
            for (auto const last = table.rend(); _1 != last; ++_0, ++_1) {
                std::get<3>(_1->second) -= std::get<1>(_0->second) * std::get<3>(_0->second);
            }
        }
    }
};

// MARK: MonotoneCubicCoefficient
//
/**
 @brief Construct monotone cubic (3rd order) spline coefficients.
 Implementation from http://en.wikipedia.org/wiki/Monotone_cubic_interpolation#Example_implementation.
 */
template <class RealType>
class MonotoneCubicCoefficient : public SplineCoefficient<RealType, 3> {
    using Base = SplineCoefficient<RealType, 3>;

public:
    template <class It>
    MonotoneCubicCoefficient(It first, It last)
    : Base(first, last)
    {
        _init_common();
    }
    template <class It1, class It2>
    MonotoneCubicCoefficient(It1 first1, It1 last1, It2 first2)
    : Base(first1, last1, first2)
    {
        _init_common();
    }

private:
    void _init_common()
    {
        constexpr RealType                     zero(0.0) /*, one(1.0)*/, three(3.0);
        typename Base::coefficient_table_type &table = Base::_table;
        if (table.size() < 3)
            throw std::invalid_argument(__PRETTY_FUNCTION__); // at least three entries are needed

        // Get consecutive differences and slopes
        // Initial coefficient array, {y_j, dx_j, _, _}
        // Upon this operation, {y_j, dy_j, dx_j, m}
        //
        {
            auto _1 = table.begin(), _0 = _1++;
            for (auto const last = table.end(); _1 != last; ++_0, ++_1) {
                RealType const                   d1 = std::get<1>(_0->second);
                RealType const                   y1 = std::get<0>(_0->second), y2 = std::get<0>(_1->second);
                typename Base::coefficient_type &c = _0->second;
                std::get<1>(c)                     = y2 - y1;                         // dy
                std::get<2>(c)                     = d1;                              // dx
                std::get<3>(c)                     = std::get<1>(c) / std::get<2>(c); // m=dy/dx
            }
        }

        // Get degree-1 coefficients
        // Upon this operation, {y, c1, dx, m}
        //
        {
            auto _1 = table.begin(), _0 = _1++, last = table.end();
            std::get<1>(_0->second) = std::get<3>(_0->second); // boundary
            for (--last; _1 != last; ++_0, ++_1) {
                const RealType m = std::get<3>(_0->second), mNext = std::get<3>(_1->second);
                if (m * mNext <= zero) { // FIXME: Need to include some tolerance.
                    std::get<1>(_1->second) = zero;
                } else {
                    const RealType dx = std::get<2>(_0->second), dxNext = std::get<2>(_1->second), common = dx + dxNext;
                    std::get<1>(_1->second) = (common * m * mNext * three) / (dx * m + dxNext * mNext + common * (m + mNext)); // three*common / ((common + dxNext)/m + (common + dx)/mNext);
                }
            }
            std::get<1>(_1->second) = std::get<3>(_0->second);
        }

        // Get degree-2 and degree-3 coefficients
        //
        {
            auto _1 = table.begin(), _0 = _1++;
            for (auto const last = table.end(); _1 != last; ++_0, ++_1) {
                typename Base::coefficient_type &c  = _0->second;
                const RealType                   c1 = std::get<1>(c), m = std::get<3>(c), dx = std::get<2>(c);
                const RealType                   common = c1 + std::get<1>(_1->second) - (m + m);
#if 1 // normalized; t = (x - x_j)/(x_j + 1-x_j)
                std::get<2>(c) = (m - c1 - common) * dx;
                std::get<3>(c) = common * dx;
                std::get<1>(c) *= dx;
#else // non-normalized; t = (x - x_j)
                std::get<2>(c)  = (m - c1 - common) / dx;
                std::get<3>(c)  = common / (dx * dx);
                // std::get<1>(c) *= one;
#endif
            }
        }
    }
};

UTILITYKIT_END_NAMESPACE
