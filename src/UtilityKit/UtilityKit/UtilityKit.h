/*
 * Copyright (c) 2016-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#if defined(__cplusplus)

// algorithms
#include <UtilityKit/AdaptiveSampling1D.h>
#include <UtilityKit/CubicSplineInterpolation.h>
#include <UtilityKit/GaussLegendreIntegrator.h>
#include <UtilityKit/LinearInterpolation.h>
#include <UtilityKit/MonotoneCubicInterpolation.h>
#include <UtilityKit/MullerRootFinder.h>
#include <UtilityKit/NewtonRootFinder.h>
#include <UtilityKit/SplineCoefficient.h>
#include <UtilityKit/SplineInterpolator.h>
#include <UtilityKit/TrapzIntegrator.h>
#include <UtilityKit/ZeroCrossings.h>

// array & vector
#include <UtilityKit/FixedSizedNumericVector.hpp>
#include <UtilityKit/PaddedArray-Dynamic.h>

// utility
#include <UtilityKit/Mutex.h>
#include <UtilityKit/Optional.h>
#include <UtilityKit/printo.h>

#endif
