/*
 * Copyright (c) 2016-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <UtilityKit/UtilityKit-config.h>

// MARK: Version 3
//
#include <UtilityKit/Optional.h>
#include <algorithm>
#include <cmath>
#include <utility>

UTILITYKIT_BEGIN_NAMESPACE

/**
 @brief Newton's root finder.
 @discussion x_n+1 = x_n - m*f(x_n)/f'(x_n), where m is the multiplicity.
 The template parameter F_dF should take a value of type double and return f(x_n)/f'(x_n) convertible to double type.
 */
template <class F_dF>
struct NewtonRootFinder {
    using value_type    = double;
    using optional_type = Optional<value_type>;

private:
    F_dF _f_df;

public:
    value_type reltol{ 1e-5 };      //!< Relative tolerance.
    value_type abstol{ 1e-10 };     //!< Absolute tolerance.
    unsigned   max_iterations{ 5 }; //!< Maximum number of iterations.
    unsigned   multiplicity{ 1 };   //!< Multiplicity; default is 1.

    /**
     @brief Construct with a function to solve.
     @param f An instance of type F_dF representing f(x)/(df/dx), where f(x) is zero at the root (it should be copy-constructable).
     */
    explicit NewtonRootFinder(F_dF f)
    : _f_df(f) {}

    /**
     @brief Find a root of `f(x) = 0` whose derivative is known.
     @discussion std::isfinite is checked against the function values, so the function object can return NaN to indicate evaluation failure.

     The StepMonitor should be a callable object with signiture `bool(unsigned, value_type)`, where the two arguments are the current iteration count and the root estimate, respectively.
     Returning false upon call exit indicates immediate termination.

     @param x0 An initial guess.
     @return A root wrapped around the optional if found any.
     An empty optional is returned when the convergence failed within max_iterations, when the root is not finite, or when the step monitor returns false.
     */
    template <class StepMonitor>
    optional_type operator()(value_type x0, StepMonitor &&monitor) const;

    /**
     @brief Find a root without monitoring iteration steps.
     */
    optional_type operator()(value_type x0) const
    {
        return (*this)(x0, &_null_monitor);
    }

private:
    static bool _null_monitor(decltype(max_iterations) const &, value_type const &) noexcept
    {
        return true;
    }
};

// MARK: Out-of-line implementations
template <class F_dF>
template <class StepMonitor>
auto NewtonRootFinder<F_dF>::operator()(value_type x0, StepMonitor &&monitor) const -> optional_type
{
    if (!std::isfinite(x0))
        return {};

    value_type x1 = x0;
    for (decltype(max_iterations) i = 0; i < max_iterations && monitor(i, x1); ++i) {
        x1                 = x0 - multiplicity * _f_df(x0);
        value_type const h = x1 - x0;
        if (!std::isfinite(h)) {
            return {};
        } else if (std::abs(h) < abstol || std::abs(h) < reltol * std::max(std::abs(x1), std::abs(x0))) {
            return x1;
        } else {
            x0 = x1;
        }
    }

    return {};
}

UTILITYKIT_END_NAMESPACE
