/*
 * Copyright (c) 2015-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <UtilityKit/UtilityKit-config.h>

// MARK: Version 3
//
#include <atomic>
#include <pthread.h>
#include <type_traits>
#include <vector>

UTILITYKIT_BEGIN_NAMESPACE

// MARK: -- Forward declarations --
//
class Mutex;
class SpinLock; // atomic-based lock
template <typename Lock>
class CondV;
template <typename Lock>
class LockG; // lock_guard
template <typename Crit, typename Lock = Mutex>
class Sema; // counting semaphore

// MARK: -- Class definitions --
//
/**
 @brief pthread mutex wrapper.
 @discussion Conceptually similar c++11 std::mutex.
 Consult documentation of std::mutex.
 */
class Mutex {
    pthread_mutex_t __m_;

    Mutex(const Mutex &) = delete;
    Mutex &operator=(const Mutex &) = delete;

public:
    ~Mutex(); // terminate when error occurs while destorying the native handle.
    explicit Mutex();

    // lock/unlock:
    void lock();
    bool try_lock() noexcept;
    void unlock() noexcept;

    // native handle:
    typedef pthread_mutex_t *native_handle_type;
    native_handle_type       native_handle() noexcept { return &__m_; }
};

/**
 @brief Simple spin lock implementation using std::atomic_flag.
 */
class SpinLock {
    std::atomic_flag __s_;

    SpinLock(const SpinLock &) = delete;
    SpinLock &operator=(const SpinLock &) = delete;

public:
    ~SpinLock() = default;
    explicit SpinLock() noexcept
    : __s_(false) {}

    enum Status {
        Unlocked = 0,
        Locked
    };
    explicit SpinLock(Status status) noexcept
    : __s_(status) {}

    // lock/unlock:
    void lock() noexcept;
    bool try_lock() noexcept;
    void unlock() noexcept;
};

/**
 @brief pthread condition variable wrapper.
 @discussion Conceptually similar to c++11 std::condition_variable.
 Consult documentation of std::condition_variable.
 */
template <>
class CondV<Mutex> {
    pthread_cond_t __c_;

    CondV(CondV const &) = delete;
    CondV &operator=(CondV const &) = delete;

public:
    typedef Mutex lock_type;
    enum timed_status {
        timeout = 0,
        no_timeout
    };

    ~CondV(); // terminate when error occurs while destorying the native handle.
    explicit CondV();

    // notification:
    void notify_one() noexcept;
    void notify_all() noexcept;

    // waiting:
    void wait(lock_type &__mx);
    /**
     @brief Wake the waiting thread up by checking the predicate.
     @discussion Spurious wake up can be prevented.
     The predicate object should be callable with no argument and return value that can be convertible to bool.
     If the return value is `true`, the waiting thread wakes up.

     Note that before enter to this method lock must be acquired, after wait(lock) exits it is also reacquired, i.e. lock can be used as a guard to pred() access.
     */
    template <typename Predicate>
    void wait(lock_type &mx, Predicate pred)
    {
        while (!pred())
            wait(mx);
    }

    /**
     @brief Blocks the current thread until the condition variable is woken up or after the specified timeout duration.
     */
    timed_status wait_for(lock_type &__mx, unsigned __dur_in_sec);

    /**
     @brief Same as wait_for, but can be used to filter out spurious wakeups.
     */
    template <typename Predicate>
    bool wait_for(lock_type &__mx, unsigned __dur_in_sec, Predicate __p);

    // native handle:
    typedef pthread_cond_t *native_handle_type;
    native_handle_type      native_handle() noexcept { return &__c_; }
};

/**
 @brief lock-free condition variable.
 */
template <>
class CondV<SpinLock> {
    CondV(CondV const &) = delete;
    CondV &operator=(CondV const &) = delete;

public:
    typedef SpinLock lock_type;
    enum timed_status {
        timeout = 0,
        no_timeout
    };

    ~CondV(); // terminate when error occurs while destorying the native handle.
    explicit CondV();

    // notification:
    void notify_one() noexcept;
    void notify_all() noexcept;

    // waiting:
    void wait(lock_type &mx);
    /**
     @brief Wake the waiting thread up by checking the predicate.
     @discussion Spurious wake up can be prevented.
     The predicate object should be callable with no argument and return value that can be convertible to bool.
     If the return value is `true`, the waiting thread wakes up.

     Note that before enter to this method lock must be acquired, after wait(lock) exits it is also reacquired, i.e. lock can be used as a guard to pred() access.
     */
    template <typename Predicate>
    void wait(lock_type &mx, Predicate pred)
    {
        while (!pred())
            wait(mx);
    }

private:
    std::vector<lock_type *> _wait_list;
    lock_type                _master_key;
};

/**
 @brief Lock guard.
 @discussion Conceptually equivalent to c++11 std::lock_guard.
 Consult documentation of std::lock_guard.
 */
template <typename Lock>
class LockG {
    Lock &__m_;

    LockG(LockG const &) = delete;
    LockG &operator=(LockG const &) = delete;

public:
    // unlock:
    ~LockG() { __m_.unlock(); }
    // guarded lock:
    explicit LockG(Lock &mx) noexcept(noexcept(__m_.lock()))
    : __m_(mx) { __m_.lock(); }
};

/**
 @brief Generic counting semaphore.
 @discussion Instead of usual integral types, any generic type can be used for the critical value type if

 - the generic type is copy-constructable,
 - pre-decrement (called upon `wait` call) and post-increment (called upon `post` call) operators are defined for the generic type, which respectively decrements and increments the value of the generic type similar to the integral types, and
 - boolean cast is defined and returns false if a critical value is reached (equivalent to zero) else true otherwise.

 If the boolean cast returns false, any `wait` call is blocked until another thread `post`s.

 One can override the pre-decrement and post-increment operators to observe `wait` and `post` calls and to do synchronized operations.
 It is guaranteed that only one thread enters these operators.

 @note For native integral types, the initial value should be non-negative.
 */
template <typename Crit, typename Lock>
class Sema {
    CondV<Lock> __cv_;
    Lock        __mx_;
    Crit        __ctr_;

    Sema(Sema const &) = delete;
    Sema &operator=(Sema const &) = delete;

public:
    ~Sema() = default;

    /**
     @brief Construct object with an initial value.
     @discussion The initial value should be non-negative for an integral type.
     */
    explicit Sema(Crit __x)
    : __mx_(), __cv_(), __ctr_(__x) {}

    /**
     @brief Decrement the critical value by 1.
     @discussion The call is blocked if the negate of the critical variable is true.
     The block is removed if another thread `post`s.
     */
    void wait();

    /**
     @brief Increment the critical variable by 1.
     @discussion The negate of the critical variable before the increment is true, this call wakes up the blocked threads by the `wait' call.
     */
    void post();

    /**
     @brief Implicit type cast to the critical type.
     */
    operator Crit() noexcept(noexcept(LockG<Lock>(__mx_)));

    // short hand to wait:
    Sema &operator--() { return wait(), *this; }
    // short hand to post:
    Sema &operator++() { return post(), *this; }
};

// MARK: -- out-of-line definitions --
//
template <class _Predicate>
bool CondV<Mutex>::wait_for(lock_type &mx, unsigned __dur_in_sec, _Predicate pred)
{
    while (!pred()) {
        if (timeout == wait_for(mx, __dur_in_sec)) {
            return pred();
        }
    }
    return true;
}

template <typename Crit, typename Lock>
void Sema<Crit, Lock>::wait()
{
    LockG<Lock> l(__mx_);
    while (!__ctr_) {
        __cv_.wait(__mx_);
    }
    --__ctr_;
}
template <typename Crit, typename Lock>
void Sema<Crit, Lock>::post()
{
    LockG<Lock> l(__mx_);
    if (!__ctr_++) {
        __cv_.notify_all();
    }
}
template <typename Crit, typename Lock>
Sema<Crit, Lock>::operator Crit() noexcept(noexcept(LockG<Lock>(__mx_)))
{
    LockG<Lock> l(__mx_);
    return __ctr_;
}

UTILITYKIT_END_NAMESPACE
