/*
 * Copyright (c) 2015-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <UtilityKit/UtilityKit-config.h>

// MARK: Version 3
//
#include <UtilityKit/SplineCoefficient.h>
#include <UtilityKit/SplineInterpolator.h>

UTILITYKIT_BEGIN_NAMESPACE

/**
 @brief A simple linear interpolation algorithm.
 @discussion Simply a wrapper for the new unified spline interpolation algorithm.
 */
template <class RealType>
class LinearInterpolation {
public:
    using spline_coefficient_type  = LinearSplineCoefficient<RealType>;
    using spline_interpolator_type = typename spline_coefficient_type::spline_interpolator_type;
    using value_type               = RealType;
    using point_type               = std::pair<RealType, RealType>; // we don't use map::value_type, because first of the pair has const qualifier.
    using point_container_type     = std::map<RealType, RealType>;

private:
    spline_interpolator_type _interpolator{};

public:
    /**
     @brief Get the underlying spline interpolator.
     */
    spline_interpolator_type const &spline_interpolator() const noexcept { return _interpolator; }

    /**
     @brief Query if the receiver is valid.
     @return true if so.
     */
    explicit operator bool() const noexcept { return bool(_interpolator); }

    /**
     @brief Return interpolated value with extrapolation by default.
     */
    value_type operator()(value_type const &x) const;

    /**
     @brief Returns integral of the interpolant between the range with which the interpolation was constructed.
     */
    value_type integral() const noexcept(noexcept(_interpolator.integrate())) { return _interpolator.integrate(); }

    /**
     @brief Returns integral of the interpolant between the range with which the interpolation was constructed.
     @discussion Nil optional is returned when `x` is out of range.
     */
    Optional<value_type> derivative(value_type const &x) const noexcept(noexcept(_interpolator.derivative(x))) { return _interpolator.derivative(x); }

    /**
     @brief Default empty constructor which is invalid.
     */
    explicit LinearInterpolation() noexcept = default;

    /**
     @brief Construct with iterator.
     @discussion At least two points with distinct abscissas should be given.
     */
    template <typename InputIterator>
    LinearInterpolation(InputIterator first, InputIterator last);

    /**
     @brief Swap receiver's content with other's content.
     */
    void swap(LinearInterpolation &o) { _interpolator.swap(o._interpolator); }

    // copy/move:
    LinearInterpolation(LinearInterpolation const &) = default;
    LinearInterpolation(LinearInterpolation &&)      = default;
    LinearInterpolation &operator=(LinearInterpolation const &) = default;
    LinearInterpolation &operator=(LinearInterpolation &&) = default;
}; // TODO: UTILITYKIT_DEPRECATED_ATTRIBUTE

template <class RealType>
template <typename InputIterator>
LinearInterpolation<RealType>::LinearInterpolation(InputIterator first, InputIterator last)
{ // Should not use ": _interpolator(spline_coefficient_type(first, last).interpolator())" due to memory corrupt
    _interpolator = spline_coefficient_type(first, last).interpolator();
}

template <class RealType>
auto LinearInterpolation<RealType>::operator()(value_type const &x) const -> value_type
{
    if (x >= _interpolator.min_abscissa() && x <= _interpolator.max_abscissa())
        return (*_interpolator(x));

    typename spline_interpolator_type::coefficient_table_type const &table
        = _interpolator.spline_coefficient().table();
    decltype(table.size()) i    = x < _interpolator.min_abscissa() ? 0 : table.size() - 2;
    auto const            &pair = table[i];
    value_type const       d    = (table[i + 1].first - pair.first);
    value_type const       t    = (x - pair.first) / d;
    return std::get<0>(pair.second) + std::get<1>(pair.second) * t;
}

UTILITYKIT_END_NAMESPACE
