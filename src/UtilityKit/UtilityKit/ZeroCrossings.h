/*
 * Copyright (c) 2017-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <UtilityKit/UtilityKit-config.h>

// MARK: Version 3
//
#include <cmath>
#include <iterator>
#include <limits>
#include <type_traits>
#include <utility>
#include <vector>

UTILITYKIT_BEGIN_NAMESPACE
namespace {

// MARK: Signed Integer Specialization
//
/**
 @brief Detect zero crossings; Specialization for signed-integeral values.
 @discussion Method taken from http://mathematica.stackexchange.com/questions/10640/find-zero-crossing-in-a-list/10653.
 There are different kinds of zero crossings:
 • {..., -1, 1, ...} is a crossing between two values
 • {..., -1, 0, 1, ...} is a crossing at a zero
 • {..., -1, 0, 0, ..., 0, 1, ...} is a crossing for a range of zeros
 and non zero crossings:
 • {..., -1, 0, -1, ...} is not a (transverse) crossing at all
 • {..., -1, 0, 0, ..., 0, -1, ...} is not a crossing either
 • {0, 0, ..., 1, ...} is not a crossing
 • {..., 1, 0, 0, ..., 0} is not a crossing.

 @tparam Iterator Iterator type whose value_type is signed-integral type.
 @param first The first iterator from which the zero crossing detection starts.
 @param last The last iterator one before which the zero crossing detection takes into account.
 @return A list of a pair of iterators bracketing the zero crossings.
 */
template <class Iterator>
typename std::enable_if<std::is_integral<typename std::iterator_traits<Iterator>::value_type>::value && std::numeric_limits<typename std::iterator_traits<Iterator>::value_type>::is_signed,
                        std::vector<std::pair<Iterator, Iterator>>>::type
ZeroCrossings(Iterator first, Iterator last)
{
    // remove zeros
    //
    std::vector<Iterator> adjacency;
    for (; first != last; ++first) {
        if (*first)
            adjacency.push_back(first);
    }
    if (adjacency.empty())
        return {};

    // detect sign reversal
    //
    std::vector<std::pair<Iterator, Iterator>> result;
    long                                       sign0 = *adjacency.front() < 0 ? -1 : 1;
    for (unsigned long i = 1, n = adjacency.size(); i < n; ++i) {
        long const sign1 = *adjacency[i] < 0 ? -1 : 1;
        if (sign1 != sign0)
            result.push_back({ adjacency[i - 1], adjacency[i] });
        sign0 = sign1;
    }

    return result;
}

// MARK: Floating Point Specialization
//
/**
 @brief Detect zero crossings; Specialization for floating-point values.
 */
template <class Iterator>
typename std::enable_if<std::is_floating_point<typename std::iterator_traits<Iterator>::value_type>::value,
                        std::vector<std::pair<Iterator, Iterator>>>::type
ZeroCrossings(Iterator first, Iterator last)
{
    typedef typename std::iterator_traits<Iterator>::value_type FloatingPoint;
    constexpr FloatingPoint                                     eps  = std::numeric_limits<FloatingPoint>::min() * 1e2;
    constexpr FloatingPoint                                     zero = 0;

    // remove zeros
    //
    std::vector<Iterator> adjacency;
    for (; first != last; ++first) {
        if (std::abs(*first) > eps)
            adjacency.push_back(first);
    }
    if (adjacency.empty())
        return {};

    // detect sign reversal
    //
    std::vector<std::pair<Iterator, Iterator>> result;
    long                                       sign0 = *adjacency.front() < zero ? -1 : 1;
    for (unsigned long i = 1, n = adjacency.size(); i < n; ++i) {
        long const sign1 = *adjacency[i] < zero ? -1 : 1;
        if (sign1 != sign0)
            result.push_back({ adjacency[i - 1], adjacency[i] });
        sign0 = sign1;
    }

    return result;
}

// MARK: Convert from Range List to Index List
//
/**
 @brief Detect zero crossings.
 @discussion Instead of returning a pair of iterators, it returns a pair of offsets from the first iterator.
 It converts the iterator ranges returned from ZeroCrossings(first, last) into a list of offsets (distance) from first.
 @return A list of a pair of ofssets from first bracketing the zero crossings.
 */
template <class Iterator>
std::vector<std::pair<typename std::iterator_traits<Iterator>::difference_type, typename std::iterator_traits<Iterator>::difference_type>> ZeroCrossingsOffset(Iterator first, Iterator last)
{
    auto const zcs = ZeroCrossings(first, last);

    typedef typename std::iterator_traits<Iterator>::difference_type Diff;
    std::vector<std::pair<Diff, Diff>>                               offsets;
    for (auto const &range : zcs) {
        offsets.push_back({ std::distance(first, std::get<0>(range)), std::distance(first, std::get<1>(range)) });
    }

    return offsets;
}

} // namespace
UTILITYKIT_END_NAMESPACE
