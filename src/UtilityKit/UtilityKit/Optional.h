/*
 * Copyright (c) 2017-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <UtilityKit/UtilityKit-config.h>

// MARK: Version 3
//
#include <exception>
#include <type_traits>
#include <utility>

UTILITYKIT_BEGIN_NAMESPACE

/**
 @brief Optional wrapper simplified from c++17 std::optional.
 */
template <typename T>
class Optional {
    T   *_v;
    char _storage[sizeof(T)];

public:
    // types:
    using valueType = T; //!< Type of the associated value.
    class nil_access;    //!< nil value access exception.

    // destructor:
    ~Optional()
    {
        if (*this)
            _v->T::~T(), _v = nullptr;
    }

    // constructor:
    /**
     @brief Nil optional.
     */
    Optional() noexcept
    : _v(nullptr) {}

    /**
     @brief Copy construct.
     */
    Optional(Optional const &o)
    : Optional()
    {
        if (o)
            _v = _init(*o);
    }
    /**
     @brief Move construct.
     */
    Optional(Optional &&o)
    : Optional()
    {
        if (o)
            _v = _init(std::move(*o)); /* leave other unaffected */
    }

    /**
     @brief Copy construct by conversion.
     */
    template <typename U, typename std::enable_if<std::is_convertible<U const &, T>::value, int>::type = 0>
    Optional(Optional<U> const &o)
    : Optional()
    {
        if (o)
            _v = _init(*o);
    }
    /**
     @brief Move construct by conversion.
     */
    template <typename U, typename std::enable_if<std::is_convertible<U &&, T>::value, int>::type = 0>
    Optional(Optional<U> &&o)
    : Optional()
    {
        if (o)
            _v = _init(std::move(*o)); /* leave other unaffected */
    }

    /**
     @brief Copy/move construct by conversion.
     */
    template <typename U, typename std::enable_if<std::is_convertible<U &&, T>::value, int>::type = 0>
    Optional(U &&u)
    : Optional() { _v = _init(std::forward<U>(u)); }

    // assignment:
    /**
     @brief Copy assignment.
     */
    Optional &operator=(Optional const &o);
    /**
     @brief Move assignment.
     */
    Optional &operator=(Optional &&o);

    /**
     @brief Copy assignment by conversion.
     */
    template <typename U, typename std::enable_if<std::is_convertible<U const &, T>::value, int>::type = 0>
    Optional &operator=(Optional<U> const &o);
    /**
     @brief Move assignment by conversion.
     */
    template <typename U, typename std::enable_if<std::is_convertible<U &&, T>::value, int>::type = 0>
    Optional &operator=(Optional<U> &&o);

    /**
     @brief Copy/move assignment by conversion.
     */
    template <typename U, typename std::enable_if<std::is_convertible<U &&, T>::value, int>::type = 0>
    Optional &operator=(U &&u);

    // observers:
    /**
     @brief Returns a pointer to the contained value.
     */
    T const *operator->() const noexcept { return _v; }
    /**
     @brief Returns a pointer to the contained value.
     */
    T *operator->() noexcept { return _v; }

    /**
     @brief Returns a reference to the contained value (unchecked).
     */
    T const &operator*() const noexcept { return *_v; }
    /**
     @brief Returns a reference to the contained value (unchecked).
     */
    T &operator*() noexcept { return *_v; }

    /**
     @brief Checks whether *this contains a value.
     */
    explicit operator bool() const noexcept { return has_value(); }
    /**
     @brief Checks whether *this contains a value.
     */
    bool has_value() const noexcept { return nullptr != _v; }

    /**
     @brief Checked value access.
     @discussion Throws Optional::nil_access when there is no value.
     */
    T const &value() const;
    /**
     @brief Checked value access.
     @discussion Throws Optional::nil_access when there is no value.
     */
    T &value();

    /**
     @brief Checked value access.
     @discussion No exception is thrown unless T constructor throws an exception.
     */
    template <typename U>
    T value_or(U &&default_value) const &;
    /**
     @brief Checked value access.
     @discussion No exception is thrown unless T constructor throws an exception.
     */
    template <typename U>
    T value_or(U &&default_value) &&;

    /**
     @brief Shorthand for value().
     */
    T const &operator()() const { return value(); }
    /**
     @brief Shorthand for value().
     */
    T &operator()() { return value(); }

    /**
     @brief Shorthand for value_or(...).
     */
    template <typename U>
    T operator()(U &&default_value) const & { return value_or(std::forward<U>(default_value)); }
    /**
     @brief Shorthand for value_or(...).
     */
    template <typename U>
    T operator()(U &&default_value) && { return value_or(std::forward<U>(default_value)); }

    // modifiers:
    /**
     @brief Swaps the contents with those of other.
     */
    void swap(Optional &o);
    /**
     @brief If *this contains a value, destroy that value.
     */
    void reset() noexcept { this->~Optional(); }
    /**
     @brief Constructs the contained value in-place.
     */
    template <typename... Args>
    void emplace(Args &&...args)
    {
        this->~Optional();
        _v = _init(std::forward<Args>(args)...);
    }

private:
    template <typename... Args>
    T *_init(Args &&...args) { return ::new (_storage) T(std::forward<Args>(args)...); }
};

// MARK: Out-of-line Implementations
//
// null_value exception:
template <typename T>
class Optional<T>::nil_access : public std::exception {
    friend class Optional<T>;
    const char *_reason;
    explicit nil_access()
    : _reason(__PRETTY_FUNCTION__) {}

public:
    virtual ~nil_access() = default;
    virtual const char *what() const noexcept { return _reason; }
};

// assignment:
template <typename T>
Optional<T> &Optional<T>::operator=(Optional const &o)
{
    if (this != &o) {
        if (o)
            *this = *o;
        else
            this->~Optional();
    }
    return *this;
}
template <typename T>
Optional<T> &Optional<T>::operator=(Optional &&o)
{
    if (this != &o) {
        if (o)
            *this = std::move(*o);
        else
            this->~Optional();
    }
    return *this;
}

template <typename T>
template <typename U, typename std::enable_if<std::is_convertible<U const &, T>::value, int>::type>
Optional<T> &Optional<T>::operator=(Optional<U> const &o)
{
    if (this != &o) {
        if (o)
            *this = *o;
        else
            this->~Optional();
    }
    return *this;
}
template <typename T>
template <typename U, typename std::enable_if<std::is_convertible<U &&, T>::value, int>::type>
Optional<T> &Optional<T>::operator=(Optional<U> &&o)
{
    if (this != &o) {
        if (o)
            *this = std::move(*o);
        else
            this->~Optional();
    }
    return *this;
}

template <typename T>
template <typename U, typename std::enable_if<std::is_convertible<U &&, T>::value, int>::type>
Optional<T> &Optional<T>::operator=(U &&u)
{
    if (*this)
        *_v = std::forward<U>(u);
    else
        _v = _init(std::forward<U>(u));
    return *this;
}

// observers:
template <typename T>
T const &Optional<T>::value() const
{
    if (*this)
        return **this;
    throw nil_access();
}
template <typename T>
T &Optional<T>::value()
{
    if (*this)
        return **this;
    throw nil_access();
}

template <typename T>
template <typename U>
T Optional<T>::value_or(U &&default_value) const &
{
    return bool(*this) ? **this : static_cast<T>(std::forward<U>(default_value));
}
template <typename T>
template <typename U>
T Optional<T>::value_or(U &&default_value) &&
{
    return bool(*this) ? std::move(**this) : static_cast<T>(std::forward<U>(default_value));
}

// modifiers:
template <typename T>
void Optional<T>::swap(Optional &o)
{
#if 1
    std::swap(_storage, o._storage);
    std::swap(_v, o._v);
    // At this point, _v cross-references the each other's storage.
    // This must be corrected.
    if (nullptr != _v)
        _v = reinterpret_cast<decltype(_v)>(_storage);
    if (nullptr != o._v)
        o._v = reinterpret_cast<decltype(o._v)>(o._storage);
#else
    if (bool(*this) && bool(o)) {
        std::swap(**this, *o);
    } else if (*this) {
        o = std::move(**this);
        this->~Optional();
    } else if (o) {
        *this = std::move(*o);
        o.~Optional();
    } else {
        // do nothing
    }
#endif
}

// MARK: Output stream:
template <class _CharT, class _Traits, class _Tp>
std::basic_ostream<_CharT, _Traits> &
operator<<(std::basic_ostream<_CharT, _Traits> &__os, Optional<_Tp> const &o)
{
    if (o)
        return __os << *o;
    else
        return __os << "(nil)";
}

UTILITYKIT_END_NAMESPACE

// std::swap:
namespace std {
template <class T>
inline void swap(UTILITYKIT_NAMESPACE::Optional<T> &x, UTILITYKIT_NAMESPACE::Optional<T> &y) noexcept(noexcept(x.swap(y)))
{
    x.swap(y);
}
} // namespace std
