/*
 * Copyright (c) 2016, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <UtilityKit/UtilityKit-config.h>
#include <ostream>
#include <type_traits>

// MARK: Ostream print
//
namespace {
template <class CharT, class Traits, class Arg>
std::basic_ostream<CharT, Traits> &printo(std::basic_ostream<CharT, Traits> &os, Arg &&arg)
{
    return os << arg;
}
template <class CharT, class Traits, class First, class... Rest>
std::basic_ostream<CharT, Traits> &printo(std::basic_ostream<CharT, Traits> &os, First &&_1, Rest &&..._n)
{
    return printo(os << _1, std::forward<Rest>(_n)...);
}
} // namespace
