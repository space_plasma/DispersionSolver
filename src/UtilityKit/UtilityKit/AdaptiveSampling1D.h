/*
 * Copyright (c) 2015-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <UtilityKit/UtilityKit-config.h>

// MARK: Version 3
//
#include <algorithm>
#include <cmath>
#include <forward_list>
#include <limits>
#include <stdexcept>
#include <string>
#include <utility>

UTILITYKIT_BEGIN_NAMESPACE

/**
 @brief 1D adaptive sampling algorithm.
 @discussion The instance of this class acts as a functor which takes a callable object (or function pointer), representing the function to be sampled, and two abscissa values.
 Then the algorithm iteratively samples intermediate points until the given conditions are met.

 The two abscissa values must be unique (otherwise there is no point of sampling); an std::exception is thrown otherwise.

 The sampled points are returned in a forward list container (c++11).
 */
template <typename _T>
class AdaptiveSampling1D {
public:
    using value_type      = _T;                                //!< Type of a value.
    using point_type      = std::pair<value_type, value_type>; //!< Type of a point, i.e., x and y.
    using point_list_type = std::forward_list<point_type>;     //!< Container type of points.

public:
    unsigned   maxRecursion;  //!< Limit the maximum number of recursions (or iterations, more precisely). Can be zero (indicating no sampling).
    unsigned   initialPoints; //!< The number of initial sub-divisions that will be broken between two abscissa points before adaptive sampling. Should be positive (otherwise an std::exception is thrown).
    int        accuracyGoal;  //!< Accuracy tolerence which is 10^-accuracyGoal.
    value_type yScaleAbsoluteTolerance;

public:
    ~AdaptiveSampling1D() = default;
    constexpr AdaptiveSampling1D() noexcept
    : maxRecursion(5), initialPoints(2), accuracyGoal(4), yScaleAbsoluteTolerance(1e-10) {}

    AdaptiveSampling1D(AdaptiveSampling1D const &) = default;
    AdaptiveSampling1D &operator=(AdaptiveSampling1D const &) = default;

    /**
     @brief Initiate adaptive sampling.
     @param f A callable object with signature "value_type(value_type)".
     @param x0 Left abscissa value. Can be larger than, but not equal to, x1.
     @param x1 Right abscissa value. Can be less than, but not equal to, x0.
     @return A std::forward_list object containing the sampled points.
     */
    template <typename F>
    point_list_type operator()(F &&f, value_type const &x0, value_type const &x1) const
    {
        if (!initialPoints)
            throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - Nil initial points");
        F _f = std::forward<F>(f); // safeguard: If F is rvalue ref, move construction occur.
        return this->sample(_f, x0, x1);
    }

    /**
     @brief Initiate adaptive sampling.
     @param f A callable object with signature "value_type(value_type)".
     @param x0 Left abscissa value. Can be larger than, but not equal to, x1.
     @param x1 Right abscissa value. Can be less than, but not equal to, x0.
     @param y_interval Uses the given y interval for scaling. Useful for sub-interval sampling.
     @return A std::forward_list object containing the sampled points.
     */
    template <typename F>
    point_list_type operator()(F &&f, value_type const &x0, value_type const &x1, value_type const &y_interval) const
    {
        if (!initialPoints)
            throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - Nil initial points");
        F _f = std::forward<F>(f); // safeguard: If F is rvalue ref, move construction occur.
        return this->sample(_f, x0, x1, y_interval);
    }

private:
    // private helpers:
    template <typename F>
    auto sample(F &&f, value_type const &x0, value_type const &x1) const -> point_list_type;
    template <typename F>
    auto sample(F &&f, value_type const &x0, value_type const &x1, value_type const &y_interval) const -> point_list_type;

    // this is recursive-based sampling algorithm. Due to two point-based sampling, the first derivative at joints may not be smooth:
    template <typename F>
    auto sample(F &&f, point_type const &pt0, point_type const &pt2, point_type const &scale, value_type const &tolerance, unsigned const maxRecursion) const -> point_list_type;
    // this is a one-pass sampling algorithm. It used three points to ensure the smooth first derivative at joints:
    template <typename F>
    auto sample(F &&f, point_type const &pt0, point_type const &pt1, point_type const &pt2, point_type const &scale, value_type const &tolerance) const -> point_list_type;
};

// vector operation helpers:
namespace AdaptiveSampling1D_helper {

template <typename T>
auto operator+(std::pair<T, T> const &_1, std::pair<T, T> const &_2) -> std::pair<T, T>
{
    return { _1.first + _2.first, _1.second + _2.second };
} // Addition
template <typename T>
auto operator-(std::pair<T, T> const &_1, std::pair<T, T> const &_2) -> std::pair<T, T>
{
    return { _1.first - _2.first, _1.second - _2.second };
} // Subtraction
template <typename T>
auto operator*(std::pair<T, T> const &_1, std::pair<T, T> const &_2) -> std::pair<T, T>
{
    return { _1.first * _2.first, _1.second * _2.second };
} // Element-wise multiplication
template <typename T>
auto dot(std::pair<T, T> const &_1, std::pair<T, T> const &_2) -> T
{
    return _1.first * _2.first + _1.second * _2.second;
} // Inner product
template <typename T>
auto norm(std::pair<T, T> const &_) -> T
{
    return std::sqrt(dot(_, _));
} // Distance

} // namespace AdaptiveSampling1D_helper

// MARK: Out-of-line Definitions
// MARK: * Preparation Version 1
/* ***************** Preparation Version 1 **************** */
template <typename _T>
template <typename F>
auto AdaptiveSampling1D<_T>::sample(F &&f, value_type const &x0, value_type const &x1) const -> point_list_type
{
    // Abscissa interval:
    auto const &dx = (x1 - x0) / value_type(initialPoints);
    if (/*std::numeric_limits<value_type>::min()*value_type(10)*/ yScaleAbsoluteTolerance >= std::abs(dx)) {
        throw std::invalid_argument("AdaptiveSampling1D::sample - x0 == x1");
    }

    // Initial points:
    point_list_type points;
    for (long i = initialPoints; i >= 0; --i) {
        auto const &x = dx * value_type(i) + x0;
        points.push_front({ x, f(x) });
    }

    // Ordinate interval:
    auto it_pair = std::minmax_element(points.begin(), points.end(), [](point_type const &a, point_type const &b) -> bool {
        return a.second < b.second;
    });
    auto abs_dy  = it_pair.second->second - it_pair.first->second;
    if (/*std::numeric_limits<value_type>::min()*value_type(10)*/ yScaleAbsoluteTolerance >= abs_dy) {
        abs_dy = yScaleAbsoluteTolerance; // value_type(1);
    }

    // Scale:
    point_type scale{
        value_type(1) / std::abs(dx), value_type(1) / abs_dy
    };

    // Adaptive sampling:
    auto acc = std::pow(value_type(10), -value_type(accuracyGoal));
#if 0
    // Recursive-based algorithm:
    for (auto it1=points.begin(), it0=it1++; points.end()!=it1; it0=it1++) {
        points.splice_after(it0, this->sample(std::forward<F>(f), *it0, *it1, scale, acc, maxRecursion));
    }
#else
    // Iterative algorithm:
    if (1 == initialPoints) { // If there are two points, halve.
        for (auto it1 = points.begin(), it0 = it1++; points.end() != it1; it0 = it1++) {
            points.splice_after(it0, this->sample(std::forward<F>(f), *it0, *it1, scale, acc, 1));
        }
    }

    // Iteration until convergence:
    // NOTE: Maybe scale can be updated at every iteration
    for (long i = 0, previousCount = 0, currentCount = std::distance(points.begin(), points.end());
         previousCount != currentCount && i < maxRecursion;
         ++i, previousCount = currentCount, currentCount = std::distance(points.begin(), points.end())) {
        // First three points in reverse order
        // This step is necessary because the sampling algorithm returns only one halved point at the second interval.
        // Three points are passed in a reversed order because the halved point at the first interval is needed.
        //
        auto it2 = points.begin();
        auto it0 = it2++;
        auto it1 = it2++;
        points.splice_after(it0, this->sample(std::forward<F>(f), *it2, *it1, *it0, scale, acc));

        // The rest in normal order
        // Note that '++it0' is abscent before for iteration. This means that I will not use the halved point because empty list may be returned in the previous step.
        //
        for (/*++it0*/; points.end() != it2; it0 = it1 /*I am not using the halved point because empty list may be returned*/, it1 = it2++) {
            points.splice_after(it1, this->sample(std::forward<F>(f), *it0, *it1, *it2, scale, acc));
        }
    }
#endif

    // Return:
    return points;
}

// MARK: * Preparation Version 2
/* ***************** Preparation Version 2 **************** */
template <typename _T>
template <typename F>
auto AdaptiveSampling1D<_T>::sample(F &&f, value_type const &x0, value_type const &x1, value_type const &y_interval) const -> point_list_type
{
    // Abscissa interval:
    auto const &dx = (x1 - x0) / value_type(initialPoints);
    if (/*std::numeric_limits<value_type>::min()*value_type(10)*/ yScaleAbsoluteTolerance >= std::abs(dx)) {
        throw std::invalid_argument("AdaptiveSampling1D::sample - x0 == x1");
    }

    // Initial points:
    point_list_type points;
    for (long i = initialPoints; i >= 0; --i) {
        auto const &x = dx * value_type(i) + x0;
        points.push_front({ x, f(x) });
    }

    // Ordinate interval:
    auto abs_dy = std::abs(y_interval);
    if (/*std::numeric_limits<value_type>::min()*value_type(10)*/ yScaleAbsoluteTolerance >= abs_dy) {
        abs_dy = yScaleAbsoluteTolerance; // value_type(1);
    }

    // Scale:
    point_type scale{
        value_type(1) / std::abs(dx), value_type(1) / abs_dy
    };

    // Adaptive sampling:
    auto acc = std::pow(value_type(10), -value_type(accuracyGoal));
#if 0
    // Recursive-based algorithm:
    for (auto it1=points.begin(), it0=it1++; points.end()!=it1; it0=it1++) {
        points.splice_after(it0, this->sample(std::forward<F>(f), *it0, *it1, scale, acc, maxRecursion));
    }
#else
    // Iterative algorithm:
    if (1 == initialPoints) { // If there are two points, halve.
        for (auto it1 = points.begin(), it0 = it1++; points.end() != it1; it0 = it1++) {
            points.splice_after(it0, this->sample(std::forward<F>(f), *it0, *it1, scale, acc, 1));
        }
    }

    // Iteration until convergence:
    for (long i = 0, previousCount = 0, currentCount = std::distance(points.begin(), points.end());
         previousCount != currentCount && i < maxRecursion;
         ++i, previousCount = currentCount, currentCount = std::distance(points.begin(), points.end())) {
        // First three points in reverse order
        // This step is necessary because the sampling algorithm returns only one halved point at the second interval.
        // Three points are passed in a reversed order because the halved point at the first interval is needed.
        //
        auto it2 = points.begin();
        auto it0 = it2++;
        auto it1 = it2++;
        points.splice_after(it0, this->sample(std::forward<F>(f), *it2, *it1, *it0, scale, acc));

        // The rest in normal order
        // Note that '++it0' is abscent before for iteration. This means that I will not use the halved point because empty list may be returned in the previous step.
        //
        for (/*++it0*/; points.end() != it2; it0 = it1 /*I am not using the halved point because empty list may be returned*/, it1 = it2++) {
            points.splice_after(it1, this->sample(std::forward<F>(f), *it0, *it1, *it2, scale, acc));
        }
    }
#endif

    // Return:
    return points;
}

// MARK: * Recursion-based sampling algorithm
/* ***************** Recursion-based sampling algorithm **************** */
template <typename _T>
template <typename F>
auto AdaptiveSampling1D<_T>::sample(F &&f, point_type const &pt0, point_type const &pt2, std::pair<value_type, value_type> const &scale, value_type const &tolerance, unsigned const maxRecursion) const -> point_list_type
{
    if (0 == maxRecursion) { // Terminate recursion
        return point_list_type(0);
    }

    // Sub-intervals:
    auto const &x0  = pt0.first;
    auto const &x2  = pt2.first;
    auto const &x1  = value_type(0.5) * (x0 + x2);
    auto const &x12 = value_type(0.5) * (x1 + x2);
    auto const &x01 = value_type(0.5) * (x0 + x1);

    point_type pt1{ x1, f(x1) };
    point_type pt12{ x12, f(x12) };
    point_type pt01{ x01, f(x01) };

    // Scaled vectors among points:
    using namespace AdaptiveSampling1D_helper;
    auto const &A = (pt1 - pt0) * scale;
    auto const &B = (pt2 - pt1) * scale;
    auto const &C = (pt12 - pt01) * scale;

    auto const &AB = norm(A) * norm(B);
    auto const &BC = norm(B) * norm(C);

    // Convergence test:
    if (std::abs(AB - dot(A, B)) < tolerance /*angle test*/ && std::abs(BC - dot(B, C)) < tolerance /*slope test*/) { // Satisfied, terminate recursion
        return { pt1 };
    } else { // More division needed, recursive sampling
        auto points = this->sample(std::forward<F>(f), pt1, pt2, scale, tolerance, maxRecursion - 1);
        points.push_front(pt1);
        points.splice_after(points.before_begin(),
                            this->sample(std::forward<F>(f), pt0, pt1, scale, tolerance, maxRecursion - 1));
        return points;
    }

    // should not reach here:
    throw std::domain_error(std::string(__PRETTY_FUNCTION__) + " - Should not reach");
}

// MARK: * One-pass sampling algorithm
/* ***************** One-pass sampling algorithm **************** */
template <typename _T>
template <typename F>
auto AdaptiveSampling1D<_T>::sample(F &&f, point_type const &pt0, point_type const &pt1, point_type const &pt2, point_type const &scale, value_type const &tolerance) const -> point_list_type
{
    auto const &x01 = value_type(0.5) * (pt0.first + pt1.first);
    auto const &x12 = value_type(0.5) * (pt1.first + pt2.first);
    point_type  pt01{ x01, f(x01) };
    point_type  pt12{ x12, f(x12) };

    // Scaled vectors among points:
    using namespace AdaptiveSampling1D_helper;
    auto const &A = (pt1 - pt0) * scale;
    auto const &B = (pt2 - pt1) * scale;
    auto const &C = A + B;
    auto const &D = (pt12 - pt01) * scale;

    auto const &AB = norm(A) * norm(B);
    auto const &CD = norm(C) * norm(D);

    // Convergence test:
    if (std::abs(AB - dot(A, B)) < tolerance /*angle test*/ && std::abs(CD - dot(C, D)) < tolerance /*slope test*/) { // Converged, return no point.
        return point_list_type(0);
    } else { // Not converged, return the halved point at the second interval.
        return { pt12 };
    }
}

UTILITYKIT_END_NAMESPACE
