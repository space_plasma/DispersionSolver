/*
 * Copyright (c) 2017-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <UtilityKit/UtilityKit-config.h>

// MARK: Version 3
//
#include <UtilityKit/Optional.h>
#include <algorithm>
#include <array>
#include <cmath>
#include <stdexcept>
#include <type_traits>
#include <utility>
#include <vector>

UTILITYKIT_BEGIN_NAMESPACE

// MARK: Forward Declarations
//
template <class RealType, long Order>
class SplineCoefficient;
template <class RealType, long Order>
class SplineInterpolator;

// MARK: SplineInterpolator
//
/**
 @brief Spline interpolator constructed using a SplineCoefficient instance.
 */
template <class RealType, long Order>
class SplineInterpolator {
    static_assert(std::is_floating_point<RealType>::value, "The template parameter should be a floating point type.");
    static_assert(Order >= 1, "The spline order should be a positive integer.");

public:
    // types:
    using value_type              = RealType;
    using optional_type           = Optional<value_type>;
    using coefficient_type        = std::array<value_type, Order + 1>; //!< The first element is the 0th order coefficient.
    using coefficient_table_type  = std::vector<std::pair<value_type, coefficient_type>>;
    using spline_coefficient_type = SplineCoefficient<RealType, Order>;

public:
    /**
     @brief Polynomial order of the spline segments.
     */
    constexpr static unsigned spline_order() noexcept { return Order; }

    /**
     @brief Query if the receiver is valid.
     */
    explicit operator bool() const noexcept { return bool(_coef); }

    /**
     @brief Return spline coefficient object.
     Sorted in ascending order.
     */
    spline_coefficient_type const &spline_coefficient() const noexcept { return _coef; }

    /**
     @brief Minimum abscissa value.
     */
    value_type const &min_abscissa() const noexcept { return _coef.min_abscissa(); }
    /**
     @brief Maximum abscissa value.
     */
    value_type const &max_abscissa() const noexcept { return _coef.max_abscissa(); }

    /**
     @brief Interpolate at a given abscissa value, x.
     */
    optional_type interpolate(value_type const x) const;
    /**
     @brief A shorthand for interpolate(x).
     */
    optional_type operator()(value_type const x) const { return interpolate(x); }

    /**
     @brief Calculate a first order derivative at x.
     */
    optional_type derivative(value_type const x) const;

    /**
     @brief Integrate the approximating function in the entire interval.
     */
    value_type integrate() const { return _coef.integrate(); }

    /**
     @brief Default constructor.
     @discussion The state is invalid.
     */
    explicit SplineInterpolator() noexcept = default;
    /**
     @brief Construct interpolator by copying the table in a SplineCoefficient instance.
     */
    explicit SplineInterpolator(spline_coefficient_type const &coef) { _init_common(coef); } // should not use ": _coef(coef)"
    /**
     @brief Construct interpolator by moving the table in a SplineCoefficient instance.
     @discussion The SplineCoefficient instance becomes invalid after construction.
     */
    explicit SplineInterpolator(spline_coefficient_type &&coef) { _init_common(std::move(coef)); } // should not use ": _coef(std::move(coef))"

    /**
     @brief Swap the receiver's content with other's.
     */
    void swap(SplineInterpolator &other);

    // copy/move:
    SplineInterpolator(SplineInterpolator const &) = default;
    SplineInterpolator(SplineInterpolator &&)      = default;
    SplineInterpolator &operator=(SplineInterpolator const &) = default;
    SplineInterpolator &operator=(SplineInterpolator &&) = default;

private:
    spline_coefficient_type _coef{};
    unsigned long (SplineInterpolator::*_locator)(value_type const) const { nullptr };
    value_type   _d{};     // either delta or correlation tolerance depending on the regularity of grid
    mutable long _j0{ 0 }; // jsaved
    mutable bool _is_correlated{ false };

    // Concrete locators
    // x bound should be checked beforehand!!
    //
    unsigned long _locator_regular(value_type const x) const
    {
        return x == max_abscissa() ? _coef.table().size() - 2 : static_cast<unsigned long>(std::floor((x - min_abscissa()) / _d));
    }
    unsigned long _locator_irregular(value_type const x) const
    {
        long const i = _is_correlated ? _hunt(x) : _locate(x);
        return i >= 0 && i < long(_coef.table().size() - 1) ? static_cast<unsigned long>(i) : throw std::runtime_error(__PRETTY_FUNCTION__);
    }
    long _locate(value_type const x) const;
    long _hunt(value_type const x) const;

    template <class Coef>
    void _init_common(Coef &&coef)
    {
        if (!coef)
            return;
        _coef = std::forward<Coef>(coef);

        // choose locator based on grid regularity
        //
        if (_coef.delta()) {
            _d       = *_coef.delta();
            _locator = &SplineInterpolator::_locator_regular;
        } else {
            _d       = std::min(value_type(1.), std::pow(value_type(_coef.table().size()), value_type(0.25)));
            _locator = &SplineInterpolator::_locator_irregular;
        }
    }
};

template <class RealType, long Order>
void SplineInterpolator<RealType, Order>::swap(SplineInterpolator &other)
{
    _coef.swap(other._coef);
    std::swap(_locator, other._locator);
    std::swap(_d, other._d);
}

template <class RealType, long Order>
auto SplineInterpolator<RealType, Order>::interpolate(value_type const x) const -> optional_type
{
    if (x < min_abscissa() || x > max_abscissa())
        return {};
    coefficient_table_type const &table = _coef.table();

    unsigned long const i    = (this->*_locator)(x);
    auto const         &pair = table[i];
    value_type const    t    = (x - pair.first) / (table[i + 1].first - pair.first);
    value_type          result(0.);
    for (int i = Order; i >= 0; --i) {
        result = result * t + pair.second[unsigned(i)];
    }
    return result;
}
template <class RealType, long Order>
auto SplineInterpolator<RealType, Order>::derivative(value_type const x) const -> optional_type
{
    if (x < min_abscissa() || x > max_abscissa())
        return {};
    coefficient_table_type const &table = _coef.table();

    unsigned long const i    = (this->*_locator)(x);
    auto const         &pair = table[i];
    value_type const    d    = table[i + 1].first - pair.first;
    value_type const    t    = (x - pair.first) / d;
    value_type          result(0.);
    for (int i = Order; i >= 1; --i) {
        result = result * t + pair.second[unsigned(i)] * value_type(i);
    }
    return result / d;
}

template <class RealType, long Order>
long SplineInterpolator<RealType, Order>::_locate(value_type const x) const
{
    coefficient_table_type const &table = _coef.table();
    using size_type                     = unsigned long;
    long const n                        = long(table.size());
    long       jl                       = 0;
    long       ju                       = n - 1;
    while (ju - jl > 1) {
        long const jm = (ju + jl) >> 1; // mid point
        if (x >= table[size_type(jm)].first) {
            jl = jm;
        } else {
            ju = jm;
        }
    }
    _is_correlated = std::abs((jl - _j0) / _d) < value_type(1.);
    _j0            = jl;
    return std::max(long(0), std::min(n - 2, jl));
}

template <class RealType, long Order>
long SplineInterpolator<RealType, Order>::_hunt(value_type const x) const
{
    coefficient_table_type const &table = _coef.table();
    using size_type                     = unsigned long;
    long const n                        = long(table.size());
    long       jl = _j0, inc = 1, ju(-1);
    if (jl < 0 || jl > n - 1) {
        jl = 0;
        ju = n - 1;
    } else {
        if (x >= table[size_type(jl)].first) { // hunt up
            for (;;) {
                ju = jl + inc;
                if (ju >= n - 1) {
                    ju = n - 1;
                    break;
                } else if (x < table[size_type(ju)].first) {
                    break;
                } else {
                    jl = ju;
                    inc += inc;
                }
            }
        } else { // hunt down
            ju = jl;
            for (;;) {
                jl = ju - inc;
                if (jl <= 0) {
                    jl = 0;
                    break;
                } else if (x >= table[size_type(jl)].first) {
                    break;
                } else {
                    ju = jl;
                    inc += inc;
                }
            }
        }
    }
    while (ju - jl > 1) { // hunt is done, so do the bisection
        long const jm = (ju + jl) >> 1;
        if (x >= table[size_type(jm)].first) {
            jl = jm;
        } else {
            ju = jm;
        }
    }
    _is_correlated = std::abs((jl - _j0) / _d) < value_type(1.);
    _j0            = jl;
    return std::max(long(0), std::min(n - 2, jl));
}

UTILITYKIT_END_NAMESPACE
