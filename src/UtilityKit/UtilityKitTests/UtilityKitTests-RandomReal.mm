//
//  UtilityKitTests-RandomReal.m
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 7/16/16.
//  Copyright © 2016 KYUNGGUK MIN. All rights reserved.
//

#import <XCTest/XCTest.h>

// MARK: Version 3
//
#import <UtilityKit/RandomRealKit.h>
#import <UtilityKit/IOKit.h>
#import <memory>
#import <limits>
#import <iostream>
#import <sstream>
#import <numeric>
#import <cmath>

@interface UtilityKitTests_RandomReal__3_ : XCTestCase

@end

@implementation UtilityKitTests_RandomReal__3_

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testBitReversedRandomReal {
    using namespace Utility::__3_;

    // exception check:
    try {
        BitReversedRandomReal(1)();
        XCTAssert(false, @"should have thrown an exception");
    } catch (std::exception&) {
        XCTAssert(true, @"");
    } catch (...) {
        XCTAssert(false, @"Oops! Unknown exception");
    }

    // check algorithm consistency:
    @autoreleasepool {
        const unsigned base = 5, seq = 2;
        BitReversedRandomReal rng_1(base, seq);
        BitReversedRandomReal rng_2(base, seq);

        for (int i=0; i<1000; ++i) {
            XCTAssert(rng_1() == rng_2(), @"");
        }
    }

    // move:
    @autoreleasepool {
        BitReversedRandomReal r1(5);
        BitReversedRandomReal r2(std::move(r1));
        XCTAssert(!r1 && !!r2, @"bool(r1) = %d, bool(r2) = %d", bool(r1), bool(r2));

        r2 = std::move(r1);
        XCTAssert(!!r1 && !r2, @"bool(r1) = %d, bool(r2) = %d", bool(r1), bool(r2));
    }
}

- (void)testBitReversedRandomRealWithRandomInitialSequence {
    using Utility::__3_::BitReversedRandomReal;

    std::ostringstream os;
    os.setf(os.fixed);
    try {
        constexpr long base = 2;
        BitReversedRandomReal rng1(base, arc4random());
        BitReversedRandomReal rng2(base, arc4random());

        constexpr long n_sequences = 20000;
        printo(os, "{");
        // first
        printo(os, "\n\t{", rng1(), ", ", rng2(), "}");
        // rest
        for (long i = 1; i < n_sequences; ++i) {
            printo(os, ",\n\t{", rng1(), ", ", rng2(), "}");
        }
        printo(os, "\n}\n");
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    if (0) {
        NSString *s = [NSString stringWithUTF8String:os.str().c_str()];
        s = [s stringByReplacingOccurrencesOfString:@"e" withString:@"*^"];

        NSString *filename = [NSString stringWithFormat:@"testBitReversedRandomRealWithRandomInitialSequence.m"];
        NSURL *url = [NSURL fileURLWithPath:[NSHomeDirectory() stringByAppendingPathComponent:@"Downloads"] isDirectory:YES];
        url = [url URLByAppendingPathComponent:filename];
        NSError *error;
        XCTAssert([s writeToURL:url atomically:YES encoding:NSUTF8StringEncoding error:&error], @"%@", error);
    }
}

- (void)testNRRandomReal {
    using namespace Utility::__3_;

    // check algorithm consistency:
    @autoreleasepool {
        const unsigned seed = arc4random();
        NRRandomReal rng_1(seed);
        NRRandomReal rng_2(seed);

        for (int i=0; i<1000; ++i) {
            XCTAssert(rng_1() == rng_2(), @"");
        }
    }

    // move:
    @autoreleasepool {
        NRRandomReal r1(5);
        NRRandomReal r2(std::move(r1));
        XCTAssert(!r1 && !!r2, @"bool(r1) = %d, bool(r2) = %d", bool(r1), bool(r2));
        
        r2 = std::move(r1);
        XCTAssert(!!r1 && !r2, @"bool(r1) = %d, bool(r2) = %d", bool(r1), bool(r2));
    }
}

@end
