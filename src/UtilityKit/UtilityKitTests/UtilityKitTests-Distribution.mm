//
//  UtilityKitTests-Distribution.mm
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 7/16/16.
//  Copyright © 2016 KYUNGGUK MIN. All rights reserved.
//

#import <XCTest/XCTest.h>

// MARK: Version 3
//
#import <UtilityKit/DistKit.h>
#import <UtilityKit/IOKit.h>
#import <memory>
#import <cmath>
#import <numeric>
#import <iostream>
#import <sstream>
#import <fstream>

@interface UtilityKitTests_Distribution__3_ : XCTestCase

@end

@implementation UtilityKitTests_Distribution__3_

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testUniformDistribution {
    using namespace Utility::__3_;

    @autoreleasepool {
        std::unique_ptr<UniformDistribution> dist(new UniformDistribution);

        XCTAssert(0.5 == dist->mean() && 1./12 == dist->variance(),
                  @"mean = %f, var = %f", dist->mean(), dist->variance());

        dist.reset( new UniformDistribution(-1, 2) );
        XCTAssert(0.5 == dist->mean() && 3./4 == dist->variance(),
                  @"mean = %f, var = %f", dist->mean(), dist->variance());

        for (int i = -1; i <= 2; ++i) XCTAssert(1./3 == dist->pdf(i),
                                                @"pdf(%d) = %f", i, dist->pdf(i));

        XCTAssert(0. == dist->cdf(-1) && 1./3 == dist->cdf(0) && 2./3 == dist->cdf(1) && 1. == dist->cdf(2),
                  @"cdf(-1) = %f, cdf(0) = %f, cdf(1) = %f, cdf(2) = %f",
                  dist->cdf(-1), dist->cdf(0), dist->cdf(1), dist->cdf(2));

        XCTAssert(-1. == dist->icdf(0) && 0.5 == dist->icdf(.5) && 2. == dist->icdf(1),
                  @"icdf(0) = %f, icdf(0.5) = %f, icdf(1) = %f",
                  dist->icdf(0), dist->icdf(.5), dist->icdf(1));
    }
}

- (void)testCustomDistribution {
    using namespace Utility::__3_;

    // test on gaussian distribution:
    @autoreleasepool {
        const double xd = 1;
        const double xmin = -5 + xd, xmax = 5 + xd;

        //        auto pdf = [xd](double x)->double {
        //            return std::exp(-(x-xd)*(x-xd)) * M_2_SQRTPI/2.;
        //        };
        struct _PDF {
            double xd = 1;
            double operator()(double x) const {
                return std::exp(-(x-xd)*(x-xd)) * M_2_SQRTPI/2.;
            }
            _PDF() = default;
            _PDF(_PDF const& p) = delete;
            _PDF& operator=(_PDF const& p) = delete;
            _PDF(_PDF&& p) : xd(p.xd) {
                p.xd = std::numeric_limits<double>::quiet_NaN();
            }
            _PDF& operator=(_PDF&& p) = delete;
        };
        const _PDF pdf{};

        auto cdf = [xd](double x)->double {
            return .5 * (1. + std::erf(x-xd));
        };

        // check mean and var:
        std::unique_ptr<CustomDistribution> dist(new CustomDistribution(xmin, xmax, pdf, cdf));

        XCTAssert(std::abs(xd - dist->mean()) < 1e-7 && std::abs(.5 - dist->variance()) < 1e-7,
                  @"mean = %f, var = %f", dist->mean(), dist->variance());

        // check pdf:
        double const denom = trapz::qsimp(pdf, std::make_pair(xmin, xmax), 4).first;
        const int len = 20;
        for (int i = 0; i <= len; ++i) {
            double x = i*(xmax-xmin)/len + xmin;
            double y1 = pdf(x)/denom, y2 = dist->pdf(x);
            XCTAssert(std::abs(y1 - y2) < 1e-7, @"x = %f, y1 = %f, y2 = %f", x, y1, y2);
        }

        // check cdf:
        for (int i = 0; i <= len; ++i) {
            double x = i*(xmax-xmin)/len + xmin;
            double y1 = cdf(x), y2 = dist->cdf(x);
            XCTAssert(std::abs(y1 - y2) < 1e-7, @"x = %f, y1 = %f, y2 = %f", x, y1, y2);
        }

        // check inverse cdf:
        for (int i = 0; i <= len; ++i) {
            double x1 = i*(xmax-xmin)/len + xmin;
            if (x1 <= -2 || x1 >= 2) continue;
            double x2 = dist->icdf(cdf(x1));
            XCTAssert(std::abs(x1 - x2) < 1e-7, @"x1 = %e, x2 = %e", x1, x2);
        }
    }
}

- (void)testParallelMaxwellianDistribution {
    using namespace Utility::__3_;

    constexpr double vth = M_SQRT2, vd = 1;
    ParallelMaxwellianDistribution mx(vth, vd, true);
    XCTAssert(std::abs(vd - mx.mean()) < 1e-10 && std::abs(1. - mx.variance()) < 1e-10, @"mean = %f, var = %f", mx.mean(), mx.variance());
    XCTAssert(std::abs(mx.pdf(vd) - M_2_SQRTPI/2/vth) < 1e-10, @"pdf(vd) = %f", mx.pdf(vd));
    XCTAssert(std::abs(mx.cdf(vd) - .5) < 1e-10, @"cdf(vd) = %f", mx.cdf(vd));
    XCTAssert(std::abs(vd - mx.icdf(.5)) < 1e-10, @"icdf(.5) = %f", mx.icdf(.5));

    BitReversedRandomReal rng(2, 1);
    RandomVariate rv(&rng, &mx);
    constexpr long sz = 50000;
    double v1s[sz];
    std::generate_n(v1s, sz, rv);

    double mean{0};
    mean = std::accumulate(v1s, v1s + sz, mean) / sz;
    XCTAssert(std::abs(mx.mean() - mean) < 1e-3, @"mean = %f", mean);
    double var{0};
    var = std::inner_product(v1s, v1s + sz, v1s, var)/sz - mean*mean;
    XCTAssert(std::abs(mx.variance() - var) < 1e-3, @"var = %f", var);

    std::ostringstream os;
    os.setf(os.fixed);
    printo(os, "{", *v1s);
    for (long i = 1; i < sz; ++i) {
        printo(os, ", ", v1s[i]);
    }
    printo(os, "}");
    if (0) {
        NSError *error;
        NSString *str = [NSString stringWithUTF8String:os.str().c_str()];
        NSURL *path = [NSURL fileURLWithPath:NSHomeDirectory() isDirectory:YES];
        path = [path URLByAppendingPathComponent:@"Downloads/ParallelMaxwellianDistribution.m" isDirectory:NO];
        BOOL tf = [str writeToURL:path atomically:YES encoding:NSUTF8StringEncoding error:&error];
        XCTAssert(tf, @"%@", error.localizedDescription);
    }
}

- (void)testPerpendicularMaxwellianDistribution {
    using namespace Utility::__3_;

    constexpr double vth = 2;
    PerpendicularMaxwellianDistribution mx(vth, true);
    XCTAssert(std::abs(mx.pdf(vth) - 2/(M_E*vth)) < 1e-10, @"pdf(vth) = %f", mx.pdf(vth));
    XCTAssert(std::abs(mx.cdf(vth) - (1 - 1/M_E)) < 1e-10, @"cdf(vth) = %f", mx.cdf(vth));
    XCTAssert(std::abs(vth - mx.icdf(1 - 1/M_E)) < 1e-10, @"icdf(...) = %f", mx.icdf(1 - 1/M_E));

    BitReversedRandomReal rng1(2, 1);
    RandomVariate rv(&rng1, &mx);
    BitReversedRandomReal rng2(3);
    UniformDistribution phi(0, 2*M_PI);
    RandomVariate rphi(&rng2, &phi);

    constexpr long sz = 50000;
    double vys[sz], vzs[sz], v2s[sz];
    for (long i = 0; i < sz; ++i) {
        v2s[i] = rv();
        double phi = rphi();
        vys[i] = v2s[i] * std::cos(phi);
        vzs[i] = v2s[i] * std::sin(phi);
    }

    double mean{0};
    mean = std::accumulate(v2s, v2s + sz, mean) / sz;
    XCTAssert(std::abs(mx.mean() - mean)/mx.mean() < 1e-3, @"mean = %f", mean);
    double var{0};
    var = std::inner_product(v2s, v2s + sz, v2s, var)/sz - mx.mean()*mx.mean();
    XCTAssert(std::abs(mx.variance() - var)/mx.variance() < 1e-2, @"var = %f", var);

    std::ostringstream os;
    os.setf(os.fixed);
    printo(os, "{", *v2s);
    for (long i = 1; i < sz; ++i) {
        printo(os, ", ", v2s[i]);
    }
    printo(os, "}");
    if (0) {
        NSError *error;
        NSString *str = [NSString stringWithUTF8String:os.str().c_str()];
        NSURL *path = [NSURL fileURLWithPath:NSHomeDirectory() isDirectory:YES];
        path = [path URLByAppendingPathComponent:@"Downloads/PerpendicularMaxwellianDistribution.m" isDirectory:NO];
        BOOL tf = [str writeToURL:path atomically:YES encoding:NSUTF8StringEncoding error:&error];
        XCTAssert(tf, @"%@", error.localizedDescription);
    }
}

- (void)testPerpendicularRingDistribution {
    using namespace Utility::__3_;

    constexpr double vth = .5, vr = 5;
    PerpendicularRingDistribution mx(vth, vr, true);
    XCTAssert(std::abs(mx.pdf(vr) - 2*vr/(mx.A()*vth*vth)) < 1e-10, @"pdf(vr) = %f", mx.pdf(vr));
    XCTAssert(std::abs(vr - mx.icdf(mx.cdf(vr))) < 1e-5, @"cdf(vr) = %f, icdf(cdf(vr)) = %f", mx.cdf(vr), mx.icdf(mx.cdf(vr)));

    BitReversedRandomReal rng1(2, 1);
    RandomVariate rv(&rng1, &mx);
    BitReversedRandomReal rng2(3);
    UniformDistribution phi(0, 2*M_PI);
    RandomVariate rphi(&rng2, &phi);

    constexpr long sz = 50000;
    double vys[sz], vzs[sz], v2s[sz];
    for (long i = 0; i < sz; ++i) {
        v2s[i] = rv();
        double phi = rphi();
        vys[i] = v2s[i] * std::cos(phi);
        vzs[i] = v2s[i] * std::sin(phi);
    }

    double mean{0};
    mean = std::accumulate(v2s, v2s + sz, mean) / sz;
    XCTAssert(std::abs(mx.mean() - mean)/mx.mean() < 1e-3, @"mean = %f", mean);
    double var{0};
    var = std::inner_product(v2s, v2s + sz, v2s, var)/sz - mx.mean()*mx.mean();
    XCTAssert(std::abs(mx.variance() - var)/mx.variance() < 1e-2, @"var = %f", var);

    std::ostringstream os;
    os.setf(os.fixed);
    printo(os, "{", *v2s);
    for (long i = 1; i < sz; ++i) {
        printo(os, ", ", v2s[i]);
    }
    printo(os, "}");
    if (0) {
        NSError *error;
        NSString *str = [NSString stringWithUTF8String:os.str().c_str()];
        NSURL *path = [NSURL fileURLWithPath:NSHomeDirectory() isDirectory:YES];
        path = [path URLByAppendingPathComponent:@"Downloads/PerpendicularRingDistribution.m" isDirectory:NO];
        BOOL tf = [str writeToURL:path atomically:YES encoding:NSUTF8StringEncoding error:&error];
        XCTAssert(tf, @"%@", error.localizedDescription);
    }
}

- (void)testShellDistribution {
    using namespace Utility::__3_;

    constexpr double vth = .5, vs = 5;
    ShellDistribution sh(vth, vs, false);
    XCTAssert(std::abs(sh.pdf(vs) - 2*M_2_SQRTPI*vs*vs/(sh.A()*vth*vth*vth)) < 1e-10, @"pdf(vs) = %f", sh.pdf(vs));
    XCTAssert(std::abs(vs - sh.icdf(sh.cdf(vs))) < 1e-5, @"cdf(vs) = %f, icdf(cdf(vs)) = %f", sh.cdf(vs), sh.icdf(sh.cdf(vs)));

    BitReversedRandomReal rng1(2, 1);
    RandomVariate rv(&rng1, &sh);
    BitReversedRandomReal rng2(3);
    UniformDistribution phi(0, 2*M_PI);
    RandomVariate rphi(&rng2, &phi);
    BitReversedRandomReal rng3(5);
    UniformDistribution cosa(-1, 1);
    RandomVariate rcosa(&rng3, &cosa);

    constexpr long sz = 50000;
    double vxs[sz], vys[sz], vzs[sz], vvs[sz];
    for (long i = 0; i < sz; ++i) {
        vvs[i] = rv();
        double phi = rphi();
        double cosa = rcosa();
        double sina = std::sqrt((1+cosa)*(1-cosa));
        vxs[i] = vvs[i] * cosa;
        vys[i] = vvs[i] * sina * std::cos(phi);
        vzs[i] = vvs[i] * sina * std::sin(phi);
    }

    double mean{0};
    mean = std::accumulate(vvs, vvs + sz, mean) / sz;
    XCTAssert(std::abs(sh.mean() - mean)/sh.mean() < 1e-3, @"mean = %f", mean);
    double var{0};
    var = std::inner_product(vvs, vvs + sz, vvs, var)/sz - sh.mean()*sh.mean();
    XCTAssert(std::abs(sh.variance() - var)/sh.variance() < 1e-2, @"var = %f", var);

    std::ostringstream os;
    os.setf(os.fixed);
    printo(os, "{", *vvs);
    for (long i = 1; i < sz; ++i) {
        printo(os, ", ", vvs[i]);
    }
    printo(os, "}");
    if (0) {
        NSError *error;
        NSString *str = [NSString stringWithUTF8String:os.str().c_str()];
        NSURL *path = [NSURL fileURLWithPath:NSHomeDirectory() isDirectory:YES];
        path = [path URLByAppendingPathComponent:@"Downloads/ShellDistribution.m" isDirectory:NO];
        BOOL tf = [str writeToURL:path atomically:YES encoding:NSUTF8StringEncoding error:&error];
        XCTAssert(tf, @"%@", error.localizedDescription);
    }
}

- (void)testSineAlphaPitchAngleDistribution {
    using namespace Utility::__3_;

    constexpr double n = .5;
    SineAlphaPitchAngleDistribution pa(n, false);
    XCTAssert(std::abs(.5 - pa.cdf(M_PI_2)) < 1e-5, @"cdf(pi/2) = %f", pa.cdf(M_PI_2));

    BitReversedRandomReal rng(2);
    RandomVariate rv(&rng, &pa);

    constexpr long sz = 50000;
    double alphas[sz];
    for (long i = 0; i < sz; ++i) {
        alphas[i] = rv();
    }

    double mean{0};
    mean = std::accumulate(alphas, alphas + sz, mean) / sz;
    XCTAssert(std::abs(pa.mean() - mean)/pa.mean() < 1e-3, @"mean = %f", mean);
    double var{0};
    var = std::inner_product(alphas, alphas + sz, alphas, var)/sz - pa.mean()*pa.mean();
    XCTAssert(std::abs(pa.variance() - var)/pa.variance() < 1e-2, @"var = %f", var);

    std::ostringstream os;
    os.setf(os.fixed);
    printo(os, "{", *alphas);
    for (long i = 1; i < sz; ++i) {
        printo(os, ", ", alphas[i]);
    }
    printo(os, "}");
    if (0) {
        NSError *error;
        NSString *str = [NSString stringWithUTF8String:os.str().c_str()];
        NSURL *path = [NSURL fileURLWithPath:@"Downloads/SineAlphaPitchAngleDistribution.m" relativeToURL:[NSURL fileURLWithPath:NSHomeDirectory()]];
        BOOL tf = [str writeToURL:path atomically:YES encoding:NSUTF8StringEncoding error:&error];
        XCTAssert(tf, @"%@", error.localizedDescription);
    }
}

- (void)testSubtractedMaxwellianDistribution {
    using namespace Utility::__3_;

    constexpr double n1 = 6.3, n2 = 5.3;
    const double vth1 = std::sqrt(0.8), vth2 = vth1*std::sqrt(0.9);

    SubtractedMaxwellianDistribution _a({n1, n2}, {vth1, vth2}), a(std::move(_a));
    BitReversedRandomReal rng(2);
    RandomVariate rv(&rng, &a);

    XCTAssert(std::abs(a.mean() - 1.28375) < 1e-4 && std::abs(a.variance() - 0.18799) < 1e-4,
              @"mean = %f, var = %f", a.mean(), a.variance());

    std::ostringstream os;
    os.setf(os.fixed);
    printo(os, "pdf = {{0, ", a.pdf(0), "}");
    for (double x = 0; x < 4; x += 0.1) {
        printo(os, ", {", x, ", ", a.pdf(x), "}");
    }
    printo(os, "}\n\n");

    printo(os, "icdf = {{0, ", a.icdf(0), "}");
    for (double i = 0; i <= 1; i += 0.02) {
        printo(os, ", {", i, ", ", a.icdf(i), "}");
    }
    printo(os, "}\n\n");
    //printo(std::cout, os.str());

    constexpr long sz = 50000;
    double sub[sz];
    std::generate_n(sub, sz, rv);

    double mean{0};
    mean = std::accumulate(sub, sub + sz, mean) / sz;
    XCTAssert(std::abs(a.mean() - mean)/a.mean() < 1e-3, @"mean = %f", mean);
    double var{0};
    var = std::inner_product(sub, sub + sz, sub, var)/sz - a.mean()*a.mean();
    XCTAssert(std::abs(a.variance() - var)/a.variance() < 1e-2, @"var = %f", var);

    os.str({});
    printo(os, "{", *sub);
    for (long i = 1; i < sz; ++i) {
        printo(os, ", ", sub[i]);
    }
    printo(os, "}");
    if (0) {
        NSError *error;
        NSString *str = [NSString stringWithUTF8String:os.str().c_str()];
        NSURL *path = [NSURL fileURLWithPath:@"Downloads/SubtractedMaxwellianDistribution.m" relativeToURL:[NSURL fileURLWithPath:NSHomeDirectory()]];
        BOOL tf = [str writeToURL:path atomically:YES encoding:NSUTF8StringEncoding error:&error];
        XCTAssert(tf, @"%@", error.localizedDescription);
    }
}

@end
