//
//  UtilityKitTests-AlgorithmKit.m
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 7/16/16.
//  Copyright © 2016 KYUNGGUK MIN. All rights reserved.
//

#import <XCTest/XCTest.h>

// MARK: Version 3
//
#import <UtilityKit/AlgorithmKit.h>
#import <UtilityKit/ArrayKit.h>
#import <vector>
#include <cmath>
#include <complex>
#include <iostream>
#include <sstream>
#include <memory>

@interface UtilityKitTests_AlgorithmKit__3_ : XCTestCase

@end

@implementation UtilityKitTests_AlgorithmKit__3_

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

// point printer:
struct print_points {
    template<typename Iterator>
    auto operator()(Iterator first, Iterator last) const -> void {
        printf("{");
        if (first!=last) { // If not empty
            printf("{%f, %f}", first->first, first->second);
            while (++first != last) {
                printf(", {%f, %f}", first->first, first->second);
            }
        }
        printf("}");
    }
};

- (void)testAdaptiveSampling1D {
    using Utility::__3_::AdaptiveSampling1D;
    using Utility::__3_::trapz::qsimp;

    // Maxwellian:
    try {
        AdaptiveSampling1D<double> adaptiveSampling;
        adaptiveSampling.maxRecursion = 10;
        adaptiveSampling.accuracyGoal = 4;
        adaptiveSampling.initialPoints = 10;
        adaptiveSampling.yScaleAbsoluteTolerance = 1e-5;

        auto f = [](double v)->double {
            auto v2 = v*v;
            return 2.*M_2_SQRTPI * v2*std::exp(-v2);
        };

        auto&& points = adaptiveSampling(f, 0, 5);
        XCTAssert(points.begin()!=points.end(), @"");
        //print_points()(points.begin(), points.end());
    } catch (std::exception &e) {
        XCTAssert(false, @"c++ exception: %s", e.what());
    }

    // Ring:
    try {
        AdaptiveSampling1D<double> adaptiveSampling;
        adaptiveSampling.maxRecursion = 10;
        adaptiveSampling.accuracyGoal = 4;
        adaptiveSampling.initialPoints = 20;
        adaptiveSampling.yScaleAbsoluteTolerance = 1e-5;

        constexpr double vr = 1.;
        auto f = [](double v)->double {
            auto v_ = v - vr;
            return 2. * v*std::exp(-(v_*v_)) /
            (std::exp(-vr*vr) + 2./M_2_SQRTPI*vr*(1+std::erf(vr)));
        };

        auto&& points = adaptiveSampling(f, 0, vr+4);
        XCTAssert(points.begin()!=points.end(), @"");
        //print_points()(points.begin(), points.end());
    } catch (std::exception &e) {
        XCTAssert(false, @"c++ exception: %s", e.what());
    }

    // Sine:
    try {
        AdaptiveSampling1D<double> sampler;
        sampler.maxRecursion = 20;
        sampler.accuracyGoal = 7;
        sampler.initialPoints = 100;
        sampler.yScaleAbsoluteTolerance = 1e-10;

        constexpr double n = 1.5, n_2 = n/2;
        auto f = [](double alpha)->double {
            double const A = std::tgamma(1 + n_2)/std::tgamma(1.5 + n_2) / M_2_SQRTPI;
            if (std::abs(std::sin(alpha)) < 1e-10) return 0;
            double y = std::pow(std::sin(alpha), n + 1) * 0.5/A;
            return y;
        };

        auto&& points = sampler([&f](double x)->double {
            return qsimp(f, std::make_pair(double(0), x), 4).first;
        }, 0, M_PI, 1);
        //print_points()(points.begin(), points.end()), printf("\n");
        printf("%ld points\n", std::distance(points.begin(), points.end()));
    } catch (std::exception &e) {
        XCTAssert(false, @"c++ exception: %s", e.what());
    }
}

- (void)testLinearInterpolation {
    using Utility::__3_::AdaptiveSampling1D;
    using Utility::__3_::LinearInterpolation;

    // For simple function
    //
    try {
        AdaptiveSampling1D<double> adaptiveSampling;
        adaptiveSampling.maxRecursion = 10;
        adaptiveSampling.accuracyGoal = 4;
        adaptiveSampling.initialPoints = 10;
        adaptiveSampling.yScaleAbsoluteTolerance = 1e-5;

        auto f = [](double v)->double {
            auto v2 = v*v;
            return 2.*M_2_SQRTPI * v2*std::exp(-v2);
        };

        auto&& points = adaptiveSampling(f, 0, 5);
        XCTAssert(points.begin()!=points.end(), @"");

        LinearInterpolation<double> lin(points.begin(), points.end());
        std::vector<decltype(lin)::point_type> pts_lin;
        for (double x=-1; x<1.5; x+=0.02) {
            pts_lin.push_back({x, lin(x)});
        }

//        printf("{");
//        print_points()(points.begin(), points.end());
//        printf(",");
//        print_points()(pts_lin.begin(), pts_lin.end());
//        printf("}\n");
    } catch (std::exception &e) {
        XCTAssert(false, @"c++ exception: %s", e.what());
    }
}

- (void)testMonotoneCubicInterpolation {
    using Utility::__3_::AdaptiveSampling1D;
    using Utility::__3_::MonotoneCubicInterpolation;

    // For discrete, monotonically increasing points:
    try {
        using Float = float;
        std::map<Float, Float> discrete_points{{1, 0.0960692}, {2, 0.117699}, {3, 0.295713}, {4, 0.386996}, {5, 0.402511}, {6, 0.441145}, {7, 0.490139}, {8, 0.546638}, {9, 0.801355}, {10, 0.974448}};
        MonotoneCubicInterpolation<Float> interp(discrete_points.begin(), discrete_points.end());
        decltype(discrete_points) interp_points;
        for (Float x=0.; x<=11; x+=.1) {
            interp_points[x] = interp(x);
        }
//        printf("{");
//        print_points()(discrete_points.begin(), discrete_points.end());
//        printf(",");
//        print_points()(interp_points.begin(), interp_points.end());
//        printf("}\n");
//        return;
    } catch (std::exception &e) {
        XCTAssert(false, @"c++ exception: %s", e.what());
    }

    // For simple function:
    try {
        AdaptiveSampling1D<double> adaptiveSampling;
        adaptiveSampling.maxRecursion = 10;
        adaptiveSampling.accuracyGoal = 4;
        adaptiveSampling.initialPoints = 10;
        adaptiveSampling.yScaleAbsoluteTolerance = 1e-5;

        auto f = [](double v)->double {
            auto v2 = v*v;
            return 2.*M_2_SQRTPI * v2*std::exp(-v2);
        };

        auto&& points = adaptiveSampling(f, 0, 5);
        XCTAssert(points.begin()!=points.end(), @"");

        MonotoneCubicInterpolation<double> interp(points.begin(), points.end());
        std::vector<decltype(interp)::point_type> pts_interp;
        for (double x=-.1; x<3; x+=0.02) {
            pts_interp.push_back({x, interp(x)});
        }

//        printf("{");
//        print_points()(points.begin(), points.end());
//        printf(",");
//        print_points()(pts_interp.begin(), pts_interp.end());
//        printf("}\n");
//        return;
    } catch (std::exception &e) {
        XCTAssert(false, @"c++ exception: %s", e.what());
    }

    // Integral and derivative
    try {
        AdaptiveSampling1D<double> adaptiveSampling;
        adaptiveSampling.maxRecursion = 10;
        adaptiveSampling.accuracyGoal = 4;
        adaptiveSampling.initialPoints = 10;
        adaptiveSampling.yScaleAbsoluteTolerance = 1e-5;

        auto f = [](double x)->double {
            return M_2_SQRTPI/2. * std::exp(-x*x);
        };

        auto const points = adaptiveSampling(f, -5, 5);
        MonotoneCubicInterpolation<double> interp(points.begin(), points.end());
        std::vector<decltype(interp)::point_type> pts_interp;
        for (double x=-4; x<4; x+=0.1) {
            pts_interp.push_back({x, interp.derivative(x)()});
        }
        XCTAssert(std::abs(1. - interp.integral()) < 1e-4, @"integral = %f", interp.integral());
        XCTAssert(!interp.derivative(-10), @"");

//        printf("{");
//        print_points()(pts_interp.begin(), pts_interp.end());
//        printf("}\n");
//        return;
    } catch (std::exception &e) {
        XCTAssert(false, @"c++ exception: %s", e.what());
    }
}

- (void)testCubicSplineInterpolation {
    using Utility::__3_::AdaptiveSampling1D;
    using Utility::__3_::CubicSplineInterpolation;

    // For discrete, monotonically increasing points:
    try {
        using Float = float;
        std::map<Float, Float> discrete_points{{1, 0.0960692}, {2, 0.117699}, {3, 0.295713}, {4, 0.386996}, {5, 0.402511}, {6, 0.441145}, {7, 0.490139}, {8, 0.546638}, {9, 0.801355}, {10, 0.974448}};
        CubicSplineInterpolation<Float> interp(discrete_points.begin(), discrete_points.end());
        decltype(discrete_points) interp_points;
        for (Float x=0.; x<=11.; x+=.1) {
            interp_points[x] = interp(x);
        }
//        printf("{");
//        print_points()(discrete_points.begin(), discrete_points.end());
//        printf(",");
//        print_points()(interp_points.begin(), interp_points.end());
//        printf("}\n");
//        return;
    } catch (std::exception &e) {
        XCTAssert(false, @"c++ exception: %s", e.what());
    }

    // For simple function:
    try {
        AdaptiveSampling1D<double> adaptiveSampling;
        adaptiveSampling.maxRecursion = 10;
        adaptiveSampling.accuracyGoal = 4;
        adaptiveSampling.initialPoints = 10;
        adaptiveSampling.yScaleAbsoluteTolerance = 1e-5;

        auto f = [](double v)->double {
            auto v2 = v*v;
            return 2.*M_2_SQRTPI * v2*std::exp(-v2);
        };

        auto&& points = adaptiveSampling(f, 0, 5);
        XCTAssert(points.begin()!=points.end(), @"");

        CubicSplineInterpolation<double> interp(points.begin(), points.end());
        std::vector<decltype(interp)::point_type> pts_interp;
        for (double x=-.1; x<3; x+=0.02) {
            pts_interp.push_back({x, interp(x)});
        }

//        printf("{");
//        print_points()(points.begin(), points.end());
//        printf(",");
//        print_points()(pts_interp.begin(), pts_interp.end());
//        printf("}\n");
//        return;
    } catch (std::exception &e) {
        XCTAssert(false, @"c++ exception: %s", e.what());
    }

    // Integral and derivative
    try {
        AdaptiveSampling1D<double> adaptiveSampling;
        adaptiveSampling.maxRecursion = 10;
        adaptiveSampling.accuracyGoal = 4;
        adaptiveSampling.initialPoints = 10;
        adaptiveSampling.yScaleAbsoluteTolerance = 1e-5;

        auto f = [](double x)->double {
            return M_2_SQRTPI/2. * std::exp(-x*x);
        };

        auto const points = adaptiveSampling(f, -5, 5);
        CubicSplineInterpolation<double> interp(points.begin(), points.end());
        std::vector<decltype(interp)::point_type> pts_interp;
        for (double x=-4; x<4; x+=0.1) {
            pts_interp.push_back({x, interp.derivative(x)()});
        }
        XCTAssert(std::abs(1. - interp.integral()) < 1e-6, @"integral = %f", interp.integral());

//        printf("{");
//        print_points()(pts_interp.begin(), pts_interp.end());
//        printf("}\n");
//        return;
    } catch (std::exception &e) {
        XCTAssert(false, @"c++ exception: %s", e.what());
    }
}

- (void)testSplineInterpolation_Base {
    constexpr long Order = 10;
    using SplineCoefficient = Utility::__3_::SplineCoefficient<float, Order>;
    using SplineInterpolator = SplineCoefficient::spline_interpolator_type;
    try {
        SplineCoefficient c;
        XCTAssert(Order == c.spline_order(), @"");
        XCTAssert(!c, @"");
        XCTAssert(!c.is_regular_grid(), @"");

        SplineInterpolator interp;
        XCTAssert(Order == interp.spline_order(), @"");
        XCTAssert(!interp, @"");
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}
- (void)testSplineInterpolation_Linear {
    using Utility::__3_::AdaptiveSampling1D;
    using LinearCoefficient = Utility::__3_::LinearSplineCoefficient<double>;
    using SplineCoefficient = Utility::__3_::SplineCoefficient<LinearCoefficient::value_type, LinearCoefficient::spline_order()>;
    using SplineInterpolator = LinearCoefficient::spline_interpolator_type;
    XCTAssert(SplineCoefficient::spline_order() == 1, @"");

    // Regular grid
    //
    try {
        SplineCoefficient c;
        std::vector<LinearCoefficient::value_type> xs = {0}, ys;
        for (auto x : xs) {
            ys.push_back(x*x);
        }
        try {
            LinearCoefficient l(xs.begin(), xs.end(), ys.begin());
            XCTAssert(false, @"");
        } catch (std::exception &e) {
            XCTAssert(true, @"%s", e.what());
        }

        xs.clear(); ys.clear();
        double const dx = .1;
        for (double x = 0; x <= 1; x += dx) {
            xs.push_back(x);
            ys.push_back(x * x);
        }

        LinearCoefficient l(xs.begin(), xs.end(), ys.begin());
        c = std::move(l); //c.swap(l);
        XCTAssert(!l && c, @"");
        XCTAssert(c.is_regular_grid() && dx == c.delta()(), @"");
        XCTAssert(c.table().size() == xs.size(), @"");
        XCTAssert(std::abs(c.min_abscissa()-0) < 1e-10 && std::abs(1-c.max_abscissa()) < 1e-10, @"min = %f, max = %f", c.min_abscissa(), c.max_abscissa());
        //XCTAssert(std::abs(c.integrate() - .5) < 1e-10, @"integrate = %f", c.integrate());

        SplineInterpolator interp = c.interpolator();
        XCTAssert(c && interp, @"");

        std::ostringstream os;
        os.setf(os.fixed);
        constexpr long n = 100;
        printo(os, "Regular Grid = {{", interp.min_abscissa(), ", ", interp(interp.min_abscissa())(), ", ", interp.derivative(interp.min_abscissa())(), "}");
        for (long i = 1; i < n; ++i) {
            double const x = double(i)/n * (interp.max_abscissa() - interp.min_abscissa()) + interp.min_abscissa();
            printo(os, ", {", x, ", ", interp(x)(), ", ", interp.derivative(x)(), "}");
        }
        printo(os, ", {", interp.max_abscissa(), ", ", interp(interp.max_abscissa())(), ", ", interp.derivative(interp.max_abscissa())(), "}}\n");

        //printo(std::cout, os.str());
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // Irregular grid
    //
    try {
        AdaptiveSampling1D<double> adaptiveSampling;
        adaptiveSampling.maxRecursion = 10;
        adaptiveSampling.accuracyGoal = 4;
        adaptiveSampling.initialPoints = 10;
        adaptiveSampling.yScaleAbsoluteTolerance = 1e-5;

        auto f = [](double v)->double {
            auto v2 = v*v;
            return M_2_SQRTPI/2. * std::exp(-v2);
        };

        auto&& points = adaptiveSampling(f, 0, 5);
        XCTAssert(points.begin()!=points.end(), @"");

        std::ostringstream os;
        os.setf(os.fixed);
        SplineInterpolator interp = LinearCoefficient(points.begin(), points.end()).interpolator();
        printo(os, "Irregular Grid = {{Indeterminate, Indeterminate}");
        for (double x=-.1; x<1.5; x+=0.02) {
            auto const y = interp(x);
            if (y) {
                printo(os, ", {", x, ", ", *y, "}");
            } else {
                printo(os, ", {", x, ", Indeterminate}");
            }
        }
        printo(os, "}\n");

        //printo(std::cout, os.str());
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}
- (void)testSplineInterpolation_CubicSpline {
    using Utility::__3_::AdaptiveSampling1D;
    using SplineCoefficient = Utility::__3_::CubicSplineCoefficient<double>;
    using SplineInterpolator = SplineCoefficient::spline_interpolator_type;
    XCTAssert(SplineCoefficient::spline_order() == 3, @"");

    // Regular grid
    //
    try {
        std::vector<SplineCoefficient::value_type> xs, ys;
        double const dx = .1;
        for (double x = -1; x <= 1; x += dx) {
            xs.push_back(x);
            ys.push_back(x * x);
        }

        SplineCoefficient c(xs.begin(), xs.end(), ys.begin());
        XCTAssert(c && c.is_regular_grid() && std::abs(dx - *c.delta()) < 1e-10, @"");
        XCTAssert(std::abs(c.integrate() - 2./3) < 1e-3, @"integrate = %f", c.integrate());

        SplineInterpolator interp = c.interpolator();
        XCTAssert(c && interp, @"");

        std::ostringstream os;
        os.setf(os.fixed);
        constexpr long n = 100;
        printo(os, "Regular Grid = {{", interp.min_abscissa(), ", ", interp(interp.min_abscissa())(), ", ", interp.derivative(interp.min_abscissa())(), "}");
        for (long i = 1; i < n; ++i) {
            double const x = double(i)/n * (interp.max_abscissa() - interp.min_abscissa()) + interp.min_abscissa();
            printo(os, ", {", x, ", ", interp(x)(), ", ", interp.derivative(x)(), "}");
        }
        printo(os, ", {", interp.max_abscissa(), ", ", interp(interp.max_abscissa())(), ", ", interp.derivative(interp.max_abscissa())(), "}}\n");

        //printo(std::cout, os.str());
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // Irregular grid
    //
    try {
        AdaptiveSampling1D<double> adaptiveSampling;
        adaptiveSampling.maxRecursion = 10;
        adaptiveSampling.accuracyGoal = 4;
        adaptiveSampling.initialPoints = 10;
        adaptiveSampling.yScaleAbsoluteTolerance = 1e-5;

        auto f = [](double v)->double {
            auto v2 = v*v;
            return M_2_SQRTPI/2. * std::exp(-v2);
        };

        auto&& points = adaptiveSampling(f, 0, 5);
        XCTAssert(points.begin()!=points.end(), @"");

        SplineInterpolator interp = SplineCoefficient(points.begin(), points.end()).interpolator();
        XCTAssert(std::abs(interp.integrate() - .5) < 1e-3, @"integrate = %f", interp.integrate());

        std::ostringstream os;
        os.setf(os.fixed);
        printo(os, "Irregular Grid = {{Indeterminate, Indeterminate}");
        for (double x=-.1; x<1.5; x+=0.02) {
            auto const y = interp(x);
            if (y) {
                printo(os, ", {", x, ", ", *y, "}");
            } else {
                printo(os, ", {", x, ", Indeterminate}");
            }
        }
        printo(os, "}\n");

        //printo(std::cout, os.str());
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}
- (void)testSplineInterpolation_MonotoneCubic {
    using Utility::__3_::AdaptiveSampling1D;
    using SplineCoefficient = Utility::__3_::MonotoneCubicCoefficient<double>;
    using SplineInterpolator = SplineCoefficient::spline_interpolator_type;
    XCTAssert(SplineCoefficient::spline_order() == 3, @"");

    // Regular grid
    //
    try {
        std::vector<SplineCoefficient::value_type> xs, ys;
        double const dx = .1;
        for (double x = -1; x <= 1; x += dx) {
            xs.push_back(x);
            ys.push_back(x * x);
        }

        SplineCoefficient c(xs.begin(), xs.end(), ys.begin());
        XCTAssert(c && c.is_regular_grid() && std::abs(dx - *c.delta()) < 1e-10, @"");
        XCTAssert(std::abs(c.integrate() - 2./3) < 1e-3, @"integrate = %f", c.integrate());

        SplineInterpolator interp = c.interpolator();
        XCTAssert(c && interp, @"");

        std::ostringstream os;
        os.setf(os.fixed);
        constexpr long n = 100;
        printo(os, "Regular Grid = {{", interp.min_abscissa(), ", ", interp(interp.min_abscissa())(), ", ", interp.derivative(interp.min_abscissa())(), "}");
        for (long i = 1; i < n; ++i) {
            double const x = double(i)/n * (interp.max_abscissa() - interp.min_abscissa()) + interp.min_abscissa();
            printo(os, ", {", x, ", ", interp(x)(), ", ", interp.derivative(x)(), "}");
        }
        printo(os, ", {", interp.max_abscissa(), ", ", interp(interp.max_abscissa())(), ", ", interp.derivative(interp.max_abscissa())(), "}}\n");

        //printo(std::cout, os.str());
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // Irregular grid
    //
    try {
        AdaptiveSampling1D<double> adaptiveSampling;
        adaptiveSampling.maxRecursion = 10;
        adaptiveSampling.accuracyGoal = 4;
        adaptiveSampling.initialPoints = 10;
        adaptiveSampling.yScaleAbsoluteTolerance = 1e-5;

        auto f = [](double v)->double {
            auto v2 = v*v;
            return M_2_SQRTPI/2. * std::exp(-v2);
        };

        auto&& points = adaptiveSampling(f, 0, 5);
        XCTAssert(points.begin()!=points.end(), @"");

        SplineInterpolator _interp = SplineCoefficient(points.begin(), points.end()).interpolator();
        SplineInterpolator interp;
        interp.swap(_interp);
        XCTAssert(!_interp && interp, @"");
        XCTAssert(std::abs(interp.integrate() - .5) < 1e-3, @"integrate = %f", interp.integrate());

        std::ostringstream os;
        os.setf(os.fixed);
        printo(os, "Irregular Grid = {{Indeterminate, Indeterminate}");
        for (double x=-.1; x<1.5; x+=0.02) {
            auto const y = interp(x);
            if (y) {
                printo(os, ", {", x, ", ", *y, "}");
            } else {
                printo(os, ", {", x, ", Indeterminate}");
            }
        }
        printo(os, "}\n");

        printo(std::cout, os.str());
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testTrapz {
    using Utility::__3_::trapz::qsimp;
    using Utility::__3_::TrapzIntegrator;
    using Utility::__3_::Numeric::Vector;

    try {
        constexpr double vr = 2.;
        auto pdf = [](double v)->double {
            auto v_vr = v - vr;
            double denom = .5*std::exp(-vr*vr) + 1./M_2_SQRTPI*vr*(1. + std::erf(vr));
            return v*std::exp(-v_vr*v_vr) / denom;
        };

        auto cdf = qsimp(pdf, std::pair<double, double>{0., 5.+vr}, 2);
        XCTAssert(std::abs(1.-cdf.first)<1e-6, @"cdf=%f, n=%d", cdf.first, cdf.second);

        TrapzIntegrator<double> integrator;
        integrator.minRecursion = 2;
        cdf = integrator(pdf, 0., 5.+vr);
        XCTAssert(cdf.second < integrator.maxRecursion && std::abs(1.-cdf.first)<1e-6, @"cdf=%f, n=%d", cdf.first, cdf.second);
    } catch (std::exception &e) {
        XCTAssert(false, @"c++ exception: %s", e.what());
    }

    // complex
    try {
        using Real = double;
        auto f = [](Real x) -> std::complex<Real> {
            return {x*x, x};
        };
        std::complex<Real> const y{1./3., 1./2.};

        TrapzIntegrator<Real, std::complex<Real>> integrator;
        auto const y1 = integrator(f, 0., 1.);
        XCTAssert(y1.second < integrator.maxRecursion && std::abs(y - y1.first)<1e-6, @"y={%f, %f}, n=%d", y1.first.real(), y1.first.imag(), y1.second);
    } catch (std::exception &e) {
        XCTAssert(false, @"c++ exception: %s", e.what());
    }

    // Vector
    try {
        auto f = [](double x) -> Vector<double, 2> {
            return {x*x, x};
        };
        Vector<double, 2> const y{1./3., 1./2.};

        TrapzIntegrator<double, Vector<double, 2>> integrator;
        integrator.accuracyGoal = 6;
        Vector<double, 2> y1;
        unsigned n;
        std::tie(y1, n) = integrator(f, 0., 1., [](Vector<double, 2> const &y) -> double {
            return std::abs(std::get<0>(y));
        });
        XCTAssert(n < integrator.maxRecursion && std::abs((y[0]*y[0] + y[1]*y[1]) - (y1[0]*y1[0] +y1[1]*y1[1]))<1e-6, @"y={%f, %f}, n=%d", y1[0], y1[1], n);
    } catch (std::exception &e) {
        XCTAssert(false, @"c++ exception: %s", e.what());
    }

    // Trapzd sum
    try {
        std::vector<double> xs;
        std::vector<Vector<double, 2>> ys;
        constexpr long nx = 501;
        for (long i = 0; i <= nx; ++i) {
            double const x = double(i)/nx;
            xs.push_back(x);
            ys.push_back({x*x, x});
        }
        Vector<double, 2> const y{1./3., 1./2.};

        TrapzIntegrator<double, Vector<double, 2>> integrator;
        Vector<double, 2> const y1 = integrator.trapzd(xs.begin(), xs.end(), ys.begin());
        XCTAssert(std::abs((y[0]*y[0] + y[1]*y[1]) - (y1[0]*y1[0] +y1[1]*y1[1]))<1e-6, @"y={%f, %f}", y1[0], y1[1]);
    } catch (std::exception &e) {
        XCTAssert(false, @"c++ exception: %s", e.what());
    }
}

- (void)testGaussLegendreIntegrator {
    using Utility::__3_::GaussLegendreIntegrator;
    using Utility::__3_::Numeric::Vector;

    std::ostringstream os;
    os.setf(os.fixed);
    os.precision(15);

    // check abscissas and weights for N = 10; see Numerical Recipe, pp. 180.
    //
    try {
        os.str("");
        GaussLegendreIntegrator<double, 10> integrator;
        for (auto x : integrator.abscissas()) {
            printo(os, x, ", ");
        }
        printo(os, "\n");
        for (auto x : integrator.weights()) {
            printo(os, x, ", ");
        }
        printo(os, "\n");

        //printo(std::cout, os.str());
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // scalar
    //
    try {
        os.str("");
        GaussLegendreIntegrator<double, 20> integrator;

        constexpr double vr = 2.;
        auto pdf = [](double v)->double {
            auto v_vr = v - vr;
            double denom = .5*std::exp(-vr*vr) + 1./M_2_SQRTPI*vr*(1. + std::erf(vr));
            return v*std::exp(-v_vr*v_vr) / denom;
        };
        double const n = integrator(pdf, 0, vr + 5, double{});
        XCTAssert(std::abs(n - 1) < 1e-10, @"n = %f", n);

        double const m = integrator([](double x)->double {
            return std::sin(2*M_PI*x);
        }, 0, 10, double{});
        XCTAssert(std::abs(m) < 1e-10, @"m = %f", m);
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // complex
    try {
        os.str("");
        using Real = double;
        GaussLegendreIntegrator<Real, 10> integrator;

        auto f = [](Real x) -> std::complex<Real> {
            return {x*x, x};
        };
        std::complex<Real> const y{Real(1./3.), Real(1./2.)};

        auto const y1 = integrator(f, 0., 1.);
        XCTAssert(std::abs(y - y1) < 1e-10, @"y=%f %+f", y1.real(), y1.imag());
    } catch (std::exception &e) {
        XCTAssert(false, @"c++ exception: %s", e.what());
    }

    // Vector
    try {
        os.str("");
        using Real = double;
        GaussLegendreIntegrator<Real, 10> integrator;

        auto f = [](Real x) -> Vector<Real, 2> {
            return {x*x, x};
        };
        Vector<Real, 2> const y{1./3., 1./2.};

        Vector<Real, 2> const y1 = integrator(f, 0., 1.);
        XCTAssert(std::abs((y[0]*y[0] + y[1]*y[1]) - (y1[0]*y1[0] +y1[1]*y1[1]))<1e-6, @"y={%f, %f}", y1[0], y1[1]);
    } catch (std::exception &e) {
        XCTAssert(false, @"c++ exception: %s", e.what());
    }
}

- (void)testNewtonRootFinder {
    using Utility::__3_::NewtonRootFinder;

    @autoreleasepool {
        double(^fOdf)(double) = ^(double x) {
            //return double(INFINITY);
            return (x*x - 1.) / (2.*x);
        };
        auto rootF = NewtonRootFinder<double(^)(double)>(fOdf);

        double const x_exact = 1;
        double const x0 = 1.1;
        decltype(rootF)::optional_type x_n = rootF(x0);
        XCTAssert(x_n && std::isfinite(x_n.value()) && std::abs(x_exact - x_n()) < rootF.reltol, @"");
    }

    @autoreleasepool {
        double __block y0 = 0;
        double(^f)(double) = ^(double x) { // 0 < x < inf
            return std::erf(x) - y0;
        };
        double(^df)(double) = ^(double x) { // 0 < x < inf
            return std::exp(-x*x) * M_2_SQRTPI;
        };
        double(^fOdf)(double) = ^(double x) {
            return f(x) / df(x);
        };
        auto rootF = NewtonRootFinder<double(^)(double)>(fOdf);

        double const dy = .05;
        std::vector<std::pair<double, double>> yx;
        for (yx.push_back({y0 = 0., 0.1}); y0 < .999; y0 += dy) {
            auto x_n = rootF(yx.back().second);
            if (x_n) {
                yx.back().second = x_n();
                yx.push_back({y0+dy, x_n()});
            }
            XCTAssert(x_n, @"y = %f", y0);
        }
        yx.pop_back();

        printo(std::cout, "{{0, 0}");
        for (unsigned i = 1; i < yx.size(); ++i) {
            printo(std::cout, ", {", yx[i].first, ", ", yx[i].second, "}");
        }
        printo(std::cout, "}\n");
    }

    @autoreleasepool {
        double c = 0;
        auto fOdf = [&c](double x)->double {
            double f = x*(x - c);
            double df = 2*x - c;
            return f/df;
        };
        auto rootF = NewtonRootFinder<decltype(fOdf)>(fOdf);
        rootF.multiplicity = 1;
        rootF.max_iterations = 10;

        c = 1 + 1e-2;
        for (double x0 = c + .1; c >= 0.; c -= .1) {
            auto x = rootF(x0);
            if (x) {
                x0 = x();
                printo(std::cout, "c = ", c, ", x = ", x0, "\n");
            } else {
                XCTAssert(x, @"c = %f, x0 = %f", c, x0);
                break;
            }
        }
    }
}

- (void)testMullerRootFinder {
    using Utility::__3_::MullerRootFinder;

    @autoreleasepool {
        double b = -1;
        auto f = [&b](std::complex<double> x) {
            return (x - 1.)*(x - 1.) + b;
        };
        auto rootF = MullerRootFinder<decltype(f)>(f);

        std::complex<double> x0 = 2.1, x1 = 2.07, x2 = 2.03;
        std::vector<std::pair<double, std::complex<double>>> bx;
        for (b = -1; b < 1; b += .1) {
            auto const x = rootF(x0, x1, x2);
            if (!x) {
                XCTAssert(x, @"b = %f", b);
                break;
            } else {
                bx.push_back({b, x()});
                x0 = x1; x1 = x2; x2 = x();
            }
        }

        auto flags = std::cout.flags();
        std::cout.setf(std::cout.showpos);
        std::cout.setf(std::cout.fixed);
        printo(std::cout, "{");
        for (auto const& pair : bx) {
            printo(std::cout, "{", pair.first, ", ", pair.second.real(), " ", pair.second.imag(), " I}, ");
        }
        printo(std::cout, "}\n");
        std::cout.flags(flags);
    }
}

- (void)testDecimal {
    using Utility::__3_::Decimal;

    Decimal<int, float> dec{1, .01f};
    std::cout << dec << std::endl;
    dec = 4.56789;
    std::cout << dec << std::endl;
}

- (void)testDecimal_arithematic {
    using Utility::__3_::Decimal;

    double fb(2.3), fc(3.6944);
    Decimal<int, double> a, b(fb), c(fc), d;

    // compound - decimal & decimal:
    a = b;
    a += c;
    XCTAssert(std::abs(double(a) - (fb + fc)) < 1e-10, @"a = %.10f, b*c = %.10f", double(a), (fb + fc));
    a = b;
    a -= c;
    XCTAssert(std::abs(double(a) - (fb - fc)) < 1e-10, @"a = %.10f, b*c = %.10f", double(a), (fb - fc));
    a = b;
    a *= c;
    XCTAssert(std::abs(double(a) - (fb * fc)) < 1e-10, @"a = %.10f, b*c = %.10f", double(a), (fb * fc));
    a = b;
    a /= c;
    XCTAssert(std::abs(double(a) - (fb / fc)) < 1e-10, @"a = %.10f, b*c = %.10f", double(a), (fb / fc));

    // compound - decimal & double:
    a = b;
    a += fc;
    XCTAssert(std::abs(double(a) - (fb + fc)) < 1e-10, @"a = %.10f, b*c = %.10f", double(a), (fb + fc));
    a = b;
    a -= fc;
    XCTAssert(std::abs(double(a) - (fb - fc)) < 1e-10, @"a = %.10f, b*c = %.10f", double(a), (fb - fc));
    a = b;
    a *= fc;
    XCTAssert(std::abs(double(a) - (fb * fc)) < 1e-10, @"a = %.10f, b*c = %.10f", double(a), (fb * fc));
    a = b;
    a /= fc;
    XCTAssert(std::abs(double(a) - (fb / fc)) < 1e-10, @"a = %.10f, b*c = %.10f", double(a), (fb / fc));

    // binary - decimal & decimal:
    a = b + c;
    XCTAssert(std::abs(double(a) - (fb + fc)) < 1e-10, @"a = %.10f, b*c = %.10f", double(a), (fb + fc));
    a = b - c;
    XCTAssert(std::abs(double(a) - (fb - fc)) < 1e-10, @"a = %.10f, b*c = %.10f", double(a), (fb - fc));
    a = b * c;
    XCTAssert(std::abs(double(a) - (fb * fc)) < 1e-10, @"a = %.10f, b*c = %.10f", double(a), (fb * fc));
    a = b / c;
    XCTAssert(std::abs(double(a) - (fb / fc)) < 1e-10, @"a = %.10f, b*c = %.10f", double(a), (fb / fc));

    // binary - decimal & double:
    a = b + fc;
    d = fc + b;
    XCTAssert(std::abs(double(a) - (fb + fc)) < 1e-10, @"a = %.10f, b*c = %.10f", double(a), (fb + fc));
    XCTAssert(std::abs(double(d) - (fc + fb)) < 1e-10, @"d = %.10f, b*c = %.10f", double(d), (fc + fb));
    a = b - fc;
    d = fc - b;
    XCTAssert(std::abs(double(a) - (fb - fc)) < 1e-10, @"a = %.10f, b*c = %.10f", double(a), (fb - fc));
    XCTAssert(std::abs(double(d) - (fc - fb)) < 1e-10, @"d = %.10f, b*c = %.10f", double(d), (fc - fb));
    a = b * fc;
    d = fc * b;
    XCTAssert(std::abs(double(a) - (fb * fc)) < 1e-10, @"a = %.10f, b*c = %.10f", double(a), (fb * fc));
    XCTAssert(std::abs(double(d) - (fc * fb)) < 1e-10, @"d = %.10f, b*c = %.10f", double(d), (fc * fb));
    a = b / fc;
    d = fc / b;
    XCTAssert(std::abs(double(a) - (fb / fc)) < 1e-10, @"a = %.10f, b*c = %.10f", double(a), (fb / fc));
    XCTAssert(std::abs(double(d) - (fc / fb)) < 1e-10, @"d = %.10f, b*c = %.10f", double(d), (fc / fb));
}




template<typename T>
void _kaijun_smoothing(T *data, T *buffer, long Mx, long My);
template<typename T>
void _kaijun_smoothing(T **_data, T **_buffer, long Mx, long My);

- (void)testHammingFilter_1D {
    using Utility::__3_::Dynamic::PaddedArray;
    using Utility::__3_::HammingFilter;

    std::stringstream os;
    try {
        PaddedArray<double, 1> km_i(5, 0.0);
        PaddedArray<double, 0> km_o(km_i.size());
        km_i[2] = 1.;
        km_i[-1] = km_i.back(); *km_i.end() = km_i.front();

        std::unique_ptr<double[]> kl_i(new double[3*km_i.max_size()]);
        std::unique_ptr<double[]> kl_o(new double[3*km_i.max_size()]);
        std::fill_n(kl_i.get(), 3*km_i.max_size(), 0.0);
        kl_i[0*km_i.max_size() + 2+km_i.pad_size()] = km_i[2];
        kl_i[1*km_i.max_size() + 2+km_i.pad_size()] = km_i[2];
        kl_i[2*km_i.max_size() + 2+km_i.pad_size()] = km_i[2];

        if ( (0) ) {
            std::cout << "km_i=" << km_i << std::endl;
            std::cout << "kl_i={";
            for (unsigned i = 0; i < 3; ++i) {
                for (unsigned j = 0; j < km_i.max_size(); ++j) {
                    std::cout << kl_i[i*km_i.max_size() + j] << ", ";
                }
            }
            std::cout << "}\n";
            return;
        }

        // filtration
        for (int i = 0; i < 4; ++i) {
            constexpr HammingFilter<1> hf{};
            hf(km_i.begin(), km_i.end(), km_o.begin());
            std::copy(km_o.begin(), km_o.end(), km_i.begin());
            km_i[-1] = km_i.back(); *km_i.end() = km_i.front();

            _kaijun_smoothing(kl_i.get(), kl_o.get(), 1, long(km_i.size()));

            os.str(""); os
            << "i=" << i << std::endl
            << "km_i=" << km_i << std::endl;
            {
                std::cout << "kl_i={";
                for (unsigned i = 1, j = 0; j < km_i.max_size(); ++j) {
                    std::cout << kl_i[i*km_i.max_size() + j] << ", ";
                }
                std::cout << "}\n";
            }
            double const* beg = kl_o.get() + km_i.max_size()+km_i.pad_size();
            bool is_equal = PaddedArray<double, 0>(beg, beg+km_o.size()) == km_o;
            XCTAssert(is_equal, @"%s", os.str().c_str());
            //std::cout << os.str() << std::endl;
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testHammingFilter_2D {
    using Utility::__3_::Dynamic::PaddedArray;
    using Utility::__3_::HammingFilter;

    std::stringstream os;
    try {
        PaddedArray<PaddedArray<double, 1>, 1> km_i(3, PaddedArray<double, 1>(5, 0.0));
        km_i[1][2] = km_i[0][0] = 1.;
        for (auto& km_i : km_i) {
            km_i[-1] = km_i.back(); *km_i.end() = km_i.front();
        }
        km_i[-1] = km_i.back(); *km_i.end() = km_i.front();

        PaddedArray<PaddedArray<double, 1>, 1> kl_i = km_i;

        if ( (0) ) {
            std::cout << "km_i=" << km_i << std::endl;
            std::cout << "kl_i=" << kl_i << std::endl;
            return;
        }

        // filtration
        for (int i = 0; i < 4; ++i) {
            decltype(km_i) km_o = km_i;
            constexpr HammingFilter<2> hf{};
            hf(km_i.begin(), km_i.end(), km_o.begin());
            km_i = km_o;
            for (auto& km_i : km_i) {
                km_i[-1] = km_i.back(); *km_i.end() = km_i.front();
            }
            km_i[-1] = km_i.back(); *km_i.end() = km_i.front();

            std::unique_ptr<double[]> ip(new double[kl_i.max_size() * kl_i[0].max_size()]);
            std::unique_ptr<double[]> op(new double[kl_i.max_size() * kl_i[0].max_size()]);
            for (long n = 0, i = -long(kl_i.pad_size()); i < long(kl_i.size()+kl_i.pad_size()); ++i) {
                for (long j = -long(kl_i[i].pad_size()); j < long(kl_i[i].size()+kl_i[i].pad_size()); ++j, ++n) {
                    ip[unsigned(n)] = kl_i[i][j];
                }
            }
            _kaijun_smoothing(ip.get(), op.get(), long(kl_i.size()), long(kl_i[0].size()));
            for (long n = 0, i = -long(kl_i.pad_size()); i < long(kl_i.size()+kl_i.pad_size()); ++i) {
                for (long j = -long(kl_i[i].pad_size()); j < long(kl_i[i].size()+kl_i[i].pad_size()); ++j, ++n) {
                    kl_i[i][j] = ip[unsigned(n)];
                }
            }

            os.str(""); os
            << "i=" << i << std::endl
            << "km_i=" << km_i << std::endl
            << "kl_i=" << kl_i << std::endl;
            //std::cout << os.str() << std::endl;
            for (unsigned i = 0; i < km_i.size(); ++i) {
                bool tf = km_i[i] == kl_i[i];
                XCTAssert(tf, @"%s", os.str().c_str());
                if (!tf) {
                    break;
                }
            }
        }
        std::cout << os.str() << std::endl;
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testHammingFilter_3D {
    using Utility::__3_::Dynamic::PaddedArray;
    using Utility::__3_::HammingFilter;

    std::stringstream os;
    try {
        PaddedArray<PaddedArray<PaddedArray<double, 1>, 1>, 1>
        km_i(1, PaddedArray<PaddedArray<double, 1>, 1>(5, PaddedArray<double, 1>(5, 0.0)));
        km_i[0][2][2] = km_i[0][0][0] = 1.;
        for (auto& km_i : km_i) {
            for (auto& km_i : km_i) {
                km_i[-1] = km_i.back(); *km_i.end() = km_i.front();
            }
            km_i[-1] = km_i.back(); *km_i.end() = km_i.front();
        }
        km_i[-1] = km_i.back(); *km_i.end() = km_i.front();

        PaddedArray<PaddedArray<double, 1>, 1> kl_i = km_i[0];

        if ( (0) ) {
            std::cout << "km_i=" << km_i[0] << std::endl;
            std::cout << "kl_i=" << kl_i << std::endl;
            return;
        }

        // filtration
        for (int i = 0; i < 4; ++i) {
            decltype(km_i) km_o = km_i;
            constexpr HammingFilter<3> hf{};
            hf(km_i.begin(), km_i.end(), km_o.begin());
            km_i = km_o;
            for (auto& km_i : km_i) {
                for (auto& km_i : km_i) {
                    km_i[-1] = km_i.back(); *km_i.end() = km_i.front();
                }
                km_i[-1] = km_i.back(); *km_i.end() = km_i.front();
            }
            km_i[-1] = km_i.back(); *km_i.end() = km_i.front();

            std::unique_ptr<double[]> ip(new double[kl_i.max_size() * kl_i[0].max_size()]);
            std::unique_ptr<double[]> op(new double[kl_i.max_size() * kl_i[0].max_size()]);
            for (long n = 0, i = -long(kl_i.pad_size()); i < long(kl_i.size()+kl_i.pad_size()); ++i) {
                for (long j = -long(kl_i[i].pad_size()); j < long(kl_i[i].size()+kl_i[i].pad_size()); ++j, ++n) {
                    ip[unsigned(n)] = kl_i[i][j];
                }
            }
            _kaijun_smoothing(ip.get(), op.get(), long(kl_i.size()), long(kl_i[0].size()));
            for (long n = 0, i = -long(kl_i.pad_size()); i < long(kl_i.size()+kl_i.pad_size()); ++i) {
                for (long j = -long(kl_i[i].pad_size()); j < long(kl_i[i].size()+kl_i[i].pad_size()); ++j, ++n) {
                    kl_i[i][j] = ip[unsigned(n)];
                }
            }

            os.str(""); os
            << "i=" << i << std::endl
            << "km_i=" << km_i[0] << std::endl
            << "kl_i=" << kl_i << std::endl;
            //std::cout << os.str() << std::endl;
            for (unsigned i = 0; i < km_i[0].size(); ++i) {
                bool tf = km_i[0][i] == kl_i[i];
                XCTAssert(tf, @"%s", os.str().c_str());
                if (!tf) {
                    break;
                }
            }
        }
        std::cout << os.str() << std::endl;
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testHammingFilter_Performance {
    using Utility::__3_::Dynamic::PaddedArray;
    using Utility::__3_::HammingFilter;

    struct Alloc : std::allocator<double> {
        pointer p;
        pointer allocate(size_type) { return p; }
        void deallocate(pointer, size_type) {}
    };

    constexpr long Nx = 100, Ny = 30000;
    std::unique_ptr<double[]> ip(new double[(Nx+2) * (Ny+2)]);
    std::unique_ptr<double[]> op(new double[(Nx+2) * (Ny+2)]);

    PaddedArray<PaddedArray<double, 1, Alloc>, 1> km_i(Nx);
    Alloc a; a.p = ip.get();
    for (long i = -long(km_i.pad_size()); i < long(km_i.size()+km_i.pad_size()); ++i, a.p += Ny+km_i.pad_size()) {
        km_i[i] = PaddedArray<double, 1, Alloc>(Ny, 0.0, a);
    }

    PaddedArray<PaddedArray<double, 1, Alloc>, 1> km_o(Nx);
    a.p = op.get();
    for (long i = -long(km_o.pad_size()); i < long(km_o.size()+km_o.pad_size()); ++i, a.p += Ny+km_o.pad_size()) {
        km_o[i] = PaddedArray<double, 1, Alloc>(Ny, 0.0, a);
    }

    if ( (0) ) {
        std::cout << "km_i=" << km_i << std::endl;
        std::cout << "km_o=" << km_o << std::endl;
        return;
    }

    // filtration
    constexpr int n_filters = 10;
    [self measureBlock:[&km_i, &km_o, self]() {
        constexpr HammingFilter<2> hf{};
        try {
            // Initialize
            std::fill_n(km_i.data()->data(), km_i.max_size() * km_i[0].max_size(), 0.);
            km_i[km_i.size()/2][km_i[0].size()/2] = 1.;

            // Initial ghost cells
            for (auto& km_i : km_i) {
                km_i[-1] = km_i.back(); *km_i.end() = km_i.front();
            }
            km_i[-1] = km_i.back(); *km_i.end() = km_i.front();

            // Filtration
            if ( (1) ) {
                for (int i = 0; i < n_filters; ++i) {
                    hf(km_i.begin(), km_i.end(), km_o.begin());
                    km_i = km_o;
                    for (auto& km_i : km_i) {
                        km_i[-1] = km_i.back(); *km_i.end() = km_i.front();
                    }
                    km_i[-1] = km_i.back(); *km_i.end() = km_i.front();
                }
            } else {
                for (int i = 0; i < n_filters; ++i) {
                    _kaijun_smoothing(km_i.data()->data(), km_o.data()->data(), long(km_i.size()), long(km_i[0].size()));
                }
            }
        } catch (std::exception &e) {
            XCTAssert(false, @"%s", e.what());
        }
    }];
    //std::cout << "km_i=" << km_i << std::endl;
}


- (void)testShape_1stOrder {
    using Shape = Utility::__3_::Shape<double, 1>;

    Shape sh;
    for (Shape::value_type x = 0.; x < 3.; x += .1) {
        sh(x);
        const long i = long(std::floor(x));
        XCTAssert(i == sh.i[0] && i+1 == sh.i[1], @"x = %f, i(x) = %ld, sh.i[0] = %ld, sh.i[1] = %ld", x, i, sh.i[0], sh.i[1]);
        XCTAssert(std::abs(sh.w[0]+sh.w[1] - 1.)<1e-10, @"w[0]=%f, w[1]=%f", sh.w[0], sh.w[1]);
        XCTAssert(std::abs(x-i - sh.w[1])<1e-10, @"w[0]=%f, w[1]=%f", sh.w[0], sh.w[1]);
    }
}

- (void)testShape_2ndOrder {
    using Shape = Utility::__3_::Shape<double, 2>;

    Shape sh;
    for (Shape::value_type x = 10.; x < 11.; x += .1) {
        sh(x);
        const long i = long(std::round(x));
        XCTAssert(i-1 == sh.i[0] && i+0 == sh.i[1] && i+1 == sh.i[2], @"x = %f, i(round(x)) = %ld, sh.i[0] = %ld, sh.i[1] = %ld, sh.i[2] = %ld", x, i, sh.i[0], sh.i[1], sh.i[2]);
        XCTAssert(std::abs(sh.w[0]+sh.w[1]+sh.w[2] - 1.)<1e-10, @"w[0]=%f, w[1]=%f, w[2]=%f", sh.w[0], sh.w[1], sh.w[2]);
        printf("x=%f, i[1]=%ld, w[0]=%f, w[1]=%f, w[2]=%f\n", x, i, sh.w[0], sh.w[1], sh.w[2]);
    }
}

- (void)testZeroCrossings {
    using Utility::__3_::ZeroCrossings;
    using Utility::__3_::ZeroCrossingsOffset;

    // integral type
    {
        using Int = int;
        std::vector<Int> const ys = {0, -1, 1, -2, 0, 0, -1, 0, 0, 0, 1, 0, 1, 0, 2, -1, -3, 0, 0};
        std::vector<std::pair<long, long>> const zero_idxs = {{1, 2}, {2, 3}, {6, 10}, {14, 15}};
        auto const offsets = ZeroCrossingsOffset(ys.begin(), ys.end());
        XCTAssert(zero_idxs.size() == offsets.size(), @"offsets() = %ld", offsets.size());
        for (unsigned long i = 0; zero_idxs.size() == offsets.size() && i < offsets.size(); ++i) {
            auto const pair0 = zero_idxs.at(i);
            auto const pair1 = offsets.at(i);
            XCTAssert(std::get<0>(pair0) == std::get<0>(pair1), @"");
            XCTAssert(std::get<1>(pair0) == std::get<1>(pair1), @"");
        }
    }

    // integral type
    {
        using Int = int;
        std::vector<Int> const ys = {0, -1};
        std::vector<std::pair<long, long>> const zero_idxs = {};
        auto const offsets = ZeroCrossingsOffset(ys.begin(), ys.end());
        XCTAssert(zero_idxs.size() == offsets.size(), @"offsets() = %ld", offsets.size());
        for (unsigned long i = 0; zero_idxs.size() == offsets.size() && i < offsets.size(); ++i) {
            auto const pair0 = zero_idxs.at(i);
            auto const pair1 = offsets.at(i);
            XCTAssert(std::get<0>(pair0) == std::get<0>(pair1), @"");
            XCTAssert(std::get<1>(pair0) == std::get<1>(pair1), @"");
        }
    }

    // floating point type
    {
        using Real = float;
        std::vector<Real> const ys = {0, -1, 1, -2, 0, 0, -1, 0, 0, 0, 1, 0, 1, 0, 2, -1, -3, 0, 0};
        std::vector<std::pair<long, long>> const zero_idxs = {{1, 2}, {2, 3}, {6, 10}, {14, 15}};
        auto const ranges = ZeroCrossings(ys.begin(), ys.end());
        XCTAssert(zero_idxs.size() == ranges.size(), @"ranges.size() = %ld", ranges.size());
        for (unsigned long i = 0; zero_idxs.size() == ranges.size() && i < ranges.size(); ++i) {
            auto const idx_pair = zero_idxs.at(i);
            auto const it_pair = ranges.at(i);
            XCTAssert(std::get<0>(idx_pair) == std::distance(ys.begin(), std::get<0>(it_pair)), @"");
            XCTAssert(std::get<1>(idx_pair) == std::distance(ys.begin(), std::get<1>(it_pair)), @"");
        }
    }

    // floating point type
    {
        using Real = double;
        std::vector<Real> const ys = {0.147787, 0.510059, -0.983471, 0.589152, -0.56546, -0.221646, 0.933317, 0.917326, -0.595636, -0.63838, 0.320904, 0.76988, 0.00532062, 0.141969, 0.184529, -0.53867, -0.529996, -0.535474, 0.855989, 0.320913};
        std::vector<std::pair<long, long>> zero_idxs = {{2, 3}, {3, 4}, {4, 5}, {6, 7}, {8, 9}, {10, 11}, {15, 16}, {18, 19}};
        for (auto &pair : zero_idxs) {
            pair.first--;
            pair.second--;
        }
        auto const ranges = ZeroCrossings(ys.begin(), ys.end());
        XCTAssert(zero_idxs.size() == ranges.size(), @"ranges.size() = %ld", ranges.size());
        for (unsigned long i = 0; zero_idxs.size() == ranges.size() && i < ranges.size(); ++i) {
            auto const idx_pair = zero_idxs.at(i);
            auto const it_pair = ranges.at(i);
            XCTAssert(std::get<0>(idx_pair) == std::distance(ys.begin(), std::get<0>(it_pair)), @"i = %ld, it0 = %ld, y = %f", i, std::distance(ys.begin(), std::get<0>(it_pair)), *std::get<0>(it_pair));
            XCTAssert(std::get<1>(idx_pair) == std::distance(ys.begin(), std::get<1>(it_pair)), @"i = %ld, it1 = %ld, y = %f", i, std::distance(ys.begin(), std::get<1>(it_pair)), *std::get<1>(it_pair));
        }
    }
}

@end


template<typename T>
void _kaijun_smoothing(T *data, T *buffer, long Mx, long My) {
    long My2 = My + 2;
    long i,j,ix0,ix1,iy0,iy1;
    for (i=1; i<=Mx; i++) { // Filtration
        ix0=i-1;
        ix1=i+1;
        for (j=1; j<=My; j++) {
            iy0=j-1;
            iy1=j+1;
            *(buffer+i*My2+j)=(4.*(*(data+i*My2+j))+2.*(*(data+ix0*My2+j)+*(data+ix1*My2+j)+*(data+i*My2+iy0)+*(data+i*My2+iy1))
                               +(*(data+ix0*My2+iy0)+*(data+ix0*My2+iy1)+*(data+ix1*My2+iy0)+*(data+ix1*My2+iy1)))/16.;
        }
    }
    for (i=1; i<=Mx; i++) { // Copy
        for (j=1; j<=My; j++) {
            *(data+i*My2+j) = *(buffer+i*My2+j);
        }
        *(data+i*My2+My+1) = *(data+i*My2+ 1);
        *(data+i*My2+   0) = *(data+i*My2+My);
    }
    for (j=0; j<My2; j++) {
        *(data+(0   )*My2+j) = *(data+Mx*My2+j);
        *(data+(Mx+1)*My2+j) = *(data+ 1*My2+j);
    }
}
template<typename T>
void _kaijun_smoothing(T **_data, T **_buffer, long Mx, long My) {
    T *data = *_data, *buffer = *_buffer;
    long My2 = My + 2;
    long i,j,ix0,ix1,iy0,iy1;
    for (i=1; i<=Mx; i++) { // Filtration
        ix0=i-1;
        ix1=i+1;
        for (j=1; j<=My; j++) {
            iy0=j-1;
            iy1=j+1;
            *(buffer+i*My2+j)=(4.*(*(data+i*My2+j))+2.*(*(data+ix0*My2+j)+*(data+ix1*My2+j)+*(data+i*My2+iy0)+*(data+i*My2+iy1))
                               +(*(data+ix0*My2+iy0)+*(data+ix0*My2+iy1)+*(data+ix1*My2+iy0)+*(data+ix1*My2+iy1)))/16.;
        }
    }
    { // Swap storage
        *_data = buffer, *_buffer = data;
        data = *_data, buffer = *_buffer;
    }
    // Fill ghost cells
    for (i=1; i<=Mx; i++) {
        *(data+i*My2+My+1) = *(data+i*My2+ 1);
        *(data+i*My2+   0) = *(data+i*My2+My);
    }
    for (j=0; j<My2; j++) {
        *(data+(0   )*My2+j) = *(data+Mx*My2+j);
        *(data+(Mx+1)*My2+j) = *(data+ 1*My2+j);
    }
}
