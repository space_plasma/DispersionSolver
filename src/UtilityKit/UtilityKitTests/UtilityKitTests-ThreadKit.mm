//
//  UtilityKitTests-ThreadKit.mm
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 7/16/16.
//  Copyright © 2016 KYUNGGUK MIN. All rights reserved.
//

#import <XCTest/XCTest.h>

// MARK: Version 3
//
#import <UtilityKit/IOKit.h>
#import <UtilityKit/ThreadKit.h>
#import <functional>
#import <atomic>
#import <iostream>

@interface UtilityKitTests_ThreadKit__3_ : XCTestCase

@end

@implementation UtilityKitTests_ThreadKit__3_

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

namespace {
    void test_function() {
        sleep(1);
        printo(std::cout, __PRETTY_FUNCTION__, '\n');
    }
}
- (void)testThread {
    using namespace Utility::__3_;

    try {
        Thread th(&::test_function);
        if (th.joinable()) {
            th.join();
        }
        XCTAssert(true, @"");

        std::atomic<int> i{0};
        Thread th2 = Thread([&i]()->void {
            i = 1;
        });
        std::swap(th, th2);
        th.join();
        XCTAssert(1==i, @"%d", i.load());

        (th = Thread([&i]()->void {
            i = 2;
        })).detach();
        sleep(1);
        XCTAssert(2==i, @"%d", i.load());
    } catch (std::exception& e) {
        XCTAssert(false, @"%s", e.what());
    } catch (...) {
        XCTAssert(false, @"unknown exception");
    }
}

- (void)testMutex {
    using namespace Utility::__3_;

    try {
        Mutex _1, _2;
        Mutex _mx;
        Mutex &mx = _mx;
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
            LockG<decltype(_1)> l(mx);
            NSLog(@"%s - Lock", __PRETTY_FUNCTION__);
            sleep(2);
            NSLog(@"%s - Unlock", __PRETTY_FUNCTION__);
        });

        {
            usleep(10000);
            LockG<decltype(_1)> l(mx);
            NSLog(@"%s - Lock", __PRETTY_FUNCTION__);
            LockG<decltype(_1)> l1(_1);
            LockG<decltype(_1)> l2(_2);
        }
        NSLog(@"%s - Unlock", __PRETTY_FUNCTION__);
    } catch (std::exception& e) {
        XCTAssert(false, @"%s", e.what());
    } catch (...) {
        XCTAssert(false, @"unknown exception");
    }
}

- (void)testCondVMutex {
    using namespace Utility::__3_;

    try {
        Mutex mx;
        CondV<Mutex> cv[2];
        std::atomic<int> cnt{0};

        Thread th([&mx,&cv,&cnt]()->void {
            NSLog(@"%s - waiting...", __PRETTY_FUNCTION__);
            {
                LockG<Mutex> l(mx);
                cv[0].wait(mx, [&cnt]()->bool {
                    NSLog(@"%s - predicate called. cnt = %d", __PRETTY_FUNCTION__, cnt.load());
                    return cnt;
                });
            }
            NSLog(@"%s - woke up and do work", __PRETTY_FUNCTION__);
            sleep(1);
            NSLog(@"%s - notify", __PRETTY_FUNCTION__);
            //cv[1].notify_one();
            cv[1].notify_all();
        });

        NSLog(@"%s - nofity 3 times", __PRETTY_FUNCTION__);
        cv[0].notify_one();
        usleep(1000);
        cv[0].notify_one();
        usleep(1000);
        cnt = 1;
        cv[0].notify_one();
        NSLog(@"%s - wait on thread...", __PRETTY_FUNCTION__);
        {
            LockG<Mutex> l(mx);
            cv[1].wait(mx);
        }
        usleep(1000);
        NSLog(@"%s - woke up", __PRETTY_FUNCTION__);

        th.join();
    } catch (std::exception& e) {
        XCTAssert(false, @"%s", e.what());
    } catch (...) {
        XCTAssert(false, @"unknown exception");
    }

    // wait_for
    try {
        Mutex mx;
        CondV<Mutex> cv;
        std::atomic<int> i{0};

        Thread
        t2([&cv,&mx,&i]()->void {
            constexpr int idx = 2;
            LockG<Mutex> l(mx);
            if (cv.wait_for(mx, 1, [&i]()->bool { return i == 1; }))
            { NSLog(@"Thread %d finished waiting", idx); }
            else
            { NSLog(@"Thread %d timed out.", idx); }
        }),
        t3([&cv,&mx,&i]()->void {
            constexpr int idx = 3;
            LockG<Mutex> l(mx);
            if (cv.wait_for(mx, 3, [&i]()->bool { return i == 1; }))
            { NSLog(@"Thread %d finished waiting", idx); }
            else
            { NSLog(@"Thread %d timed out.", idx); }
        });

        sleep(1);
        //        cv.notify_all();
        sleep(1);
        i++;
        cv.notify_all();
        t2.join();
        t3.join();
    } catch (std::exception& e) {
        XCTAssert(false, @"%s", e.what());
    } catch (...) {
        XCTAssert(false, @"unknown exception");
    }
}
- (void)testCondVSpinLock {
    using namespace Utility::__3_;

    try {
        SpinLock mx;
        CondV<SpinLock> cv[2];
        std::atomic<int> cnt{0};

        Thread th([&mx,&cv,&cnt]()->void {
            NSLog(@"%s - waiting...", __PRETTY_FUNCTION__);
            {
                LockG<SpinLock> l(mx);
                cv[0].wait(mx, [&cnt]()->bool {
                    NSLog(@"%s - predicate called. cnt = %d", __PRETTY_FUNCTION__, cnt.load());
                    return cnt;
                });
            }
            NSLog(@"%s - woke up and do work", __PRETTY_FUNCTION__);
            sleep(1);
            NSLog(@"%s - notify", __PRETTY_FUNCTION__);
            //cv[1].notify_one();
            cv[1].notify_all();
        });

        NSLog(@"%s - nofity 3 times", __PRETTY_FUNCTION__);
        cv[0].notify_one();
        usleep(1000);
        cv[0].notify_one();
        usleep(1000);
        cnt = 1;
        cv[0].notify_one();
        NSLog(@"%s - wait on thread...", __PRETTY_FUNCTION__);
        {
            LockG<SpinLock> l(mx);
            cv[1].wait(mx);
        }
        usleep(1000);
        NSLog(@"%s - woke up", __PRETTY_FUNCTION__);

        th.join();
    } catch (std::exception& e) {
        XCTAssert(false, @"%s", e.what());
    } catch (...) {
        XCTAssert(false, @"unknown exception");
    }
}

- (void)testMutexSemaPerformance {
    using namespace Utility::__3_;

    [self measureBlock:^{
        @autoreleasepool {
            constexpr int crt{20};
            Sema<int, Mutex> sem(crt);

            std::atomic<int> sum(0);
            int _sum = 0;
            constexpr int cnt = 2000;
            for (int i = 0; i < cnt; _sum += ++i) {
                --sem; //sem.wait();
                Thread([&sem,&sum,i]()->void {
                    sum += i+1;
                    ++sem; //sem.post();
                }).detach();
            }
            for (int i = 0; i < crt; ++i) sem.wait();
            XCTAssert(_sum == sum.load(), @"_sum = %d, sum = %d", _sum, sum.load());
        }
    }];
}
- (void)testSpinLockSemaPerformance {
    using namespace Utility::__3_;

    [self measureBlock:^{
        @autoreleasepool {
            constexpr int crt{20};
            Sema<int, SpinLock> sem(crt);

            std::atomic<int> sum(0);
            int _sum = 0;
            constexpr int cnt = 2000;
            for (int i = 0; i < cnt; _sum += ++i) {
                --sem; //sem.wait();
                Thread([&sem,&sum,i]()->void {
                    sum += i+1;
                    ++sem; //sem.post();
                }).detach();
            }
            for (int i = 0; i < crt; ++i) sem.wait();
            XCTAssert(_sum == sum.load(), @"_sum = %d, sum = %d", _sum, sum.load());
        }
    }];
}

- (void)testBlockQueue {
    using namespace Utility::__3_;

    try {
        BlockQueue<SpinLock> q;
        constexpr int njobs = 100;

        // consumer
        Thread t1([&q]() {
            for (int i = 0; i < njobs/2; ++i) {
                q.dequeue()();
            }
        });
        Thread t2([&q]() {
            for (int i = 0; i < njobs/2; ++i) {
                q.dequeue()();
            }
        });

        // producer
        SpinLock lock;
        printo(std::cout, __PRETTY_FUNCTION__, ": i = ");
        for (int i = 0; i < njobs; ++i) {
            q.enqueue([i, &lock]() {
                LockG<decltype(lock)> l(lock);
                printo(std::cout, i, ", ");
            });
        }

        // clean-up
        t1.join();
        t2.join();
        printo(std::cout, '\n');
    } catch (std::exception& e) {
        XCTAssert(false, @"%s", e.what());
    } catch (...) {
        XCTAssert(false, @"unknown exception");
    }
}

- (void)testSerialThreadQueue {
    using namespace Utility::__3_;

    try {
        const char *label = __PRETTY_FUNCTION__;
        ThreadQueue q(label, 1);
        XCTAssert([@(label) isEqualToString:@(q.label())] && 1 == q.concurrency(), @"q.label() = %s", q.label());

        std::atomic<int> i{0};
        q.async([&i]()->void {
            printo(std::cout, __PRETTY_FUNCTION__, ": i=", i, "\n");
            i = 1;
        });
        sleep(1);
        XCTAssert(1==i, @"i=%d", i.load());

        constexpr int n = 10;
        int a[n];
        q.apply(n, [&a](unsigned long idx)->void {
            if (!(idx%3) ) {
                sleep(1);
            }
            a[idx] = int(idx);
            printo(std::cout, __PRETTY_FUNCTION__, ": idx=", idx, "\n");
        });
        for (int i=0; i<n; ++i) {
            XCTAssert(a[i]==i, @"a[%d]=%d", i, a[i]);
        }

        // Recursive submission
        //
        i = 0;
        q.async([&i,&q]()->void {
            printo(std::cout, "Recursion ", ++i, "\n");
            q.async([&i,&q]()->void {
                printo(std::cout, "Recursion ", ++i, "\n");
                q.async([&i,&q]()->void {
                    printo(std::cout, "Recursion ", ++i, "\n");
                    q.async([&i,&q]()->void {
                        printo(std::cout, "Recursion ", ++i, "\n");
                        q.async([&i,&q]()->void {
                            printo(std::cout, "Recursion ", ++i, "\n");
                        });
                    });
                });
            });
        });

        sleep(1);
        XCTAssert(5==i, @"i=%d", i.load());
    } catch (std::exception& e) {
        XCTAssert(false, @"%s", e.what());
    } catch (...) {
        XCTAssert(false, @"unknown exception");
    }
}

- (void)testConcurrentThreadQueue {
    using namespace Utility::__3_;

    try {
        const char *label = __PRETTY_FUNCTION__;
        ThreadQueue q(label, 2);
        XCTAssert([@(label) isEqualToString:@(q.label())] && 2 == q.concurrency(), @"q.label() = %s", q.label());

        std::atomic<int> i{0};
        q.async([&i]()->void {
            printf("%s : i = %d\n", __PRETTY_FUNCTION__, i.load());
            i++;
            sleep(1);
        });
        std::function<void(void)> const& f = [&i]()->void {
            printf("%s : i = %d\n", __PRETTY_FUNCTION__, i.load());
            i++;
            sleep(1);
        };
        q.sync([&i,&q,&f]()->void {
            q.async(f);
        });
        q.sync(f);
        XCTAssert(3==i, @"i=%d", i.load());

        constexpr int n = 10;
        int a[n];
        q.apply(n, [&a](unsigned long idx)->void {
            if (!(idx%10) ) {
                sleep(1);
            }
            a[idx] = int(idx);
            //printo(std::cout, __PRETTY_FUNCTION__, ": idx=", idx, "\n");
        });
        for (int i=0; i<n; ++i) {
            XCTAssert(a[i]==i, @"a[%d]=%d", i, a[i]);
        }
    } catch (std::exception& e) {
        XCTAssert(false, @"%s", e.what());
    } catch (...) {
        XCTAssert(false, @"unknown exception");
    }
}

- (void)testRCPtr {
    using namespace Utility::__3_;

    // non-virtual:
    try {
        using T = std::atomic<long>;
        RCPtr<T> obj;
        XCTAssert(!obj, @"");
        obj = decltype(obj)(nullptr, 1);
        XCTAssert(!!obj && 1 == obj.reference_count() && 1 == *obj, @"");
        {
            decltype(obj) a(obj);
            XCTAssert(!!a && 2 == obj.reference_count() && a == obj, @"");
        }
        XCTAssert(1 == obj.reference_count(), @"");
        //RCPtr<std::pair<long, bool>> a(obj);

        *obj = 0;
        ThreadQueue q(__PRETTY_FUNCTION__, 10);
        constexpr long n = 100;
        q.apply(n, [obj](unsigned long i) {
            *obj += long(i) + 1;
        });
        long sum = 0;
        for (long i = 1; i <= n; ++i) sum += i;
        XCTAssert(1 == obj.reference_count() && sum == *obj, @"sum = %ld", long(*obj));
    } catch (std::exception& e) {
        XCTAssert(false, @"%s", e.what());
    } catch (...) {
        XCTAssert(false, @"unknown exception");
    }

    // virtual:
    struct A { virtual ~A() = default; };
    struct Base {
        virtual ~Base() = default;
        virtual std::atomic<long>& operator()() = 0;
    };
    struct Derived : Base {
        std::atomic<long> i;
        ~Derived() { printo(std::cout, __PRETTY_FUNCTION__, '\n'); }
        Derived(long _i = 0) : i(_i) {}
        Derived(Derived const&) = delete;
        Derived& operator=(Derived const&) = delete;
        std::atomic<long>& operator()() override { return i; }
    };
    try {
        RCPtr<Base> d;
        d = RCPtr<Derived>(nullptr, 1);
        XCTAssert(!!d && 1 == d.reference_count(), @"");
        {
            RCPtr<Base> d_as_b(d);
            XCTAssert(!!d && 2 == d.reference_count(), @"");
            RCPtr<Derived> d_as_d(d_as_b);
            XCTAssert(!!d && 3 == d.reference_count() && d == d_as_d, @"");
            RCPtr<Base> d1_as_b(std::move(d));
            XCTAssert(!d && 3 == d1_as_b.reference_count() && d != d1_as_b, @"");
            XCTAssert(1 == (*d1_as_b)(), @"");
            d = decltype(d)(std::move(d_as_d));
        }
        XCTAssert(!!d && 1 == d.reference_count(), @"");
        {
            try {
                RCPtr<A> a(d);
                XCTAssert(false, @"an exception should have thrown");
            } catch (std::bad_cast&) {
                XCTAssert(true, @"");
            } catch (std::exception& e) {
                XCTAssert(false, @"%s", e.what());
            }
        }

        (*d)() = 0;
        {
            RCPtr<Derived> obj(d);
            ThreadQueue q(__PRETTY_FUNCTION__, 10);
            constexpr long n = 100;
            q.apply(n, [obj](unsigned long i) {
                obj->i += long(i) + 1;
                if (i%10 == 0) {
                    printo(std::cout, "ref cnt = ", obj.reference_count(), "\n");
                }
            });
            d = decltype(d)();
            long sum = 0;
            for (long i = 1; i <= n; ++i) sum += i;
            XCTAssert(1 == obj.reference_count() && sum == obj->i, @"sum = %ld", long(obj->i));
        }

        d = RCPtr<Derived>(nullptr, 10);
        RCPtr<Derived> e, f;
        e = d; f = std::move(d);
        XCTAssert(!d && e && f && (*e)() == (*f)(), @"");
    } catch (std::exception& e) {
        XCTAssert(false, @"%s", e.what());
    } catch (...) {
        XCTAssert(false, @"unknown exception");
    }
}

@end
