//
//  UtilityKitTests-RandomVariate.mm
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 7/16/16.
//  Copyright © 2016 KYUNGGUK MIN. All rights reserved.
//

#import <XCTest/XCTest.h>

// MARK: Version 3
//
#import <UtilityKit/DistKit.h>
#import <numeric>
#import <cmath>
#import <algorithm>

@interface UtilityKitTests_RandomVariate__3_ : XCTestCase

@end

@implementation UtilityKitTests_RandomVariate__3_

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testRandomVariate {
    using namespace Utility::__3_;

    // uniform distribution:
    @autoreleasepool {
        // rng:
        BitReversedRandomReal rng(2);
        // dist:
        UniformDistribution dist;
        // rv:
        RandomVariate rv(&rng, &dist);

        // generate:
        const int sz = 50000;
        double ud[sz];
        std::generate_n(ud, sz, rv);

        // statistics:
        const double mean = std::accumulate(ud, ud+sz, 0.) / sz;
        XCTAssert(std::abs(dist.mean()-mean) < 1e-4, @"dist.mean = %f, mean = %f", dist.mean(), mean);
        const double var = std::inner_product(ud, ud+sz, ud, 0.)/sz - mean*mean;
        XCTAssert(std::abs(dist.variance()-var) < 1e-4, @"dist.var = %f, var = %f", dist.variance(), var);
    }

    // gaussian distribution:
    @autoreleasepool {
        // rng:
        BitReversedRandomReal rng(2);
        // dist:
        double min = -5, max = 5;
        CustomDistribution dist(min, max, [](double x)->double {
            const double xd = 0;
            return std::exp(-(x-xd)*(x-xd)) * M_2_SQRTPI/2.;
        });
        // rv:
        RandomVariate rv(&rng, &dist);

        // generate:
        const int sz = 50000;
        std::unique_ptr<double[]> ud(new double[sz]);
        std::generate_n(ud.get(), sz, rv);

        // statistics:
        const double mean = std::accumulate(ud.get(), ud.get()+sz, 0.) / sz;
        XCTAssert(std::abs(dist.mean()-mean) < 1e-3, @"dist.mean = %f, mean = %f", dist.mean(), mean);
        const double var = std::inner_product(ud.get(), ud.get()+sz, ud.get(), 0.)/sz - mean*mean;
        XCTAssert(std::abs(dist.variance()-var) < 1e-3, @"dist.var = %f, var = %f", dist.variance(), var);

        // binning:
        const int n_bins = 20;
        double x_bins[n_bins + 1];
        std::fill_n(x_bins, n_bins+1, 0.);
        for (const double *x = ud.get(), *e = x + sz; x != e; ++x) {
            double y = (*x - min)/(max - min) * n_bins;
            int i = int(std::round(y));
            if (i < 0 || i >= n_bins) {
                XCTAssert(false, @"i = %d, y = %f", i, y);
                break;
            }
            x_bins[i] += 1.;
        }

        // pdf:
        const double dx = (max - min)/n_bins;
        for (auto& x : x_bins) x /= sz * dx;
        for (int i = 0; i <= n_bins; ++i) {
            const double x = i*(max-min)/n_bins + min;
            if (std::abs(x) > 1) continue;
            XCTAssert(std::abs(dist.pdf(x) - x_bins[i]) < 1e-1,
                      @"x = %f, dist.pdf(x) = %f, x_bins[i] = %f", x, dist.pdf(x), x_bins[i]);
        }
    }
}

@end
