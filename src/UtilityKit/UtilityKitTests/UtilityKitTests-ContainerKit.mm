//
//  UtilityKitTests-ContainerKit.mm
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 10/17/16.
//  Copyright © 2016 KYUNGGUK MIN. All rights reserved.
//

#import <XCTest/XCTest.h>

// MARK: Version 3
//
#import <UtilityKit/ContainerKit.h>
#include <iostream>

@interface UtilityKitTests_ContainerKit__3_ : XCTestCase

@end

@implementation UtilityKitTests_ContainerKit__3_

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testPaddedVectorField__1D {
    using Utility::__3_::PaddedVectorField;

    constexpr long Nx = 5, Pad = 1;
    typedef double T;
    PaddedVectorField<T, 1, Pad> a(Nx);

    XCTAssert(Nx == a.Nx() && a.padNxPad() == long(a.max_size()), @"");

    decltype(a)::value_type const _v(arc4random()), _0(0);
    a.fill(_v);
    for (long i = -a.pad(); i < a.padNx(); ++i) {
        XCTAssert(a[i].x == _v.x, @"b[%ld].x = %f", i, a[i].x);
    }
    a.reset();
    for (long i = -a.pad(); i < a.padNx(); ++i) {
        XCTAssert(a[i].x == _0.x, @"a[%ld].x = %f", i, a[i].x);
    }
}

- (void)testPaddedVectorField__2D {
    using Utility::__3_::PaddedVectorField;

    constexpr long Nx = 5, Ny = 10, Pad = 1;
    typedef double T;
    PaddedVectorField<T, 2, Pad> a({Nx, Ny});

    XCTAssert(Nx == a.Nx() && a.padNxPad() == long(a.max_size()) &&
              Ny == a.Ny() && a.padNyPad() == long(a[0].max_size()),
              @"");

    decltype(a)::value_type const _v(arc4random()), _0(0);
    a.fill(_v);
    for (long i = -a.pad(); i < a.padNx(); ++i) {
        for (long j = -a.pad(); j < a.padNy(); ++j) {
            XCTAssert(a[i][j].x == _v.x, @"b[%ld][%ld].x = %f", i, j, a[i][j].x);
        }
    }
    a.reset();
    for (long i = -a.pad(); i < a.padNx(); ++i) {
        for (long j = -a.pad(); j < a.padNy(); ++j) {
            XCTAssert(a[i][j].x == _0.x, @"a[%ld][%ld].x = %f", i, j, a[i][j].x);
        }
    }
}

- (void)testPaddedVectorField__3D {
    using Utility::__3_::PaddedVectorField;

    constexpr long Nx = 5, Ny = 10, Nz = 15, Pad = 1;
    typedef double T;
    PaddedVectorField<T, 3, Pad> a({Nx, Ny, Nz});

    XCTAssert(Nx == a.Nx() && a.padNxPad() == long(a.max_size()) &&
              Ny == a.Ny() && a.padNyPad() == long(a[0].max_size()) &&
              Nz == a.Nz() && a.padNzPad() == long(a[0][0].max_size()),
              @"");

    decltype(a)::value_type const _v(arc4random()), _0(0);
    a.fill(_v);
    for (long i = -a.pad(); i < a.padNx(); ++i) {
        for (long j = -a.pad(); j < a.padNy(); ++j) {
            for (long k = -a.pad(); k < a.padNz(); ++k) {
                XCTAssert(a[i][j][k].x == _v.x, @"b[%ld][%ld][%ld].x = %f", i, j, k, a[i][j][k].x);
            }
        }
    }
    a.reset();
    for (long i = -a.pad(); i < a.padNx(); ++i) {
        for (long j = -a.pad(); j < a.padNy(); ++j) {
            for (long k = -a.pad(); k < a.padNz(); ++k) {
                XCTAssert(a[i][j][k].x == _0.x, @"a[%ld][%ld][%ld].x = %f", i, j, k, a[i][j][k].x);
            }
        }
    }
}

- (void)testParticleBucketBase {
    using Utility::__3_::ParticleBucket;

    constexpr long ND = 2, Np = 10;
    ParticleBucket<ND> bucket;
    ParticleBucket<ND> const& cbucket = bucket;
    XCTAssert(bucket.empty() && 0 == bucket.size(), @"");
    bucket.clear();
    XCTAssert(bucket.empty() && 0 == bucket.size(), @"");
    bucket.resize(Np);
    XCTAssert(!bucket.empty() && Np == bucket.size(), @"");

    bucket.push_back(decltype(bucket)::particle_type());
    XCTAssert(!bucket.empty() && Np+1 == bucket.size(), @"");

    try {
        for (long n = long(bucket.size()); n > 0; --n) {
            bucket.pop_back();
        }
        XCTAssert(bucket.empty());
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    try {
        unsigned const n = Np + Np/2;
        for (unsigned i = 1; i <= n; ++i) {
            decltype(bucket)::particle_type ptl;
            ptl.vel.x = i;
            bucket.push_back(ptl);
        }
        XCTAssert(n == bucket.size(), @"");

        XCTAssert(1. == bucket.front().vel.x && double(n) == bucket.back().vel.x, @"bucket[0] = %f, bucket[n-1] = %f", bucket.front().vel.x, bucket.back().vel.x);
        for (unsigned i = 0; i < n; ++i) {
            XCTAssert(double(i+1) == bucket.at(i).vel.x && double(i+1) == bucket[i].vel.x, @"bucket[%d] = %f", i, bucket.at(i).vel.x);
        }

        unsigned i = 1;
        for (auto ptl : cbucket) {
            XCTAssert(double(i) == ptl.vel.x, @"i = %d: ptl.vel.x = %f", i, ptl.vel.x);
            ++i;
        }
        i = n;
        for (auto first = cbucket.rbegin(), last = cbucket.rend(); first != last; ++first) {
            XCTAssert(double(i) == first->vel.x, @"i = %d: ptl.vel.x = %f", i, first->vel.x);
            --i;
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    try {
        bucket.at(bucket.size());
        XCTAssert(false, @"an exception should have thrown");
    } catch (std::exception&) {
    }
}

- (void)testParticleBucketEvict_If_Iterator {
    using Utility::__3_::ParticleBucket;
    using Utility::__3_::Aggregate::PaddedArray;

    constexpr long ND = 2, Np = 10;
    ParticleBucket<ND> bucket;
    decltype(bucket)::velocity_type vel(0);
    decltype(bucket)::position_type pos(0);

    constexpr unsigned n = Np + Np/2;
    for (unsigned i = 0; i < n; ++i) {
        pos.x = double(i) + .5;
        bucket.push_back({vel, pos});
    }

    PaddedArray<decltype(bucket)::particle_type, Np, 0> spillover;
    decltype(spillover)::iterator last;
    try { // with sufficient bucket buffer
        last = bucket.evict_if(spillover.begin(), spillover.end(), [](decltype(bucket)::position_type const& pos) {
            return pos.x < n/2;
        });
        XCTAssert(std::distance(spillover.begin(), last) == n/2, @"number of evicted ptls = %ld", std::distance(spillover.begin(), last));
        std::cout << "bucket contents.x = {";
        for (auto first = spillover.begin(); first != last; ++first) {
            std::cout << first->pos.x << ", ";
        }
        std::cout << "}\n";

        std::cout << "remaining particles.x = {";
        for (auto const& p : bucket) {
            std::cout << p.pos.x << ", ";
        }
        std::cout << "}\n";
    } catch (std::exception& e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    bucket.insert_back(spillover.begin(), last);
    XCTAssert(n == bucket.size(), @"");
    try { // with not enough bucket buffer
        decltype(spillover)::iterator last = bucket.evict_if(spillover.begin(), spillover.end(), [](decltype(bucket)::position_type const& pos) {
            return pos.x < n;
        });
        XCTAssert(spillover.end() == last && bucket.size() == n - spillover.size(), @"");
        std::cout << "bucket contents.x = {";
        for (auto first = spillover.begin(); first != last; ++first) {
            std::cout << first->pos.x << ", ";
        }
        std::cout << "}\n";

        std::cout << "remaining particles.x = {";
        for (auto const& p : bucket) {
            std::cout << p.pos.x << ", ";
        }
        std::cout << "}\n";
    } catch (std::exception& e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}

- (void)testParticleBucketEvict_If_Bucket {
    using Utility::__3_::ParticleBucket;

    constexpr long ND = 2, Np = 10;
    ParticleBucket<ND> bucket, spillover;
    decltype(bucket)::velocity_type vel(0);
    decltype(bucket)::position_type pos(0);

    constexpr unsigned n = Np + Np/2;
    for (unsigned i = 0; i < n; ++i) {
        pos.x = double(i) + .5;
        bucket.push_back({vel, pos});
    }

    try {
        bucket.evict_if(spillover, [](decltype(bucket)::position_type const& pos) {
            return pos.x < n/2;
        });
        XCTAssert(spillover.size() == n/2, @"number of evicted ptls = %ld", spillover.size());
        std::cout << "bucket contents.x = {";
        for (auto& ptl : spillover) {
            std::cout << ptl.pos.x << ", ";
        }
        std::cout << "}\n";

        std::cout << "remaining particles.x = {";
        for (auto const& p : bucket) {
            std::cout << p.pos.x << ", ";
        }
        std::cout << "}\n";
    } catch (std::exception& e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
    bucket << spillover;
    XCTAssert(n == bucket.size(), @"");
    bucket.clear();
    spillover.clear();

    constexpr double Lx = n - 2;
    for (unsigned i = 0; i < n; ++i) {
        pos.x = double(i);
        bucket.push_back({vel, pos});
    }
    bucket -= {.5, 0};
    std::cout << "bucket(init) = " << bucket << std::endl;
    //    std::cout << "L = " << L << std::endl;

    try {
        bucket.evict_if(spillover, [](decltype(bucket)::position_type const& pos) {
            return pos.x < 0;
        });
        XCTAssert(1 == spillover.size(), @"spillover.size() = %ld", spillover.size());
        std::cout << "spileover(left) = " << spillover << std::endl;
        bucket << spillover + decltype(bucket)::position_type{Lx, 0};
        spillover.clear();
        std::cout << "bucket(left) = " << bucket << std::endl;

        bucket.evict_if(spillover, [](decltype(bucket)::position_type const& pos) {
            return pos.x < 0;
        });
        XCTAssert(0 == spillover.size(), @"spillover.size() = %ld", spillover.size());
    } catch (std::exception& e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    try {
        bucket.evict_if(spillover, [Lx](decltype(bucket)::position_type const& pos) {
            return pos.x >= Lx;
        });
        XCTAssert(1 == spillover.size(), @"spillover.size() = %ld", spillover.size());
        std::cout << "spileover(right) = " << spillover << std::endl;
        bucket << spillover - decltype(bucket)::position_type{Lx, 0};
        spillover.clear();
        std::cout << "bucket(right) = " << bucket << std::endl;

        bucket.evict_if(spillover, [Lx](decltype(bucket)::position_type const& pos) {
            return pos.x >= Lx;
        });
        XCTAssert(0 == spillover.size(), @"spillover.size() = %ld", spillover.size());
    } catch (std::exception& e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}

- (void)testParticleBucketInsertionWithOperation {
    using Utility::__3_::ParticleBucket;
    using Utility::__3_::Particle;

    constexpr int ND = 1;
    constexpr long L = 1;

    ParticleBucket<ND> bucket;
    Particle<ND> ptl;
    ptl.vel = {0, 0, 0};
    ptl.pos.x = -L + .4;
    bucket.push_back(ptl);
    std::cout << bucket << std::endl;

    decltype(bucket) spillover;
    bucket.evict_if(spillover, [](Particle<ND>::position_type pos)->bool {
        return pos.x < 0;
    });
    bucket << spillover + Particle<ND>::position_type{L};
    std::cout << bucket << std::endl;
    XCTAssert(0 == long(bucket.front().pos.x), @"");
}

@end
