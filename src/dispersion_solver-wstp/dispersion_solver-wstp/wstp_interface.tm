/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

:Evaluate:      BeginPackage["DispersionKit`"]

:Evaluate:      (* Remove /@ Names["DispersionKit`*"] *)
:Evaluate:      DispersionKit::warn = "`1`"
:Evaluate:      DispersionKit::erro = "`1`"


// MARK: Public Interfaces
//

// Dispersion Relation
//
:Evaluate:      Clear[wstpDRingDistribution]
:Evaluate:      wstpDRingDistribution::usage = "\!\(wstpDRingDistribution[\[CapitalOmega]\_c, \[Omega]\_p, \[Theta]\_\[DoubleVerticalBar], \[Theta]\_\[UpTee], v\_d, v\_r]\) represents a Maxwellian-ring velocity distribution, which is proportional to \!\(\*SuperscriptBox[\(\[ExponentialE]\), \(\*SuperscriptBox[\((\*SubscriptBox[\(v\), \(\[DoubleVerticalBar]\)] - \*SubscriptBox[\(v\), \(d\)])\), \(2\)]/\*SubsuperscriptBox[\(\[Theta]\), \(\[DoubleVerticalBar]\), \(2\)]\)] \*SuperscriptBox[\(\[ExponentialE]\), \(\*SuperscriptBox[\((\*SubscriptBox[\(v\), \(\[UpTee]\)] - \*SubscriptBox[\(v\), \(r\)])\), \(2\)]/\*SubsuperscriptBox[\(\[Theta]\), \(\[UpTee]\), \(2\)]\)]\).\n\!\(\[CapitalOmega]\_c\) is the real-valued cyclotron frequency and \!\(\[Omega]\_p\) is either pure real- or pure imaginary-valued plasma frequency. The parallel and perpendicular thermal speeds can be any non-negative real number (including 0), and the parallel drift and ring speeds can be any real number (note that there is a limit on the maximum of \!\(-v\_r/\[Theta]\_\[UpTee]\)).\nAt least the first three arguments should be given. For any other parameters not specified, the default values are: \!\(\[Theta]\_\[UpTee] = \[Theta]\_\[DoubleVerticalBar]\), \!\(v\_d = 0\) and \!\(v\_r = 0\)."
:Evaluate:      wstpDRingDistribution::argx = "Invalid argument(s)."

:Evaluate:      Clear[wstpDMaxwellianDistribution]
:Evaluate:      wstpDMaxwellianDistribution::usage = "\!\(wstpDMaxwellianDistribution[\[CapitalOmega]\_c, \[Omega]\_p, \[Theta]\_\[DoubleVerticalBar], \[Theta]\_\[UpTee], v\_d]\) represents a bi-Maxwellian velocity distribution, which is proportional to \!\(\*SuperscriptBox[\(\[ExponentialE]\), \(\*SuperscriptBox[\((\*SubscriptBox[\(v\), \(\[DoubleVerticalBar]\)] - \*SubscriptBox[\(v\), \(d\)])\), \(2\)]/\*SubsuperscriptBox[\(\[Theta]\), \(\[DoubleVerticalBar]\), \(2\)]\)] \*SuperscriptBox[\(\[ExponentialE]\), \(\*SubsuperscriptBox[\(v\), \(\[UpTee]\), \(2\)]/\*SubsuperscriptBox[\(\[Theta]\), \(\[UpTee]\), \(2\)]\)]\).\n\!\(\[CapitalOmega]\_c\) is the real-valued cyclotron frequency and \!\(\[Omega]\_p\) is either pure real- or pure imaginary-valued plasma frequency. The parallel and perpendicular thermal speeds can be any non-negative real number (including 0), and the parallel drift speed can be any real number.\nAt least the first three arguments should be given. For any other parameters not specified, the default values are: \!\(\[Theta]\_\[UpTee] = \[Theta]\_\[DoubleVerticalBar]\) and \!\(v\_d = 0\)."
:Evaluate:      wstpDMaxwellianDistribution::argx = "Invalid argument(s)."

:Evaluate:      Clear[wstpDProductKappaDistribution]
:Evaluate:      wstpDProductKappaDistribution::usage = "\!\(wstpDProductKappaDistribution[\[CapitalOmega]\_c, \[Omega]\_p, {\[Kappa]}, \[Theta]\_\[DoubleVerticalBar], \[Theta]\_\[UpTee], v\_d]\) represents a product bi-kappa velocity distribution, \!\(TraditionalForm\`\(\*SubscriptBox[\(f\), \(\[Kappa]\)]\)[\*SubscriptBox[\(v\), \(\[DoubleVerticalBar]\)], \*SubscriptBox[\(v\), \(\[UpTee]\)]] = \*FractionBox[\(\*SqrtBox[SubscriptBox[\(\[Kappa]\), \(\[DoubleVerticalBar]\)]] \[CapitalGamma][\*SubscriptBox[\(\[Kappa]\), \(\[DoubleVerticalBar]\)]]\), \(\[CapitalGamma][\*SubscriptBox[\(\[Kappa]\), \(\[DoubleVerticalBar]\)] + 1/2]\)] \*FractionBox[SuperscriptBox[\((1 + \*SuperscriptBox[\((\*SubscriptBox[\(v\), \(\[DoubleVerticalBar]\)] - \*SubscriptBox[\(v\), \(d\)])\), \(2\)]/\*SubscriptBox[\(\[Kappa]\), \(\[DoubleVerticalBar]\)] \*SubsuperscriptBox[\(\[Theta]\), \(\[DoubleVerticalBar]\), \(2\)])\), \(-\((\*SubscriptBox[\(\[Kappa]\), \(\[DoubleVerticalBar]\)] + 1)\)\)], \(\*SqrtBox[\(\[Pi]\)] \*SubscriptBox[\(\[Theta]\), \(\[DoubleVerticalBar]\)]\)] \*FractionBox[SuperscriptBox[\((1 + \*SubsuperscriptBox[\(v\), \(\[UpTee]\), \(2\)]/\*SubscriptBox[\(\[Kappa]\), \(\[UpTee]\)] \*SubsuperscriptBox[\(\[Theta]\), \(\[UpTee]\), \(2\)])\), \(-\((\*SubscriptBox[\(\[Kappa]\), \(\[UpTee]\)] + 1)\)\)], \(\[Pi]\\ \*SubsuperscriptBox[\(\[Theta]\), \(\[UpTee]\), \(2\)]\)]\), where \[Kappa] > 1 is the real-valued power index. The parallel and perpendicular temperatures are \!\(\*SubscriptBox[\(T\), \(\[DoubleVerticalBar]\)]\)=\!\(\*FractionBox[\(\[Kappa]\), \(2  \[Kappa] - 1\)]\)\!\(\*SubsuperscriptBox[\(\[Theta]\), \(\[DoubleVerticalBar]\), \(2\)]\) and \!\(\*SubscriptBox[\(T\), \(\[UpTee]\)]\)=\!\(\*FractionBox[\(\[Kappa]\), \(2  \[Kappa] - 2\)]\)\!\(\*SubsuperscriptBox[\(\[Theta]\), \(\[UpTee]\), \(2\)]\), respectively.\n\!\(\[CapitalOmega]\_c\) is the real-valued cyclotron frequency and \!\(\[Omega]\_p\) is either pure real- or pure imaginary-valued plasma frequency. The parallel and perpendicular thermal speeds can be any positive real number (\!\(\[Theta]\_\[DoubleVerticalBar] \[GreaterEqual] 0\) and \!\(\[Theta]\_\[UpTee] > 0\)), and the parallel drift speed can be any real number.\nAt least the first four arguments should be given. For any other parameters not specified, the default values are: \!\(\[Theta]\_\[UpTee] = \[Theta]\_\[DoubleVerticalBar]\) and \!\(v\_d = 0\).\n\!\(wstpDProductKappaDistribution[\[Ellipsis], {\[Kappa]\_\[DoubleVerticalBar], \[Kappa]\_\[UpTee]}, \[Ellipsis]]\) uses anisotropic \[Kappa] indexes corresponding to the parallel and perpendicular components of the product bi-kappa distribution function, respectively, where \!\(\[Kappa]\_\[DoubleVerticalBar] > 1/2\) and \!\(\[Kappa]\_\[UpTee] > 1\)."
:Evaluate:      wstpDProductKappaDistribution::argx = "Invalid argument(s)."

:Evaluate:      Clear[wstpDKappaDistribution]
:Evaluate:      wstpDKappaDistribution::usage = "\!\(wstpDKappaDistribution[\[CapitalOmega]\_c, \[Omega]\_p, {\[Kappa]}, \[Theta]\_\[DoubleVerticalBar], \[Theta]\_\[UpTee], v\_d]\) represents a bi-kappa velocity distribution, \!\(TraditionalForm\`\(\*SubscriptBox[\(f\), \(\[Kappa]\)]\)[\*SubscriptBox[\(v\), \(\[DoubleVerticalBar]\)], \*SubscriptBox[\(v\), \(\[UpTee]\)]] = \*FractionBox[\(1\), \(\*SuperscriptBox[\(\[Pi]\), \(3/2\)] \*SubscriptBox[\(\[Theta]\), \(\[DoubleVerticalBar]\)] \*SubsuperscriptBox[\(\[Theta]\), \(\[UpTee]\), \(2\)]\)] \*FractionBox[\(\[CapitalGamma][\[Kappa]]\), \(\*SqrtBox[\(\[Kappa]\)] \[CapitalGamma][\[Kappa] - 1/2]\)] \*SuperscriptBox[\((1 + \*FractionBox[SuperscriptBox[\((\*SubscriptBox[\(v\), \(\[DoubleVerticalBar]\)] - \*SubscriptBox[\(v\), \(d\)])\), \(2\)], \(\[Kappa]\\ \*SubsuperscriptBox[\(\[Theta]\), \(\[DoubleVerticalBar]\), \(2\)]\)] + \*FractionBox[SubsuperscriptBox[\(v\), \(\[UpTee]\), \(2\)], \(\[Kappa]\\ \*SubsuperscriptBox[\(\[Theta]\), \(\[UpTee]\), \(2\)]\)])\), \(-\((\[Kappa] + 1)\)\)]\), where \[Kappa] > 3/2 is the real-valued power index. The parallel and perpendicular temperatures are \!\(\*FractionBox[SubscriptBox[\(T\), \(\[UpTee]\)], SubsuperscriptBox[\(\[Theta]\), \(\[UpTee]\), \(2\)]]\)=\!\(\*FractionBox[SubscriptBox[\(T\), \(\[DoubleVerticalBar]\)], SubsuperscriptBox[\(\[Theta]\), \(\[DoubleVerticalBar]\), \(2\)]]\)=\!\(\*FractionBox[\(\[Kappa]\), \(2  \[Kappa] - 3\)]\).\n\!\(\[CapitalOmega]\_c\) is the real-valued cyclotron frequency and \!\(\[Omega]\_p\) is either pure real- or pure imaginary-valued plasma frequency. The parallel and perpendicular thermal speeds can be any positive real number (\!\(\[Theta]\_\[DoubleVerticalBar] \[GreaterEqual] 0\) and \!\(\[Theta]\_\[UpTee] > 0\)), and the parallel drift speed can be any real number.\nAt least the first four arguments should be given. For any other parameters not specified, the default values are: \!\(\[Theta]\_\[UpTee] = \[Theta]\_\[DoubleVerticalBar]\) and \!\(v\_d = 0\)."
:Evaluate:      wstpDKappaDistribution::argx = "Invalid argument(s)."

:Evaluate:      Clear[wstpDDispersionObject]
:Evaluate:      wstpDDispersionObject::usage = "wstpDDispersionObject[uniqueid] is a handle to the internal dispersion object returned by wstpDCreateDispersionObject."
:Evaluate:      wstpDDispersionObject::argx = "Invalid handle."

:Evaluate:      Clear[wstpDCreateDispersionObject]
:Evaluate:      wstpDCreateDispersionObject::usage = "\!\(wstpDCreateDispersionObject[c_?Positive, dists:{(_wstpDRingDistribution | _wstpDMaxwellianDistribution | _wstpDProductKappaDistribution | _wstpDKappaDistribution)...}, \(j\_min\) : _Integer?Positive : 5]\) creates an object that represents a dispersion matrix constructed with the provided parameters and returns a handle (an expression with wstpDDispersionObject head) to it.\n\!\(c\) is the speed of light and \!\(dists\) is a list of velocity distributions. \!\(j\_\(min\) \[LessEqual] 2000\) is the minimum cyclotron order (see usage of wstpDSetMinimumCyclotronOrder)."

:Evaluate:      Clear[wstpDListAllDispersionObjects]
:Evaluate:      wstpDListAllDispersionObjects::usage = "\!\(wstpDListAllDispersionObjects[]\) returns a list of all dispersion object handles internally kept."

:Evaluate:      Clear[wstpDRemoveObject]
:Evaluate:      SetAttributes[wstpDRemoveObject, {Listable}]
:Evaluate:      wstpDRemoveObject::usage = "\!\(wstpDRemoveObject[handle_wstpDDispersionObject]\) removes, if it exists, the dispersion object identified by the passed handle from the internal table.\n\!\(wstpDRemoveObject[handles : {___wstpDDispersionObject}]\) automatically threads over the elements of the list."

:Evaluate:      Clear[wstpDSetMinimumCyclotronOrder]
:Evaluate:      wstpDSetMinimumCyclotronOrder::usage = "\!\(wstpDSetMinimumCyclotronOrder[handle_wstpDDispersionObject, \(j\_min\)]\) sets the minimum cyclotron order (positive integer), \!\(j\_\(min\) \[LessEqual] 2000\), of the given dispersion object. When calculating the dispersion tensor, summation \!\(\*SubsuperscriptBox[\(\[Sum]\), \(j = \(-\*SubscriptBox[\(j\), \(min\)]\)\), SubscriptBox[\(j\), \(min\)]](\[CenterEllipsis])\) is first carried out. After that, convergence of the dispersion tensor is tested for \!\(\[LeftBracketingBar]j\[RightBracketingBar] > \*SubscriptBox[\(j\), \(min\)]\), and if converged, the summation is terminated. If not converged, the summation continues until \!\(\[LeftBracketingBar]j\[RightBracketingBar] = \(j\_max\)\). It returns \!\({\(j\_min\), \(j\_max\)}\)."

:Evaluate:      Clear[wstpDMinimumCyclotronOrder]
:Evaluate:      wstpDMinimumCyclotronOrder::usage = "\!\(wstpDMinimumCyclotronOrder[handle_wstpDDispersionObject]\) returns the minimum cyclotron order associated with the given dispersion object."

:Evaluate:      Clear[wstpDDescription]
:Evaluate:      wstpDDescription::usage = "wstpDDescription[handle_wstpDDispersionObject] returns a textual description of the given dispersion object."

:Evaluate:      Clear[wstpDDispersionMatrix]
:Evaluate:      wstpDDispersionMatrix::usage = "\!\(wstpDDispersionMatrix[handle_wstpDDispersionObject, \(k\_\[DoubleVerticalBar]\), \(k\_\[UpTee]\), \[Omega]]\) evaluates the dispersion matrix for the given wave number (real number) and complex frequency. It returns a 3x3 matrix or Indeterminate, the latter indicating error. The minimum cyclotron order may need to be set first.\n\!\(wstpDDispersionMatrix[handle_wstpDDispersionObject, \(k\_\[DoubleVerticalBar]\) : {___?NumberQ}, \(k\_\[UpTee]\) : {___?NumberQ}, \[Omega] : {___?NumberQ}]\) automatically thread over the list of wave numbers and complex frequencies."
:Evaluate:      wstpDDispersionMatrix::argx = "Invalid argument(s)."

:Evaluate:      Clear[wstpDFindRoot]
:Evaluate:      Options[wstpDFindRoot] = { MaxIterations -> 20, "ReturnDispersionMatrix" -> False, "UpdateMinimumCyclotronOrderFromPreviousIteration" -> False }
:Evaluate:      wstpDFindRoot::usage = "\!\(wstpDFindRoot[handle_wstpDDispersionObject, \(k\_\[DoubleVerticalBar]\) : {__?NumberQ}, \(k\_\[UpTee]\) : {__?NumberQ}, {\[Omega]\_1, \[Omega]\_2, \[Omega]\_3}]\) finds complex frequency roots for the given wave numbers (real numbers). First, the three initial guesses, \!\({\[Omega]\_1, \[Omega]\_2, \[Omega]\_3}\), of the complex frequency are used to find the first root corresponding to the first element of the wave number. When there is a second element of the wave number, \!\(\[Omega]\_2\), \!\(\[Omega]\_3\) and the newly found root are used as the initial guesses to find the second root. Roots for the subsequent wave numbers, if any, are found in a similar fashion.\nIt returns a list of rules. The rule \"\[Omega]\"->{\[CenterEllipsis]} contains the found roots (not including \!\({\[Omega]\_1, \[Omega]\_2, \[Omega]\_3}\)). It can contain no element when the root finding failed for the first wave number or less than the number of wave numbers given if the root finding failed at an intermediate wave number. The rules \"k1\"->{\[CenterEllipsis]} and \"k2\"->{\[CenterEllipsis]} contain corresponding parallel and perpendicular wave numbers, respectively. The rule \"flag\"->(0|1|2) indicates a reason for termination, where 0 indicates normal termination (in which case, all roots for the given wave numbers are found), 1 indicates failed root finding at an intermediate wave number, and 2 indicates user interruption (abort signal) while finding the roots. For flag other than 0, all roots found up to the point of interruption or failure are returned.\nThere are several options. The \!\(MaxIterations\) option sets the maximum number of iterations to be performed for root finding (default is 20). If the \"ReturnDispersionMatrix\" option is \!\(True\), the dispersion matrixes is also calculated and returned as a rule \"D\"->{\[CenterEllipsis]} (default is \!\(False\)). If the \"UpdateMinimumCyclotronOrderFromPreviousIteration\" option is \!\(True\), the cyclotron order that led to the convergence of the dispersion tensor is set with wstpDSetMinimumCyclotronOrder for root finding for the next wave number (default is \!\(False\))."
:Evaluate:      wstpDFindRoot::usage = wstpDFindRoot::usage <> "\n\!\(wstpDFindRoot[handle_wstpDDispersionObject, \(k\_\[DoubleVerticalBar]\) : _?NumberQ, \(k\_\[UpTee]\) : _?NumberQ, {\[Omega]\_1, \[Omega]\_2, \[Omega]\_3}]\) finds a complex frequency root at the wave vector specified by a pair of \!\(k\_\[DoubleVerticalBar]\) and \!\(k\_\[UpTee]\)."
:Evaluate:      wstpDFindRoot::argx = "Invalid argument(s)."
:Evaluate:      wstpDFindRoot::optx = "Invalid option(s)."
:Evaluate:      wstpDFindRoot::noroot = "No root found."




// MARK: Private Interfaces to WSTP Process
//
:Evaluate:      Begin["`Private`"]


// --> wstpDRingDistribution
:Evaluate:      wstpDRingDistribution[Oc_, op_, th_] := wstpDRingDistribution[Oc, op, th, th]
:Evaluate:      wstpDRingDistribution[Oc_, op_, th1_, th2_] := wstpDRingDistribution[Oc, op, th1, th2, 0.]
:Evaluate:      wstpDRingDistribution[Oc_, op_, th1_, th2_, vd_] := wstpDRingDistribution[Oc, op, th1, th2, vd, 0.]
// <-- wstpDRingDistribution


// --> wstpDMaxwellianDistribution
:Evaluate:      wstpDMaxwellianDistribution[Oc_, op_, th_] := wstpDMaxwellianDistribution[Oc, op, th, th]
:Evaluate:      wstpDMaxwellianDistribution[Oc_, op_, th1_, th2_] := wstpDMaxwellianDistribution[Oc, op, th1, th2, 0.]
// <-- wstpDMaxwellianDistribution


// --> wstpDProductKappaDistribution
:Evaluate:      wstpDProductKappaDistribution[Oc_, op_, {k_}, th_] := wstpDProductKappaDistribution[Oc, op, {k}, th, th]
:Evaluate:      wstpDProductKappaDistribution[Oc_, op_, {k_}, th1_, th2_] := wstpDProductKappaDistribution[Oc, op, {k}, th1, th2, 0.]
:Evaluate:      wstpDProductKappaDistribution[Oc_, op_, {k1_, k2_}, th_] := wstpDProductKappaDistribution[Oc, op, {k1, k2}, th, th]
:Evaluate:      wstpDProductKappaDistribution[Oc_, op_, {k1_, k2_}, th1_, th2_] := wstpDProductKappaDistribution[Oc, op, {k1, k2}, th1, th2, 0.]
// <-- wstpDProductKappaDistribution


// --> wstpDKappaDistribution
:Evaluate:      wstpDKappaDistribution[Oc_, op_, {k_}, th_] := wstpDKappaDistribution[Oc, op, {k}, th, th]
:Evaluate:      wstpDKappaDistribution[Oc_, op_, {k_}, th1_, th2_] := wstpDKappaDistribution[Oc, op, {k}, th1, th2, 0.]
// <-- wstpDKappaDistribution


// --> wstpDCreateDispersionObject
:Evaluate:      Clear[mutateVelocityDistribution]
// wstpDRingDistribution
:Evaluate:      mutateVelocityDistribution[wstpDRingDistribution, ___] := (Message[wstpDRingDistribution::argx]; Abort[])
:Evaluate:      mutateVelocityDistribution[wstpDRingDistribution, Oc_Real, op_?NumberQ, th1_Real?NonNegative, th2_Real?NonNegative, vd_Real, vr_Real] /; Head[Chop[op^2]] =!= Complex := wstpDRingDistribution[Oc, Chop[op^2] // N, th1, th2, vd, vr]
// wstpDMaxwellianDistribution
:Evaluate:      mutateVelocityDistribution[wstpDMaxwellianDistribution, ___] := (Message[wstpDMaxwellianDistribution::argx]; Abort[])
:Evaluate:      mutateVelocityDistribution[wstpDMaxwellianDistribution, Oc_Real, op_?NumberQ, th1_Real?NonNegative, th2_Real?NonNegative, vd_Real] /; Head[Chop[op^2]] =!= Complex := wstpDMaxwellianDistribution[Oc, Chop[op^2] // N, th1, th2, vd]
// wstpDProductKappaDistribution
:Evaluate:      mutateVelocityDistribution[wstpDProductKappaDistribution, ___] := (Message[wstpDProductKappaDistribution::argx]; Abort[])
:Evaluate:      mutateVelocityDistribution[wstpDProductKappaDistribution, Oc_Real, op_?NumberQ, {k_Real /; k > 1}, th1_Real?NonNegative, th2_Real?NonNegative, vd_Real] /; Head[Chop[op^2]] =!= Complex := wstpDProductKappaDistribution[Oc, Chop[op^2] // N, Sequence @@ {k, k}, th1, th2, vd]
:Evaluate:      mutateVelocityDistribution[wstpDProductKappaDistribution, Oc_Real, op_?NumberQ, {k1_Real /; k1 > 1/2, k2_Real /; k2 > 1}, th1_Real?NonNegative, th2_Real?NonNegative, vd_Real] /; Head[Chop[op^2]] =!= Complex := wstpDProductKappaDistribution[Oc, Chop[op^2] // N, Sequence @@ {k1, k2}, th1, th2, vd]
// wstpDKappaDistribution
:Evaluate:      mutateVelocityDistribution[wstpDKappaDistribution, ___] := (Message[wstpDKappaDistribution::argx]; Abort[])
:Evaluate:      mutateVelocityDistribution[wstpDKappaDistribution, Oc_Real, op_?NumberQ, {k_Real /; k > 3/2}, th1_Real?NonNegative, th2_Real?NonNegative, vd_Real] /; Head[Chop[op^2]] =!= Complex := wstpDKappaDistribution[Oc, Chop[op^2] // N, Sequence @@ {k}, th1, th2, vd]

:Begin:
:Function:      wstpDCreateDispersionObject
:Pattern:       wstpDCreateDispersionObject[c_?Positive, dists : {(_wstpDRingDistribution | _wstpDMaxwellianDistribution | _wstpDProductKappaDistribution | _wstpDKappaDistribution)...}, jmin : _Integer?Positive : 5] /; jmin <= 2000
:Arguments:     {N[c], jmin, mutateVelocityDistribution[Head[#], Sequence @@ #]& /@ N[dists]}
:ArgumentTypes: {Real, Integer, Manual}
:ReturnType:    Manual
:End:
// <-- wstpDCreateDispersionObject


// --> wstpDListAllDispersionObjects
:Begin:
:Function:      wstpDListAllDispersionObjects
:Pattern:       wstpDListAllDispersionObjects[]
:Arguments:     {}
:ArgumentTypes: {}
:ReturnType:    Manual
:End:
// <-- wstpDListAllDispersionObjects


// --> wstpDRemoveObject
:Begin:
:Function:      wstpDRemoveObject
:Pattern:       wstpDRemoveObject[wstpDDispersionObject[id_String]]
:Arguments:     {id}
:ArgumentTypes: {String}
:ReturnType:    Manual
:End:

:Evaluate:      wstpDRemoveObject[handle_] := (Message[wstpDDispersionObject::argx]; Abort[])
// <-- wstpDRemoveObject


// --> wstpDSetMinimumCyclotronOrder
:Begin:
:Function:      wstpDSetMinimumCyclotronOrder
:Pattern:       wstpDSetMinimumCyclotronOrder[wstpDDispersionObject[id_String], jmin_Integer ? Positive]
:Arguments:     {id, jmin}
:ArgumentTypes: {String, Integer}
:ReturnType:    Manual
:End:

:Evaluate:      wstpDSetMinimumCyclotronOrder[handle_, _Integer ? Positive] := (Message[wstpDDispersionObject::argx]; Abort[])
// <-- wstpDSetMinimumCyclotronOrder


// --> wstpDMinimumCyclotronOrder
:Begin:
:Function:      wstpDMinimumCyclotronOrder
:Pattern:       wstpDMinimumCyclotronOrder[wstpDDispersionObject[id_String]]
:Arguments:     {id}
:ArgumentTypes: {String}
:ReturnType:    Manual
:End:

:Evaluate:      wstpDMinimumCyclotronOrder[handle_] := (Message[wstpDDispersionObject::argx]; Abort[])
// <-- wstpDMinimumCyclotronOrder


// --> wstpDDescription
:Begin:
:Function:      wstpDDescription
:Pattern:       wstpDDescription[wstpDDispersionObject[id_String]]
:Arguments:     {id}
:ArgumentTypes: {String}
:ReturnType:    Manual
:End:

:Evaluate:      wstpDDescription[handle_] := (Message[wstpDDispersionObject::argx]; Abort[])
// <-- wstpDDescription


// --> wstpDDispersionMatrix
:Begin:
:Function:      wstpDDispersionMatrix
:Pattern:       wstpDDispersionMatrix[wstpDDispersionObject[id_String], k1_Real, k2_Real, o_Complex]
:Arguments:     {id, k1, k2, Re[o] // N, Im[o] // N}
:ArgumentTypes: {String, Real, Real, Real, Real}
:ReturnType:    Manual
:End:

:Evaluate:      wstpDDispersionMatrix[wstpDDispersionObject[id_String], k1_?NumberQ, k2_?NumberQ, o_?NumberQ] := If[!And[Head[k1] =!= Complex, Head[k2] =!= Complex], (Message[wstpDDispersionMatrix::argx]; Abort[]), wstpDDispersionMatrix[wstpDDispersionObject[id], k1 // N, k2 // N, o + 0. I]]
:Evaluate:      wstpDDispersionMatrix[handle_wstpDDispersionObject, k1 : {___?NumberQ}, k2 : {___?NumberQ}, o : {___?NumberQ}] /; If[!Equal[Length[k1], Length[k2], Length[o]], (Message[wstpDDispersionMatrix::argx]; Abort[]), True] := MapThread[wstpDDispersionMatrix[handle, ##]&, {k1, k2, o}]
:Evaluate:      wstpDDispersionMatrix[handle_, _?NumberQ, _?NumberQ, _?NumberQ] := (Message[wstpDDispersionObject::argx]; Abort[])
// <-- wstpDDispersionMatrix


// --> wstpDFindRoot
:Evaluate:      Clear[findRoot]
:Begin:
:Function:      wstpDFindRoot
:Pattern:       findRoot[wstpDDispersionObject[id_String], k1 : {__Real}, k2 : {__Real}, o : {_Complex, _Complex, _Complex}, maxIt_Integer?Positive, calD:(True|False), updateJMin:(True|False)]
:Arguments:     {id, {maxIt, calD // Boole, updateJMin // Boole}, k1, k2, Re[o] // N, Im[o] // N}
:ArgumentTypes: {String, IntegerList, RealList, RealList, RealList, RealList}
:ReturnType:    Manual
:End:

:Evaluate:      wstpDFindRoot[wstpDDispersionObject[id_String], k1 : {__?NumberQ}, k2 : {__?NumberQ}, o : {_?NumberQ, _?NumberQ, _?NumberQ}, OptionsPattern[wstpDFindRoot]] := Which[!And[Head[k1] =!= Complex, Head[k2] =!= Complex, Length[k1] == Length[k2]], (Message[wstpDFindRoot::argx]; Abort[]), !And[OptionValue[MaxIterations] // IntegerQ, OptionValue[MaxIterations] // Positive, OptionValue["ReturnDispersionMatrix"] // BooleanQ, OptionValue["UpdateMinimumCyclotronOrderFromPreviousIteration"] // BooleanQ], (Message[wstpDFindRoot::optx]; Abort[]), True, Replace[findRoot[wstpDDispersionObject[id], k1 // N, k2 // N, o + 0. I, OptionValue[MaxIterations], OptionValue["ReturnDispersionMatrix"], OptionValue["UpdateMinimumCyclotronOrderFromPreviousIteration"]], ("w" -> "\[Omega]"), {2}]]
:Evaluate:      wstpDFindRoot[handle_wstpDDispersionObject, k1_ ? NumberQ, k2_ ? NumberQ, o : {__ ? NumberQ} /; Length[o] == 3, opts : OptionsPattern[wstpDFindRoot]] := With[{result = wstpDFindRoot[handle, {k1}, {k2}, o, opts]}, If[Length["\[Omega]" /. result] == 0, (Message[wstpDFindRoot::noroot]; Abort[])]; result /. Rule[key_String, {value_}] :> Rule[key, value]]
:Evaluate:      wstpDFindRoot[handle_, {__?NumberQ}, {__?NumberQ}, {_?NumberQ, _?NumberQ, _?NumberQ}, OptionsPattern[wstpDFindRoot]] := (Message[wstpDDispersionObject::argx]; Abort[])
// <-- wstpDFindRoot


:Evaluate:      End[] (* Begin["`Private`"] *)


:Evaluate:      EndPackage[] (* BeginPackage["wstpDispersionSolver`"] *)
