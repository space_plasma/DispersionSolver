/*
 * This file automatically produced by /Applications/Mathematica.app/Contents/SystemFiles/Links/WSTP/DeveloperKit/MacOSX-x86-64/CompilerAdditions/wsprep from:
 *	wstp_interface.tm
 * wsprep Revision 19 Copyright (c) Wolfram Research, Inc. 1990-2021
 */

#define WSPREP_REVISION 19

#include "wstp.h"

int WSAbort = 0;
int WSDone  = 0;
long WSSpecialCharacter = '\0';

WSLINK stdlink = 0;
WSEnvironment stdenv = 0;
WSYieldFunctionObject stdyielder = (WSYieldFunctionObject)0;
WSMessageHandlerObject stdhandler = (WSMessageHandlerObject)0;

extern int WSDoCallPacket(WSLINK);
extern int WSEvaluate( WSLINK, char *);
extern int WSEvaluateString( WSLINK, char *);

/********************************* end header *********************************/


# line 1 "wstp_interface.tm"
/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

# line 35 "wstp_interface.tm.cc"


# line 14 "wstp_interface.tm"
// MARK: Public Interfaces
//

// Dispersion Relation
//
# line 44 "wstp_interface.tm.cc"


# line 73 "wstp_interface.tm"
// MARK: Private Interfaces to WSTP Process
//
# line 50 "wstp_interface.tm.cc"


# line 78 "wstp_interface.tm"
// --> wstpDRingDistribution
# line 55 "wstp_interface.tm.cc"


# line 82 "wstp_interface.tm"
// <-- wstpDRingDistribution


// --> wstpDMaxwellianDistribution
# line 63 "wstp_interface.tm.cc"


# line 88 "wstp_interface.tm"
// <-- wstpDMaxwellianDistribution


// --> wstpDProductKappaDistribution
# line 71 "wstp_interface.tm.cc"


# line 96 "wstp_interface.tm"
// <-- wstpDProductKappaDistribution


// --> wstpDKappaDistribution
# line 79 "wstp_interface.tm.cc"


# line 102 "wstp_interface.tm"
// <-- wstpDKappaDistribution


// --> wstpDCreateDispersionObject
# line 87 "wstp_interface.tm.cc"


# line 107 "wstp_interface.tm"
// wstpDRingDistribution
# line 92 "wstp_interface.tm.cc"


# line 110 "wstp_interface.tm"
// wstpDMaxwellianDistribution
# line 97 "wstp_interface.tm.cc"


# line 113 "wstp_interface.tm"
// wstpDProductKappaDistribution
# line 102 "wstp_interface.tm.cc"


# line 117 "wstp_interface.tm"
// wstpDKappaDistribution
# line 107 "wstp_interface.tm.cc"


# line 128 "wstp_interface.tm"
// <-- wstpDCreateDispersionObject


// --> wstpDListAllDispersionObjects
# line 115 "wstp_interface.tm.cc"


# line 139 "wstp_interface.tm"
// <-- wstpDListAllDispersionObjects


// --> wstpDRemoveObject
# line 123 "wstp_interface.tm.cc"


# line 152 "wstp_interface.tm"
// <-- wstpDRemoveObject


// --> wstpDSetMinimumCyclotronOrder
# line 131 "wstp_interface.tm.cc"


# line 165 "wstp_interface.tm"
// <-- wstpDSetMinimumCyclotronOrder


// --> wstpDMinimumCyclotronOrder
# line 139 "wstp_interface.tm.cc"


# line 178 "wstp_interface.tm"
// <-- wstpDMinimumCyclotronOrder


// --> wstpDDescription
# line 147 "wstp_interface.tm.cc"


# line 191 "wstp_interface.tm"
// <-- wstpDDescription


// --> wstpDDispersionMatrix
# line 155 "wstp_interface.tm.cc"


# line 206 "wstp_interface.tm"
// <-- wstpDDispersionMatrix


// --> wstpDFindRoot
# line 163 "wstp_interface.tm.cc"


# line 222 "wstp_interface.tm"
// <-- wstpDFindRoot


# line 170 "wstp_interface.tm.cc"


void wstpDCreateDispersionObject ( double _tp1, int _tp2);

static int _tr0( WSLINK wslp)
{
	int	res = 0;
	double _tp1;
	int _tp2;
	if ( ! WSGetReal( wslp, &_tp1) ) goto L0;
	if ( ! WSGetInteger( wslp, &_tp2) ) goto L1;

	wstpDCreateDispersionObject(_tp1, _tp2);

	res = 1;
 L1:
L0:	return res;
} /* _tr0 */


void wstpDListAllDispersionObjects ( void);

static int _tr1( WSLINK wslp)
{
	int	res = 0;
	if ( ! WSNewPacket(wslp) ) goto L0;
	if( !wslp) return res; /* avoid unused parameter warning */

	wstpDListAllDispersionObjects();

	res = 1;

L0:	return res;
} /* _tr1 */


void wstpDRemoveObject ( const char * _tp1);

static int _tr2( WSLINK wslp)
{
	int	res = 0;
	const char * _tp1;
	if ( ! WSGetString( wslp, &_tp1) ) goto L0;
	if ( ! WSNewPacket(wslp) ) goto L1;

	wstpDRemoveObject(_tp1);

	res = 1;
L1:	WSReleaseString(wslp, _tp1);

L0:	return res;
} /* _tr2 */


void wstpDSetMinimumCyclotronOrder ( const char * _tp1, int _tp2);

static int _tr3( WSLINK wslp)
{
	int	res = 0;
	const char * _tp1;
	int _tp2;
	if ( ! WSGetString( wslp, &_tp1) ) goto L0;
	if ( ! WSGetInteger( wslp, &_tp2) ) goto L1;
	if ( ! WSNewPacket(wslp) ) goto L2;

	wstpDSetMinimumCyclotronOrder(_tp1, _tp2);

	res = 1;
L2: L1:	WSReleaseString(wslp, _tp1);

L0:	return res;
} /* _tr3 */


void wstpDMinimumCyclotronOrder ( const char * _tp1);

static int _tr4( WSLINK wslp)
{
	int	res = 0;
	const char * _tp1;
	if ( ! WSGetString( wslp, &_tp1) ) goto L0;
	if ( ! WSNewPacket(wslp) ) goto L1;

	wstpDMinimumCyclotronOrder(_tp1);

	res = 1;
L1:	WSReleaseString(wslp, _tp1);

L0:	return res;
} /* _tr4 */


void wstpDDescription ( const char * _tp1);

static int _tr5( WSLINK wslp)
{
	int	res = 0;
	const char * _tp1;
	if ( ! WSGetString( wslp, &_tp1) ) goto L0;
	if ( ! WSNewPacket(wslp) ) goto L1;

	wstpDDescription(_tp1);

	res = 1;
L1:	WSReleaseString(wslp, _tp1);

L0:	return res;
} /* _tr5 */


void wstpDDispersionMatrix ( const char * _tp1, double _tp2, double _tp3, double _tp4, double _tp5);

static int _tr6( WSLINK wslp)
{
	int	res = 0;
	const char * _tp1;
	double _tp2;
	double _tp3;
	double _tp4;
	double _tp5;
	if ( ! WSGetString( wslp, &_tp1) ) goto L0;
	if ( ! WSGetReal( wslp, &_tp2) ) goto L1;
	if ( ! WSGetReal( wslp, &_tp3) ) goto L2;
	if ( ! WSGetReal( wslp, &_tp4) ) goto L3;
	if ( ! WSGetReal( wslp, &_tp5) ) goto L4;
	if ( ! WSNewPacket(wslp) ) goto L5;

	wstpDDispersionMatrix(_tp1, _tp2, _tp3, _tp4, _tp5);

	res = 1;
L5: L4: L3: L2: L1:	WSReleaseString(wslp, _tp1);

L0:	return res;
} /* _tr6 */


void wstpDFindRoot ( const char * _tp1, int * _tp2, long _tpl2, double * _tp3, long _tpl3, double * _tp4, long _tpl4, double * _tp5, long _tpl5, double * _tp6, long _tpl6);

static int _tr7( WSLINK wslp)
{
	int	res = 0;
	const char * _tp1;
	int * _tp2;
	long _tpl2;
	double * _tp3;
	long _tpl3;
	double * _tp4;
	long _tpl4;
	double * _tp5;
	long _tpl5;
	double * _tp6;
	long _tpl6;
	if ( ! WSGetString( wslp, &_tp1) ) goto L0;
	if ( ! WSGetIntegerList( wslp, &_tp2, &_tpl2) ) goto L1;
	if ( ! WSGetRealList( wslp, &_tp3, &_tpl3) ) goto L2;
	if ( ! WSGetRealList( wslp, &_tp4, &_tpl4) ) goto L3;
	if ( ! WSGetRealList( wslp, &_tp5, &_tpl5) ) goto L4;
	if ( ! WSGetRealList( wslp, &_tp6, &_tpl6) ) goto L5;
	if ( ! WSNewPacket(wslp) ) goto L6;

	wstpDFindRoot(_tp1, _tp2, _tpl2, _tp3, _tpl3, _tp4, _tpl4, _tp5, _tpl5, _tp6, _tpl6);

	res = 1;
L6:	WSReleaseReal64List(wslp, _tp6, _tpl6);
L5:	WSReleaseReal64List(wslp, _tp5, _tpl5);
L4:	WSReleaseReal64List(wslp, _tp4, _tpl4);
L3:	WSReleaseReal64List(wslp, _tp3, _tpl3);
L2:	WSReleaseInteger32List( wslp, _tp2, _tpl2);
L1:	WSReleaseString(wslp, _tp1);

L0:	return res;
} /* _tr7 */


static struct func {
	int   f_nargs;
	int   manual;
	int   (*f_func)(WSLINK);
	const char  *f_name;
	} _tramps[8] = {
		{ 2, 2, _tr0, "wstpDCreateDispersionObject" },
		{ 0, 0, _tr1, "wstpDListAllDispersionObjects" },
		{ 1, 0, _tr2, "wstpDRemoveObject" },
		{ 2, 0, _tr3, "wstpDSetMinimumCyclotronOrder" },
		{ 1, 0, _tr4, "wstpDMinimumCyclotronOrder" },
		{ 1, 0, _tr5, "wstpDDescription" },
		{ 5, 0, _tr6, "wstpDDispersionMatrix" },
		{ 6, 0, _tr7, "wstpDFindRoot" }
		};

static const char* evalstrs[] = {
	"BeginPackage[\"DispersionKit`\"]",
	(const char*)0,
	"(* Remove /@ Names[\"DispersionKit`*\"] *)",
	(const char*)0,
	"DispersionKit::warn = \"`1`\"",
	(const char*)0,
	"DispersionKit::erro = \"`1`\"",
	(const char*)0,
	"Clear[wstpDRingDistribution]",
	(const char*)0,
    "wstpDRingDistribution::usage = \"\\!\\(wstpDRingDistribution[\\[CapitalOmega]\\_c, \\[Omega]\\_p, \\[Theta]\\_\\[DoubleVerticalBar], \\[Theta]\\_\\[UpTee], v\\_d, v\\_r]\\) represents a Maxwellian-ring velocity distribution, which is proportional to \\!\\(\\*SuperscriptBox[\\(\\[ExponentialE]\\), \\(\\*SuperscriptBox[\\((\\*SubscriptBox[\\(v\\), \\(\\[DoubleVerticalBar]\\)] - \\*SubscriptBox[\\(v\\), \\(d\\)])\\), \\(2\\)]/\\*SubsuperscriptBox[\\(\\[Theta]\\), \\(\\[DoubleVerticalBar]\\), \\(2\\)]\\)] \\*SuperscriptBox[\\(\\[ExponentialE]\\), \\(\\*SuperscriptBox[\\((\\*SubscriptBox[\\(v\\), \\(\\[UpTee]\\)] - \\*SubscriptBox[\\(v\\), \\(r\\)])\\), \\(2\\)]/\\*SubsuperscriptBox[\\(\\[Theta]\\), \\(\\[UpTee]\\), \\(2\\)]\\)]\\).\\n\\!\\(\\[CapitalOmega]\\_c\\) is the real-valued cyclotron frequency and \\!\\(\\[Omega]\\_p\\) is either pure real- or pure imaginary-valued plasma frequency. The parallel and perpendicular thermal speeds can be any non-negative real number (including 0), and the parallel drift and ring speeds can be any real number (note that there is a limit on the maximum of \\!\\(-v\\_r/\\[Theta]\\_\\[UpTee]\\)).\\nAt least the first three arguments should be given. For any other parameters not specified, the default values are: \\!\\(\\[Theta]\\_\\[UpTee] = \\[Theta]\\_\\[DoubleVerticalBar]\\), \\!\\(v\\_d = 0\\) and \\!\\(v\\_r = 0\\).\"",
	(const char*)0,
	"wstpDRingDistribution::argx = \"Invalid argument(s).\"",
	(const char*)0,
	"Clear[wstpDMaxwellianDistribution]",
	(const char*)0,
    "wstpDMaxwellianDistribution::usage = \"\\!\\(wstpDMaxwellianDistribution[\\[CapitalOmega]\\_c, \\[Omega]\\_p, \\[Theta]\\_\\[DoubleVerticalBar], \\[Theta]\\_\\[UpTee], v\\_d]\\) represents a bi-Maxwellian velocity distribution, which is proportional to \\!\\(\\*SuperscriptBox[\\(\\[ExponentialE]\\), \\(\\*SuperscriptBox[\\((\\*SubscriptBox[\\(v\\), \\(\\[DoubleVerticalBar]\\)] - \\*SubscriptBox[\\(v\\), \\(d\\)])\\), \\(2\\)]/\\*SubsuperscriptBox[\\(\\[Theta]\\), \\(\\[DoubleVerticalBar]\\), \\(2\\)]\\)] \\*SuperscriptBox[\\(\\[ExponentialE]\\), \\(\\*SubsuperscriptBox[\\(v\\), \\(\\[UpTee]\\), \\(2\\)]/\\*SubsuperscriptBox[\\(\\[Theta]\\), \\(\\[UpTee]\\), \\(2\\)]\\)]\\).\\n\\!\\(\\[CapitalOmega]\\_c\\) is the real-valued cyclotron frequency and \\!\\(\\[Omega]\\_p\\) is either pure real- or pure imaginary-valued plasma frequency. The parallel and perpendicular thermal speeds can be any non-negative real number (including 0), and the parallel drift speed can be any real number.\\nAt least the first three arguments should be given. For any other parameters not specified, the default values are: \\!\\(\\[Theta]\\_\\[UpTee] = \\[Theta]\\_\\[DoubleVerticalBar]\\) and \\!\\(v\\_d = 0\\).\"",
	(const char*)0,
	"wstpDMaxwellianDistribution::argx = \"Invalid argument(s).\"",
	(const char*)0,
	"Clear[wstpDProductKappaDistribution]",
	(const char*)0,
    "wstpDProductKappaDistribution::usage = \"\\!\\(wstpDProductKappaDistribution[\\[CapitalOmega]\\_c, \\[Omega]\\_p, {\\[Kappa]}, \\[Theta]\\_\\[DoubleVerticalBar], \\[Theta]\\_\\[UpTee], v\\_d]\\) represents a product bi-kappa velocity distribution, \\!\\(TraditionalForm\\`\\(\\*SubscriptBox[\\(f\\), \\(\\[Kappa]\\)]\\)[\\*SubscriptBox[\\(v\\), \\(\\[DoubleVerticalBar]\\)], \\*SubscriptBox[\\(v\\), \\(\\[UpTee]\\)]] = \\*FractionBox[\\(\\*SqrtBox[SubscriptBox[\\(\\[Kappa]\\), \\(\\[DoubleVerticalBar]\\)]] \\[CapitalGamma][\\*SubscriptBox[\\(\\[Kappa]\\), \\(\\[DoubleVerticalBar]\\)]]\\), \\(\\[CapitalGamma][\\*SubscriptBox[\\(\\[Kappa]\\), \\(\\[DoubleVerticalBar]\\)] + 1/2]\\)] \\*FractionBox[SuperscriptBox[\\((1 + \\*SuperscriptBox[\\((\\*SubscriptBox[\\(v\\), \\(\\[DoubleVerticalBar]\\)] - \\*SubscriptBox[\\(v\\), \\(d\\)])\\), \\(2\\)]/\\*SubscriptBox[\\(\\[Kappa]\\), \\(\\[DoubleVerticalBar]\\)] \\*SubsuperscriptBox[\\(\\[Theta]\\), \\(\\[DoubleVerticalBar]\\), \\(2\\)])\\), \\(-\\((\\*SubscriptBox[\\(\\[Kappa]\\), \\(\\[DoubleVerticalBar]\\)] + 1)\\)\\)], \\(\\*SqrtBox[\\(\\[Pi]\\)] \\*SubscriptBox[\\(\\[Theta]\\), \\(\\[DoubleVerticalBar]\\)]\\)] \\*FractionBox[SuperscriptBox[\\((1 + \\*SubsuperscriptBox[\\(v\\), \\(\\[UpTee]\\), \\(2\\)]/\\*SubscriptBox[\\(\\[Kappa]\\), \\(\\[UpTee]\\)] \\*SubsuperscriptBox[\\(\\[Theta]\\), \\(\\[UpTee]\\), \\(2\\)])\\), \\(-\\((\\*SubscriptBox[\\(\\[Kappa]\\), \\(\\[UpTee]\\)] + 1)\\)\\)], \\(\\[Pi]\\\\ \\*SubsuperscriptBox[\\(\\[Theta]\\), \\(\\[UpTee]\\), \\(2\\)]\\)]\\), where \\[Kappa] > 1 is the real-valued power index. The parallel and perpendicular temperatures are \\!\\(\\*SubscriptBox[\\(T\\), \\(\\[DoubleVerticalBar]\\)]\\)=\\!\\(\\*FractionBox[\\(\\[Kappa]\\), \\(2  \\[Kappa] - 1\\)]\\)\\!\\(\\*SubsuperscriptBox[\\(\\[Theta]\\), \\(\\[DoubleVerticalBar]\\), \\(2\\)]\\) and \\!\\(\\*SubscriptBox[\\(T\\), \\(\\[UpTee]\\)]\\)=\\!\\(\\*FractionBox[\\(\\[Kappa]\\), \\(2  \\[Kappa] - 2\\)]\\)\\!\\(\\*SubsuperscriptBox[\\(\\[Theta]\\), \\(\\[UpTee]\\), \\(2\\)]\\), respectively.\\n\\!\\(\\[CapitalOmega]\\_c\\) is the real-valued cyclotron frequency and \\!\\(\\[Omega]\\_p\\) is either pure real- or pure imaginary-valued plasma frequency. The parallel and perpendicular thermal speeds can be any positive real number (\\!\\(\\[Theta]\\_\\[DoubleVerticalBar] \\[GreaterEqual] 0\\) and \\!\\(\\[Theta]\\_\\[UpTee] > 0\\)), and the parallel drift speed can be any real number.\\nAt least the first four arguments should be given. For any other parameters not specified, the default values are: \\!\\(\\[Theta]\\_\\[UpTee] = \\[Theta]\\_\\[DoubleVerticalBar]\\) and \\!\\(v\\_d = 0\\).\\n\\!\\(wstpDProductKappaDistribution[\\[Ellipsis], {\\[Kappa]\\_\\[DoubleVerticalBar], \\[Kappa]\\_\\[UpTee]}, \\[Ellipsis]]\\) uses anisotropic \\[Kappa] indexes corresponding to the parallel and perpendicular components of the product bi-kappa distribution function, respectively, where \\!\\(\\[Kappa]\\_\\[DoubleVerticalBar] > 1/2\\) and \\!\\(\\[Kappa]\\_\\[UpTee] > 1\\).\"",
	(const char*)0,
	"wstpDProductKappaDistribution::argx = \"Invalid argument(s).\"",
	(const char*)0,
	"Clear[wstpDKappaDistribution]",
	(const char*)0,
    "wstpDKappaDistribution::usage = \"\\!\\(wstpDKappaDistribution[\\[CapitalOmega]\\_c, \\[Omega]\\_p, {\\[Kappa]}, \\[Theta]\\_\\[DoubleVerticalBar], \\[Theta]\\_\\[UpTee], v\\_d]\\) represents a bi-kappa velocity distribution, \\!\\(TraditionalForm\\`\\(\\*SubscriptBox[\\(f\\), \\(\\[Kappa]\\)]\\)[\\*SubscriptBox[\\(v\\), \\(\\[DoubleVerticalBar]\\)], \\*SubscriptBox[\\(v\\), \\(\\[UpTee]\\)]] = \\*FractionBox[\\(1\\), \\(\\*SuperscriptBox[\\(\\[Pi]\\), \\(3/2\\)] \\*SubscriptBox[\\(\\[Theta]\\), \\(\\[DoubleVerticalBar]\\)] \\*SubsuperscriptBox[\\(\\[Theta]\\), \\(\\[UpTee]\\), \\(2\\)]\\)] \\*FractionBox[\\(\\[CapitalGamma][\\[Kappa]]\\), \\(\\*SqrtBox[\\(\\[Kappa]\\)] \\[CapitalGamma][\\[Kappa] - 1/2]\\)] \\*SuperscriptBox[\\((1 + \\*FractionBox[SuperscriptBox[\\((\\*SubscriptBox[\\(v\\), \\(\\[DoubleVerticalBar]\\)] - \\*SubscriptBox[\\(v\\), \\(d\\)])\\), \\(2\\)], \\(\\[Kappa]\\\\ \\*SubsuperscriptBox[\\(\\[Theta]\\), \\(\\[DoubleVerticalBar]\\), \\(2\\)]\\)] + \\*FractionBox[SubsuperscriptBox[\\(v\\), \\(\\[UpTee]\\), \\(2\\)], \\(\\[Kappa]\\\\ \\*SubsuperscriptBox[\\(\\[Theta]\\), \\(\\[UpTee]\\), \\(2\\)]\\)])\\), \\(-\\((\\[Kappa] + 1)\\)\\)]\\), where \\[Kappa] > 3/2 is the real-valued power index. The parallel and perpendicular temperatures are \\!\\(\\*FractionBox[SubscriptBox[\\(T\\), \\(\\[UpTee]\\)], SubsuperscriptBox[\\(\\[Theta]\\), \\(\\[UpTee]\\), \\(2\\)]]\\)=\\!\\(\\*FractionBox[SubscriptBox[\\(T\\), \\(\\[DoubleVerticalBar]\\)], SubsuperscriptBox[\\(\\[Theta]\\), \\(\\[DoubleVerticalBar]\\), \\(2\\)]]\\)=\\!\\(\\*FractionBox[\\(\\[Kappa]\\), \\(2  \\[Kappa] - 3\\)]\\).\\n\\!\\(\\[CapitalOmega]\\_c\\) is the real-valued cyclotron frequency and \\!\\(\\[Omega]\\_p\\) is either pure real- or pure imaginary-valued plasma frequency. The parallel and perpendicular thermal speeds can be any positive real number (\\!\\(\\[Theta]\\_\\[DoubleVerticalBar] \\[GreaterEqual] 0\\) and \\!\\(\\[Theta]\\_\\[UpTee] > 0\\)), and the parallel drift speed can be any real number.\\nAt least the first four arguments should be given. For any other parameters not specified, the default values are: \\!\\(\\[Theta]\\_\\[UpTee] = \\[Theta]\\_\\[DoubleVerticalBar]\\) and \\!\\(v\\_d = 0\\).\"",
	(const char*)0,
	"wstpDKappaDistribution::argx = \"Invalid argument(s).\"",
	(const char*)0,
	"Clear[wstpDDispersionObject]",
	(const char*)0,
	"wstpDDispersionObject::usage = \"wstpDDispersionObject[uniqueid] ",
	"is a handle to the internal dispersion object returned by wstpDC",
	"reateDispersionObject.\"",
	(const char*)0,
	"wstpDDispersionObject::argx = \"Invalid handle.\"",
	(const char*)0,
	"Clear[wstpDCreateDispersionObject]",
	(const char*)0,
    "wstpDCreateDispersionObject::usage = \"\\!\\(wstpDCreateDispersionObject[c_?Positive, dists:{(_wstpDRingDistribution | _wstpDMaxwellianDistribution | _wstpDProductKappaDistribution | _wstpDKappaDistribution)...}, \\(j\\_min\\) : _Integer?Positive : 5]\\) creates an object that represents a dispersion matrix constructed with the provided parameters and returns a handle (an expression with wstpDDispersionObject head) to it.\\n\\!\\(c\\) is the speed of light and \\!\\(dists\\) is a list of velocity distributions. \\!\\(j\\_\\(min\\) \\[LessEqual] 2000\\) is the minimum cyclotron order (see usage of wstpDSetMinimumCyclotronOrder).\"",
	(const char*)0,
	"Clear[wstpDListAllDispersionObjects]",
	(const char*)0,
    "wstpDListAllDispersionObjects::usage = \"\\!\\(wstpDListAllDispersionObjects[]\\) returns a list of all dispersion object handles internally kept.\"",
	(const char*)0,
	"Clear[wstpDRemoveObject]",
	(const char*)0,
	"SetAttributes[wstpDRemoveObject, {Listable}]",
	(const char*)0,
    "wstpDRemoveObject::usage = \"\\!\\(wstpDRemoveObject[handle_wstpDDispersionObject]\\) removes, if it exists, the dispersion object identified by the passed handle from the internal table.\\n\\!\\(wstpDRemoveObject[handles : {___wstpDDispersionObject}]\\) automatically threads over the elements of the list.\"",
	(const char*)0,
	"Clear[wstpDSetMinimumCyclotronOrder]",
	(const char*)0,
    "wstpDSetMinimumCyclotronOrder::usage = \"\\!\\(wstpDSetMinimumCyclotronOrder[handle_wstpDDispersionObject, \\(j\\_min\\)]\\) sets the minimum cyclotron order (positive integer), \\!\\(j\\_\\(min\\) \\[LessEqual] 2000\\), of the given dispersion object. When calculating the dispersion tensor, summation \\!\\(\\*SubsuperscriptBox[\\(\\[Sum]\\), \\(j = \\(-\\*SubscriptBox[\\(j\\), \\(min\\)]\\)\\), SubscriptBox[\\(j\\), \\(min\\)]](\\[CenterEllipsis])\\) is first carried out. After that, convergence of the dispersion tensor is tested for \\!\\(\\[LeftBracketingBar]j\\[RightBracketingBar] > \\*SubscriptBox[\\(j\\), \\(min\\)]\\), and if converged, the summation is terminated. If not converged, the summation continues until \\!\\(\\[LeftBracketingBar]j\\[RightBracketingBar] = \\(j\\_max\\)\\). It returns \\!\\({\\(j\\_min\\), \\(j\\_max\\)}\\).\"",
	(const char*)0,
	"Clear[wstpDMinimumCyclotronOrder]",
	(const char*)0,
    "wstpDMinimumCyclotronOrder::usage = \"\\!\\(wstpDMinimumCyclotronOrder[handle_wstpDDispersionObject]\\) returns the minimum cyclotron order associated with the given dispersion object.\"",
	(const char*)0,
	"Clear[wstpDDescription]",
	(const char*)0,
	"wstpDDescription::usage = \"wstpDDescription[handle_wstpDDispersi",
	"onObject] returns a textual description of the given dispersion ",
	"object.\"",
	(const char*)0,
	"Clear[wstpDDispersionMatrix]",
	(const char*)0,
    "wstpDDispersionMatrix::usage = \"\\!\\(wstpDDispersionMatrix[handle_wstpDDispersionObject, \\(k\\_\\[DoubleVerticalBar]\\), \\(k\\_\\[UpTee]\\), \\[Omega]]\\) evaluates the dispersion matrix for the given wave number (real number) and complex frequency. It returns a 3x3 matrix or Indeterminate, the latter indicating error. The minimum cyclotron order may need to be set first.\\n\\!\\(wstpDDispersionMatrix[handle_wstpDDispersionObject, \\(k\\_\\[DoubleVerticalBar]\\) : {___?NumberQ}, \\(k\\_\\[UpTee]\\) : {___?NumberQ}, \\[Omega] : {___?NumberQ}]\\) automatically thread over the list of wave numbers and complex frequencies.\"",
	(const char*)0,
	"wstpDDispersionMatrix::argx = \"Invalid argument(s).\"",
	(const char*)0,
	"Clear[wstpDFindRoot]",
	(const char*)0,
	"Options[wstpDFindRoot] = { MaxIterations -> 20, \"ReturnDispersio",
	"nMatrix\" -> False, \"UpdateMinimumCyclotronOrderFromPreviousItera",
	"tion\" -> False }",
	(const char*)0,
    "wstpDFindRoot::usage = \"\\!\\(wstpDFindRoot[handle_wstpDDispersionObject, \\(k\\_\\[DoubleVerticalBar]\\) : {__?NumberQ}, \\(k\\_\\[UpTee]\\) : {__?NumberQ}, {\\[Omega]\\_1, \\[Omega]\\_2, \\[Omega]\\_3}]\\) finds complex frequency roots for the given wave numbers (real numbers). First, the three initial guesses, \\!\\({\\[Omega]\\_1, \\[Omega]\\_2, \\[Omega]\\_3}\\), of the complex frequency are used to find the first root corresponding to the first element of the wave number. When there is a second element of the wave number, \\!\\(\\[Omega]\\_2\\), \\!\\(\\[Omega]\\_3\\) and the newly found root are used as the initial guesses to find the second root. Roots for the subsequent wave numbers, if any, are found in a similar fashion.\\nIt returns a list of rules. The rule \\\"\\[Omega]\\\"->{\\[CenterEllipsis]} contains the found roots (not including \\!\\({\\[Omega]\\_1, \\[Omega]\\_2, \\[Omega]\\_3}\\)). It can contain no element when the root finding failed for the first wave number or less than the number of wave numbers given if the root finding failed at an intermediate wave number. The rules \\\"k1\\\"->{\\[CenterEllipsis]} and \\\"k2\\\"->{\\[CenterEllipsis]} contain corresponding parallel and perpendicular wave numbers, respectively. The rule \\\"flag\\\"->(0|1|2) indicates a reason for termination, where 0 indicates normal termination (in which case, all roots for the given wave numbers are found), 1 indicates failed root finding at an intermediate wave number, and 2 indicates user interruption (abort signal) while finding the roots. For flag other than 0, all roots found up to the point of interruption or failure are returned.\\nThere are several options. The \\!\\(MaxIterations\\) option sets the maximum number of iterations to be performed for root finding (default is 20). If the \\\"ReturnDispersionMatrix\\\" option is \\!\\(True\\), the dispersion matrixes is also calculated and returned as a rule \\\"D\\\"->{\\[CenterEllipsis]} (default is \\!\\(False\\)). If the \\\"UpdateMinimumCyclotronOrderFromPreviousIteration\\\" option is \\!\\(True\\), the cyclotron order that led to the convergence of the dispersion tensor is set with wstpDSetMinimumCyclotronOrder for root finding for the next wave number (default is \\!\\(False\\)).\"",
	(const char*)0,
    "wstpDFindRoot::usage = wstpDFindRoot::usage <> \"\\n\\!\\(wstpDFindRoot[handle_wstpDDispersionObject, \\(k\\_\\[DoubleVerticalBar]\\) : _?NumberQ, \\(k\\_\\[UpTee]\\) : _?NumberQ, {\\[Omega]\\_1, \\[Omega]\\_2, \\[Omega]\\_3}]\\) finds a complex frequency root at the wave vector specified by a pair of \\!\\(k\\_\\[DoubleVerticalBar]\\) and \\!\\(k\\_\\[UpTee]\\).\"",
	(const char*)0,
	"wstpDFindRoot::argx = \"Invalid argument(s).\"",
	(const char*)0,
	"wstpDFindRoot::optx = \"Invalid option(s).\"",
	(const char*)0,
	"wstpDFindRoot::noroot = \"No root found.\"",
	(const char*)0,
	"Begin[\"`Private`\"]",
	(const char*)0,
	"wstpDRingDistribution[Oc_, op_, th_] := wstpDRingDistribution[Oc",
	", op, th, th]",
	(const char*)0,
	"wstpDRingDistribution[Oc_, op_, th1_, th2_] := wstpDRingDistribu",
	"tion[Oc, op, th1, th2, 0.]",
	(const char*)0,
	"wstpDRingDistribution[Oc_, op_, th1_, th2_, vd_] := wstpDRingDis",
	"tribution[Oc, op, th1, th2, vd, 0.]",
	(const char*)0,
	"wstpDMaxwellianDistribution[Oc_, op_, th_] := wstpDMaxwellianDis",
	"tribution[Oc, op, th, th]",
	(const char*)0,
	"wstpDMaxwellianDistribution[Oc_, op_, th1_, th2_] := wstpDMaxwel",
	"lianDistribution[Oc, op, th1, th2, 0.]",
	(const char*)0,
	"wstpDProductKappaDistribution[Oc_, op_, {k_}, th_] := wstpDProdu",
	"ctKappaDistribution[Oc, op, {k}, th, th]",
	(const char*)0,
	"wstpDProductKappaDistribution[Oc_, op_, {k_}, th1_, th2_] := wst",
	"pDProductKappaDistribution[Oc, op, {k}, th1, th2, 0.]",
	(const char*)0,
	"wstpDProductKappaDistribution[Oc_, op_, {k1_, k2_}, th_] := wstp",
	"DProductKappaDistribution[Oc, op, {k1, k2}, th, th]",
	(const char*)0,
	"wstpDProductKappaDistribution[Oc_, op_, {k1_, k2_}, th1_, th2_] ",
	":= wstpDProductKappaDistribution[Oc, op, {k1, k2}, th1, th2, 0.]",
	(const char*)0,
	"wstpDKappaDistribution[Oc_, op_, {k_}, th_] := wstpDKappaDistrib",
	"ution[Oc, op, {k}, th, th]",
	(const char*)0,
	"wstpDKappaDistribution[Oc_, op_, {k_}, th1_, th2_] := wstpDKappa",
	"Distribution[Oc, op, {k}, th1, th2, 0.]",
	(const char*)0,
	"Clear[mutateVelocityDistribution]",
	(const char*)0,
	"mutateVelocityDistribution[wstpDRingDistribution, ___] := (Messa",
	"ge[wstpDRingDistribution::argx]; Abort[])",
	(const char*)0,
	"mutateVelocityDistribution[wstpDRingDistribution, Oc_Real, op_?N",
	"umberQ, th1_Real?NonNegative, th2_Real?NonNegative, vd_Real, vr_",
	"Real] /; Head[Chop[op^2]] =!= Complex := wstpDRingDistribution[O",
	"c, Chop[op^2] // N, th1, th2, vd, vr]",
	(const char*)0,
	"mutateVelocityDistribution[wstpDMaxwellianDistribution, ___] := ",
	"(Message[wstpDMaxwellianDistribution::argx]; Abort[])",
	(const char*)0,
	"mutateVelocityDistribution[wstpDMaxwellianDistribution, Oc_Real,",
	" op_?NumberQ, th1_Real?NonNegative, th2_Real?NonNegative, vd_Rea",
	"l] /; Head[Chop[op^2]] =!= Complex := wstpDMaxwellianDistributio",
	"n[Oc, Chop[op^2] // N, th1, th2, vd]",
	(const char*)0,
	"mutateVelocityDistribution[wstpDProductKappaDistribution, ___] :",
	"= (Message[wstpDProductKappaDistribution::argx]; Abort[])",
	(const char*)0,
	"mutateVelocityDistribution[wstpDProductKappaDistribution, Oc_Rea",
	"l, op_?NumberQ, {k_Real /; k > 1}, th1_Real?NonNegative, th2_Rea",
	"l?NonNegative, vd_Real] /; Head[Chop[op^2]] =!= Complex := wstpD",
	"ProductKappaDistribution[Oc, Chop[op^2] // N, Sequence @@ {k, k}",
	", th1, th2, vd]",
	(const char*)0,
	"mutateVelocityDistribution[wstpDProductKappaDistribution, Oc_Rea",
	"l, op_?NumberQ, {k1_Real /; k1 > 1/2, k2_Real /; k2 > 1}, th1_Re",
	"al?NonNegative, th2_Real?NonNegative, vd_Real] /; Head[Chop[op^2",
	"]] =!= Complex := wstpDProductKappaDistribution[Oc, Chop[op^2] /",
	"/ N, Sequence @@ {k1, k2}, th1, th2, vd]",
	(const char*)0,
	"mutateVelocityDistribution[wstpDKappaDistribution, ___] := (Mess",
	"age[wstpDKappaDistribution::argx]; Abort[])",
	(const char*)0,
	"mutateVelocityDistribution[wstpDKappaDistribution, Oc_Real, op_?",
	"NumberQ, {k_Real /; k > 3/2}, th1_Real?NonNegative, th2_Real?Non",
	"Negative, vd_Real] /; Head[Chop[op^2]] =!= Complex := wstpDKappa",
	"Distribution[Oc, Chop[op^2] // N, Sequence @@ {k}, th1, th2, vd]",
	(const char*)0,
	"wstpDRemoveObject[handle_] := (Message[wstpDDispersionObject::ar",
	"gx]; Abort[])",
	(const char*)0,
	"wstpDSetMinimumCyclotronOrder[handle_, _Integer ? Positive] := (",
	"Message[wstpDDispersionObject::argx]; Abort[])",
	(const char*)0,
	"wstpDMinimumCyclotronOrder[handle_] := (Message[wstpDDispersionO",
	"bject::argx]; Abort[])",
	(const char*)0,
	"wstpDDescription[handle_] := (Message[wstpDDispersionObject::arg",
	"x]; Abort[])",
	(const char*)0,
	"wstpDDispersionMatrix[wstpDDispersionObject[id_String], k1_?Numb",
	"erQ, k2_?NumberQ, o_?NumberQ] := If[!And[Head[k1] =!= Complex, H",
	"ead[k2] =!= Complex], (Message[wstpDDispersionMatrix::argx]; Abo",
	"rt[]), wstpDDispersionMatrix[wstpDDispersionObject[id], k1 // N,",
	" k2 // N, o + 0. I]]",
	(const char*)0,
	"wstpDDispersionMatrix[handle_wstpDDispersionObject, k1 : {___?Nu",
	"mberQ}, k2 : {___?NumberQ}, o : {___?NumberQ}] /; If[!Equal[Leng",
	"th[k1], Length[k2], Length[o]], (Message[wstpDDispersionMatrix::",
	"argx]; Abort[]), True] := MapThread[wstpDDispersionMatrix[handle",
	", ##]&, {k1, k2, o}]",
	(const char*)0,
	"wstpDDispersionMatrix[handle_, _?NumberQ, _?NumberQ, _?NumberQ] ",
	":= (Message[wstpDDispersionObject::argx]; Abort[])",
	(const char*)0,
	"Clear[findRoot]",
	(const char*)0,
	"wstpDFindRoot[wstpDDispersionObject[id_String], k1 : {__?NumberQ",
	"}, k2 : {__?NumberQ}, o : {_?NumberQ, _?NumberQ, _?NumberQ}, Opt",
	"ionsPattern[wstpDFindRoot]] := Which[!And[Head[k1] =!= Complex, ",
	"Head[k2] =!= Complex, Length[k1] == Length[k2]], (Message[wstpDF",
	"indRoot::argx]; Abort[]), !And[OptionValue[MaxIterations] // Int",
	"egerQ, OptionValue[MaxIterations] // Positive, OptionValue[\"Retu",
	"rnDispersionMatrix\"] // BooleanQ, OptionValue[\"UpdateMinimumCycl",
	"otronOrderFromPreviousIteration\"] // BooleanQ], (Message[wstpDFi",
	"ndRoot::optx]; Abort[]), True, Replace[findRoot[wstpDDispersionO",
	"bject[id], k1 // N, k2 // N, o + 0. I, OptionValue[MaxIterations",
	"], OptionValue[\"ReturnDispersionMatrix\"], OptionValue[\"UpdateMin",
	"imumCyclotronOrderFromPreviousIteration\"]], (\"w\" -> \"\\[Omega]\"), {2}]]",
	(const char*)0,
	"wstpDFindRoot[handle_wstpDDispersionObject, k1_ ? NumberQ, k2_ ?",
	" NumberQ, o : {__ ? NumberQ} /; Length[o] == 3, opts : OptionsPa",
	"ttern[wstpDFindRoot]] := With[{result = wstpDFindRoot[handle, {k",
	"1}, {k2}, o, opts]}, If[Length[\"\\[Omega]\" /. result] == 0, (Message[wst",
	"pDFindRoot::noroot]; Abort[])]; result /. Rule[key_String, {valu",
	"e_}] :> Rule[key, value]]",
	(const char*)0,
	"wstpDFindRoot[handle_, {__?NumberQ}, {__?NumberQ}, {_?NumberQ, _",
	"?NumberQ, _?NumberQ}, OptionsPattern[wstpDFindRoot]] := (Message",
	"[wstpDDispersionObject::argx]; Abort[])",
	(const char*)0,
	"End[] (* Begin[\"`Private`\"] *)",
	(const char*)0,
	"EndPackage[] (* BeginPackage[\"wstpDispersionSolver`\"] *)",
	(const char*)0,
	(const char*)0
};
#define CARDOF_EVALSTRS 77

static int _definepattern( WSLINK, char*, char*, int);

static int _doevalstr( WSLINK, int);

int  _WSDoCallPacket( WSLINK, struct func[], int);


int WSInstall( WSLINK wslp)
{
	int _res;
	_res = WSConnect(wslp);
	if (_res) _res = _doevalstr( wslp, 0);
	if (_res) _res = _doevalstr( wslp, 1);
	if (_res) _res = _doevalstr( wslp, 2);
	if (_res) _res = _doevalstr( wslp, 3);
	if (_res) _res = _doevalstr( wslp, 4);
	if (_res) _res = _doevalstr( wslp, 5);
	if (_res) _res = _doevalstr( wslp, 6);
	if (_res) _res = _doevalstr( wslp, 7);
	if (_res) _res = _doevalstr( wslp, 8);
	if (_res) _res = _doevalstr( wslp, 9);
	if (_res) _res = _doevalstr( wslp, 10);
	if (_res) _res = _doevalstr( wslp, 11);
	if (_res) _res = _doevalstr( wslp, 12);
	if (_res) _res = _doevalstr( wslp, 13);
	if (_res) _res = _doevalstr( wslp, 14);
	if (_res) _res = _doevalstr( wslp, 15);
	if (_res) _res = _doevalstr( wslp, 16);
	if (_res) _res = _doevalstr( wslp, 17);
	if (_res) _res = _doevalstr( wslp, 18);
	if (_res) _res = _doevalstr( wslp, 19);
	if (_res) _res = _doevalstr( wslp, 20);
	if (_res) _res = _doevalstr( wslp, 21);
	if (_res) _res = _doevalstr( wslp, 22);
	if (_res) _res = _doevalstr( wslp, 23);
	if (_res) _res = _doevalstr( wslp, 24);
	if (_res) _res = _doevalstr( wslp, 25);
	if (_res) _res = _doevalstr( wslp, 26);
	if (_res) _res = _doevalstr( wslp, 27);
	if (_res) _res = _doevalstr( wslp, 28);
	if (_res) _res = _doevalstr( wslp, 29);
	if (_res) _res = _doevalstr( wslp, 30);
	if (_res) _res = _doevalstr( wslp, 31);
	if (_res) _res = _doevalstr( wslp, 32);
	if (_res) _res = _doevalstr( wslp, 33);
	if (_res) _res = _doevalstr( wslp, 34);
	if (_res) _res = _doevalstr( wslp, 35);
	if (_res) _res = _doevalstr( wslp, 36);
	if (_res) _res = _doevalstr( wslp, 37);
	if (_res) _res = _doevalstr( wslp, 38);
	if (_res) _res = _doevalstr( wslp, 39);
	if (_res) _res = _doevalstr( wslp, 40);
	if (_res) _res = _doevalstr( wslp, 41);
	if (_res) _res = _doevalstr( wslp, 42);
	if (_res) _res = _doevalstr( wslp, 43);
	if (_res) _res = _doevalstr( wslp, 44);
	if (_res) _res = _doevalstr( wslp, 45);
	if (_res) _res = _doevalstr( wslp, 46);
	if (_res) _res = _doevalstr( wslp, 47);
	if (_res) _res = _doevalstr( wslp, 48);
	if (_res) _res = _doevalstr( wslp, 49);
	if (_res) _res = _doevalstr( wslp, 50);
	if (_res) _res = _doevalstr( wslp, 51);
	if (_res) _res = _doevalstr( wslp, 52);
	if (_res) _res = _doevalstr( wslp, 53);
	if (_res) _res = _doevalstr( wslp, 54);
	if (_res) _res = _doevalstr( wslp, 55);
	if (_res) _res = _doevalstr( wslp, 56);
	if (_res) _res = _doevalstr( wslp, 57);
	if (_res) _res = _doevalstr( wslp, 58);
	if (_res) _res = _doevalstr( wslp, 59);
	if (_res) _res = _doevalstr( wslp, 60);
	if (_res) _res = _doevalstr( wslp, 61);
	if (_res) _res = _doevalstr( wslp, 62);
	if (_res) _res = _doevalstr( wslp, 63);
	if (_res) _res = _definepattern(wslp, (char *)"wstpDCreateDispersionObject[c_?Positive, dists : {(_wstpDRingDistribution | _wstpDMaxwellianDistribution | _wstpDProductKappaDistribution | _wstpDKappaDistribution)...}, jmin : _Integer?Positive : 5] /; jmin <= 2000", (char *)"{N[c], jmin, mutateVelocityDistribution[Head[#], Sequence @@ #]& /@ N[dists]}", 0);
	if (_res) _res = _definepattern(wslp, (char *)"wstpDListAllDispersionObjects[]", (char *)"{}", 1);
	if (_res) _res = _definepattern(wslp, (char *)"wstpDRemoveObject[wstpDDispersionObject[id_String]]", (char *)"{id}", 2);
	if (_res) _res = _doevalstr( wslp, 64);
	if (_res) _res = _definepattern(wslp, (char *)"wstpDSetMinimumCyclotronOrder[wstpDDispersionObject[id_String], jmin_Integer ? Positive]", (char *)"{id, jmin}", 3);
	if (_res) _res = _doevalstr( wslp, 65);
	if (_res) _res = _definepattern(wslp, (char *)"wstpDMinimumCyclotronOrder[wstpDDispersionObject[id_String]]", (char *)"{id}", 4);
	if (_res) _res = _doevalstr( wslp, 66);
	if (_res) _res = _definepattern(wslp, (char *)"wstpDDescription[wstpDDispersionObject[id_String]]", (char *)"{id}", 5);
	if (_res) _res = _doevalstr( wslp, 67);
	if (_res) _res = _definepattern(wslp, (char *)"wstpDDispersionMatrix[wstpDDispersionObject[id_String], k1_Real, k2_Real, o_Complex]", (char *)"{id, k1, k2, Re[o] // N, Im[o] // N}", 6);
	if (_res) _res = _doevalstr( wslp, 68);
	if (_res) _res = _doevalstr( wslp, 69);
	if (_res) _res = _doevalstr( wslp, 70);
	if (_res) _res = _doevalstr( wslp, 71);
	if (_res) _res = _definepattern(wslp, (char *)"findRoot[wstpDDispersionObject[id_String], k1 : {__Real}, k2 : {__Real}, o : {_Complex, _Complex, _Complex}, maxIt_Integer?Positive, calD:(True|False), updateJMin:(True|False)]", (char *)"{id, {maxIt, calD // Boole, updateJMin // Boole}, k1, k2, Re[o] // N, Im[o] // N}", 7);
	if (_res) _res = _doevalstr( wslp, 72);
	if (_res) _res = _doevalstr( wslp, 73);
	if (_res) _res = _doevalstr( wslp, 74);
	if (_res) _res = _doevalstr( wslp, 75);
	if (_res) _res = _doevalstr( wslp, 76);
	if (_res) _res = WSPutSymbol( wslp, "End");
	if (_res) _res = WSFlush( wslp);
	return _res;
} /* WSInstall */


int WSDoCallPacket( WSLINK wslp)
{
	return _WSDoCallPacket( wslp, _tramps, 8);
} /* WSDoCallPacket */

/******************************* begin trailer ********************************/

#ifndef EVALSTRS_AS_BYTESTRINGS
#	define EVALSTRS_AS_BYTESTRINGS 1
#endif


#if CARDOF_EVALSTRS
static int  _doevalstr( WSLINK wslp, int n)
{
	long bytesleft, charsleft, bytesnow;
#if !EVALSTRS_AS_BYTESTRINGS
	long charsnow;
#endif
	char **s, **p;
	char *t;

	s = (char **)evalstrs;
	while( n-- > 0){
		if( *s == 0) break;
		while( *s++ != 0){}
	}
	if( *s == 0) return 0;
	bytesleft = 0;
	charsleft = 0;
	p = s;
	while( *p){
		t = *p; while( *t) ++t;
		bytesnow = t - *p;
		bytesleft += bytesnow;
		charsleft += bytesnow;
#if !EVALSTRS_AS_BYTESTRINGS
		t = *p;
		charsleft -= WSCharacterOffset( &t, t + bytesnow, bytesnow);
		/* assert( t == *p + bytesnow); */
#endif
		++p;
	}


	WSPutNext( wslp, WSTKSTR);
#if EVALSTRS_AS_BYTESTRINGS
	p = s;
	while( *p){
		t = *p; while( *t) ++t;
		bytesnow = t - *p;
		bytesleft -= bytesnow;
		WSPut8BitCharacters( wslp, bytesleft, (unsigned char*)*p, bytesnow);
		++p;
	}
#else
	WSPut7BitCount( wslp, charsleft, bytesleft);
	p = s;
	while( *p){
		t = *p; while( *t) ++t;
		bytesnow = t - *p;
		bytesleft -= bytesnow;
		t = *p;
		charsnow = bytesnow - WSCharacterOffset( &t, t + bytesnow, bytesnow);
		/* assert( t == *p + bytesnow); */
		charsleft -= charsnow;
		WSPut7BitCharacters(  wslp, charsleft, *p, bytesnow, charsnow);
		++p;
	}
#endif
	return WSError( wslp) == WSEOK;
}
#endif /* CARDOF_EVALSTRS */


static int  _definepattern( WSLINK wslp, char *patt, char *args, int func_n)
{
	WSPutFunction( wslp, "DefineExternal", (long)3);
	  WSPutString( wslp, patt);
	  WSPutString( wslp, args);
	  WSPutInteger( wslp, func_n);
	return !WSError(wslp);
} /* _definepattern */


int _WSDoCallPacket( WSLINK wslp, struct func functable[], int nfuncs)
{
	int len;
	int n, res = 0;
	struct func* funcp;

	if( ! WSGetInteger( wslp, &n) ||  n < 0 ||  n >= nfuncs) goto L0;
	funcp = &functable[n];

	if( funcp->f_nargs >= 0
	&& ( ! WSTestHead(wslp, "List", &len)
	     || ( !funcp->manual && (len != funcp->f_nargs))
	     || (  funcp->manual && (len <  funcp->f_nargs))
	   )
	) goto L0;

	stdlink = wslp;
	res = (*funcp->f_func)( wslp);

L0:	if( res == 0)
		res = WSClearError( wslp) && WSPutSymbol( wslp, "$Failed");
	return res && WSEndPacket( wslp) && WSNewPacket( wslp);
} /* _WSDoCallPacket */


wsapi_packet WSAnswer( WSLINK wslp)
{
	wsapi_packet pkt = 0;
	int waitResult;

	while( ! WSDone && ! WSError(wslp)
		&& (waitResult = WSWaitForLinkActivity(wslp),waitResult) &&
		waitResult == WSWAITSUCCESS && (pkt = WSNextPacket(wslp), pkt) &&
		pkt == CALLPKT)
	{
		WSAbort = 0;
		if(! WSDoCallPacket(wslp))
			pkt = 0;
	}
	WSAbort = 0;
	return pkt;
} /* WSAnswer */



/*
	Module[ { me = $ParentLink},
		$ParentLink = contents of RESUMEPKT;
		Message[ MessageName[$ParentLink, "notfe"], me];
		me]
*/


static int refuse_to_be_a_frontend( WSLINK wslp)
{
	int pkt;

	WSPutFunction( wslp, "EvaluatePacket", 1);
	  WSPutFunction( wslp, "Module", 2);
	    WSPutFunction( wslp, "List", 1);
		  WSPutFunction( wslp, "Set", 2);
		    WSPutSymbol( wslp, "me");
	        WSPutSymbol( wslp, "$ParentLink");
	  WSPutFunction( wslp, "CompoundExpression", 3);
	    WSPutFunction( wslp, "Set", 2);
	      WSPutSymbol( wslp, "$ParentLink");
	      WSTransferExpression( wslp, wslp);
	    WSPutFunction( wslp, "Message", 2);
	      WSPutFunction( wslp, "MessageName", 2);
	        WSPutSymbol( wslp, "$ParentLink");
	        WSPutString( wslp, "notfe");
	      WSPutSymbol( wslp, "me");
	    WSPutSymbol( wslp, "me");
	WSEndPacket( wslp);

	while( (pkt = WSNextPacket( wslp), pkt) && pkt != SUSPENDPKT)
		WSNewPacket( wslp);
	WSNewPacket( wslp);
	return WSError( wslp) == WSEOK;
}


int WSEvaluate( WSLINK wslp, char *s)
{
	if( WSAbort) return 0;
	return WSPutFunction( wslp, "EvaluatePacket", 1L)
		&& WSPutFunction( wslp, "ToExpression", 1L)
		&& WSPutString( wslp, s)
		&& WSEndPacket( wslp);
} /* WSEvaluate */


int WSEvaluateString( WSLINK wslp, char *s)
{
	int pkt;
	if( WSAbort) return 0;
	if( WSEvaluate( wslp, s)){
		while( (pkt = WSAnswer( wslp), pkt) && pkt != RETURNPKT)
			WSNewPacket( wslp);
		WSNewPacket( wslp);
	}
	return WSError( wslp) == WSEOK;
} /* WSEvaluateString */


WSMDEFN( void, WSDefaultHandler, ( WSLINK wslp, int message, int n))
{
	switch (message){
	case WSTerminateMessage:
		WSDone = 1;
	case WSInterruptMessage:
	case WSAbortMessage:
		WSAbort = 1;
	default:
		return;
	}
}


static int _WSMain( char **argv, char **argv_end, char *commandline)
{
	WSLINK wslp;
	int err;

	if( !stdenv)
		stdenv = WSInitialize( (WSEnvironmentParameter)0);

	if( stdenv == (WSEnvironment)0) goto R0;

	if( !stdhandler)
		stdhandler = (WSMessageHandlerObject)WSDefaultHandler;


	wslp = commandline
		? WSOpenString( stdenv, commandline, &err)
		: WSOpenArgcArgv( stdenv, (int)(argv_end - argv), argv, &err);
	if( wslp == (WSLINK)0){
		WSAlert( stdenv, WSErrorString( stdenv, err));
		goto R1;
	}

	if( stdyielder) WSSetYieldFunction( wslp, stdyielder);
	if( stdhandler) WSSetMessageHandler( wslp, stdhandler);

	if( WSInstall( wslp))
		while( WSAnswer( wslp) == RESUMEPKT){
			if( ! refuse_to_be_a_frontend( wslp)) break;
		}

	WSClose( wslp);
R1:	WSDeinitialize( stdenv);
	stdenv = (WSEnvironment)0;
R0:	return !WSDone;
} /* _WSMain */


int WSMainString( char *commandline)
{
	return _WSMain( (charpp_ct)0, (charpp_ct)0, commandline);
}

int WSMainArgv( char** argv, char** argv_end) /* note not FAR pointers */
{
	static char * far_argv[128];
	int count = 0;

	while(argv < argv_end)
		far_argv[count++] = *argv++;

	return _WSMain( far_argv, far_argv + count, (charp_ct)0);

}


int WSMain( int argc, char **argv)
{
 	return _WSMain( argv, argv + argc, (char *)0);
}
