/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "mlDispersionRelation.h"
#include <DispersionKit/DispersionKit.h>
#include <UtilityKit/UtilityKit.h>
#include <cmath>
#include <deque>
#include <exception>
#include <iostream>
#include <map>
#include <memory>
#include <string>
#include <utility>
#include <wstp.h>

// MARK: Bookkeeping
//
namespace {
// it can only be used in a manual return type
// right after the entry, WSNewPacket(...) is called
// after this call, message followed by Abort[] is put to the stream, and then the enclosing function is returned
#define errorAndAbort(erro) \
    _errorAndAbort(erro);   \
    return
void _errorAndAbort(std::string const &erro)
{
    WSClearError(stdlink), WSNewPacket(stdlink);              // clear out link stream for return
    printo(std::cout, "DispersionKit::erro : ", erro, ".\n"); // print to console
    WSPutFunction(stdlink, "CompoundExpression", int{ 2 });
    {
        // message
        WSPutFunction(stdlink, "Message", int{ 2 });
        {
            // message name
            WSPutFunction(stdlink, "MessageName", int{ 2 });
            {
                WSPutSymbol(stdlink, "DispersionKit");
                WSPutString(stdlink, "erro");
            }
            // erro
            WSPutString(stdlink, (erro + ".").c_str());
        }
        // abort
        WSPutFunction(stdlink, "Abort", int{ 0 });
    }
}

// issue waring message to mathematica using `WSEvaluateString'
// stdlink stream is not affected
void issueWarning(std::string const &warn)
{
    printo(std::cout, "DispersionKit::warn : ", warn, ".\n"); // print to console
    // compose message expression
    std::ostringstream os;
    printo(os, "Message[DispersionKit::warn, \"", warn, ".\"]");
    std::string const s = os.str();
    WSEvaluateString(stdlink, const_cast<char *>(s.c_str()));
}

// check whether WSAbort is set
// synchronous access
Utility::SpinLock *_sync_lock;
inline bool        userInterrupted()
{
    Utility::LockG<Utility::SpinLock> l(*_sync_lock);
    return WSAbort;
}

// table
using DispersionTable = std::map<std::string, std::unique_ptr<Disp::Dispersion>>;
DispersionTable *table;
} // namespace
void initDispersionRelation(int, const char **)
{
    _sync_lock = new Utility::SpinLock;
    table      = new DispersionTable;
}

// MARK: Trampolines
//
void wstpDCreateDispersionObject(double const c, int const jmin)
{
    std::unique_ptr<Disp::Dispersion> disp;
    try {
        // create dispersion object
        //
        disp.reset(new Disp::Dispersion(c)); // c should be a positive number

        // set jmin
        //
        if (jmin < 0) {
            std::ostringstream os;
            printo(os, __FUNCTION__, " - jmin is negative");
            errorAndAbort(os.str());
        } else {
            unsigned j = static_cast<unsigned>(jmin);
            if (j > disp->jmax()) {
                j = disp->jmax();
                std::ostringstream os;
                printo(os, __FUNCTION__, " - jmin > jmax; cliping to jmax");
                issueWarning(os.str());
            }
            disp->set_jmin(j);
        }

        // add distributions
        //
        int n_dists;
        if (WSTestHead(stdlink, "List", &n_dists)) {
            // loop over the elements
            //
            for (int i = 0; i < n_dists; ++i) {
                int n_params;
                if (WSTestHead(stdlink, "wstpDRingDistribution", &n_params)) {
                    if (6 != n_params) { // [Oc, op^2, th1, th2, vd, vr]
                        std::ostringstream os;
                        printo(os, __FUNCTION__, " - 6 arguments are expected for wstpDRingDistribution");
                        errorAndAbort(os.str());
                    }
                    double Oc, op2;
                    WSGetReal(stdlink, &Oc), WSGetReal(stdlink, &op2);
                    double th1, th2;
                    WSGetReal(stdlink, &th1), WSGetReal(stdlink, &th2);
                    double vd, vr;
                    WSGetReal(stdlink, &vd), WSGetReal(stdlink, &vr);
                    disp->addVDF<Disp::MaxwellianRingVDF>(Oc, std::sqrt(CX::Number{ op2 }), th1, th2, vd, vr);
                    continue;
                } else {
                    WSClearError(stdlink);
                }
                if (WSTestHead(stdlink, "wstpDMaxwellianDistribution", &n_params)) {
                    if (5 != n_params) { // [Oc, op^2, th1, th2, vd]
                        std::ostringstream os;
                        printo(os, __FUNCTION__, " - 5 arguments are expected for wstpDMaxwellianDistribution");
                        errorAndAbort(os.str());
                    }
                    double Oc, op2;
                    WSGetReal(stdlink, &Oc), WSGetReal(stdlink, &op2);
                    double th1, th2;
                    WSGetReal(stdlink, &th1), WSGetReal(stdlink, &th2);
                    double vd;
                    WSGetReal(stdlink, &vd);
                    disp->addVDF<Disp::BiMaxwellianVDF>(Oc, std::sqrt(CX::Number{ op2 }), th1, th2, vd);
                    continue;
                } else {
                    WSClearError(stdlink);
                }
                if (WSTestHead(stdlink, "wstpDProductKappaDistribution", &n_params)) {
                    if (7 != n_params) { // [Oc, op^2, Sequence @@ {k1, k2}, th1, th2, vd]
                        std::ostringstream os;
                        printo(os, __FUNCTION__, " - 6 arguments are expected for wstpDProductKappaDistribution");
                        errorAndAbort(os.str());
                    }
                    double Oc, op2;
                    WSGetReal(stdlink, &Oc), WSGetReal(stdlink, &op2);
                    std::pair<double, double> kappa;
                    WSGetReal(stdlink, &kappa.first);
                    WSGetReal(stdlink, &kappa.second);
                    double th1, th2;
                    WSGetReal(stdlink, &th1), WSGetReal(stdlink, &th2);
                    double vd;
                    WSGetReal(stdlink, &vd);
                    disp->addVDF<Disp::ProductBiLorentzianVDF>(Oc, std::sqrt(CX::Number{ op2 }), kappa, th1, th2, vd);
                    continue;
                } else {
                    WSClearError(stdlink);
                }
                if (WSTestHead(stdlink, "wstpDKappaDistribution", &n_params)) {
                    if (6 != n_params) { // [Oc, op^2, Sequence @@ {k}, th1, th2, vd]
                        std::ostringstream os;
                        printo(os, __FUNCTION__, " - 6 arguments are expected for wstpDProductKappaDistribution");
                        errorAndAbort(os.str());
                    }
                    double Oc, op2;
                    WSGetReal(stdlink, &Oc), WSGetReal(stdlink, &op2);
                    std::tuple<double> kappa;
                    WSGetReal(stdlink, &std::get<0>(kappa));
                    double th1, th2;
                    WSGetReal(stdlink, &th1), WSGetReal(stdlink, &th2);
                    double vd;
                    WSGetReal(stdlink, &vd);
                    disp->addVDF<Disp::BiLorentzianVDF>(Oc, std::sqrt(CX::Number{ op2 }), kappa, th1, th2, vd);
                    continue;
                } else {
                    WSClearError(stdlink);
                }

                // error
                std::ostringstream os;
                printo(os, __FUNCTION__, " - the ", i, "th element of the distribution list is not a predefined distribution type");
                errorAndAbort(os.str());
            }

            // consistency check
            //
            if (disp->vdfs().size() != static_cast<unsigned>(n_dists)) {
                std::ostringstream os;
                printo(os, __FUNCTION__, " - inconsistency between the number of distributions passed and the number of distributions actually created");
                errorAndAbort(os.str());
            }
        } else { // not a list of distributions
            std::ostringstream os;
            printo(os, __FUNCTION__, " - a list of distributions are expected");
            errorAndAbort(os.str());
        }
    } catch (std::exception &e) {
        std::ostringstream os;
        printo(os, __FUNCTION__, "::exception - ", e.what());
        errorAndAbort(os.str());
    }
    printo(std::cout, disp->description(), "\n"); // print to console of dispersion object description

    // tabulate dispersion object
    //
    std::string key;
    {
        std::ostringstream os;
        os.setf(os.hex, os.basefield);
        os.setf(os.showbase);
        printo(os, identifier(*disp));
        key = os.str();
    }
    (*table)[key] = std::move(disp);

    // return handle
    //
    WSClearError(stdlink), WSNewPacket(stdlink); // clear the stream for return
    WSPutFunction(stdlink, "wstpDDispersionObject", int{ 1 });
    {
        WSPutString(stdlink, key.c_str());
    }
}

void wstpDListAllDispersionObjects(void)
{
    DispersionTable const &table = *::table;
    WSClearError(stdlink), WSNewPacket(stdlink); // clear stream for return
    WSPutFunction(stdlink, "List", static_cast<int>(table.size()));
    for (auto const &kv : table) {
        WSPutFunction(stdlink, "wstpDDispersionObject", int{ 1 });
        {
            WSPutString(stdlink, kv.first.c_str());
        }
    }
}

void wstpDRemoveObject(const char *_key)
{
    std::string const key{ _key };
    DispersionTable  &table = *::table;
    if (!table.erase(key)) {
        std::ostringstream os;
        printo(os, __FUNCTION__, " - no object for wstpDDispersionObject[", key, "]");
        issueWarning(os.str());
    }
    WSClearError(stdlink), WSNewPacket(stdlink); // clear stream for return
    WSPutSymbol(stdlink, "Null");
}

void wstpDSetMinimumCyclotronOrder(const char *_key, int const jmin)
{
    std::string const               key{ _key };
    DispersionTable                &table = *::table;
    DispersionTable::iterator const it    = table.find(key);
    if (table.end() == it) { // no object for the key
        std::ostringstream os;
        printo(os, __FUNCTION__, " - no object for wstpDDispersionObject[", key, "]");
        errorAndAbort(os.str());
    }
    Disp::Dispersion &disp = *it->second;

    // set jmin
    //
    if (jmin < 0) { // jmin check
        std::ostringstream os;
        printo(os, __FUNCTION__, " - jmin is negative");
        errorAndAbort(os.str());
    }
    unsigned j = static_cast<unsigned>(jmin);
    if (j > disp.jmax()) {
        j = disp.jmax();
        std::ostringstream os;
        printo(os, __FUNCTION__, " - jmin > jmax; cliping to jmax");
        issueWarning(os.str());
    }
    disp.set_jmin(j);

    // return
    //
    WSClearError(stdlink), WSNewPacket(stdlink); // clear stream for return
    WSPutFunction(stdlink, "List", int{ 2 });
    {
        WSPutInteger(stdlink, static_cast<int>(disp.jmin()));
        WSPutInteger(stdlink, static_cast<int>(disp.jmax()));
    }
}

void wstpDMinimumCyclotronOrder(const char *_key)
{
    std::string const                     key{ _key };
    DispersionTable const                &table = *::table;
    DispersionTable::const_iterator const it    = table.find(key);
    if (table.end() == it) { // no object for the key
        std::ostringstream os;
        printo(os, __FUNCTION__, " - no object for wstpDDispersionObject[", key, "]");
        errorAndAbort(os.str());
    }
    Disp::Dispersion &disp = *it->second;
    WSClearError(stdlink), WSNewPacket(stdlink); // clear stream for return
    WSPutInteger(stdlink, static_cast<int>(disp.jmin()));
}

void wstpDDescription(const char *_key)
{
    std::string const                     key{ _key };
    DispersionTable const                &table = *::table;
    DispersionTable::const_iterator const it    = table.find(key);
    if (table.end() == it) { // no object for the key
        std::ostringstream os;
        printo(os, __FUNCTION__, " - no object for wstpDDispersionObject[", key, "]");
        errorAndAbort(os.str());
    }
    Disp::Dispersion &disp = *it->second;
    WSClearError(stdlink), WSNewPacket(stdlink); // clear stream for return
    WSPutString(stdlink, disp.description().c_str());
}

void wstpDDispersionMatrix(const char *_key, double const k1, double const k2, double const wr, double const wi)
{
    std::string const                     key{ _key };
    DispersionTable const                &table = *::table;
    DispersionTable::const_iterator const it    = table.find(key);
    if (table.end() == it) { // no object for the key
        std::ostringstream os;
        printo(os, __FUNCTION__, " - no object for wstpDDispersionObject[", key, "]");
        errorAndAbort(os.str());
    }
    Disp::Dispersion &disp = *it->second;
    // disp.clear_cache(); // clear cache

    // calculate D
    //
    CX::Matrix const D = disp.D({ k1, k2 }, { wr, wi }, [](unsigned const, CX::Matrix const &) -> bool {
        return !userInterrupted(); // in case calculation hang up
    });

    // return
    //
    WSClearError(stdlink), WSNewPacket(stdlink); // clear stream for return
    if (isfinite(D)) {
        WSPutFunction(stdlink, "List", static_cast<int>(D.size()));
        for (CX::Vector const &row : D) {
            WSPutFunction(stdlink, "List", static_cast<int>(row.size()));
            for (CX::Number const &cx : row) {
                WSPutFunction(stdlink, "Complex", int{ 2 });
                {
                    WSPutReal(stdlink, cx.real());
                    WSPutReal(stdlink, cx.imag());
                }
            }
        }
    } else {
        WSPutSymbol(stdlink, "Indeterminate");
    }
}

void wstpDFindRoot(const char *_key,
                   int *opts, long const n_opts,
                   double *k1s, long const n_k1s,
                   double *k2s, long const n_k2s,
                   double *wrs, long const n_wrs,
                   double *wis, long const n_wis)
{
    // handle
    //
    std::string const                     key{ _key };
    DispersionTable const                &table = *::table;
    DispersionTable::const_iterator const it    = table.find(key);
    if (table.end() == it) { // no object for the key
        std::ostringstream os;
        printo(os, __FUNCTION__, " - no object for wstpDDispersionObject[", key, "]");
        errorAndAbort(os.str());
    }
    Disp::Dispersion &disp = *it->second;

    // options
    //
    if (3 != n_opts) {
        std::ostringstream os;
        printo(os, __FUNCTION__, " - the length of the option list should be 3");
        errorAndAbort(os.str());
    } else if (opts[0] <= 0) {
        std::ostringstream os;
        printo(os, __FUNCTION__, " - the MaxIterations option should be a positive integer");
        errorAndAbort(os.str());
    }
    unsigned const max_iterations     = static_cast<unsigned>(opts[0]);
    int const      should_calculate_D = opts[1];
    int const      should_update_jmin = opts[2];

    // k list
    //
    if (n_k1s != n_k2s) {
        std::ostringstream os;
        printo(os, __FUNCTION__, " - inconsistent dimension of wave number lists");
        errorAndAbort(os.str());
    }
    std::deque<std::pair<double, double>> ks;
    for (long i = 0; i < n_k1s; ++i) {
        ks.push_back({ k1s[i], k2s[i] });
    }

    // omega list
    //
    if (3 != n_wrs || n_wrs != n_wis) {
        std::ostringstream os;
        printo(os, __FUNCTION__, " - the length of initial guesses for complex frequency should be 3");
        errorAndAbort(os.str());
    }
    std::deque<CX::Number> ws;
    for (long i = 0; i < n_wrs; ++i) {
        ws.push_back({ wrs[i], wis[i] });
    }

    // process
    //
    std::deque<CX::Matrix> Ds;
    try {
        // clear cache
        //
        disp.clear_cache();

        // dispersion step monitor
        //
        unsigned   jmin              = disp.jmin();
        auto const disp_step_monitor = [&jmin](unsigned const j, CX::Matrix const &) -> bool {
            jmin = j;
            return !userInterrupted();
        };

        // root finder
        //
        std::pair<double, double> k;
        bool                      is_disp_failed = false;
        Utility::MullerRootFinder<std::function<CX::Number(CX::Number)>>
            root([&disp, &k, &disp_step_monitor, &is_disp_failed](CX::Number const &o) -> CX::Number {
                CX::Matrix const o2D = disp.o2_x_D(k, o, disp_step_monitor);
                is_disp_failed       = !isfinite(o2D);
                return det(o2D);
            });
        root.max_iterations = max_iterations;
        root.reltol         = 1e-5;
        root.abstol         = 1e-10;

        // find roots
        //
        auto const root_step_monitor = [&jmin](unsigned const i, CX::Number const &o) -> bool {
            printo(std::cout, "i = ", i, ", o = ", o, ", jmin = ", jmin, "\n");
            return !userInterrupted();
        };
        for (unsigned long i = 0, n = ks.size(); i < n && !userInterrupted(); ++i) {
            k              = ks[i];
            is_disp_failed = false;
            // log current iteration
            printo(std::cout, "k1=", k.first, ", k2=", k.second, ", |k|=", std::sqrt(k.first * k.first + k.second * k.second), ", psi=", std::atan2(k.second, k.first) * 180. / M_PI, "\n");
            // find a root
            decltype(root)::optional_type const w_opt = root(ws.at(i), ws.at(i + 1), ws.at(i + 2), root_step_monitor);
            if (!w_opt) {                 // not found
                if (!userInterrupted()) { // exclude user interruption
                    std::ostringstream os;
                    if (is_disp_failed) { // error in D calculation
                        printo(os, __FUNCTION__, " - dispersion matrix calculation failed at j = ", jmin, " for k1 = ", k.first, " and k2 = ", k.second);
                    } else { // error in root finder
                        printo(os, __FUNCTION__, " - root finder failed at k1 = ", k.first, " and k2 = ", k.second);
                    }
                    issueWarning(os.str());
                }
                break;
            } else { // found
                // keep result
                ws.push_back(*w_opt);
                // update jmin
                if (should_update_jmin) {
                    if (jmin > disp.jmax()) {
                        std::ostringstream os;
                        printo(os, __FUNCTION__, " - updated jmin exceeds jmax; cliping to jmax");
                        issueWarning(os.str());
                        disp.set_jmin(disp.jmax());
                    } else {
                        disp.set_jmin(jmin);
                    }
                }
                // cal D
                if (should_calculate_D) {
                    Ds.push_back(disp.D(k, *w_opt, [](unsigned const, CX::Matrix const &) -> bool {
                        return true;
                    }));
                }
            }
        }
    } catch (std::exception &e) {
        std::ostringstream os;
        printo(os, __FUNCTION__, "::exception - ", e.what());
        errorAndAbort(os.str());
    }

    // post-process
    //
    ws.erase(ws.begin(), ws.begin() + 3); // get rid of first 3 initial guesses
    ks.resize(ws.size());                 // slash out wave numbers not covered
    if (should_calculate_D)
        Ds.resize(ws.size());

    // flagging
    //
    int flag = 0; // normal termination
    if (userInterrupted()) {
        flag = 2; // user interrupted
        std::ostringstream os;
        printo(os, __FUNCTION__, " - user interrupted");
        issueWarning(os.str()); // this will not be evaluated in the wolfram language because WSAbort is set; only console print
    } else if (ws.size() != static_cast<unsigned long>(n_k1s)) {
        flag = 1; // root finding failed at an intermediate wave number
    }

    // return
    // neither issueWarning(...) nor errorAndAbort(...) should be called from here
    //
    WSClearError(stdlink), WSNewPacket(stdlink); // move to the end of stream
    // list of rules {"flag"->(0|1|2), "\[Omega]"->{...}, "k1"->{...}, "k2"->{...}, "D"->{...}}
    WSPutFunction(stdlink, "List", int{ 4 } + int(should_calculate_D ? 1 : 0));
    {
        // flag rule
        WSPutFunction(stdlink, "Rule", int{ 2 });
        {
            WSPutString(stdlink, "flag");
            WSPutInteger(stdlink, flag);
        }
        // omega rule
        WSPutFunction(stdlink, "Rule", int{ 2 });
        {
            WSPutString(stdlink, "w");
            WSPutFunction(stdlink, "List", static_cast<int>(ws.size()));
            for (CX::Number const &w : ws) {
                WSPutFunction(stdlink, "Complex", int{ 2 });
                {
                    WSPutReal(stdlink, w.real());
                    WSPutReal(stdlink, w.imag());
                }
            }
        }
        // k1 rule
        WSPutFunction(stdlink, "Rule", int{ 2 });
        {
            WSPutString(stdlink, "k1");
            WSPutFunction(stdlink, "List", static_cast<int>(ks.size()));
            for (std::pair<double, double> const &k : ks) {
                WSPutReal(stdlink, k.first);
            }
        }
        // k2 rule
        WSPutFunction(stdlink, "Rule", int{ 2 });
        {
            WSPutString(stdlink, "k2");
            WSPutFunction(stdlink, "List", static_cast<int>(ks.size()));
            for (std::pair<double, double> const &k : ks) {
                WSPutReal(stdlink, k.second);
            }
        }
        // D rule
        if (should_calculate_D) { // should not check for emptiness of Ds
            WSPutFunction(stdlink, "Rule", int{ 2 });
            {
                WSPutString(stdlink, "D");
                WSPutFunction(stdlink, "List", static_cast<int>(Ds.size())); // D list
                for (CX::Matrix const &D : Ds) {
                    WSPutFunction(stdlink, "List", static_cast<int>(D.size())); // D matrix
                    for (CX::Vector const &row : D) {
                        WSPutFunction(stdlink, "List", static_cast<int>(row.size())); // D row
                        for (CX::Number const &cx : row) {
                            WSPutFunction(stdlink, "Complex", int{ 2 });
                            {
                                WSPutReal(stdlink, cx.real());
                                WSPutReal(stdlink, cx.imag());
                            }
                        }
                    }
                }
            } // Rule
        }     // if (!Ds.empty())
    }
}
