/*
 * Copyright (c) 2017-2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include <csignal>
#include <gsl/gsl_errno.h>
#include <iostream>
#include <wstp.h>

#include "mlDispersionRelation.h"

extern "C" WSEnvironment stdenv;

int main(int argc, char *argv[])
{
    // turn off gsl's error handling
    //
    gsl_set_error_handler_off();

    // dispersion solver initialization
    //
    initDispersionRelation(argc, const_cast<const char **>(argv));

    // call WSMain
    return [&] {
        if (!stdenv) {
            WSEnvironmentParameter const envP = WSNewParameters(WSREVISION, WSAPIREVISION);
            if (!envP) {
                std::cerr << "\u001b[31m" << *argv << "\u001b[0m :: Unable to create environment parameter object.\n";
                return 1;
            }

            // disable signal handling
            // this is to allow users to terminate the program using "^C"
            if (WSEOK != WSDoNotHandleSignalParameter(envP, SIGTERM))
                std::cerr << "\u001b[31m" << *argv << "\u001b[0m :: Failed to disable SIGTERM signal handling.\n";
            if (WSEOK != WSDoNotHandleSignalParameter(envP, SIGINT))
                std::cerr << "\u001b[31m" << *argv << "\u001b[0m :: Failed to disable SIGINT signal handling.\n";

            stdenv = WSInitialize(envP);
            WSReleaseParameters(envP);
            if (!stdenv) {
                std::cerr << "\u001b[31m" << *argv << "\u001b[0m :: Unable to create environment object.\n";
                return 1;
            }
        }

        return WSMain(argc, argv);
    }();
}
