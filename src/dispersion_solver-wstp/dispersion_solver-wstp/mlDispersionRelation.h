/*
 * Copyright (c) 2017, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

namespace {
template <class T>
inline long identifier(T const &a) noexcept
{
    union {
        long        address;
        void const *pointer;
    } u;
    u.pointer = &a;
    return u.address;
}
} // namespace

void initDispersionRelation(int argc, const char *argv[]);
