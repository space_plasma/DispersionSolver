(* ::Package:: *)

ClearAll[adaptiveSampling1D];


Options[adaptiveSampling1D]={(*MaxRecursion\[Rule]5,*)AccuracyGoal->4,PlotPoints->5,MaxIterations->10};
adaptiveSampling1D::pltpts="PlotPoints option should be greater than 1.";
adaptiveSampling1D::iter="Max iterations reached.";
adaptiveSampling1D::usage="adaptiveSampling1D[f,{x0,x1}] samples points between x0 and x1
with a varying abscissa density depending on the first and second order differences.";


(*Interface*)
adaptiveSampling1D[f_,{x0_?NumberQ,x1_?NumberQ},opts:OptionsPattern[]]:=With[{
xList=N[(x1-x0)Range[0,OptionValue[PlotPoints]]/OptionValue[PlotPoints]+x0]
},Module[{ptList={#,f[#]}&/@xList,scale,i},
scale=Chop[Abs[N[{x1-x0,Subtract@@Through[{Last,First}[Sort[Last/@ptList]]]}]]];
scale=1./If[0==Last[scale],{First[scale],1.},scale];
(*Append[Join@@Map[Most,adaptiveSampling1D[f,##,scale,OptionValue[MaxRecursion],opts]&@@@Partition[ptList,2,1]],Last[ptList]]*)
i=0;
NestWhile[Function[pts,Module[{ptFirst,ptRest},
If[++i==OptionValue[MaxIterations],Message[adaptiveSampling1D::iter]];
(*First three points in reverse order*)
ptFirst=Prepend[adaptiveSampling1D[f,Sequence@@Reverse[Take[pts,3]],scale,opts],First[pts]];
(*Rest in normal order*)
ptRest=Apply[Join,Prepend[adaptiveSampling1D[f,##,scale,opts],#2]&@@@Partition[pts,3,1]];
(*Splice*)
Join[ptFirst,ptRest,Take[pts,-1]]
]],ptList,Unequal[Length[#1],Length[#2]]&,2,OptionValue[MaxIterations]]
]]/;If[OptionValue[PlotPoints]>1,True,Message[adaptiveSampling1D::pltpts];Abort[]];


(*Terminator*)
adaptiveSampling1D[f_,p0:{_Real,_Real},p2:{_Real,_Real},{_Real,_Real},0,OptionsPattern[]]:={p0,p2};


(*Two-point recursive sampling*)
adaptiveSampling1D[f_,p0:{x0_,_Real},p2:{x2_,_Real},scale:{_Real,_Real},depth_Integer?Positive,
opts:OptionsPattern[adaptiveSampling1D]]:=With[{
reltol=Power[10.,-OptionValue[AccuracyGoal]],
x1=.5(x0+x2),
x12=.25(x0+x2)+.5x2,
x01=.25(x0+x2)+.5x0
},Module[{p1={x1,f[x1]},p12={x12,f[x12]},p01={x01,f[x01]},A,B,\[DoubleStruckCapitalC],AB,BC,AC},
A=(p1-p0)scale;
B=(p2-p1)scale;
\[DoubleStruckCapitalC]=(p12-p01)scale;
AB=Norm[A]Norm[B];
BC=Norm[B]Norm[\[DoubleStruckCapitalC]];
AC=Norm[A]Norm[\[DoubleStruckCapitalC]];
If[Abs[AB-A.B]<reltol&&Abs[BC-B.\[DoubleStruckCapitalC]]<reltol&&Abs[AC-A.\[DoubleStruckCapitalC]]<reltol,
{p0,p1,p2},
Join[adaptiveSampling1D[f,p0,p1,scale,depth-1,opts],Rest[adaptiveSampling1D[f,p1,p2,scale,depth-1,opts]]],
Throw[{p0,p1,p2}]]
]];


(*Three-point one-pass sampling*)
adaptiveSampling1D[f_,p0:{x0_,_Real},p1:{x1_,_Real},p2:{x2_,_Real},scale:{_Real,_Real},
OptionsPattern[adaptiveSampling1D]]:=With[{
reltol=Power[10.,-OptionValue[AccuracyGoal]],
x01=.5(x0+x1),
x12=.5(x1+x2)
},Module[{p01={x01,f[x01]},p12={x12,f[x12]},A,B,\[DoubleStruckCapitalC],\[DoubleStruckCapitalD],AB,CD},
A=(p1-p0)scale;
B=(p2-p1)scale;
\[DoubleStruckCapitalC]=A+B;
\[DoubleStruckCapitalD]=(p12-p01)scale;
AB=Norm[A]Norm[B];
CD=Norm[\[DoubleStruckCapitalC]]Norm[\[DoubleStruckCapitalD]];
If[Abs[AB-A.B]<reltol&&Abs[CD-\[DoubleStruckCapitalC].\[DoubleStruckCapitalD]]<reltol,{},{p12}](*return halved point at the second interval*)
]];



