(* ::Package:: *)

(* ::Input:: *)
(*Clear["`*"];*)


(* ::Section:: *)
(*Muller's Method*)


ClearAll[findRootMuller];
findRootMuller::conv="Not converged after `1` iterations.";
findRootMuller::div="Division by 0.";
Options[findRootMuller]={StepMonitor->(Null&),MaxIterations->10,AccuracyGoal->5};
findRootMuller[f_,xL:{xL0_?NumberQ,xL1_?NumberQ,xL2_?NumberQ},OptionsPattern[]]:=With[{
reltol=Power[10,-OptionValue[AccuracyGoal]],
maxIt=OptionValue[MaxIterations],
stepMonitor=OptionValue[StepMonitor]
},Module[{
x0,x1,x2,
h1,h2,\[Delta]1,\[Delta]2,d,b,D,E,h,p,i,abs
},
For[{x0,x1,x2}=N[xL];h=1;i=0;p=Indeterminate,And[Abs[h]>reltol,i<maxIt],++i,
h1=x1-x0;
h2=x2-x1;
abs=Norm[{x0,x1,x2}];
If[Chop[h1/abs]==0||Chop[h2/abs]==0||Chop[(h2+h1)/abs]==0,Message[findRootMuller::div];Throw[{p,i}]];
\[Delta]1=(f[x1]-f[x0])/h1;
\[Delta]2=(f[x2]-f[x1])/h2;
d=(\[Delta]2-\[Delta]1)/(h2+h1);
b=\[Delta]2+h2*d;
D=b^2-4f[x2]d;
If[Chop[D]==0,Message[findRootMuller::div];Throw[{p,i}]];
D=Sqrt[D];
E=If[Abs[b-D]<Abs[b+D],b+D,b-D];
If[Chop[E]==0,Message[findRootMuller::div];Throw[{p,i}]];
h=-2f[x2]/E;
p=x2+h;
stepMonitor[{i,p}];
x0=x1;
x1=x2;
x2=p
];
If[i>=maxIt,Message[findRootMuller::conv,i];Throw[{p,i}]];
p
]];


ClearAll[findRootNewton];
findRootNewton::conv="Not converged after `1` iterations.";
findRootNewton::nan="Not a number.";
findRootNewton::usage="findRootNewton[f[x]/f'[x],\!\(\*SubscriptBox[\(x\), \(0\)]\)] searches root of f[x] using the initial guess \!\(\*SubscriptBox[\(x\), \(0\)]\).
findRootNewton[f[x]/f'[x],\!\(\*SubscriptBox[\(x\), \(0\)]\),multiplicity] uses multiplicity\[GreaterEqual]1.";
Options[findRootNewton]={StepMonitor->(Null&),MaxIterations->10,AccuracyGoal->5};
findRootNewton[fOdf_,xIn_Real,multiplicity:(_Integer?Positive):1,OptionsPattern[]]:=With[{
reltol=Power[10,-OptionValue[AccuracyGoal]],
maxIt=OptionValue[MaxIterations],
stepMonitor=OptionValue[StepMonitor]
},Module[{i,x0,x1,h},
For[x0=x1=xIn;i=0,And[StepMonitor[i,x1];i<maxIt],++i,
x1=x0-multiplicity*fOdf[x0];
h=x1-x0;
Which[
Or[!NumberQ[h],SameQ[Complex,Head[h]]],(Message[findRootNewton::nan];Abort[]),
Abs[h]<Times[reltol,Max[Abs[{x0,x1}]]],Return[x1]
];
x0=x1
];
Message[findRootNewton::conv,i];Throw[{x1,i}]
]
]


(* ::Input:: *)
(*?findRootNewton*)


(* ::Input:: *)
(*findRootNewton[Function[x,((-3+x) (-1+x))/(2 (-2+x))],3.1,1]*)
