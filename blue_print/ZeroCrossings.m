(* ::Package:: *)

ClearAll[zeroCrossings];
zeroCrossings::cmplx="`1` contains complex number(s).";
zeroCrossings::usage="zeroCrossings[\*StyleBox[l,Italic]] returns a list of a pair of indices bracketing the zero-crossings given a list of numbers \*StyleBox[l,Italic] (not including complex).";


zeroCrossings[l:{__?NumberQ}]:=Replace[
SparseArray[#]["AdjacencyLists"]&,SApos_Function:>With[{c=SApos[l]},If[Length[c]<=1,{},{c[[#]],c[[#+1]]}\[Transpose]&@SApos@Differences@Sign@l[[c]]]]
]/;If[!FreeQ[l,_Complex],CompoundExpression[Message[zeroCrossings::cmplx,l],False],True]



