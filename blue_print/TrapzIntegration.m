(* ::Package:: *)

ClearAll[trapzd];
Options[trapzd]={AccuracyGoal->MachinePrecision/2};
trapzd::iter="Solution is not found after `1` iterations.";
trapzd[f_,ab:{a_?NumberQ,b_?NumberQ}]:=With[{s=-.5(Subtract@@ab)Total[f/@ab]},{s,List[f,ab,s,1]}];
trapzd[f_,ab:{a_,b_},sold_,n_]:=With[{dx=(b-a)/n},
With[{snew=.5(sold+dx Total[f/@(a+Range[.5,n]dx)]),nnew=2n},{snew,List[f,ab,snew,nnew]}]
];


Clear[qtrap]
qtrap[f_,ab:{_?NumberQ,_?NumberQ},nMinimumIteration:(_Integer?Positive):4,opts:OptionsPattern[trapzd]
]:=Module[{it=0,eps,s,n,maxIt=Log2[$IterationLimit]},
eps=N[10^(-OptionValue[AccuracyGoal])];
{s,n}=Through[{First,Last}[
NestWhile[(++it;trapzd@@Last[#])&,
trapzd[f,ab],And[If[it>maxIt,(Message[trapzd::iter,it];False),True],
Or@@Map[Norm[First[#]]>eps&,{##}],eps Norm[First[#1]]<Norm[Subtract@@(First/@{##})]]&,
{nMinimumIteration+1,2}]
]];
n=Last[n];
s
]


Clear[qtrapCompile]
qtrapCompile[f_,nMinimumIteration:(_Integer?(#>1&)):4,opts:OptionsPattern[trapzd]]:=With[{
eps=N[10^(-OptionValue[AccuracyGoal])],
niter=$IterationLimit
},
Compile[{{ab,_Real,1}}(*Function[{ab}*),Module[{i,n,s,sold,a,b,dx},
{a,b}=ab;
For[i=0;n=1;sold=s=-.5(a-b)Total[f/@ab],
Or[i<nMinimumIteration,And[i<niter,Abs[s]+Abs[sold]>eps,eps Abs[sold]<Abs[s-sold]]],
i++;n*=2;sold=s,
dx=(b-a)/n;
s=.5(s+dx Total[f/@(a+Range[.5,n]dx)]);
];
If[i>=niter,Message[trapzd::iter,i]];
s(*{s,n}*)
],CompilationOptions->{"InlineExternalDefinitions"->True,"InlineCompiledFunctions"->True}]
]


ClearAll[qsimp]
qsimp[f_,ab:{_?NumberQ,_?NumberQ},nMinimumIteration:(_Integer?Positive):4,opts:OptionsPattern[trapzd]
]:=Module[{it=0,eps,st,s,n,maxIt=Log2[$IterationLimit]},
eps=N[10^(-OptionValue[AccuracyGoal])];
{s,n}=Through[{First,Last}[
NestWhile[(++it;st=trapzd@@Last[#];{(4First[st]-#[[2]])/3,First[st],Last[st]})&,
{0,Sequence@@trapzd[f,ab]},And[If[it>maxIt,(Message[trapzd::iter,it];False),True],
Or@@Map[Norm[First[#]]>eps&,{##}],eps Norm[First[#1]]<Norm[Subtract@@(First/@{##})]]&,
{nMinimumIteration+1,2}]
]];
n=Last[n];
s
]


ClearAll[qsimpCompile]
qsimpCompile[f_,nMinimumIteration:(_Integer?(#>1&)):4,opts:OptionsPattern[trapzd]]:=With[{
eps=N[10^(-OptionValue[AccuracyGoal])],
niter=$IterationLimit
},
Compile[{{ab,_Real,1}}(*Function[{ab}*),Module[{i,n,s,sold,st,stold,a,b,dx},
{a,b}=ab;
For[i=0;n=1;stold=st=-.5(a-b)Total[f/@ab];sold=s=0.st,
Or[i<nMinimumIteration,And[i<niter,Abs[s]+Abs[sold]>eps,eps Abs[sold]<Abs[s-sold]]],
i++;n*=2;stold=st;sold=s,
dx=(b-a)/n;
st=.5(st+dx Total[f/@(a+Range[.5,n]dx)]);
s=(4.st-stold)/3.;
];
If[i>=niter,Message[trapzd::iter,i]];
s(*{s,n}*)
],CompilationOptions->{"InlineExternalDefinitions"->True,"InlineCompiledFunctions"->True}]
]



