(* ::Package:: *)

Clear[GaussLegendreCoefficients];
GaussLegendreCoefficients[n_Integer?Positive]:=With[{eps=1.*^-14,maxIt=10},
Module[{i,m,z,pp,it,p1,p2,j,p3,z1,x,w},
w=x=Table[0.,{n}];
For[i=0;m=Quotient[n+1,2],i<m,++i,
z=Cos[Pi (i+.75)/(n+.5)];
pp=0;
For[it=0,it<maxIt,++it,
p1=1.;
p2=0.;
For[j=0,j<n,++j,
p3=p2;
p2=p1;
p1=((2j+1)z*p2-j*p3)/(j+1);
];
pp=n (z*p1-p2)/(z^2-1);
z1=z;
z=z1-p1/pp;
If[Abs[z-z1]<eps,Break[]]
];
If[it>=maxIt,Throw["root finding failed."]];
x[[i+1]]=-z;
x[[(n-1-i)+1]]=z;
w[[(n-1-i)+1]]=w[[i+1]]=2./((1-z^2)pp^2);
];
{x,w}
]
]


Clear[GaussLegendreIntegrator];
GaussLegendreIntegrator[n_Integer?Positive]:=With[{t=First@GaussLegendreCoefficients[n],w=Last@GaussLegendreCoefficients[n]},
Function[{f,a,b},Module[{xr=.5(b-a)},Total[f[(t+1)xr+a]w]xr],{Listable}]
]



