(* ::Package:: *)

(* ::Input:: *)
(*Clear["`*"];*)


(* ::Section:: *)
(*Cubic Spline Interpolation*)


(* ::Text:: *)
(*Cubic spline interpolation with complex numbers*)
(**)
(*Let Subscript[S, j][t] the jth spline cubic polynomial*)
(*Subscript[S, j][t]=Subscript[c, 0]+Subscript[c, 1]t+Subscript[c, 2] t^2+Subscript[c, 3] t^3, where t=(x-Subscript[x, j])/(Subscript[x, j+1]-Subscript[x, j])=(x-Subscript[x, j])/Subscript[\[CapitalDelta], j].*)
(*Subscript[S, j]'[t]=Subscript[c, 1]+2Subscript[c, 2]t+3Subscript[c, 3] t^2;*)
(*Subscript[S, j]''[t]=2Subscript[c, 2]+6Subscript[c, 3]t; and*)
(*Subscript[S, j]'''[t]=6Subscript[c, 3].*)
(*Note that Subscript[S, j], Subscript[S, j]' and Subscript[S, j]'' are continuous across boundaries, but Subscript[S, j]''' is not.*)
(**)
(*Say we want to approximate f[z] and \!\( *)
(*\*SubscriptBox[\(\[PartialD]\), \(x\)]\(f[z]\)\) at a complex abscissa value z=x+I y.*)
(*Then we define the imaginary part of the normalized variable u=y/Subscript[\[CapitalDelta], j] so that (z-Subscript[x, j])/Subscript[\[CapitalDelta], j]=t+I u.*)
(*\[Zeta]=t+I u;*)
(*\[Zeta]^2=t^2-u^2+I 2t u; and*)
(*\[Zeta]^3=t^3-3t u^2+I(3t^2 u-u^3).*)
(**)
(*\[GothicCapitalR] f[z]=\[GothicCapitalR] Subscript[S, j][\[Zeta]]*)
(*=Subscript[c, 0]+Subscript[c, 1]t+Subscript[c, 2](t^2-u^2)+Subscript[c, 3](t^3-3t u^2)*)
(*=Subscript[c, 0]+Subscript[c, 1]t+Subscript[c, 2] t^2+Subscript[c, 3] t^3-Subscript[c, 2] u^2-3Subscript[c, 3]t u^2*)
(*=Subscript[S, j][t]-1/2 Subscript[S, j]''[t]u^2;*)
(*\[GothicCapitalI] f[z]=\[GothicCapitalI] Subscript[S, j][\[Zeta]]*)
(*=Subscript[c, 1]u+2Subscript[c, 2]t u+Subscript[c, 3](3t^2 u-u^3)*)
(*=Subscript[c, 1]u+2Subscript[c, 2]t u+3Subscript[c, 3] t^2 u-Subscript[c, 3] u^3*)
(*=Subscript[S, j]'[t]u-Subscript[c, 3] u^3;*)
(**)
(*Subscript[\[CapitalDelta], j]\[GothicCapitalR] \!\( *)
(*\*SubscriptBox[\(\[PartialD]\), \(x\)]\(f[z]\)\)=\[GothicCapitalR] Subscript[S, j]'[\[Zeta]]*)
(*=Subscript[c, 1]+2Subscript[c, 2]t+3Subscript[c, 3](t^2-u^2)*)
(*=Subscript[c, 1]+2Subscript[c, 2]t+3Subscript[c, 3] t^2-3Subscript[c, 3] u^2*)
(*=Subscript[S, j]'[t]-3Subscript[c, 3] u^2;*)
(*Subscript[\[CapitalDelta], j]\[GothicCapitalI] \!\( *)
(*\*SubscriptBox[\(\[PartialD]\), \(x\)]\(f[z]\)\)=\[GothicCapitalI] Subscript[S, j]'[\[Zeta]]*)
(*=2Subscript[c, 2]u+6Subscript[c, 3]t u*)
(*=Subscript[S, j]''[t]u;*)
(**)
(*Therefore, \[GothicCapitalR] f[z] and \[GothicCapitalI] \!\( *)
(*\*SubscriptBox[\(\[PartialD]\), \(x\)]\(f[z]\)\) are continuous, but \[GothicCapitalI] f[z] and \[GothicCapitalR] \!\( *)
(*\*SubscriptBox[\(\[PartialD]\), \(x\)]\(f[z]\)\) are not.*)


(* ::Input:: *)
(*Module[{A,B,C,D},*)
(*(*A=(x1-x)/\[Delta]x;*)*)
(*B=(xmx0)/\[Delta]x;*)
(*A=1-B;*)
(*C=1/6(A^3-A)\[Delta]x^2;*)
(*D=1/6(B^3-B)\[Delta]x^2;*)
(*y=A y0+B y1+C ypp0+D ypp1/.{\[Delta]x->x1-x0,\[Delta]y->y1-y0};*)
(*yp=\[Delta]y/\[Delta]x-(3A^2-1)/6\[Delta]x ypp0+(3B^2-1)/6\[Delta]x ypp1/.{\[Delta]x->x1-x0,\[Delta]y->y1-y0};*)
(*ypp=A ypp0+B ypp1/.{\[Delta]x->x1-x0,\[Delta]y->y1-y0};*)
(*]*)


(* ::Input:: *)
(*CoefficientList[y,xmx0]//Simplify*)


(* ::Input:: *)
(*CoefficientList[yp,xmx0]//Simplify*)


(* ::Input:: *)
(*CoefficientList[ypp,xmx0]//Simplify*)


(*M.y''=b*)
Clear[solve2ndD];
solve2ndD[pts:{{_Real,_Real}..}]/;Length[pts]>=3:=Module[{n=Length[pts],b,xj,m},
b=Subtract[
(Divide@@@(Rest[#]-Most[#]))&@Rest[Reverse/@pts],
(Divide@@@(Rest[#]-Most[#]))&@Most[Reverse/@pts]
];
xj=First/@pts;
m=SparseArray[Table[{i,i-1},{i,2,n-2}]->Differences[xj[[2;;-2]]]/6,{1,1}n-2];
m=SparseArray[Table[{i,i},{i,1,n-2}]->(Drop[xj,2]-Drop[xj,-2])/3]+m+Transpose[m];
Flatten[{0.,LinearSolve[m,b],0.}]
]


(* ::Input:: *)
(*solve2ndD@Table[{i,i^2}//N,{i,{0,1,2,3}}]*)


Clear[cubicSplineCoefs];
cubicSplineCoefs[pts:{{_Real,_Real}..}]/;Length[pts]>=3:=With[{yppj=solve2ndD[pts]},
Module[{xj,yj,coefs,dxj},
{xj,yj}=Transpose[pts];
coefs=MapThread[Function[{x0,x1,y0,y1,ypp0,ypp1},
(*{y0,(y0-y1)/(x0-x1)+((x0-x1)(2 ypp0+ypp1))/6,ypp0/2,(ypp0-ypp1)/(6(x0-x1))}*)
{y0,-(y0-y1)-((x0-x1)^2 (2 ypp0+ypp1))/6,(x0-x1)^2 ypp0/2,-(((x0-x1)^2 (ypp0-ypp1))/6)}
],{Most[xj],Rest[xj],Most[yj],Rest[yj],Most[yppj],Rest[yppj]}];
dxj=Differences[xj];
If[And@@Thread[Equal[0,Chop[Differences[dxj]/Mean[dxj]]]],
(*regular spacing*)
{{First[xj],Last[xj],xj[[2]]-xj[[1]]},coefs},
(*irregular spacing*)
{Partition[xj,2,1],coefs}]
]
]


(* ::Input:: *)
(*cubicSplineCoefs@Table[{i,i^2}//N,{i,{0,1.0001,2}}]*)


(* ::Input:: *)
(*FromCoefficientRules[Thread[Rule[List/@Range[0,3],{0.`,0.5`,0.`,0.5`}]],x]*)


(* ::Input:: *)
(*Plot[{0.` +0.5` x+0.5` x^3,x^2},{x,0,1.}]*)


Clear[cubicSplineInterpolator];
(*Irregular spacing*)
cubicSplineInterpolator[xlims:{{_Real,_Real}..},coefs:{{_Real,_Real,_Real,_Real}..}]:=Module[{cond},
Block[{x,func,pw,f,df},
cond=MapThread[And,{Less@@@Thread[{First/@xlims,Re@x}],LessEqual@@@Thread[{Re@x,Last/@xlims}]}];
f=func[x,pw[MapThread[
{FromCoefficientRules[Thread[Rule[List/@Range[0,3],#]],x]/.{x->(x-First[#3])/(Last[#3]-First[#3])},#2}&,
{coefs,cond,xlims}],0]]/.{func->Function,pw->Piecewise};
df=func[x,pw[MapThread[
{FromCoefficientRules[Thread[Rule[List/@Range[0,2],Range[3]#]],x]/(-Subtract@@#3)/.{x->(x-First[#3])/(Last[#3]-First[#3])},#2}&,
{Rest/@coefs,cond,xlims}],0]]/.{func->Function,pw->Piecewise};
{f,df}
]
]/;Length[xlims]>=2&&Length[xlims]==Length[coefs]
(*Regular spacing*)
cubicSplineInterpolator[{x0_Real,xn_Real,dx_Real?Positive},coefs:{{_Real,_Real,_Real,_Real}..}/;Length[coefs]>=2]:=With[{
dCoefs=Times[{0.,1.,2.,3.},#]&/@coefs,ddCoefs=Times[{0.,0.,2.,6.},#]&/@coefs
},Module[{f,df},
(*f(x)*)
f=Function[z,Module[{S=coefs,Sp=dCoefs,Spp=ddCoefs,t=0.,t2,u=0.,u2,n=0,i=0,result=0.I},
u=Im[z]/dx;
u2=u*u;
t=Re[z];(*t is actually x*)
(*Locate spline*)
If[Not[x0<=t<=xn],Return[result]];
n=Length[S];
t=(t-x0)/dx;(*now t is normalized by dx*)
i=Floor[t];
i=If[i==n,i,i+1];
t=t-(i-1);(*now t scaled between 0 and 1*)
t2=t*t;
(*Interpolate real part*)
result+=Total[S[[i]]{1.,t,t2,t2*t}]-.5Total[Spp[[i]]{0.,0.,1.,t}]u2;
(*Interpolate imaginary part*)
result+=I(Total[Sp[[i]]{0.,1.,t,t2}]u);
result-=I*If[i<n,
(*linear interpolation*)
Last[S[[i]]](1.-t)+Last[S[[i+1]]]t,
(*linear extrapolation*)
Last[S[[n-1]]]t+Last[S[[n]]](t+1.)
]u2*u;
result
],Listable];
f=Compile[{{z,_Complex}},Module[{},
f[z]
],RuntimeAttributes->{Listable},Parallelization->True,CompilationOptions->{"InlineExternalDefinitions"->True}];
(*df(x)*)
df=Function[z,Module[{Sp=dCoefs,Spp=ddCoefs,t=0.,t2,u=0.,u2,n=0,i=0,result=0.I},
u=Im[z]/dx;
u2=u*u;
t=Re[z];(*t is actually x*)
(*Locate spline*)
If[Not[x0<=t<=xn],Return[result]];
n=Length[Sp];
t=(t-x0)/dx;(*now t is normalized by dx*)
i=Floor[t];
i=If[i==n,i,i+1];
t=t-(i-1);(*now t scaled between 0 and 1*)
t2=t*t;
(*Interpolate real part*)
result+=Total[Sp[[i]]{0.,1.,t,t2}];
result-=If[i<n,
(*linear interpolation*)
Last[Sp[[i]]](1.-t)+Last[Sp[[i+1]]]t,
(*linear extrapolation*)
Last[Sp[[n-1]]]t+Last[Sp[[n]]](t+1.)
]u2;
(*Interpolate imaginary part*)
result+=I(Total[Spp[[i]]{0.,0.,1.,t}]u);
result/dx
],Listable];
df=Compile[{{z,_Complex}},Module[{},
df[z]
],RuntimeAttributes->{Listable},Parallelization->True,CompilationOptions->{"InlineExternalDefinitions"->True}];
(*Return*)
{f,df}
]]


(* ::Input:: *)
(*With[{f0=#^2&},*)
(*Module[{f1,df1},*)
(*{f1,df1}=cubicSplineInterpolator@@cubicSplineCoefs[Table[{i,f0[i]}//N,{i,{0,.51,1}}]];*)
(*(*f1[.51]//Throw;*)*)
(*{Plot[{(*f0[x],*)f1[x]},{x,0,1}],Plot[{2x,df1[x]},{x,0,1}]}*)
(*]*)
(*]*)


(* ::Input:: *)
(*With[{f0=#^2&},*)
(*Module[{f1,df1},*)
(*{f1,df1}=cubicSplineInterpolator@@cubicSplineCoefs[Table[{i/10,f0[i/10]}//N,{i,0,10}]];*)
(*(*f1[2.1]//Throw;*)*)
(*{Plot[{f0[x],f1[x]//Re},{x,0,1}],Plot[{2x,df1[x]//Re},{x,0,1}]}*)
(*]*)
(*]*)


(* ::Input:: *)
(*With[{f0=#^2&},*)
(*Module[{f1,df1},*)
(*{f1,df1}=cubicSplineInterpolator@@cubicSplineCoefs[Table[{i/10,f0[i/10]}//N,{i,0,10}]];*)
(*AbsoluteTiming[Through[{f1,df1}[RandomReal[{0,1},1000]]]]//First*)
(*]*)
(*]*)


Clear[cubicSplineIntegration];
cubicSplineIntegration[xlims:{{_Real,_Real}..},coefs:{{_Real,_Real,_Real,_Real}..}]:=Total[
MapThread[Function[{xlim,coef},
Plus@@((-Subtract@@xlim)Divide[coef,Range[Length[coef]]])
],{xlims,coefs}]
]/;Length[xlims]>=2&&Length[xlims]==Length[coefs]
cubicSplineIntegration[{x0_Real,xn_Real,dx_Real?Positive},coefs:{{_Real,_Real,_Real,_Real}..}]:=dx Total[
Map[Function[c,
Plus@@Divide[c,Range[Length[c]]]
],coefs]
]/;Length[coefs]>=2


(* ::Input:: *)
(*cubicSplineIntegration@@cubicSplineCoefs[{#,#^2}&/@Range[0,1,.01]]*)


(* ::Input:: *)
(*With[{f0=Exp[-#^2]/Sqrt[\[Pi]]&,xj=Range[-5,5,.1]//N,nx=500},*)
(*Module[{coefs,f1,x},*)
(*coefs=cubicSplineCoefs[Thread[{xj,f0[xj]}]];*)
(*f1=cubicSplineInterpolator[Sequence@@coefs]//First;*)
(*x=Rest@Range[0,nx](-Subtract@@xj[[{1,-1}]])/nx+xj[[1]];*)
(*x+=1I;*)
(*Print[Row[{"integral = ",cubicSplineIntegration[Sequence@@coefs]}]];*)
(*{ListPlot[Re@{f0/@x,f1/@x},DataRange->xj[[{1,-1}]],Joined->True,PlotRange->All,PlotStyle->Thread[Directive[{Black,Red}(*,{Dashing[{}],Dashed}*)]],ImageSize->Medium],ListPlot[Im@{f0/@x,f1/@x},DataRange->xj[[{1,-1}]],Joined->True,PlotRange->All,PlotStyle->Thread[Directive[{Black,Red}(*,{Dashing[{}],Dashed}*)]],ImageSize->Medium]}*)
(*]*)
(*]*)


(* ::Input:: *)
(*With[{f0=Exp[-#^2]&,df0=-2# Exp[-#^2]&,xj=Range[-5,5,.1]//N,nx=500},*)
(*Module[{coefs,f1,x},*)
(*coefs=cubicSplineCoefs[Thread[{xj,f0[xj]}]];*)
(*f1=cubicSplineInterpolator[Sequence@@coefs]//Last;*)
(*x=Rest@Range[0,nx](-Subtract@@xj[[{1,-1}]])/nx+xj[[1]];*)
(*x+=1I;*)
(*{ListPlot[Re@{df0/@x,f1/@x},DataRange->xj[[{1,-1}]],Joined->True,PlotRange->All,PlotStyle->Thread[Directive[{Black,Red}(*,{Dashing[{}],Dashed}*)]],ImageSize->Medium],ListPlot[Im@{df0/@x,f1/@x},DataRange->xj[[{1,-1}]],Joined->True,PlotRange->All,PlotStyle->Thread[Directive[{Black,Red}(*,{Dashing[{}],Dashed}*)]],ImageSize->Medium]}*)
(*]*)
(*]*)


(* ::Input:: *)
(*With[{f0=Exp[-#^2]/Sqrt[\[Pi]]&,xj=Sort[RandomReal[{-5,5},101]],nx=500},*)
(*Module[{coefs,f1,x},*)
(*coefs=cubicSplineCoefs[Thread[{xj,f0[xj]}]];*)
(*f1=cubicSplineInterpolator[Sequence@@coefs]//First;*)
(*x=Rest@Range[0,nx](-Subtract@@xj[[{1,-1}]])/nx+xj[[1]];*)
(*Print[Row[{"integral = ",cubicSplineIntegration[Sequence@@coefs]}]];*)
(*{ListPlot[{f0/@x,f1/@x},DataRange->xj[[{1,-1}]],Joined->True,PlotStyle->Thread[Directive[{Black,Red},{Dashing[{}],Dashed}]],ImageSize->Medium],ListPlot[Subtract@@{f0/@x,f1/@x},Joined->True,PlotStyle->Thread[Directive[{Black,Red},{Dashing[{}],Dashed}]],ImageSize->Medium,PlotRange->All]}*)
(*]*)
(*]*)


(* ::Section:: *)
(*Monotone Cubic Interpolation*)


Clear[monotoneCubicCoefs];
monotoneCubicCoefs[pts:{{_Real,_Real}..}]/;Length[pts]>=2:=With[{n=Length[pts]},
Module[{xj,yj,dxj,dyj,mj,denom,c1,c2,c3,coef},
{xj,yj}=Transpose[pts];
(*difference and slope*)
dxj=Differences[xj];
dyj=Differences[yj];
mj=dyj/dxj;
(*1st-order*)
c1=Chop[3*Most[mj]Rest[mj](Most[dxj]+Rest[dxj])]/._?NonPositive->0;
denom=Most[dxj]Most[mj]+Rest[dxj]Rest[mj]+(Most[dxj]+Rest[dxj])(Most[mj]+Rest[mj]);
denom=ReplacePart[denom,Position[c1,0]->1];
c1=Flatten[{First[mj],c1/denom,Last[mj]}];
(*2nd- and 3rd-orders*)
If[False,
CompoundExpression[(*non-normalized; t=(x-x_j)*)
c2=(3mj-2Most[c1]-Rest[c1])/dxj,
c3=(Most[c1]+Rest[c1]-2mj)/Power[dxj,2],
c1=Most[c1]],
CompoundExpression[(*normalized; t=(x-x_j)/(x_j+1-x_j*)
c2=(3mj-2Most[c1]-Rest[c1])dxj,
c3=(Most[c1]+Rest[c1]-2mj)dxj,
c1=Most[c1]dxj]
];
coef=Thread[{Most[yj],c1,c2,c3}];
If[And@@Thread[Equal[0,Chop[Differences[dxj]/Mean[dxj]]]],
(*regular spacing*)
{{First[xj],Last[xj],xj[[2]]-xj[[1]]},coef},
(*irregular spacing*)
{Partition[xj,2,1],coef}]
]
]


(* ::Input:: *)
(*monotoneCubicCoefs@Table[{i,i^3}//N,{i,0,2}]*)


(* ::Input:: *)
(*FromCoefficientRules[Thread[Rule[List/@Range[0,3],{1.`,1.75`,10.5`,-5.25`}]],x]/.x->x-1*)


(* ::Input:: *)
(*Plot[{1.` +1.75` (-1+x)+10.5` (-1+x)^2-5.25` (-1+x)^3,(x)^3},{x,1,2}]*)


(* ::Input:: *)
(*With[{f0=#^2&},*)
(*Module[{f1,df1},*)
(*{f1,df1}=cubicSplineInterpolator@@monotoneCubicCoefs[Table[{i,f0[i]}//N,{i,{0,.51,1}}]];*)
(*(*f1[.51]//Throw;*)*)
(*{Plot[{(*f0[x],*)f1[x]},{x,0,1}],Plot[{2x,df1[x]},{x,0,1}]}*)
(*]*)
(*]*)


(* ::Input:: *)
(*With[{f0=#^2&},*)
(*Module[{f1,df1},*)
(*{f1,df1}=cubicSplineInterpolator@@monotoneCubicCoefs[Table[{i/10,f0[i/10]}//N,{i,0,10}]];*)
(*(*f1[2.1]//Throw;*)*)
(*{Plot[{f0[x],f1[x]//Re},{x,0,1}],Plot[{2x,df1[x]//Re},{x,0,1}]}*)
(*]*)
(*]*)


(* ::Input:: *)
(*cubicSplineIntegration@@monotoneCubicCoefs[{#,#^2}&/@Range[0,1,.01]]*)


(* ::Input:: *)
(*With[{f0=Exp[-#^2]/Sqrt[\[Pi]]&,xj=Range[-5,5,.1]//N,nx=500},*)
(*Module[{coefs,f1,x},*)
(*coefs=monotoneCubicCoefs[Thread[{xj,f0[xj]}]];*)
(*f1=cubicSplineInterpolator[Sequence@@coefs]//First;*)
(*x=Rest@Range[0,nx](-Subtract@@xj[[{1,-1}]])/nx+xj[[1]];*)
(*(*x+=.4I;*)*)
(*Print[Row[{"integral = ",cubicSplineIntegration[Sequence@@coefs]}]];*)
(*{ListPlot[Re@{f0/@x,f1/@x},DataRange->xj[[{1,-1}]],Joined->True,PlotRange->All,PlotStyle->Thread[Directive[{Black,Red},{Dashing[{}],Dashed}]],ImageSize->Medium],ListPlot[Im@{f0/@x,f1/@x},DataRange->xj[[{1,-1}]],Joined->True,PlotRange->All,PlotStyle->Thread[Directive[{Black,Red},{Dashing[{}],Dashed}]],ImageSize->Medium]}*)
(*]*)
(*]*)


(* ::Input:: *)
(*With[{f0=Exp[-#^2]&,df0=-2# Exp[-#^2]&,xj=Range[-5,5,.1]//N,nx=500},*)
(*Module[{coefs,f1,x},*)
(*coefs=monotoneCubicCoefs[Thread[{xj,f0[xj]}]];*)
(*f1=cubicSplineInterpolator[Sequence@@coefs]//Last;*)
(*x=Rest@Range[0,nx](-Subtract@@xj[[{1,-1}]])/nx+xj[[1]];*)
(*(*x+=.2I;*)*)
(*{ListPlot[Re@{df0/@x,f1/@x},DataRange->xj[[{1,-1}]],Joined->True,PlotRange->All,PlotStyle->Thread[Directive[{Black,Red},{Dashing[{}],Dashed}]],ImageSize->Medium],ListPlot[Im@{df0/@x,f1/@x},DataRange->xj[[{1,-1}]],Joined->True,PlotRange->All,PlotStyle->Thread[Directive[{Black,Red},{Dashing[{}],Dashed}]],ImageSize->Medium]}*)
(*]*)
(*]*)


(* ::Input:: *)
(*With[{f0=Exp[-#^2]/Sqrt[\[Pi]]&,xj=Sort[RandomReal[{-5,5},101]],nx=500},*)
(*Module[{coefs,f1,x},*)
(*coefs=monotoneCubicCoefs[Thread[{xj,f0[xj]}]];*)
(*f1=cubicSplineInterpolator[Sequence@@coefs]//First;*)
(*x=Rest@Range[0,nx](-Subtract@@xj[[{1,-1}]])/nx+xj[[1]];*)
(*Print[Row[{"integral = ",cubicSplineIntegration[Sequence@@coefs]}]];*)
(*{ListPlot[{f0/@x,f1/@x},DataRange->xj[[{1,-1}]],Joined->True,PlotStyle->Thread[Directive[{Black,Red},{Dashing[{}],Dashed}]],ImageSize->Medium],ListPlot[Subtract@@{f0/@x,f1/@x},Joined->True,PlotStyle->Thread[Directive[{Black,Red},{Dashing[{}],Dashed}]],ImageSize->Medium,PlotRange->All]}*)
(*]*)
(*]*)
