(* ::Package:: *)

ClearAll[unitPDF];
unitPDF::negI="Not defined for Im[z]<0.";
unitPDF::invN="Invalid, or not supported, power index.";
unitPDF=With[{eps=1.*^-10,one=1.+0.I},Compile[{{n,_Integer},{zz,_Complex}},
Module[{x=0.,y=0.,z,Z},
x=Re[zz];
If[Abs[x]<eps,x=0.];
y=Im[zz];
If[Abs[y]<eps,y=eps];
z=x + y*I;
(*If[y<0,(Message[unitPDF::negI];Abort[])];
If[n<0,(Message[unitPDF::invN];Abort[])];*)
Z=Log[(z - one)/z];
Which[
n==0,Z,
n==1,one + z*Z,
True,Total[Power[z,Range[n-1]]/Reverse[Range[n-1]]] + one/n + Power[z,n]*Z
]
],
RuntimeAttributes->{Listable},Parallelization->True
(*,CompilationOptions->{"InlineExternalDefinitions"\[Rule]False,"InlineCompiledFunctions"\[Rule]False}*)
]];


ClearAll[plasmaDispersionFunction];
plasmaDispersionFunction::invN="Invalid, or not supported, power index.";

plasmaDispersionFunction[xlim:{_Real,_Real,_Real?Positive},coefs:{{_Real,_Real,_Real,_Real}..}/;Length[coefs]>=2
]:=plasmaDispersionFunction[xlim,coefs,cubicSplineInterpolator[xlim,coefs]]

plasmaDispersionFunction[{f_,df_},{x0_?NumberQ,xn_?NumberQ}/;xn>x0,nx_Integer/;nx>=3
]:=plasmaDispersionFunction[Sequence@@cubicSplineCoefs[{#,f[#]}&/@N[Range[0,nx](xn-x0)/nx+x0]],{f,df}]

plasmaDispersionFunction[{x0_Real,xn_Real,\[CapitalDelta]j_Real?Positive},coefs:{{_Real,_Real,_Real,_Real}..}/;Length[coefs]>=2,
{f_,df_}]:=With[{xjs=Range[0,Length[coefs]-1]\[CapitalDelta]j+x0,k=Range[4]-1,m=Length[coefs],unitPDF=unitPDF,large=50},
Module[{\[ScriptCapitalZ],\[ScriptCapitalW]},
(*Z*)
\[ScriptCapitalZ]=Function[{n,zIn},Module[{negI=True,z=0.I,Z=0.I,i=0,j=0,xj=0.,c={0.},zj=0.I},
negI=Im[zIn]<0;
z=Re[zIn]+Abs[Im[zIn]]I;
(*PDF for Im[z]\[GreaterEqual]0*)
If[Abs[z]>large,(*Large argument*)
Which[n==0,
For[i=1,i<=m,++i,
c=coefs[[i]];
xj=xjs[[i]];
Z+=Plus@@Times[\[CapitalDelta]j c,(xj+z)(xj^2+z^2)/(k+1)+(3xj^2+2xj z+z^2)\[CapitalDelta]j/(k+2)+(3xj+z)\[CapitalDelta]j^2/(k+3)+\[CapitalDelta]j^3/(k+4)]
],n==1,
For[i=1,i<=m,++i,
c=coefs[[i]];
xj=xjs[[i]];
Z+=Plus@@Times[\[CapitalDelta]j c,xj(xj+z)(xj^2+z^2)/(k+1)+(4xj^3+3xj^2z+2xj z^2+z^3)\[CapitalDelta]j/(k+2)+
(6xj^2+3xj z+z^2)\[CapitalDelta]j^2/(k+3)+(4xj+z)\[CapitalDelta]j^3/(k+4)+\[CapitalDelta]j^4/(k+5)]
],n==2,
For[i=1,i<=m,++i,
c=coefs[[i]];
xj=xjs[[i]];
Z+=Plus@@Times[\[CapitalDelta]j c,xj^2(xj+z)(xj^2+z^2)/(k+1)+xj(5xj^3+4xj^2z+3xj z^2+2z^3)\[CapitalDelta]j/(k+2)+
(10xj^3+6xj^2z+3xj z^2+z^3)\[CapitalDelta]j^2/(k+3)+(10xj^2+4xj z+z^2)\[CapitalDelta]j^3/(k+4)+(5xj+z)\[CapitalDelta]j^4/(k+5)+\[CapitalDelta]j^5/(k+6)]
](*,True,(Message[plasmaDispersionFunction::invN];Abort[])*)
];
Z/=-z^4
,(*Piecewise splice of unitPDF*)
Which[
n==0,For[i=1,i<=m,++i,
c=coefs[[i]];
xj=xjs[[i]];
zj=Divide[z-xj,\[CapitalDelta]j];
For[j=1,j<=4,++j,
Z+=c[[j]](unitPDF[j-1,zj])
]
],
n==1,For[i=1,i<=m,++i,
c=coefs[[i]];
xj=xjs[[i]];
zj=Divide[z-xj,\[CapitalDelta]j];
For[j=1,j<=4,++j,
Z+=c[[j]](xj unitPDF[j-1,zj]+\[CapitalDelta]j unitPDF[j,zj])
]
],
n==2,For[i=1,i<=m,++i,
c=coefs[[i]];
xj=xjs[[i]];
zj=Divide[z-xj,\[CapitalDelta]j];
For[j=1,j<=4,++j,
Z+=c[[j]](xj^2 unitPDF[j-1,zj]+2xj \[CapitalDelta]j unitPDF[j,zj]+\[CapitalDelta]j^2unitPDF[j+1,zj])
]
](*,True,(Message[plasmaDispersionFunction::invN];Abort[])*)
]
];
(*Analytic continuation if Im[z]<0*)
z=zIn;
If[negI,Conjugate[Z]+2.\[Pi] I f[z]If[n>0,z^n,1.],Z]
]];
\[ScriptCapitalZ]=Compile[{{n,_Integer},{zIn,_Complex}},\[ScriptCapitalZ][n,zIn],
RuntimeAttributes->{Listable},Parallelization->True,
CompilationOptions->{"InlineExternalDefinitions"->True,"InlineCompiledFunctions"->False}
];
(*W*)
\[ScriptCapitalW]=Function[{n,zIn},Module[{negI=True,z=0.I,Z=0.I,i=0,j=0,xj=0.,c={0.},zj=0.I},
negI=Im[zIn]<0;
z=Re[zIn]+Abs[Im[zIn]]I;
(*PDF for Im[z]\[GreaterEqual]0*)
If[Abs[z]>large,(*Large argument*)
Which[n==0,
For[i=1,i<=m,++i,
c=coefs[[i]];
xj=xjs[[i]];
Z+=Plus@@Times[\[CapitalDelta]j c,(3xj^2+2xj z+z^2)/(k+1)+2(3xj+z)\[CapitalDelta]j/(k+2)+3\[CapitalDelta]j^2/(k+3)]
],n==1,
For[i=1,i<=m,++i,
c=coefs[[i]];
xj=xjs[[i]];
Z+=Plus@@Times[\[CapitalDelta]j c,(4xj^3+3xj^2z+2xj z^2+z^3)/(k+1)+2(6xj^2+3xj z+z^2)\[CapitalDelta]j/(k+2)+3(4xj+z)\[CapitalDelta]j^2/(k+3)+4\[CapitalDelta]j^3/(k+4)]
],n==2,
For[i=1,i<=m,++i,
c=coefs[[i]];
xj=xjs[[i]];
Z+=Plus@@Times[\[CapitalDelta]j c,xj (5xj^3+4xj^2z+3xj z^2+2z^3)/(k+1)+2(10xj^3+6xj^2z+3xj z^2+z^3)\[CapitalDelta]j/(k+2)+
3(10xj^2+4xj z+z^2)\[CapitalDelta]j^2/(k+3)+4(5xj+z)\[CapitalDelta]j^3/(k+4)+5\[CapitalDelta]j^4/(k+5)]
](*,True,(Message[plasmaDispersionFunction::invN];Abort[])*)
];
Z/=z^4
,(*Piecewise splice of unitPDF*)
Which[
n==0,For[i=1,i<=m,++i,
c=coefs[[i]];
xj=xjs[[i]];
zj=Divide[z-xj,\[CapitalDelta]j];
For[j=1,j<=3,++j,
Z+=c[[j+1]]j(1/\[CapitalDelta]j unitPDF[j-1,zj])
]
],
n==1,For[i=1,i<=m,++i,
c=coefs[[i]];
xj=xjs[[i]];
zj=Divide[z-xj,\[CapitalDelta]j];
For[j=1,j<=3,++j,
Z+=c[[j+1]]j(xj/\[CapitalDelta]j unitPDF[j-1,zj]+unitPDF[j,zj])
]
],
n==2,For[i=1,i<=m,++i,
c=coefs[[i]];
xj=xjs[[i]];
zj=Divide[z-xj,\[CapitalDelta]j];
For[j=1,j<=3,++j,
Z+=c[[j+1]]j(xj^2/\[CapitalDelta]j unitPDF[j-1,zj]+2xj unitPDF[j,zj]+\[CapitalDelta]j unitPDF[j+1,zj])
]
](*,True,(Message[plasmaDispersionFunction::invN];Abort[])*)
]
];
(*Analytic continuation if Im[z]<0*)
z=zIn;
If[negI,Conjugate[Z]+2.\[Pi] I df[z]If[n>0,z^n,1.],Z]
]];
\[ScriptCapitalW]=Compile[{{n,_Integer},{zIn,_Complex}},\[ScriptCapitalW][n,zIn],
RuntimeAttributes->{Listable},Parallelization->True,
CompilationOptions->{"InlineExternalDefinitions"->True,"InlineCompiledFunctions"->False}
];
{\[ScriptCapitalZ],\[ScriptCapitalW]}
]
]



