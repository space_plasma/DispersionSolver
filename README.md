# Kinetic Plasma Dispersion Solver

[![pipeline status](https://gitlab.com/space_plasma/DispersionSolver/badges/master/pipeline.svg)](https://gitlab.com/space_plasma/DispersionSolver/-/commits/master)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

This is a kinetic plasma dispersion relation solver in a homogeneous, magnetized plasma with a uniform background magnetic field.

## Preface

I wrote the predecessor version (say, **Ver. 1**) when I was writing a [paper](https://doi.org/10.1002/2015JA021041)
about the kinetic plasma instability due to a proton shell velocity distribution in space plasma.
Back then, it was written in **Objective-C** and **C++** using **Xcode** as my primary IDE. I was a young fella who was
fascinated by **Objective-C**, **Xcode**, and the Apple technology provided to developers.
As I was using it in my following projects, I found places where the performance and the user interface can be improved.
Sometime later, I made a major revision (**Ver. 2**) and converted most of the **Objective-C** code to **C++**.
In addition, I switched the version control system from **Subversion** to **Git**.

Although the code quality and performance got better in **Ver. 2**, I was still relying on the Apple-provided toolsets
and the fact that some **Objective-C** code was still there made it unportable to non-**macOS** systems.
Besides, the setup of the build environment and dependency management was not so user-friendly that only I could compile
and use the program.
At the same time, I grew *tired* of the fast update cycle of Apple-provided developer tools with more bugs introduced in
every update cycle than the fixes of previously introduced ones.

In **Ver. 3** (which is what you are seeing now), I attempt to increase the portability of the program, while I maintain
the code structure of **Ver. 2**. For that goal, I chose **CMake** as my build environment and dependency-management
tool and pruned the last bits of the **Objective-C** code in favor of the pure **C++** code. In addition, I utilize
auxiliary tools, such as **clang-format** and **pre-commit** hooks, to maintain the consistent code quality down the road.

Having said that, there are still a few major limitations:

- While it builds in **Linux** systems (and supposedly runs fine), I doubt that it will build in **Windows** systems.
- It is still a **plug-in** for **Mathematica** which I use as my main analysis tool.
As such, it will not gain a broad audience.

Besides, I want to modernize the code base which was written in the time when my understanding and experience of **C++**
programming was premature. Unfortunately, however, it's unlikely that this will get done, considering the code size to my
available time ratio.

## Source Tree

The project is made of thee components:

- [`src/DispersionKit`](src/DispersionKit): A library of functions which do all the heavy lifting.
- [`src/dispersion_solver-wstp`](src/dispersion_solver-wstp): An executable which interfaces with the **Mathematica** frontend using the **WSTP** protocol.
- [`src/DispersionLink`](src/DispersionLink): A dynamic library that exposes the `DispersionKit` functions to **Mathematica** through the **LibraryLink** interface.

The last one is not built by default in non-**macOS** systems due to linking failure.

## Build and Installation

### Build requirement

- It requires to have `cmake` available in your system with the minimum version of 3.18.
- It requires to use either the **GNU** compiler (`g++`) or the **LLVM** compiler (`clang++`) with the support of **C++17** language dialect.
- It requires to have **Mathematica** installed in your system and know the path to the `SystemFiles` directory.
  (Technically, you only need the **WSTP** library and optionally the **Wolfram LibraryLink** header.)

### External dependencies

- The [**GNU Scientific**](https://www.gnu.org/software/gsl/) library (`libgsl`)
- The [**Taskflow**](https://taskflow.github.io) library (needed in non-**macOS** systems for parallelism)
- The [**WSTP**](https://www.wolfram.com/wstp/) library

The first two will be automatically fetched over the internet and made available for compilation,
while the last one should be shipped with your **Mathematica** software.

### Configure, compile, and install

First, clone the project.

Second, make an empty build directory.

Third, inside your build directory, execute

```shell
$ cmake \
  -DMATHEMATICA_SYSTEM_ROOT=/Applications/Mathematica.app/Contents/SystemFiles \
  -DCMAKE_INSTALL_PREFIX=$HOME/Desktop \
  -DCMAKE_CXX_COMPILER=g++ \
  -DCMAKE_BUILD_TYPE=Release \
  -DENABLE_IPO=On \
  -G "Ninja" \
  /path/to/project/clone
$ ninja
$ ninja install
```

This will put the built executable under `$HOME/Desktop/DispersionSolver`.
You will need to tweak the parameters appropriate for your system.
If you don't have `ninja` installed but have `make` available,
change `"Ninja"` to `"Unix Makefiles"` and `ninja` to `make`.

## User Guide

See the [User Handbook](https://space_plasma.gitlab.io/DispersionSolver).

## LICENSE

All my contributions are provided under the BSD-2 Clause license (see [LICENSE](LICENSE)),
*except* the **Faddeeva** functions (found in
[`Faddeeva.hh`](src/DispersionKit/DispersionKit/Faddeeva.hh) and
[`Faddeeva.cc`](src/DispersionKit/DispersionKit/Faddeeva.cc))
which are provided under the MIT license and
available at [http://ab-initio.mit.edu/Faddeeva](http://ab-initio.mit.edu/Faddeeva).
